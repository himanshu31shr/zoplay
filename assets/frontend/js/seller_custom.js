// Add Seller Account
$(document).ready(function(){
        
    $("#seller_form").validate({
        rules: {
               email: {
                    required: true,
                    email: true,
                    // remote: {
                    // url: site_url+"seller/checkseller",
                    // type: "post"
                 // }
                },
                phone: {
                    required: true,
                    number: true,
                    minlength:9,
                    maxlength:10
                  }
         },
        messages: {
             email:{
                    required:"Please enter a email address",
                    emial:"please enter a valid email",
                    remote:"Email already in use!"
                },
            phone:{
                     required:"please enter a phone number",
                     number:"Please enter a valid phone number",
                     minlength: "phone number must be at least 10 digit",
                     maxlength: "phone number must be at least 10 digit"
                }
        },
        submitHandler: function (form){ 
          //  $(".loadermodal").show();		
		  var formdata = new FormData(form);
            $.ajax({ 
                url: site_url+"api/seller_api/addseller", 
                // data: $('#seller_form').serialize(),
                data: formdata,
                type: 'POST',
				processData : false,
				cache : false,
				contentType :false,
                success: function(data){ alert(data);
                  //  $(".loadermodal").hide();
                    if(data.status){
                       var obj=data.data.sellerData; 
                       var email =obj.seller_email;
                       var phone =obj.seller_phone;
                       window.location.href =site_url+"seller/accountCreation/"+data.seller_id;
                    }else{
                       // console.log(data);
                    }
                 
                }			
            }); 			
        }
    });
    
    // $("#sellerfrm").validate({
        // rules: {
            // seller_name:"required",
             // password: "required",
             // seller_email:{
                    // required: true,
                    // email: true,
             // },
            // confirm_password: {
              // equalTo: "#password"
            // }
         // },
        // messages: {
               // seller_name:"Please enter a seller name",
               // seller_email:{
                    // required:"Please enter a email address",
                    // emial:"please enter a valid email"
                // },
            // phone:{
                     // required:"please enter a phone number",
                     // number:"Please enter a valid phone number"
                // }
        // },
        // submitHandler: function (form){ 
           // $(".loadermodal").show();		
            // $.ajax({ 
                // url: site_url+"api/seller_api/saveseller", 
                // data: $('#sellerfrm').serialize(),
                // type: 'POST',
                // success: function(data){
                   // $(".loadermodal").hide();
                    // if(data.status){
                       // var obj=data.data.sellerData; 
                       // var email =obj.seller_email;
                       // var phone =obj.seller_phone;
                       // window.location.href =site_url+"seller/dashboard/";
                    // }else{
                        // console.log(data);
                    // }
                 
                // }			
            // }); 			
        // }
    // });
});
    

