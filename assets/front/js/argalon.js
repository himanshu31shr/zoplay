console.log(" I am Called... ");

	var base_url_value = $("#base_url_value").val();

	function loginme()
		{
			var email			=	$("#loginemail").val();
			var password		=	$("#loginpassword").val();
			var loginremember	=	0;
			
			
			if($("#loginremember").is(':checked'))
				{
					loginremember = 1;
				} else {
					loginremember = 0;
				}

			
					$.ajax({
						type: "POST",
						async: true,
						url: base_url_value+'Informationajax/dologin',
						data: {
							'password'		:	password,
							'loginremember'	:	loginremember,
							'email'			:	email
						},
						success: function(tempdata) 
							{
								if (tempdata.trim() != '') 
									{
										var values = JSON.parse(tempdata);
										showresponse(values.status,values.message,"adminmessage","adminloginform");
											if(values.refresh==1)
												{
													window.location.reload();
												}
									} else {
										showresponse(0,"Something went wrong, Please try again later.","adminmessage","adminloginform");
									}
							}
					});
			return false;
		}