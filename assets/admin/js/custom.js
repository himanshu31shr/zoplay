// Add feature package
$(document).ready(function() {
    
    $('#packageList').DataTable( {
        "processing": true,
        "serverSide": true,
            ajax: {
                url: site_url+"admin/getPackageList",
                type: 'POST'
            }
    });
    
    
    $("#example1").click(function() {
        if (this.checked) {
           $('#status').val('1');
        }else{
	  $('#status').val('0');
	}
    });
    
    $("#addpackagefrm").validate({
        rules: {
                package_name: "required",				
                package_amount: {
                        required: true,
                        number: true
                },
                package_day: {
                        required: true,
                },				
         },
        messages: {
                package_name: "Please enter your package name",
                package_amount: {
                        required: "Please enter a package amount",
                        number:"Please enter a valid number"
                },
                package_day: "Please enter package day"
        },
        submitHandler: function (form){ 
            $(".loadermodal").show();		
            $.ajax({ 
                url:site_url+'api/admin_api/addpackage', 
                data: $('#addpackagefrm').serialize(),
                type: 'POST',
                success: function(data){
                    $(".loadermodal").hide();
                    if(data.status){
                        toastr.success(data.message, 'Success');
                    }else{
                        console.log(data);
                    }
                   window.location.href =site_url+"admin/featurepackagelist";
                }			
            }); 			
        }
    });
    // Add Affiliate settings
     $("#Affiliatesettingsfrm").validate({
        rules: {
                one_point: "required",				
                one_referral: "required",
                expiration_duration: "required"				
         },
        messages: {
        },
        submitHandler: function (form){ 
            $(".loadermodal").show();		
            $.ajax({ 
                url:site_url+'api/admin_api/saveaffiliatesettings', 
                data: $('#Affiliatesettingsfrm').serialize(),
                type: 'POST',
                success: function(data){
                    $(".loadermodal").hide();
                    if(data.status){
                        toastr.success(data.message, 'Success');
                    }else{
                        console.log(data);
                    }
                 //  window.location.href =site_url+"admin/featurepackagelist";
                }			
            }); 			
        }
    });
    
    // Add New  Currency
    $("#addcurrencyfrm").validate({
        rules: {
                currency_name: "required",				
                currency_code : "required",
                currency_symbol : "required",
                currency_value : "required",
        },
        messages: {
                currency_name: "Please enter currency name",
                currency_code: "Please enter your currency code",
                currency_symbol: "Please enter your currency symbol",
                currency_value: "Please enter your currency value",			
        },
        submitHandler: function (form) {                 
           $(".loadermodal").show();
           $.ajax({ 
               url:site_url+'api/admin_api/add_currency', 
               data: $('#addcurrencyfrm').serialize(),
               type: 'POST',
               success: function(data)
               {	
                   if(data.status == true){
                        $(".loadermodal").hide();
                        toastr.success('Currency Add Successfully', 'Success');
                       // window.location.href =site_url+"admin/add_currency"
                   }else{
                        toastr.success('Data Not Inserted Something Went Wrong', 'error');
                       // window.location.href =site_url+"admin/add_currency"
                   }
               }			
           }); 			
        }
    });
    
    
});
