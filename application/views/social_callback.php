<!DOCTYPE html>
<html>
<head>
	<title>Please provide missing detials</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">	
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

	<style type="text/css">
		body {
			font-family: 'Open Sans', sans-serif;
   	 	background-color: rgba(0, 0, 0, 0.75);
		}

		*[role="form"] {
		     padding: 15px;
		    margin: 0 auto;
		    background-color: #fff;
		    border-radius: 0.3em;
		    width: 60%;
		}
		#add{
			    margin-top: 19%;
		}
		*[role="form"] h2 {
		    margin-bottom: 1em;
		} 
		.overlay{
			z-index: 9999;
		    background: rgba(0, 0, 0, 0.66);
			position: fixed;
		    top: -50%;
		    left: -50%;
		    width: 200%;
		    height: 200%;
		}

		.overlay img {
			position: absolute; 
			top: 0; 
			left: 0; 
			right: 0; 
			bottom: 0; 
			margin: auto; 
		}
	</style>
</head>
<body>
<div class="container">
            <form id="add" class="form-horizontal" role="form" >
                <h2 class="text-center">We need this information too!</h2>
                <div class="form-group">
                	<div class="status">
                	</div>
                    <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                    <div class="col-sm-9">
                        <input name="name" type="text" value="<?=$user['name'];?>" placeholder="Full Name" class="form-control" autofocus>
                        <span class="help-block">First Name, Last Name, eg.: Harry, Smith</span>
                        <span class="text text-danger" id="err_name"></span>
                    </div>
                </div>
                <?php
                if(!isset($user['email'])){

               ?>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" name="email" placeholder="Email" class="form-control">
                         <span class="text text-danger" id="err_email"></span>
                    </div>
                </div>
                <?php
                 }else{
                 	?>
									<input type="hidden" name="email" value="<?=($user['email']);?>" >
                 	<?php
                 }
                ?>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" name="password" placeholder="Password" class="form-control">
                         <span class="text text-danger" id="err_password"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Repeat Password</label>
                    <div class="col-sm-9">
                        <input type="password" name="r_password" placeholder="Password" class="form-control">
                         <span class="text text-danger" id="err_r_password"></span>
                    </div>
                </div>
                <input type="hidden" name="gender" value="<?=($user['gender']);?>" >
                <input type="hidden" name="id" value="<?=($user['id']);?>" >
                <input type="hidden" name="role" value="<?=($user['role']);?>" >
                <input type="hidden" name="type" value="<?=($user['type']);?>" >
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary btn-block">Complete registration!</button>
                    </div>
                </div>
            </form> <!-- /form -->
        </div> <!-- ./container -->
        <script type="text/javascript">
        	$('#add').on('submit', function(e){
        		e.preventDefault();
        		var form = new FormData($(this)[0]);
        		$.ajax({
        			url: '<?=site_url();?>user/set_remaining_data',
        			data: form,
        			type:"POST",
        			processData:false,
        			contentType:false,
        			cache:false,
        			beforeSend:function(){
        				$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
        			},
        			success:function(response){
        				if(response.status == true){
        					$('.status').html('<p class="text-center alert alert-success">'+ response.message+'</p>');
        					$('.text-danger').fadeOut(300);
        					window.location.replace('<?=site_url();?>user/dashboard');
        				}

        			},
        			error:function(response){
        				var data = $.parseJSON(response.responseText);
        				if(data.status == false){
        					$('.status').html('<p class="text-center alert alert-warning">'+ data.message+'</p>');
        				}else{
									$('.status').html('<p class="text-center alert alert-warning">'+ data.message+'</p>');
									$.each(data.data, function(key, value){
										$('#'+key).html(value);
									});
        				}
        			},
        			complete:function(){
        				$('.status').fadeOut(5000);
								$('.overlay').fadeOut(300);
        			}
        		});
        	});
        </script>
</body>
</html>