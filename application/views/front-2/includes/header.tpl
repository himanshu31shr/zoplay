 <!DOCTYPE html>
 <script>
 var prev_token = localStorage.getItem("access_token");
 /*if( prev_token ){
	 httpChannel.setRequestHeader("Authorization", "Bearer" +  prev_token, false);
 }*/
 
  
 </script>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">

    <title>Flipmart premium HTML5 & CSS3 Template</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/bootstrap.min.css">
	    
	    <!-- Customizable CSS -->
	
	  <link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/shop-homepage.css">
	  <link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/custom.css">
	  <link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/main.css">
	  <link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/blue.css">
	  <link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/owl.carousel.css">
	  <link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/custom.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/owl.transitions.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/animate.min.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/rateit.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/bootstrap-select.min.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/bootstrap-slider.min.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/plugins/datatables/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/lightbox.css">
		<link  rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/plugins/sweetalert/sweetalert.css" />
		<!-- Icons/Glyphs -->
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/font-awesome.css">

     <!-- Fonts --> 
           <link rel="stylesheet" href="<?=base_URL();?>assets/front/hero/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        
		<script src="<?=base_URL();?>assets/frontend/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?=base_URL();?>assets/frontend/js/jquery-1.11.1.min.js"></script>
		 <script src="<?=base_URL();?>assets/front/hero/js/main.js"></script>
		<script src="<?=base_URL();?>assets/frontend/js/bootstrap.min.js"></script>
                <script src="<?php echo base_URL();?>assets/frontend/js/bootstrap-slider.min.js"></script>
		<link href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css" rel="stylesheet" />
		<script src="<?=base_URL();?>assets/admin/js/select2.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?=base_URL();?>assets/frontend/js/jquery.base64/jquery.base64.js"></script>
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/jquery-ui.css">
        <style>
		.forcartrow
		{
			padding: 15px;
			border-radius: 3px;
			border: 1px solid rgba(16, 16, 16, 0.26);
		}
		.ui-widget.ui-widget-content	{
			width:841px !important;			
		}	
		.ui-menu ui-widget ui-widget-content ui-autocomplete ui-front{
			width:841px !important;			
		}
		</style>
		 <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/frontend/css/jquery-picZoomer.css">    
    
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.picZoomer.js"></script>
	</head>
<body class="cnt-home">
		<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">
	<!-- ============================================== TOP MENU ============================================== -->
<?php
	$CI =& get_instance(); 
	$CI->load->model('AdminModel'); 
	$list = $CI->AdminModel->productList();	
	$proname= array();
	foreach($list as $productname){
		$proname[] = $productname->product_name;
	}
?>
<div class="top-bar animate-dropdown">
	<div class="container">
		<div class="header-top-inner">
			<div class="cnt-account">
				<ul class="list-unstyled">
					<?php
					if(isset($_SESSION['user'])){
					?>
					<li><a href="<?=site_url();?>user/dashboard"><i class="icon fa fa-user"></i>My Account</a></li>
					<li><a onclick="see_more('wishlist', '', 'wishlist')"><i class="icon fa fa-heart"></i>Wishlist</a></li>
					<li><a href="<?=site_url();?>user/logout"><i class="icon fa fa-lock"></i>Logout</a></li>
					<?php }else{ ?>
					<li><a href="<?=site_url();?>user/login"><i class="icon fa fa-lock"></i>Login</a></li>
					<?php } ?>
					<li><a href="<?=site_url();?>mycart"><i class="icon fa fa-shopping-cart"></i>My Cart</a></li>
				</ul>
			</div><!-- /.cnt-account -->

			<!-- <div class="cnt-block">
				<ul class="list-unstyled list-inline">
					<li class="dropdown dropdown-small">
						<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"><span class="value">USD </span><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="sign-in.html#">USD</a></li>
							<li><a href="sign-in.html#">INR</a></li>
							<li><a href="sign-in.html#">GBP</a></li>
						</ul>
					</li>

					<!-- <li class="dropdown dropdown-small">
						<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"><span class="value">English </span><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="sign-in.html#">English</a></li>
							<li><a href="sign-in.html#">French</a></li>
							<li><a href="sign-in.html#">German</a></li>
						</ul>
					</li> -->
				</ul>
			</div> 
			<div class="clearfix"></div>
		</div><!-- /.header-top-inner -->
	</div><!-- /.container -->
</div><!-- /.header-top -->
<!-- ============================================== TOP MENU : END ============================================== -->
	<div class="main-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
					<!-- ============================================================= LOGO ============================================================= -->
<div class="logo">
	<a href="<?=base_url();?>">
		
		<img src="<?=base_url();?>assets/frontend/images/logo.png" alt="">

	</a>
</div><!-- /.logo -->
<!-- ============================================================= LOGO : END ============================================================= -->				</div><!-- /.logo-holder -->

				<div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
					<!-- /.contact-row -->
<!-- ============================================================= SEARCH AREA ============================================================= -->
<div class="search-area">
   <form id="select2Form"  class="form-horizontal"> 
	<?php 
		$this->load->helper('category');		
		$jscategories = $this->CommonModel->jsCategories(0);
		$data = array('jscategories'=>$jscategories);
		$categories = $data['jscategories'];		
	?>
		<div class="ui-widget control-group">								
			<input class="search-field" name="searchproduct" id="search_product" multiple placeholder="Search here..." />
			<a href="<?= site_url() ?>" class="search-button"></a>	 
		</div>
		<input type='hidden' id="e6" style="width: 500px;" value=""  />		
    </form>
</div><!-- /.search-area -->
<!-- ============================================================= SEARCH AREA : END ============================================================= -->				</div><!-- /.top-search-holder -->

				<div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
					<!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
	<?php 
		$myproduct_count = $carthtml['myproduct_count'];
		$mygrand_total = $carthtml['mygrand_total'];
		$mycart_products = $carthtml['mycart_products'];
	?>				
	<div class="dropdown dropdown-cart">
		
		<a href="<?php echo site_url('mycart');?>" class="dropdown-toggle lnk-cart">
		<a href="<?=site_url();?>mycart" class="dropdown-toggle lnk-cart">

			<div class="items-cart-inner">
            <div class="basket">
					<i class="glyphicon glyphicon-shopping-cart"></i>
				</div>
				<div class="basket-item-count"><span class="myproduct_count count"><?php echo $myproduct_count;?></span></div>
				<div class="total-price-basket">
					<span class="lbl">cart -</span>
					<span class="total-price">
						<span class="sign">$</span><span class="mygrand_total value"><?php echo $mygrand_total;?></span>
					</span>
				</div>
				
			
		    </div>
		</a>
		<ul class="dropdown-menu" style="width: 300px !important;">
			<li>
			
			<div class="pull-right">
				<button class="label label-danger" onclick="clear_cart('all');"><i class="fa fa-trash"></i> Clear cart</button>
			</div>
			<div class="pull-left">
				<span class="label label-primary">Total : $<span class="mygrand_total price"><?php echo $mygrand_total;?></span></span>
			</div>
			
			<div class="clearfix"></div>
			<hr>
			<div id="cartasyncdata">
			<?php if(isset($carthtml))
			{
				echo $mycart_products;
			}?>
			</div>
				<div class="clearfix"></div>
			</br>
			<div class="text-center">
				<a href="<?=site_url()?>mycart" class="btn btn-upper btn-info btn-block m-t-20"> More Items</a>
			</div>
			<hr>
			<div class="clearfix"></div>
			<div class="clearfix cart-total">					
				<a href="<?=site_url()?>place_order" class="btn btn-upper btn-primary btn-block">Checkout</a>	
			</div><!-- /.cart-total-->
					
				
		</li>
		</ul><!-- /.dropdown-menu-->
	</div><!-- /.dropdown-cart -->

<!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->				</div><!-- /.top-cart-row -->
			</div><!-- /.row -->

		</div><!-- /.container -->

	</div><!-- /.main-header -->

	<!-- ============================================== NAVBAR ============================================== -->
<div class="header-nav animate-dropdown">
    <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
	<div class="nav-outer">
	
	<ul class="nav navbar-nav">
	<?php foreach($categories as $key=>$categorylist){ ?>
			<li class="dropdown yamm-fw">
				<a  data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">
				<?= $categorylist['category_name']; ?>
				</a>	
					<ul class="dropdown-menu container">						
							<li>
								<div class="yamm-content ">
									<div class="row"> 
									<?php if(!empty($categorylist['data'])){
										foreach($categorylist['data'] as $key=>$subcategory) { 
									?>									
										   <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
												<h2 class="title"><a href="<?= site_url(); ?>products/<?= base64_encode($subcategory['category_name']);?>"><?= $subcategory['category_name']; ?></a></h2>
													<ul class="links">
														<?php if(!empty($subcategory['data'])){
																foreach($subcategory['data'] as $key=>$sub) { ?>
															<li>
																<a href="<?= site_url(); ?>products/<?= base64_encode($sub['category_name']);?>"><?= $sub['category_name']; ?></a>
															</li>
														<?php } }?>	
													</ul>
											</div>
											<?php } ?>
											<!--<div class="col-xs-12 col-sm-6 col-md-4 col-menu banner-image">
													<img class="img-responsive" src="<?=base_url();?>assets/frontend/images/banners/top-menu-banner.jpg" alt="">        
											</div>-->
									</div>
								</div>
							</li>
						<?php }  ?>
					</ul>				
			</li>
			<?php } ?>
	</ul>	



					
			
		</ul><!-- /.navbar-nav -->
		<div class="clearfix"></div>				
	</div><!-- /.nav-outer -->
</div><!-- /.navbar-collapse -->


            </div><!-- /.nav-bg-class -->
        </div><!-- /.navbar-default -->
    </div><!-- /.container-class -->

</div><!-- /.header-nav -->
<!-- ============================================== NAVBAR : END ============================================== -->
  <script>  
  $( "#select2Form" ).submit(function( event ) {
	  var product_name = $('#search_product').val();
	  $.base64.utf8encode = true
	  product_name = $.base64.btoa(product_name);
	  location.replace('<?= site_url() ?>search/'+product_name+'/search');
	  event.preventDefault();
  });
	
  $(function() {
    var availableTags =  <?php echo json_encode($proname); ?>;
    $( "#search_product" ).autocomplete({
	  maxResults: 10,
      source: function(request, response) {
		var results = $.ui.autocomplete.filter(availableTags, request.term);
		response(results.slice(0, this.options.maxResults));
		}
	 // source: availableTags
    });
  }); 
  
  $(document).ready(function () {		
		$('#search_product').keyup(function() {
			var product_name = this.value; 
		});
		$('#search_product').on('autocompleteselect', function (e, ui) {			
				var product_name = ui.item.value;
				$.base64.utf8encode = true
				product_name = $.base64.btoa(product_name);
				location.replace('<?= site_url() ?>search/'+product_name+'/search');
		});		
	});
</script>
</header>