<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Contact Us</h4>
                    </div><!-- module-heading -->

		<div class="module-body">
			<ul class="toggle-footer" style="">
				<li class="media">
					<div class="pull-left">
						 <span class="icon fa-stack fa-lg">
								<i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="media-body">
						<p>ThemesGround, 789 Main rd, Anytown, CA 12345 USA</p>
					</div>
				</li>

				  <li class="media">
					<div class="pull-left">
						 <span class="icon fa-stack fa-lg">
						  <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="media-body">
						<p>+(888) 123-4567<br>+(888) 456-7890</p>
					</div>
				</li>

				  <li class="media">
					<div class="pull-left">
						 <span class="icon fa-stack fa-lg">
						  <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="media-body">
						<span><a href="sign-in.html#">flipmart@themesground.com</a></span>
					</div>
				</li>
              
            </ul>
    </div><!-- /.module-body -->
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Customer Service</h4>
                    </div><!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="sign-in.html#" title="Contact us">My Account</a></li>
                <li><a href="sign-in.html#" title="About us">Order History</a></li>
                <li><a href="sign-in.html#" title="faq">FAQ</a></li>
                <li><a href="sign-in.html#" title="Popular Searches">Specials</a></li>
                <li class="last"><a href="sign-in.html#" title="Where is my order?">Help Center</a></li>
                        </ul>
                    </div><!-- /.module-body -->
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Corporation</h4>
                    </div><!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                          <li class="first"><a title="Your Account" href="sign-in.html#">About us</a></li>
                <li><a title="Information" href="sign-in.html#">Customer Service</a></li>
                <li><a title="Addresses" href="sign-in.html#">Company</a></li>
                <li><a title="Addresses" href="sign-in.html#">Investor Relations</a></li>
                <li class="last"><a title="Orders History" href="sign-in.html#">Advanced Search</a></li>
                        </ul>
                    </div><!-- /.module-body -->
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Why Choose Us</h4>
                    </div><!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="sign-in.html#" title="About us">Shopping Guide</a></li>
                <li><a href="sign-in.html#" title="Blog">Blog</a></li>
                <li><a href="sign-in.html#" title="Company">Company</a></li>
                <li><a href="sign-in.html#" title="Investor Relations">Investor Relations</a></li>
                <li class=" last"><a href="http://themesground.com/flipmart-demo/HTML/contact-us.html" title="Suppliers">Contact Us</a></li>
                        </ul>
                    </div><!-- /.module-body -->
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding social">
                <ul class="link">
                  <li class="fb pull-left"><a target="_blank" rel="nofollow" href="sign-in.html#" title="Facebook"></a></li>
                  <li class="tw pull-left"><a target="_blank" rel="nofollow" href="sign-in.html#" title="Twitter"></a></li>
                  <li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="sign-in.html#" title="GooglePlus"></a></li>
                  <li class="rss pull-left"><a target="_blank" rel="nofollow" href="sign-in.html#" title="RSS"></a></li>
                  <li class="pintrest pull-left"><a target="_blank" rel="nofollow" href="sign-in.html#" title="PInterest"></a></li>
                  <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="sign-in.html#" title="Linkedin"></a></li>
                  <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="sign-in.html#" title="Youtube"></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="clearfix payment-methods">
                    <ul>
                        <li><img src="<?=base_URL();?>assets/frontend/images/payments/1.png" alt=""></li>
                        <li><img src="<?=base_URL();?>assets/frontend/images/payments/2.png" alt=""></li>
                        <li><img src="<?=base_URL();?>assets/frontend/images/payments/3.png" alt=""></li>
                        <li><img src="<?=base_URL();?>assets/frontend/images/payments/4.png" alt=""></li>
                        <li><img src="<?=base_URL();?>assets/frontend/images/payments/5.png" alt=""></li>
                    </ul>
                </div><!-- /.payment-methods -->
            </div>
        </div>
    </div>
</footer>

<!-- ============================================================= FOOTER : END============================================================= -->
	<!-- JavaScripts placed at the end of the document so the pages load faster -->
	<!-- <script src="<?=base_URL();?>assets/frontend/js/plugins/datatable/jquery.dataTables.min.js"></script>	 -->
	<script src="<?=base_URL();?>assets/frontend/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/owl.carousel.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/echo.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/jquery.easing-1.3.min.js"></script>

        <script src="<?=base_URL();?>assets/frontend/js/jquery.rateit.min.js"></script>
        <script type="text/javascript" src="<?=base_URL();?>assets/frontend/js/lightbox.min.js"></script>
        <script src="<?=base_URL();?>assets/frontend/js/bootstrap-select.min.js"></script>
        <script src="<?=base_URL();?>assets/frontend/js/bootstrap-slider.min.js"></script>
        <script src="<?=base_URL();?>assets/frontend/js/wow.min.js"></script>
        <script src="<?=base_URL();?>assets/frontend/js/scripts.js"></script>


	

	<script type="text/javascript"> 

	//$(document).ready(function(){
		//$('.dataTable').DataTable();
	//});

	//add to wish list
	var addtowish = function(flag, product_id){
		var form = new FormData();
		form.append('product_id', product_id);
		$.ajax({
			url: '<?=site_url();?>products/wishlist/'+flag,
			data: form,
			type: "POST",
			processData:false,
			contentType:false,
			cache:false,
			beforeSend:function(){
				$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success:function(response){
				if(response.status ==  true){
					if(flag == 0){
						$('#inwishlist'+product_id).attr('class', 'btn btn-primary');
						$('#inwishlist'+product_id).attr('onclick', 'addtowish(1,'+product_id+')');
						$('#inwishlist'+product_id).attr('title', 'Remove from wishlist!');
					}

					if(flag == 1){
						$('#inwishlist'+product_id).attr('class', 'btn btn-info');
						$('#inwishlist'+product_id).attr('onclick', 'addtowish(0,'+product_id+')');
						$('#inwishlist'+product_id).attr('title', 'Add to wishlist!');
					}
					
				}
			},
			error:function(response){
				var data = response.responseText;
				if(data.status ==  false){
					//console.log(data.message);
				}
			},
			complete:function(){
				$('.overlay').remove();
			}
		});
	}

	// add data to cart
			var add_to_cart = function(product_id){
			var formdata = new FormData($('#variations')[0]);
			//formdata.append('cart_productid', product_id);
			//formdata.append('inputquantity', $('#inputquantity').val());
			
			$.ajax({
				url: '<?=site_url();?>Checkout/addtocart',
				data: formdata,
				type:"POST",
				contentType: false,
				processData:false,
				cache: false,
				beforeSend:function(){
					$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
				},
				success: function(response){
					console.log(response);
					var data = response;
					if(data.status == true){
						$('.text-danger').empty();
						$('#cartasyncdata').html(data.html);
						$.each(response.data, function(key, value){
							$('.'+key).text(value);
						});
					}
				},
				error: function(response){ 
					var data = response.responseText;
					data = $.parseJSON(data);
					if(data.status == false){
						swal({
						  title: 'Error!',
						  text: data.message,
						  html: true,
						  timer: 3000,
						  type: "warning",
						  showConfirmButton: false
						});
					}
				},
				complete:function(){
				$('.overlay').remove();
				}
			});
			}
		
		
	</script>
			
	<script type="text/javascript"> 

	// $(document).ready(function(){
	// 	$('.dataTable').DataTable();
	// });
 
	// For loading wishlist
	var see_more = function(modal_name, product_id, option){
		var form = new FormData();
		form.append('modal', modal_name);
		form.append('product_id', product_id);
		form.append('option', option);
		$.ajax({
			url:'<?=site_url();?>Products/load_modal',
			data: form,
			type: "POST",
			processData:false,
			contentType:false,
			cache:false,
			beforeSend:function(){
				$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success:function(response){
				$('#myModal').remove();
				$('body').append(response);
				$('#myModal').modal('show');
				$(function () { $('.rating').rateit();});
			},
			complete:function(){
				$('.overlay').remove();
			}
		});
	}
		
		<!--Clear all data from cart-->	
		var clear_cart = function(deleteaction){
			
			var formdata = new FormData();
			formdata.append('deletecart', deleteaction);
			var result = confirm('Are you sure want to clear all Items?');
			if (result) {		
				$.ajax({
					url: '<?php echo site_url(); ?>Checkout/removeallcart',
					data: formdata,
					type:"POST",
					contentType: false,
					processData:false,
					cache: false,
					beforeSend:function(){
							$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
						},
					success: function(response){
						var data = response;
						if(data.status == true){
							
							$('.text-danger').empty();
							//$('.addtocart').removeAttr('disabled').html('Success');
							//$('.addtocart').closest('tr').remove();
							//tr.remove();
							$("#carttable > tbody").empty();
							$('#cartasyncdata').html(data.html);
							$('#carttable > tbody').append('<tr><td colspan="7"><div id="text" class="text-center text-danger"><h4>Cart Empty! To add products to your shopping cart click on "Add to Cart" Button</h4></div></td></tr>');
							$('.myproduct_count').html('0');
							$('.mygrand_total').html('0');
							$('#proceedlink').bind('click', false);
							
							//$('.forcartrow:last').remove();
							
						}
					},
					error: function(response){ 
						var data = response.responseText;
						data = $.parseJSON(data);
						if(data.status == false){
							data = data.data;
							$.each(data, function(key, value){
								$('#'+key+'_error').empty().html(value);
							});
							$('#status').html('<p class="alert alert-danger">'+response.message+'<p>');
						}
					
					},
					complete:function(){
					$('.overlay').remove();
					}
				});
			}
			else {
				return false; // cancel button
			}
		}
	</script>

	<!--Delete single product from Cart-->
	<script type="text/javascript">
		$(document).ready(function() { 	
			$('.removeFromcart').click(function(e){	
				e.preventDefault();
				//var tr = $('.removeFromcart').closest('tr');
				var rowid = $(this).attr('data-cartid');
				var formdata = new FormData();
				formdata.append('cart_listid', $(this).attr('data-cartid'));
				//console.log(checkout_data);
				$.ajax({
				url: '<?=site_url();?>Checkout/deletecartdata',
				data: formdata,
				type:"POST",
				contentType: false,
				processData:false,
				cache: false,
				beforeSend:function(){
						$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
					},
				success: function(response){
					var data = response;
					console.log(data);
					if(data.status == true){
						$('.text-danger').empty();
						//$('.removeFromcart').removeAttr('disabled').html('Success');
						//$('#cartrow_').closest('tr').remove();
						//tr[0].remove();
						//data = $.parseJSON(data);
						//data = data.data;
						$.each(response.data, function(key, value){
							if(key != 'mycartrow_id'){
								$('.'+key).text(value);
							}
							if(key == 'mycartrow_id'){
								$('#cartrow_'+rowid).remove();
							}
						});
						//$('#'+data).remove();
					}
				},
				error: function(response){ 
					var data = response.responseText;
					data = $.parseJSON(data);
					if(data.status == false){
						data = data.data;
						$.each(data, function(key, value){
							$('#'+key+'_error').empty().html(value);
						});
						$('#status').html('<p class="alert alert-danger">'+response.message+'<p>');
						//$('.removeFromcart').removeAttr('disabled').html('Try Again');
					}
				},
				complete:function(){
				$('.overlay').remove();
				}
			});
			});
		});
		
		// $.fn.loader = function(){
		// 	$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		// }
		// $.fn.removeLoader = function(){
		// 	$('.overlay').remove();
		// }
	</script>
	<?php
	$cart_check = $this->cart->contents();
	if(empty($cart_check)) { ?>
		<script>
		$('#proceedlink').bind('click', false);
		</script>
	<?php }
	?>
<!-- ============================================================= FOOTER : END============================================================= -->
</body>
</html>
