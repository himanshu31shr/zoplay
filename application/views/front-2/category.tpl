<?php
		echo $layout['header'];
?>
<style>
    
  .ui-widget.ui-widget-content {
    width: 275px !important;
}
    
</style>

<div class="breadcrumb"> 
  <div class="container">
    <div class="breadcrumb-inner">
      <ul class="list-inline list-unstyled">
        <li><a href="category.html#">Home</a></li>
        <li class='active'>Handbags</li>
      </ul>
    </div>
    <!-- /.breadcrumb-inner --> 
  </div>
  <!-- /.container --> 
</div> 
<div class="body-content outer-top-xs">
  <div class='container'>
    <div class='row'>
      <div class='col-md-3 sidebar'> 
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="side-menu animate-dropdown outer-bottom-xs">
          <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
          <nav class="yamm megamenu-horizontal">
            <ul class="nav">
                  <?php
                if(!empty($allCategories)){
                    foreach ($allCategories as $categoriesVal){ 
                        $subcategory =  getCategoryParentId($categoriesVal->category_id); ?>
                        
                    <li class="dropdown menu-item"> 
                        <a href="category.html#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon fa fa-shopping-bag" aria-hidden="true"></i>
                          <?php echo !empty($categoriesVal->category_name)? $categoriesVal->category_name :'' ;?>
                        </a>
                <ul class="dropdown-menu mega-menu">
                  <li class="yamm-content">
                    <div class="row">
                     <div class="col-sm-12 col-md-3">
                        <ul class="links list-unstyled">
                       <?php  if(!empty($subcategory)){
                                    foreach ($subcategory as $subcategoryVal){ ?>
                                     <li>
                                         <a href="<?php echo  base_url();?>products/<?php echo base64_encode($subcategoryVal->category_id);?>">
                                               <?php echo !empty($subcategoryVal->category_name)? $subcategoryVal->category_name :'' ;?>
                                         </a>
                                     </li>
                                <?php }
                               }?>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>       <!-- /.dropdown-menu --> 
                        
         <?php      }
              
                }
              
                 ?>          
            </ul>
          </nav>
        </div>
        <!-- ================================== TOP NAVIGATION : END ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
          <form name="serach" id="serach">    
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
            <div class="sidebar-widget wow fadeInUp">
              <h3 class="section-title">shop by</h3>
              <div class="widget-header">
                <h4 class="widget-title">Category</h4>
              </div>
              <div class="sidebar-widget-body">
                <div class="accordion">
                  
                        <input type="hidden" name="categoryid" value="<?php echo !empty($category_id)? $category_id : ''; ?>">  
                      <?php
                if(!empty($allCategories)){
                $i=1;
                    foreach ($allCategories as $categoriesVal){ 
                        $subcategory =  getCategoryParentId($categoriesVal->category_id); ?>    
                    
                        <div class="accordion-group">
                          <div class="accordion-heading">
                              <a href="category.html#collapseOne<?php echo $i; ?>" data-toggle="collapse" class="accordion-toggle collapsed"> 
                               <?php echo !empty($categoriesVal->category_name)? $categoriesVal->category_name :'' ;?>
                              </a> 
                          </div>
                          <!-- /.accordion-heading -->
                          <div class="accordion-body collapse" id="collapseOne<?php echo $i; ?>" style="height: 0px;">
                            <div class="accordion-inner">
                              <ul>
                                <?php  if(!empty($subcategory)){
                                             foreach ($subcategory as $subcategoryVal){ ?>  
                                                 <li>
                                                     <a href="javascript:void(0)">
                                                         <input type="checkbox"  name="subcategory[]" onclick="search();" value="<?php echo $subcategoryVal->category_id; ?>">&nbsp;
                                                         <?php echo !empty($subcategoryVal->category_name)? $subcategoryVal->category_name :'' ;?>
                                                     </a>
                                                 </li>
                                <?php        }
                                         }
                                      ?>        
                              </ul>
                            </div>
                          </div>
                        </div>
         <?php     $i++; } 
               } ?>         
             
                </div>
                <!-- /.accordion --> 
              </div>
            </div>
            <!-- ============================================== PRICE SILDER============================================== -->
            <div class="sidebar-widget wow fadeInUp">
              <div class="widget-header">
                <h4 class="widget-title">Price Slider</h4>
              </div>
              <div class="sidebar-widget-body m-t-10">
                <div class="price-range-holder">
                    <span class="min-max"> 
                       <!-- <span class="pull-left">$200.00</span>
                        <span class="pull-right">$800.00</span> 
                    </span>-->
                    <input type="text" id="amount" value="" readonly style="border:0; color:#f6931f; font-weight:bold;">
                  <div id="slider-range"></div>
                </div>
                <!-- /.price-range-holder --> 
                <input type="hidden" id="minPrice" name="minPrice" value="">
                <input type="hidden" id="maxPrice" name="maxPrice" value="">
               <!-- <a href="javascript:void();" onclick="search();" class="lnk btn btn-primary">Serach</a> -->
              </div>
            </div>
              </form>
            <!-- ============================================== PRICE SILDER : END ============================================== --> 
            <div class="home-banner"> <img src="<?= base_url() ?>assets/frontend/images/banners/LHS-banner.jpg" alt="Image"> </div>
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.sidebar-module-container --> 
      </div>
      <!-- /.sidebar -->
      <div class='col-md-9'> 
        <!-- ========================================== SECTION – HERO ========================================= -->
        
        <div id="category" class="category-carousel hidden-xs">
          <div class="item">
            <div class="img-responsive"> <img style="width:100%" src="<?= base_url() ?>assets/frontend/images/banners/cat-banner-1.jpg" alt="" > </div>
            <div class="container-fluid">
              <div class="caption vertical-top text-left">
                <div class="big-text"> Big Sale </div>
                <div class="excerpt hidden-sm hidden-md"> Save up to 49% off </div>
                <div class="excerpt-normal hidden-sm hidden-md"> Lorem ipsum dolor sit amet, consectetur adipiscing elit </div>
              </div>
              <!-- /.caption --> 
            </div>
            <!-- /.container-fluid --> 
          </div>
        </div>
        
     
        <div class="clearfix filters-container m-t-10">
          <div class="row">
            <!-- /.col -->
            <div class="col col-sm-12 col-md-6">
              <div class="col col-sm-3 col-md-6 no-padding">
                <div class="lbl-cnt"> <span class="lbl">Sort by</span>
                  <div class="fld inline">
                    <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                      <button data-toggle="dropdown" type="button" class="btn dropdown-toggle"> Position <span class="caret"></span> </button>
                      <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a onclick="sort('0')">No Filter</a></li>
                        <li role="presentation"><a onclick="sort('plh')">Price:Lowest first</a></li>
                        <li role="presentation"><a onclick="sort('phl')">Price:Highest first</a></li>
                        <li role="presentation"><a onclick="sort('name')">Product Name:A to Z</a></li>
                      </ul>
                    </div>
                  </div>
				  
				  <script>
					var sort = function(option){
						var form = new FormData();
						form.append('criteria', option);
						form.append('category_id', $('input[name=category_id]').val());
						$.ajax({
							url:'<?=site_url();?>/load_products',
							data: form,
							type:"POST",
							processData:false,
							contentType:false,
							cache:false,
							beforeSend:function(){
								$('#load_more').html('<div class="loader"><img src="<?=base_url();?>assets/frontend/images/loader.svg" /></div>');
								$('#load_products').hide();
							},
							success:function(response){
								if(response.status == true){
									$('#load_more').html(response.data);
									$('#grid-container').append('<input type="hidden" name="filter" value="'+ option +'"/>');
									$('#load_products').show();
									$('input[name=last_id]').val(response.last_id);
								}
							}, 
							error:function(response){
								if(response.status == false){
									$('#load_more').html(response.message);
								}
							}
						});
					}
				  </script>
                  <!-- /.fld --> 
                </div>
                <!-- /.lbl-cnt --> 
              </div>
            </div>
            <!-- /.col --> 
          </div>
          <!-- /.row --> 
        </div>
        <div class="search-result-container ">
          <div id="myTabContent" class="tab-content category-list">
            <div class="tab-pane active " id="grid-container">
              <div class="category-product">

                <?php
                if($products['status'] == false){
                ?>
                <div class="row" id="load_more">
                  <div class="col-sm-12 wow fadeInUp extrapadd">
                    <p class="text text-center"><i class="fa fa-5x fa-info-circle"></i></p>
                    <h4 class="text text-center"><?php echo $products['data'];?></h4>
                  </div>
                <?php
                } else {
					echo '<div class="row" id="load_more">';
                  foreach($products['data'] as $p){
					        $url_prod_id = site_url()."product/".md5($p->product_id);

                  if($p->product_image == ''){
                    $image = base_url().'files/default.png';
                  }else if(substr($p->product_image, 0 , 4) == 'http'){
                    $image = $p->product_image;
                  }else{
                    $image = base_url().'files/'.$p->product_image;
                  }
                ?>
				<form id="variations">
                  <div class="col-sm-6 col-md-4 wow fadeInUp height">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?=$url_prod_id;?>"><img src="<?=$image;?>" alt=""></a> </div>
                          <!-- /.image -->
                          
                          
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-left">
                          <h3 class="name"><a href="<?=$url_prod_id;?>"><?=substr($p->product_name,0, 80);?></a></h3>
                          <div class="rating rateit-small" data-rateit-value="<?=$p->average_rating == 'NAN' ? 0 : $p->average_rating;?>" ></div>
                          <div class="description"></div>
                          <div class="product-price"> <span class="price"> <?=$p->product_price-$p->product_discount;?></span> <span class="price-before-discount">$<?=$p->product_price;?></span> </div>
                          <!-- /.product-price --> 
                          <?php
                            $flag = $p->added_to_wish == '0' ? '1' : '0';
                            $button = $p->added_to_wish == '0' ? 'btn btn-primary' : 'btn btn-info';
                          ?>
                        </div>
						
					 <div class="row hide">
						<?php 
						$variations = $p->product_variations;
						if(isset($variations['types']) && $variations != ''){
							$all_variations = '';
							foreach($variations['types'] as $v){
						?>
							<div class="col-sm-2 m-t-10">
								<div class="stock-box">
									<span class="label"><?=$v;?> :</span>
								</div>	
							</div>
							
							<div class="col-sm-9 m-t-10">
							
								<div class="btn-group" data-toggle="buttons">
								<?php
								$i=0;
								foreach($variations['options'][$v] as $param){
								?>
								<label class="btn btn-info <?=$i==0 ? 'active' : '';?>" onclick="$('input[name=<?=$v;?>]').removeAttr( 'checked');$('#<?=$i.$v;?>').attr( 'checked', 'checked' );">
									<input type="radio" name="<?=$v;?>" id="option2" value="<?=$param;?>" autocomplete="off" <?=$i==0 ? 'checked' : '';?>>
									<span class="glyphicon glyphicon-check"></span> <?=$param;?>
									
									<input type="hidden" id="variationtype" name="variationtype" value="<?=$v;?>">
									<input type="radio" id="<?=$i.$v;?>" name="<?=$v;?>" value="<?=$param;?>">
									
								</label>
								<?php
								$i++;
								}
								$all_variations .= $v."," ;
								echo "</div>";
							echo "</div>";
							}
							?>
							<input type="hidden" value="<?=substr($all_variations, 0, -1);?>" name="all_vars" />
							<?php
						}
							?>
						</div><!-- /.row -->

						<?php
							$disable_addcart = '';
							$availabiltyto_addcart = $p->product_quantity;
							if($availabiltyto_addcart==0)
							{
								$disable_addcart = 'title="Product Out Of Stock!" disabled';
							}
						?>
						<input type="hidden" name="cart_productid" value="<?php echo $p->product_id;?>">
						<input type="hidden" id="inputquantity" value="1">
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                          <div class="action">
                            <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                <button onclick="add_to_cart('<?php echo $p->product_id;?>')" class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                              </li>
                              <li class="lnk wishlist"> 
                                <a class="add-to-cart" id="inwishlist<?=$p->product_id;?>" onclick="addtowish('<?=$flag;?>','<?=$p->product_id;?>')" > 
                                  <i class="icon fa fa-heart"></i>
                                </a> 
                              </li>
                              <li class="lnk"> <a class="add-to-cart" href="detail.html" title="Compare"> <i class="fa fa-signal"></i> </a> </li>
                            </ul>
                          </div>
                          <!-- /.action --> 
                        </div>
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
			  </form>

                  <?php

                    }
				?>
					</div>
					<input type="hidden" name="last_id" value="<?= $p->product_id;?>" />
					<input type="hidden" name="category_id" value="<?= $p->category_id;?>" />
					<div class="row">
						<div class="col-md-2 col-md-offset-5">	
							<a id="load_products" class="btn extra" ><i class="fa fa-ellipsis-h fa-2x"></i></a>
						</div>
					</div>
					<div id="outer">
					
					</div>
				<?php
                  }
                  ?>
                <!-- /.row --> 
              </div>
              <!-- /.category-product --> 
              <script>
				$(document).ready(function(){
					$('#load_products').on('click', function(e){
						e.preventDefault();
						var form = new FormData();
						form.append('last_id', $('input[name=last_id]').val());
						form.append('category_id', $('input[name=category_id]').val());
						form.append('criteria', $('input[name=criteria]').val());
						console.log(form);
						$.ajax({
							url:'<?=site_url();?>/load_products',
							data:form,
							type:"POST",
							processData:false,
							contentType:false,
							cache:false,
							beforeSend:function(){
								$('#load_products').hide();
								$('#outer').html('<div class="loader"><img src="<?=base_url();?>assets/frontend/images/loader.svg" /></div>');
							},
							success:function(response){
								console.log(response);
								if(response.status == true){
									$('#outer').empty();
									$('#load_more').append(response.data);
									$('#load_products').html('<i class="fa fa-ellipsis-h fa-2x"></i>');
									$('input[name=last_id]').val(response.last_id);
									$('#load_products').show();
								}
								
								$(function () { $('.rating').rateit();});
							}, 
							error:function(response){
								var data = $.parseJSON(response.responseText);
								if(data.status == false){
									$('#load_more').append(data.message);
									//$('input[name=last_id]').val(response.last_id);
									$('#outer').empty();
								}
							}
						});
					});
				});
                                
                                
                                
                                
                                
                                
			  </script>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  
</div>
<!-- <pre>
<?php //print_r($products) ;?> 
</pre> -->
<?php echo $layout['footer'];?>

<script>
  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 800,
      values: [0, 800 ],
      slide: function( event, ui ) {
       $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
     
      },
      stop: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
          console.log(ui.values[ 0 ] + "--------" + ui.values[ 1 ]);
          $('#minPrice').val(ui.values[ 0 ]);
          $('#maxPrice').val(ui.values[ 1 ]);
         search(); 
      }
    });
    
    $( "#amount" ).val( "$"+$( "#slider-range" ).slider( "values", 0 ) +" - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });
  
  function search() {
            var minPrice= $('#minPrice').val();
            var maxprice= $('#maxPrice').val();
            $.ajax({
              type:"post",
              url:"<?php echo base_url(); ?>Products/searchProduct",
              data:$('#serach').serialize(),
              beforeSend:function(){
                $('#load_more').html('<div class="loader"><img src="<?=base_url();?>assets/frontend/images/loader.svg" /></div>');
             },
              success:function(response) {
                  console.log(response.data);
                    $('.loader').remove();
                  if(response.status == true){
                       $("#load_more").html('');
                       $("#load_more").html(response.data);
                  }
              }
            });
    }
  
  
  </script>
