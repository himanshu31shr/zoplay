<?php
		echo	$layout['header'];
?>
<style>
	tr.style1{
		border-top: 1px solid #8c8b8b;
	}
	.label-custom1{
		background: rgba(204, 204, 204, 0.48);
		color: #000;
		font-weight: 400;
		text-transform: capitalize;
		font-size: 11px;   
		border: 1px solid #ccc;
		float: left;
		margin: 5px;
	}
</style>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo site_url();?>">Home</a></li>
				<li class='active'>My Shopping Cart</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content outer-top-xs">
	<div class="container">
		<div class="row ">
			<div class="shopping-cart">
				<div class="shopping-cart-table ">
				<div class="col-md-12">
					<button class="btn btn-danger btn-sm pull-right" onclick="clear_cart('all');"><i class="fa fa-trash"></i> Clear cart</button>
				</div>
				<div class="text-center success_message text-success"></div>
	<div class="table-responsive">
	<?php 
		$myproduct_count = $allcartdata['myproduct_count'];
		$mygrand_total = $allcartdata['mygrand_total'];
		$mycart_products = $allcartdata['mycart_products'];
	?>	
		<table class="table" id="carttable">
			<thead>
				<tr>
					<th class="cart-product-name item">Item</th>
					<th class="cart-qty item">Quantity</th>
					<th class="cart-sub-total item">Price</th>
					<th class="cart-total last-item">Subtotal</th>
					<th class="cart-romove item">Remove</th>
					<th class="cart-edit item">Update</th>
				</tr>
			</thead><!-- /thead -->
		
			
			<tbody id="cartemptymessage">
			
			<?php echo $mycart_products;?>
				
			</tbody><!-- /tbody -->
		</table><!-- /table -->
	</div>
</div><!-- /.shopping-cart-table -->

<div class="col-md-12 col-sm-12 cart-shopping-total">
	<table class="table">
		<thead>
			<tr>
				<th>
				&nbsp;
				</th>
				<th class="">
					<div class="cart-sub-total">
						Total<span class="inner-left-md">$<span class="mygrand_total"><?php echo $mygrand_total;?></span></span>
					</div>
					<div class="cart-grand-total">
						Grand Total<span class="inner-left-md">$<span class="mygrand_total"><?php echo $mygrand_total;?></span></span>
					</div>
				</th>
				
			</tr>
		</thead><!-- /thead -->
		<tbody>
				<tr>
					<td>
						<div class="cart-checkout-btn pull-left">
							<div class="shopping-cart-btn">
								<span class="">
									<a href="<?php echo site_url();?>" class="btn btn-upper btn-primary outer-left-xs">Continue Shopping</a>
									<!--a href="shopping-cart.html#" class="btn btn-upper btn-primary pull-right outer-right-xs">Update shopping cart</a-->
								</span>
							</div><!-- /.shopping-cart-btn -->
						</div>
					</td>
					<td>
						<div class="cart-checkout-btn pull-right">
							<a href="<?php echo site_url('place_order');?>" class="btn btn-primary checkout-btn" id="proceedlink">PROCEED TO CHECKOUT</a>
						</div>
					</td>
				</tr>
		</tbody><!-- /tbody -->
	</table><!-- /table -->
</div><!-- /.cart-shopping-total -->			</div><!-- /.shopping-cart -->
		</div> <!-- /.row -->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->

<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->
	<!--Update Cart Data-->
	<script type="text/javascript">
		$(document).ready(function() {   
			$('.updateFromcart').click(function(e){
				
				if( !$(this).val() ) {
					  $(this).parents('p').addClass('warning');
				}
				var tr = $(this).closest('tr'),
				id = tr[0].id;
				e.preventDefault();
				var formdata = new FormData();
				var cid = $(this).attr('data-cartid');
				
				formdata.append('cart_listid', $(this).attr('data-cartid'));
				formdata.append('quantity', $('#qty_'+cid).val());
				formdata.append('price', $('#price_'+cid).val());
				formdata.append('pid', $('#pid_'+cid).val());
				
				$.ajax({
				url: '<?=site_url();?>Checkout/updatecartdata',
				data: formdata,
				type:"POST",
				contentType: false,
				processData:false,
				cache: false,
				beforeSend:function(){
					$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
				},
				success: function(response){
					var data = response;
					if(data.status == true){
						$('.text-danger').empty();
						$('#cartasyncdata').html(data.html);
						$.each(response.data, function(key, value){
							$('.'+key).text(value);
							$('#'+cid+'_'+key).text(value);
						});
					}
				},
				error: function(response){ 
					var data = response.responseText;
					data = $.parseJSON(data);
					if(data.status == false){
						swal({
						  title: 'Error!',
						  text: data.message,
						  html: true,
						  timer: 3000,
						  type: "warning",
						  showConfirmButton: false
						});
					}
				},
				complete: function(){
					$('.overlay').remove();
				}
			});
			
			});
		});
	</script>
<?php
		echo	$layout['footer'];
?>