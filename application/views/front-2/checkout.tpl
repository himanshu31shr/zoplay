<script src='https://www.google.com/recaptcha/api.js'></script>
<?php
		echo	$layout['header'];
?>
<style>
	tr.style1{
	border-top: 1px solid #8c8b8b;
	}
	.label-custom1{
		background: rgba(204, 204, 204, 0.48);
		color: #000;
		font-weight: 400;
		text-transform: capitalize;
		font-size: 11px;   
		border: 1px solid #ccc;
		float: left;
		margin: 5px;
	}
</style>
<?php 
	$myproduct_count = $carthtml['myproduct_count'];
	$mygrand_total = $carthtml['mygrand_total'];
	$mycart_products = $carthtml['mycart_products'];
?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="checkout.html#">Home</a></li>
				<li class='active'>Checkout</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="checkout-box ">
			<div class="row">
				<div class="col-md-12">
					<div class="panel-group checkout-steps" id="accordion">
						<!-- checkout-step-01  -->
<div class="panel panel-default checkout-step-01">

	<!-- panel-heading -->
		<div class="panel-heading">
    	<h4 id="mdheading1" class="unicase-checkout-title">
	        <a data-toggle="collapse" id="mdfirststep" class="" data-parent="#accordion" href="checkout.html#collapseOne">
	          <span id="mdcheckicon1">1</span>Before You Place Your Order > Sign in
	        </a>
	     </h4>
    </div>
    <!-- panel-heading -->

	<div id="collapseOne" class="panel-collapse collapse in">

		<!-- panel-body  -->
	    <div class="panel-body">
			<div class="row">		

				<!-- guest-login -->
				<div id="logedinmessage">
				<?php if(!isset($userdata))
				{ ?>
				<div class="col-md-6 col-sm-6 guest-login">
					<h4 class="checkout-subtitle outer-top-vs">Register and save time</h4>
					<p class="text title-tag-line ">Register with us for future convenience:</p>
					
					<ul class="text instruction inner-bottom-30">
						<li class="save-time-reg">- Fast and easy check out</li>
						<li>- Easy access to your order history and status</li>
					</ul>
					<div class="col-md-12 mt-20">
					<div class="col-md-6">
					<label class="info-title" for="exampleInputPassword1">New Registration </label>
					</br>
					<button type="submit" id="register" class="btn-upper btn btn-primary checkout-page-button checkout-continue">Register</button>
					</div>
					<div class="col-md-6">
					<label class="info-title" for="exampleInputPassword1">Already registered? </label>
					</br>
					<button type="submit" id="login" class="btn-upper btn btn-primary checkout-page-button checkout-continue ">Login</button>
					</div>
					</div>
				</div>
				

				<!-- already-registered-login -->
				<div class="col-md-6 col-sm-6 already-registered-login">
					<h4 class="unicase-checkout-title">Already registered?</h4>
					<p class="text title-tag-line">Please log in below:</p>
					<form class="register-form outer-top-xs" role="form" id="login_formcheckout">
						<div id="alert"></div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1"><b>Email Address </b><span>*</span></label>
							<input type="email" class="form-control unicase-form-control text-input" name="emaill" id="email" >
							<p class="text text-danger" id="emaill_err"></p>
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputPassword1"><b>Password </b><span>*</span></label>
							<input type="password" class="form-control unicase-form-control text-input" name="passwordl" id="password" >
							<p class="text text-danger" id="passwordl_err"></p>
						</div>
						<div class="radio outer-xs">
							<button type="submit" id="loginbtn" class="btn-upper btn btn-primary checkout-page-button">Login</button>
							
						</div>
						
					</form>
					<script>
						$(document).ready(function(){
							$('#loginbtn').on('click', function(e){
								e.preventDefault();
								var form = new FormData($('#login_formcheckout')[0]);
								$.ajax({
									url:'<?=site_url();?>/user/log_user',
									data: form,
									type:"POST",
									processData:false,
									contentType: false,
									cache:false,
									beforeSend:function(){
										$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
									},
									success:function(response){
										var data = response;
										console.log(data);
										//var data = $.parseJSON(data);
										if(data.status == true){
											/* $('#alert').html('<p class="alert alert-success">'+data.message+'</p>');
											$('#loginbtn').html('<i class="fa fa-check"></i> Logged In!');
											$('#logedinmessage').html('<p><h4 class="text-success text-center">You Are Logged In !</h4></p>');
											
											$('#collapseOne').collapse('hide');
											$('.checkout-step-02').removeClass('hide');
											$('#mdsecondstep').click();
											$('#mdheading1').css('background', '#dff0d8');
											$( "#mdcheckicon1" ).html( "<i class='fa fa-check text-right'><i>" ); */
											window.location = "<?php  echo site_url('place_order'); ?>";
										}
									},
									error:function(response){
										var data = response.responseText;
										var data = $.parseJSON(data);
										if(data.status == false){
											$('#alert').html('<p class="alert alert-danger">'+data.message+'</p>');
											var err = data.errorMsg;
											$.each(err, function(key, value){
												$('#'+key+'l_err').text(value);
											});
										}
									}, 
									complete:function(){
										$('.overlay').remove();
									}
								})
							})
						});
					</script>
				</div>	
					
				
				<!-- new-registration -->
				<div class="col-md-6 col-sm-6 create-new-account hide">

					<form class="register-form outer-top-xs" id="reg_formcheckout" role="form">
						<div id="status">
										
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail2">Email Address <span>*</span></label>
							<input type="email" name="email" class="form-control unicase-form-control text-input" id="exampleInputEmail2" >
							<div class="text-danger" id="email_error" ></div>
							<div class="text-danger" id="is_unique_error" ></div>
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Name <span>*</span></label>
							<input type="text" name="username" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
							<div class="text-danger" id="username_error" ></div>
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
							<input type="text" name="phone" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
							<div class="text-danger" id="phone_error" ></div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label class="info-title" for="exampleInputEmail1">Password <span>*</span></label>
									<input type="password" name="password" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
									<div class="text-danger" id="password_error" ></div>
								</div>
								<div class="col-md-6">
									<label class="info-title" for="exampleInputEmail1">Confirm Password <span>*</span></label>
									<input type="password" name="passconf" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
									<div class="text-danger" id="passconf_error" ></div>
								</div>
							</div>
						</div>
						</hr>
						
						<button type="submit" id="click_form" class="btn-upper btn btn-primary mt-20 checkout-page-button">Sign Up</button>
						
					</form>
					<script type="text/javascript">
						$(document).ready(function() {   
							$('#click_form').click(function(e){
								e.preventDefault();
								var formdata = new FormData($('#reg_formcheckout')[0]);
								//console.log(reg_form);
								$.ajax({
								url: '<?=site_url();?>User/register',
								data: formdata,
								type:"POST",
								contentType: false,
								processData:false,
								cache: false,
								beforeSend:function(){ 
										$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
									},
								success: function(response){
									var data = response;
									if(data.status == true){
										$('#status').html('<p class="alert alert-success">'+response.message+'<p>');
										$('.text-danger').empty();
										$('#click_form').removeAttr('disabled').html('<i class="fa fa-check"></i> Registration Successful!</p>');
										$('#reg_formcheckout')[0].reset();
										$('.error').html();
									}
								},
								error: function(response){  
									var data = response.responseText;
									data = $.parseJSON(data);
									if(data.status == false){
										data = data.data;
										$.each(data, function(key, value){
											$('#'+key+'_error').empty().html(value);
										});
										$('#click_form').removeAttr('disabled').html('<i class="fa fa-times"></i> Try again!');
									}
								}, 
								complete:function(){
									$('.overlay').remove();
								}
							});
							});
						});
					</script>

				</div>
				<?php
				} else {
					$username = $userdata->user_name;
					$useremail = $userdata->email;
					echo '<div class="col-md-12 text-center"><h4 class="text-success">Logged In As '.$username.' ('.$useremail.').</h4></div>
					<div class="col-md-12">
					<button class="btn btn-primary pull-right" id="logdcontinue">Continue</button>
				</div>
					';
				}?>
				</div>
			</div>			
		</div>
		<!-- panel-body  -->

	</div><!-- row -->
</div>
<!-- checkout-step-01  -->
					    <!-- checkout-step-02  -->
					  	<div class="panel panel-default checkout-step-02 hide">
						    <div class="panel-heading">
						      <h4 id="mdheading2" class="unicase-checkout-title">
						        <a data-toggle="collapse" id="mdsecondstep" class="collapsed" data-parent="#accordion" href="checkout.html#collapseTwo">
						          <span id="mdcheckicon2">2</span> Delivery Address
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwo" class="panel-collapse collapse">
						      <div class="panel-body">
							  
								<div class="col-md-4 col-sm-6 saved_address" style="width:100%;" id="mdaddressul">
									<div id="addressalert"></div>
									<legend>Saved Addresses</legend>
									<ul class="scrollbar" id="style-8" style="max-height:400px !important;">
									<?php print_r($useraddress);?>
									</ul>
									
								</div>
								
								<div class="col-md-8 col-sm-6 hide" id="newaddressdiv">
								
								<legend>Add New Address</legend>
								<form class="register-form outer-top-xs" id="address_form" role="form">
									<div id="addressstatus">
													
									</div>
									<div class="form-group">
										<label class="info-title" for="exampleInputEmail2">Name <span>*</span></label>
										<input type="text" name="orderedusername" class="form-control" placeholder="Enter your Name">
										<div class="text-danger" id="orderedusername_error" ></div>
									</div>
									
									<div class="form-group">
										<label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
										<input type="text" name="orederedphone" class="form-control" placeholder="Enter your Phone number">
										<div class="text-danger" id="orederedphone_error" ></div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-4">
												<label class="info-title" for="exampleInputEmail1">Country <span>*</span></label>
												<select id="selectshipcountry" name="orderedcountry" required class="form-control" onchange="initAutocomplete(this);" required>
													 <?php echo getCountries('','','shipping'); ?>
												</select>
												<div class="text-danger" id="orderedcountry_error" ></div>
											</div>
											<div class="col-md-4">
												<label class="info-title" for="exampleInputEmail1">City <span>*</span></label>
												<input id="autocomplete" placeholder="Enter your City"
             onFocus="geolocate()" class="form-control" type="text">

											</div>
											<div class="col-md-4">
												<label class="info-title" for="exampleInputEmail1">Pincode <span>*</span></label>
												<input type="text" name="orderedaddrespincode" class="form-control" id="postal_code">
												<div class="text-danger" id="orderedaddrespincode_error" ></div>
											</div>
										</div>
									</div>
									<div id="address">
										<input type="hidden" name="orderedcity" id="locality">
										<input type="hidden" name="orderedstate" id="administrative_area_level_1">
										<input type="hidden" name="orderedgcn" id="country">
										<input type="hidden" name="orderedstreet" id="street_number">
										<input type="hidden" name="orderedroute" id="route">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">Address Line1 <span>*</span></label>
												<textarea name="orderedaddress1" class="form-control" placeholder="Enter your address line1" rows="4"></textarea>
												<div class="text-danger" id="orderedaddress1_error" ></div>
											</div>
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">Address Line2 <span>*</span></label>
												<textarea name="orderedaddress2" class="form-control" placeholder="Enter your address line2" rows="4"></textarea>
												<div class="text-danger" id="orderedaddress2_error" ></div>
											</div>
										</div>
									</div>
									</hr>
									</br>
									
									<button type="submit" id="click_addressinfoform" class="btn-upper btn btn-primary mt-20 checkout-page-button pull-right">Save</button>
									
								</form>
								<script type="text/javascript">
									$(document).ready(function() {   
										$('#click_addressinfoform').click(function(e){
											e.preventDefault();
											var formdata = new FormData($('#address_form')[0]);
											//console.log(reg_form);
											$.ajax({
											url: '<?=site_url();?>Checkout/checkoutaddress',
											data: formdata,
											type:"POST",
											contentType: false,
											processData:false,
											cache: false,
											beforeSend:function(){ 
													$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
												},
											success: function(response){
												var data = response;
												if(data.status == true){
													$('#status').html('<p class="alert alert-success">'+response.message+'<p>');
													data = data.data;
													
													$('.text-danger').empty();
													$('#click_addressinfoform').removeAttr('disabled').html('<i class="fa fa-check"></i> Save Successful!</p>');
													$('#address_form')[0].reset();
													$('.error').html();
													$("div#mdaddressul").css('width', '100%');
													$( "#newaddressdiv" ).addClass( "hide" );
													
													$('#addressalert').html('<p class="alert alert-success">'+response.message+'</p>');
													$('#style-8').html(response.html);
													$('#style-8 li').click(function(){
														var id = $(this).attr('id');
														$('#style-8 li').attr('class', '');
														$('#'+id).attr('class', 'active');
														$('#addedaddrid').val(id);
														$('#mdaddedaddrid').val(id);
														console.log(id);
														
													});
												}
											},
											error: function(response){  
												var data = response.responseText;
												data = $.parseJSON(data);
												if(data.status == false){
													data = data.data;
													$.each(data, function(key, value){
														$('#'+key+'_error').empty().html(value);
													});
													$('#click_addressinfoform').removeAttr('disabled').html('<i class="fa fa-times"></i> Try again!');
												}
											}, 
											complete:function(){
												$('.overlay').remove();
											}
										});
										});
									});
								</script>
								</div>
																
						      </div>
							  <div class="panel-body">
								<div class="col-md-12 col-sm-12">
								<div class="col-md-12 text-danger text-center alert" id="dlvraddressid_error"></div>
								<div class="col-md-6 text-left">
								
								  <button id="newaddressbtn" class="btn-upper btn btn-primary checkout-page-button checkout-continue">+ Add New Address</button>
								</div>  
								<div class="col-md-6 text-right">

								  <input type="hidden" id="addedaddrid" name="dlvraddressid">
								  
								  <button  type="submit" id="addcountinuebtn" class="btn-upper btn btn-primary checkout-page-button checkout-continue">Continue With Selected</button>
								</div>
								</div>
							  </div>
						    </div>
					  	</div>
						
						<script type="text/javascript">
							$(document).ready(function() {   
								$('#addcountinuebtn').on('click',function(e){
									e.preventDefault();
									var addid = $('#addedaddrid').val();
									var formdata = new FormData();
									formdata.append('addid',addid);									
									$.ajax({
									url: '<?=site_url();?>Checkout/deliveryaddress',
									data: formdata,
									type:"POST",
									contentType: false,
									processData:false,
									cache: false,
									beforeSend:function(){
											$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
										},
									success: function(response){
										var data = response;
										if(data.status == true){
											$('.text-danger').empty();
											$('.overlay').remove();
											$('#collapseTwo').collapse('hide');
											$('.checkout-step-03').removeClass('hide');
											$('#mdthirdstep').click();
											$('#mdheading2').css('background', '#dff0d8');
											$( "#mdcheckicon2" ).html( "<i class='fa fa-check text-right'><i>" );
										}
									},
									error: function(response){ 
										var data = response.responseText;
										data = $.parseJSON(data);
										if(data.status == false){
											data = data.data;
											$.each(data, function(key, value){
												$('#'+key+'_error').empty().html(value);
											});
											$('#status').html('<p class="alert alert-danger">'+response.message+'<p>');
											$('.overlay').remove();
										}
									
									}
								});
								
								});
							});
						</script>
					  	<!-- checkout-step-02  -->
						
						<!-- checkout-step-03  -->
					  	<div class="panel panel-default checkout-step-03 hide">
						    <div class="panel-heading">
						      <h4 class="unicase-checkout-title" id="mdheading3">
						        <a data-toggle="collapse" id="mdthirdstep" class="collapsed" data-parent="#accordion" href="checkout.html#collapseThree">
						       		<span id="mdcheckicon3">3</span>Order Summary
						        </a>
						      </h4>
						    </div>
						    <div id="collapseThree" class="panel-collapse collapse">
						      <div class="panel-body">
								<div class="row ">
									<div class="shopping-cart">
										<div class="shopping-cart-table ">
											<div class="text-center success_message text-success"></div>
												<div class="table-responsive">
												<?php 
													$myproduct_count = $allcartdata['myproduct_count'];
													$mygrand_total = $allcartdata['mygrand_total'];
													$mycart_products = $allcartdata['mycart_products'];
												?>	
													<table class="table" id="carttable">
														<thead>
															<tr>
																<th class="cart-product-name item">Item</th>
																<th class="cart-qty item">Quantity</th>
																<th class="cart-sub-total item">Price</th>
																<th class="cart-total last-item">Subtotal</th>
																<th class="cart-edit item">Remove</th>
																<th class="cart-romove item">Update</th>
															</tr>
														</thead><!-- /thead -->
														
														<tbody id="cartemptymessage">
															
															<?php
																echo $mycart_products;
															?>
															
														</tbody><!-- /tbody -->
														
													</table><!-- /table -->
												</div>
										</div><!-- /.shopping-cart-table -->
										<?php
										/* $cart_check = $this->cart->contents();
										if(empty($cart_check))
										{
											echo '<div class="col-md-12 col-sm-12 cart-shopping-total"><span class=" text-center text-danger">Your Cart Have No Items To Purchase.Please Continue Shopping</span></div>';
										}
										else
										{ */
										?>
										<div class="col-md-12 col-sm-12 cart-shopping-total">
										<table class="table">
											<thead>
												<tr>
													<th>
													&nbsp;
													</th>
													<th class="">
														<div class="cart-sub-total">
															Total<span class="inner-left-md">$<span class="mygrand_total"><?php echo $mygrand_total;?></span></span>
														</div>
														<div class="cart-sub-total">
															Coupon Discount<span class="inner-left-md">- $<span id="mycoupon_discount">0</span></span>
														</div>
														<div class="cart-grand-total">
															Grand Total<span class="inner-left-md">$<span class="mygrand_total" id="mygreatgrand_total"><?php echo $mygrand_total;?></span></span>
														</div>
													</th>
													
												</tr>
											</thead><!-- /thead -->
											<tbody>
													<tr>
														<td>
															<div class="cart-checkout-btn pull-left">
																<div class="shopping-cart-btn">
																	<span class="">
																		<div class="col-md-8"><input class="form-control" placeholder="Have a coupon code?" type="text" id="checkcoupon_code"></div>
																		<div class="col-md-4">
																		<a class="btn btn-upper btn-primary outer-left-xs"  onclick="check_to_coupon('')">Apply</a>
																		</div>
																		<!--a href="shopping-cart.html#" class="btn btn-upper btn-primary pull-right outer-right-xs">Update shopping cart</a-->
																	</span>
																</div><!-- /.shopping-cart-btn -->
															</div>
														</td>
														<td>
															<div class="cart-checkout-btn pull-right">
																<a href="<?php echo site_url('place_order');?>" class="btn btn-primary" id="proceedlink">Continue</a>
															</div>
														</td>
													</tr>
											</tbody><!-- /tbody -->
										</table><!-- /table -->
									</div><!-- /.cart-shopping-total -->
									<?php
										//}
									?>
									</div><!-- /.shopping-cart -->
								</div> <!-- /.row -->
						      </div>
						    </div>
					  	</div>
						
						<!--Update Cart Data-->
						<script type="text/javascript">
							$(document).ready(function() {   
								$('.updateFromcart').click(function(e){
									
									if( !$(this).val() ) {
										  $(this).parents('p').addClass('warning');
									}
									var tr = $(this).closest('tr'),
									id = tr[0].id;
									e.preventDefault();
									var formdata = new FormData();
									var cid = $(this).attr('data-cartid');
									//alert(formdata);
									formdata.append('cart_listid', $(this).attr('data-cartid'));
									formdata.append('quantity', $('#qty_'+cid).val());
									formdata.append('price', $('#price_'+cid).val());
									formdata.append('pid', $('#pid_'+cid).val());
									
									
									$.ajax({
									url: '<?=site_url();?>Checkout/updatecartdata',
									data: formdata,
									type:"POST",
									contentType: false,
									processData:false,
									cache: false,
									beforeSend:function(){
											$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
										},
									success: function(response){
										var data = response;
										if(data.status == true){
											$('.text-danger').empty();
											$('#cartasyncdata').html(data.html);
											$.each(response.data, function(key, value){
												$('.'+key).text(value);
												$('#'+cid+'_'+key).text(value);
											});
											$('.tcodamount').val(response.data.mygrand_total);
										}
									},
									error: function(response){ 
										var data = response.responseText;
										data = $.parseJSON(data);
										if(data.status == false){
											swal({
											  title: 'Error!',
											  text: data.message,
											  html: true,
											  timer: 3000,
											  type: "warning",
											  showConfirmButton: false
											});
										}
									
									},
									complete: function(){
										$('.overlay').remove();
									}
								});
								
								});
							});
						</script>
						
					  	<!-- checkout-step-03  -->

						<!-- checkout-step-04  -->
					    <div class="panel panel-default checkout-step-04 hide">
						    <div class="panel-heading">
						      <h4 class="unicase-checkout-title" id="mdheading3">
						        <a data-toggle="collapse" id="mdfourthstep" class="collapsed" data-parent="#accordion" href="checkout.html#collapseFour">
						        	<span id="mdcheckicon4">4</span>Human verification
						        </a>
						      </h4>
						    </div>
						    <div id="collapseFour" class="panel-collapse collapse">
							    <div class="panel-body">
								<div class="row single-product">
									<div class="col-md-12">
										<div class="product-tabs inner-bottom-xs  wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
											<div class="row">
												<div class="col-sm-12">
													<div class="tab-content" id="mytab-content">
															<form id="humanverify_form" class="form-horizontal" data-parsley-focus="first">
																<div id="update_alert"></div>
																<fieldset>
																	<div class="form-group">
																	<div class="col-md-6" id="capatchaalert"></div>
																	</div>
																	<div class="form-group">
																		<div class="col-md-12">
																		<div name="checkcapactha" class="g-recaptcha" data-sitekey="6LcLyhMUAAAAAPmk2aseJhcaCr7chu93N1T8XFtv"></div>
																		</div>
																	</div>
																	
																	<div class="form-group">
																		<div class="col-md-4">
																			<button id="humanverify" name="sbnupdate" class="btn btn-primary">Continue</button>
																		</div>
																		<label class="col-md-4 control-label" for="singlebutton"></label>
																	</div>
																	
																</fieldset>
															</form>
														</div>
														<!-- /.tab-pane -->
													</div>
													<!-- /.tab-content -->
												</div>
												<!-- /.col -->
											</div>
											<!-- /.row -->
										</div>
									</div>
            </div>
							    </div>
					    	</div>
						<!-- checkout-step-04  -->
						
						<!-- checkout-step-05  -->
					    <div class="panel panel-default checkout-step-05 hide">
						    <div class="panel-heading">
						      <h4 class="unicase-checkout-title">
						        <a data-toggle="collapse" id="mdfifthstep" class="collapsed" data-parent="#accordion" href="checkout.html#collapseFifth">
						        	<span>5</span>Payment Method
						        </a>
						      </h4>
						    </div>
						    <div id="collapseFifth" class="panel-collapse collapse">
							    <div class="panel-body">
								<div class="row single-product">
									<div class="col-md-12">
										<div class="product-tabs inner-bottom-xs wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
											<div class="row">
												<div class="col-sm-3">
													<ul id="gatwaytab" class="nav nav-tabs nav-tab-cell">
														<?php 
															if(isset($payment_gateways))
															{
																$i=0;
																foreach($payment_gateways as $gateways_list)
																{ 
																$acclass='';
																	if($i==0)
																	{
																		$acclass = 'active';
																	}
																	echo'<li class="'.$acclass.'" id="gw_'.$gateways_list->payment_gateway_id.'">
																			<a data-toggle="tab" href="gwtab_'.$gateways_list->payment_gateway_id.'">'.$gateways_list->payment_gateway_name.'</a>
																		 </li>'; 
																	$i++;	 
																}
															} 
														?>
														
													</ul>
													
													<!-- /.nav-tabs #product-tabs -->
												</div>
												<div class="col-sm-9">
												
													<div class="tab-content" id="mytab-content">
														<?php 
														if(isset($payment_gateways))
														{
															$i=0;
															foreach($payment_gateways as $gateways_list)
															{ 
															
														?>
														<div id="tabgw_<?php echo $gateways_list->payment_gateway_id;?>" class="tab-pane <?=$i==0 ? 'active' : '';?>">
															<form method="post" action="<?php echo site_url('Checkout/buy');?>" class="form-horizontal">
																<div id="update_alert"></div>
																<fieldset>
																	<!-- Form Name -->
																	<legend><?php echo $gateways_list->payment_gateway_name;?></legend>
																	<!-- Text input-->
																	</br>
																	<div class="form-group">
																	
																		<input type="hidden" id="mdaddedaddrid" name="cashaddressid">
																		<input type="hidden" name="paygtwtype" value="<?php echo $gateways_list->payment_gateway_name;?>">
																		<input type="hidden" name="coupondiscnt" value="0" id="paymentmycoupon_discount">
																		<input type="hidden" name="couponid" value="0" id="paymentmycoupon_id">
																		<div class="col-md-12">
																		<label class="col-md-4" for="textinput">Amount</label>
																		<div class="col-md-4">
																			<span class="inner-left-md">$<span class="mygrand_total" id="mygreatgrand_total"><?php echo $mygrand_total;?></span></span>
																			
																			<div class="text-danger" id="cashtamount_error" ></div>
																		</div>
																		<div class="col-md-4">
																			<button  type="submit" name="sbnupdate" class="btn btn-primary">Pay Now</button>
																		</div>
																		</div>
																	</div>
																</fieldset>
															</form>
														</div>
														<!-- /.tab-pane -->
														<?php
														$i++;
															}
														}
														?>
														
														<!-- /.tab-pane -->
													</div>
													<!-- /.tab-content -->
												</div>
												<!-- /.col -->
											</div>
											<!-- /.row -->
										</div>
									</div>
            </div>
							    </div>
					    	</div>
						</div>
						<!-- checkout-step-05  -->
					  	
					</div><!-- /.checkout-steps -->
				</div>
				<script> 
					$('#gatwaytab li').on('click', function(){
						var id = $(this).attr('id');
						$('#gatwaytab li').attr('class', '');
						$('#'+id).attr('class', 'active');
						$('#mytab-content div').removeClass('active');
						$('#tab'+id).addClass('active');
						console.log(id);
					});
					
				</script>
				<script type="text/javascript">
					$(document).ready(function() {   
						$('#humanverify').click(function(e){
							e.preventDefault();
							var formdata = new FormData($('#humanverify_form')[0]);
							
							$.ajax({
							url: '<?=site_url();?>Checkout/capatcha_verify',
							data: formdata,
							type:"POST",
							contentType: false,
							processData:false,
							cache: false,
							beforeSend:function(){ 
									$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
								},
							success: function(response){
								var data = response;
								if(data.status == true){
									$('#capatchaalert').html('<p class="alert alert-success">'+response.message+'<p>');
									data = data.data;
									
									$('.text-danger').empty();
									$('#humanverify').removeAttr('disabled').html('<i class="fa fa-check"></i> verified!</p>');
									$('.error').html();
									$('#collapseFourth').collapse('hide');
									$('.checkout-step-05').removeClass('hide');
									$('#mdfifthstep').click();
									$('#mdheading3').css('background', '#dff0d8');
									$( "#mdcheckicon4" ).html( "<i class='fa fa-check text-right'><i>" );
									 								
								}
							},
							error: function(response){  
								var data = response.responseText;
								data = $.parseJSON(data);
								if(data.status == false){
									$('#capatchaalert').html('<p class="alert alert-danger">'+data.message+'</p>');
									
									$('#humanverify').removeAttr('disabled').html('<i class="fa fa-times"></i> Try again!');
								}
							}, 
							complete:function(){
								$('.overlay').remove();
							}
						});
						});
					});
				</script>
				<script type="text/javascript">
						var paymentconfirm = function(paygtname){
							var formdata = new FormData();
							formdata.append('paygtname', paygtname);
							$.ajax({
								//<?php echo site_url().'Checkout/buy/5'; ?>
							url: '<?=site_url();?>Checkout/cash_on_delivery',
							data: formdata,
							type:"POST",
							contentType: false,
							processData:false,
							cache: false,
							beforeSend:function(){ 
									$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
								},
							success: function(response){
								var data = response;
								if(data.status == true){
									$('#capatchaalert').html('<p class="alert alert-success">'+response.message+'<p>');
									data = data.data;
									
									$('.text-danger').empty();
									$('#confirmpayment').removeAttr('disabled').html('<i class="fa fa-check"></i> Ordered Successful!</p>');
									$('.error').html();
									 setTimeout(function() {
										swal({
											title: "Thank You!",
											text: "Your Order Has Been Accepted!",
											type: "success"
										}, function() {
											window.location = data;
										});
									}, 500);								
								}
							},
							error: function(response){  
								var data = response.responseText;
								data = $.parseJSON(data);
								if(data.status == false){
									$('#capatchaalert').html('<p class="alert alert-danger">'+data.message+'</p>');
									
									
									$('#confirmpayment').removeAttr('disabled').html('<i class="fa fa-times"></i> Try again!');
								}
							}, 
							complete:function(){
								$('.overlay').remove();
							}
						});
						};
				</script>
				<!--div class="col-md-4">
					
<div class="checkout-progress-sidebar ">
	<div class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading">
		    	<h4 class="unicase-checkout-title">Your Checkout Progress</h4>
		    </div>
		    <div class="">
				<ul class="nav nav-checkout-progress list-unstyled">
					<li><a href="checkout.html#">Billing Address</a></li>
					<li><a href="checkout.html#">Shipping Address</a></li>
					<li><a href="checkout.html#">Shipping Method</a></li>
					<li><a href="checkout.html#">Payment Method</a></li>
				</ul>		
			</div>
		</div>
	</div>
</div> 
			</div--> 
			</div><!-- /.row -->
		</div><!-- /.checkout-box -->
		
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->

		<script>
			$('#login').on('click', function(e){
				e.preventDefault();
				$('.already-registered-login').attr('class', 'col-md-6 col-sm-6 already-registered-login');
				$('.create-new-account').attr('class', 'col-md-6 col-sm-6 create-new-account hide');
			});
			
			$('#register').on('click', function(e){
				e.preventDefault();
				$('.create-new-account').attr('class', 'col-md-6 col-sm-6 create-new-account');
				$('.already-registered-login').attr('class', 'col-md-6 col-sm-6 already-registered-login hide');
			});
			
		</script>
		<script>
			$('#newaddressbtn').on('click', function(e){
				e.preventDefault();
				$("div#mdaddressul").removeAttr("style");
				$( "#newaddressdiv" ).removeClass( "hide" );
			});	
			
			$('#proceedlink').on('click', function(e){
				e.preventDefault();
				$('#collapseThree').collapse('hide');
				$('.checkout-step-04').removeClass('hide');
				$('#mdfourthstep').click();
				$('#mdheading3').css('background', '#dff0d8');
				$( "#mdcheckicon3" ).html( "<i class='fa fa-check text-right'><i>" );
			});			
		</script>
		<script>
			$('#logdcontinue').on('click', function(e){
				e.preventDefault();
				$('#collapseOne').collapse('hide');
				$('.checkout-step-02').removeClass('hide');
				$('#mdsecondstep').click();
				$('#mdheading1').css('background', '#dff0d8');
				$( "#mdcheckicon1" ).html( "<i class='fa fa-check text-right'><i>" );
			});			
		</script>
		<script>
			$('#style-8 li').on('click', function(){
				var id = $(this).attr('id');
				$('#style-8 li').attr('class', '');
				$('#'+id).attr('class', 'active');
				$('#addedaddrid').val(id);
				$('#mdaddedaddrid').val(id);
				console.log(id);
			});
			$(document).ready(function(){
				$('.remove').remove();
			});
		</script>
		<style>
			.saved_address ul li{
				cursor: pointer;
			}
		</style>
		<script> 
			$('#gatwaytab li').on('click', function(){
				var id = $(this).attr('id');
				$('#gatwaytab li').attr('class', '');
				$('#'+id).attr('class', 'active');
				console.log(id);
			});
			
		</script>
		<script>
			var check_to_coupon = function(){
				var formdata = new FormData();	
				formdata.append('checkcoupon_code',$('#checkcoupon_code').val());
				
				$.ajax({
					url: '<?=site_url();?>Checkout/verifycoupon',
					data: formdata,
					type:"POST",
					contentType: false,
					processData:false,
					cache: false,
					beforeSend:function(){
						$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
					},
					success: function(response){
						var data = response;
						if(data.status == true){
							var coupondisc = data.data
							$.each(coupondisc, function(key, value){
									if(key=='mycoupon_discount')
									{
										$('#payment'+key).val(value);
									}
									if(key=='mycoupon_id')
									{
										$('#paymentmycoupon_id').val(value);
									}
									$('#'+key).html(value);
							});
							swal({
							  title: 'Thankyou!',
							  text: data.message,
							  html: true,
							  timer: 3000,
							  type: "success",
							  showConfirmButton: false
							});
						}
					}, 
					error: function(response){ 
						var data = response.responseText;
						data = $.parseJSON(data);
						if(data.status == false){
							swal({
							  title: 'Error!',
							  text: data.message,
							  html: true,
							  timer: 3000,
							  type: "warning",
							  showConfirmButton: false
							});
						}
					},
					complete:function(){
					$('.overlay').remove();
					}
				});
				
			}
		</script>
		
		<!--For autocomplete shipping address-->
		<script>
		  // This example displays an address form, using the autocomplete feature
		  // of the Google Places API to help users fill in the information.

		  // This example requires the Places library. Include the libraries=places
		  // parameter when you first load the API. For example:
		  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

		  var placeSearch, autocomplete;
		  var componentForm = {
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'short_name',
			country: 'long_name',
			postal_code: 'short_name'
		  };

		  function initAutocomplete() {
			// Create the autocomplete object, restricting the search to geographical
			// location types.
			autocomplete = new google.maps.places.Autocomplete(
				/** @type {!HTMLInputElement} */
				(document.getElementById('autocomplete')),
				{
				types: ['(cities)'],
				componentRestrictions: {country: $('#selectshipcountry').val()}
				
				});

			// When the user selects an address from the dropdown, populate the address
			// fields in the form.
			autocomplete.addListener('place_changed', fillInAddress);
		  }

		  function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();

			for (var component in componentForm) {
			  document.getElementById(component).value = '';
			  document.getElementById(component).disabled = false;
			}

			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			for (var i = 0; i < place.address_components.length; i++) {
			  var addressType = place.address_components[i].types[0];
			  if (componentForm[addressType]) {
				var val = place.address_components[i][componentForm[addressType]];
				document.getElementById(addressType).value = val;
			  }
			}
		  }

		  // Bias the autocomplete object to the user's geographical location,
		  // as supplied by the browser's 'navigator.geolocation' object.
		  function geolocate() {
			if (navigator.geolocation) {
			  navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
				  lat: position.coords.latitude,
				  lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
				  center: geolocation,
				  radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			  });
			}
		  }
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDv5IBJrYFB-S06EOdq6ugqNxSahQnDnh4&libraries=places&callback=initAutocomplete"
			async defer></script>
	<!--eND autocomplete shipping address-->
	
<?php
		echo	$layout['footer'];
?>