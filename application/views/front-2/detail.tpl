<?php
		echo	$layout['header'];
?>
<!--
<div class="picZoomer">
<img src="http://socialdemo.ilearnersindia.com/filpmart/assets/frontend/images/banners/cat-banner-1.jpg" height="320" width="320" alt="">
</div>-->
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="detail.html#">Home</a></li>
				<li><a href="detail.html#">Clothing</a></li>
				<li class='active'>Floral Print Buttoned</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
	<div class='container'>
		<div class='row single-product'>
			<div class='col-md-3 sidebar'>
				<div class="sidebar-module-container">
				<div class="home-banner outer-top-n">
<img src="<?=base_url() ?>assets/frontend/images/banners/LHS-banner.jpg" alt="Image">
</div>		
  
    
    
    	<!-- ============================================== HOT DEALS ============================================== -->
<div class="sidebar-widget hot-deals wow fadeInUp outer-top-vs">
	<h3 class="section-title">hot deals</h3>
	<div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-xs">
		
				<div class="item">
					<div class="products">
						<div class="hot-deal-wrapper">
							<div class="image">
								<img src="<?=base_url() ?>assets/frontend/images/hot-deals/p5.jpg" alt="">
							</div>
							<div class="sale-offer-tag"><span>35%<br>off</span></div>
							<div class="timing-wrapper">
								<div class="box-wrapper">
									<div class="date box">
										<span class="key">120</span>
										<span class="value">Days</span>
									</div>
								</div>
				                
				                <div class="box-wrapper">
									<div class="hour box">
										<span class="key">20</span>
										<span class="value">HRS</span>
									</div>
								</div>

				                <div class="box-wrapper">
									<div class="minutes box">
										<span class="key">36</span>
										<span class="value">MINS</span>
									</div>
								</div>

				                <div class="box-wrapper hidden-md">
									<div class="seconds box">
										<span class="key">60</span>
										<span class="value">SEC</span>
									</div>
								</div>
							</div>
						</div><!-- /.hot-deal-wrapper -->

						<div class="product-info text-left m-t-20">
							<h3 class="name"><a href="detail.html">Sample</a></h3>
							<div class="rating rateit-small" ></div>

							<div class="product-price">	
								<span class="price">
									$600.00
								</span>
									
							    <span class="price-before-discount">$800.00</span>					
							
							</div><!-- /.product-price -->
							
						</div><!-- /.product-info -->

						<div class="cart clearfix animate-effect">
							<div class="action">
								
								<div class="add-cart-button btn-group">
									<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
										<i class="fa fa-shopping-cart"></i>													
									</button>
									<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
															
								</div>
								
							</div><!-- /.action -->
						</div><!-- /.cart -->
					</div>	
					</div>		        
													<div class="item">
					<div class="products">
						<div class="hot-deal-wrapper">
							<div class="image">
								<img src="<?=base_url() ?>assets/frontend/images/products/p6.jpg" alt="">
							</div>
							<div class="sale-offer-tag"><span>35%<br>off</span></div>
							<div class="timing-wrapper">
								<div class="box-wrapper">
									<div class="date box">
										<span class="key">120</span>
										<span class="value">Days</span>
									</div>
								</div>
				                
				                <div class="box-wrapper">
									<div class="hour box">
										<span class="key">20</span>
										<span class="value">HRS</span>
									</div>
								</div>

				                <div class="box-wrapper">
									<div class="minutes box">
										<span class="key">36</span>
										<span class="value">MINS</span>
									</div>
								</div>

				                <div class="box-wrapper hidden-md">
									<div class="seconds box">
										<span class="key">60</span>
										<span class="value">SEC</span>
									</div>
								</div>
							</div>
						</div><!-- /.hot-deal-wrapper -->

						<div class="product-info text-left m-t-20">
							<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
							<div class="rating rateit-small"></div>

							<div class="product-price">	
								<span class="price">
									$600.00
								</span>
									
							    <span class="price-before-discount">$800.00</span>					
							
							</div><!-- /.product-price -->
							
						</div><!-- /.product-info -->

						<div class="cart clearfix animate-effect">
							<div class="action">
								
								<div class="add-cart-button btn-group">
									<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
										<i class="fa fa-shopping-cart"></i>													
									</button>
									<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
															
								</div>
								
							</div><!-- /.action -->
						</div><!-- /.cart -->
					</div>	
					</div>		        
													<div class="item">
					<div class="products">
						<div class="hot-deal-wrapper">
							<div class="image">
								<img src="<?=base_url() ?>assets/frontend/images/products/p7.jpg" alt="">
							</div>
							<div class="sale-offer-tag"><span>35%<br>off</span></div>
							<div class="timing-wrapper">
								<div class="box-wrapper">
									<div class="date box">
										<span class="key">120</span>
										<span class="value">Days</span>
									</div>
								</div>
				                
				                <div class="box-wrapper">
									<div class="hour box">
										<span class="key">20</span>
										<span class="value">HRS</span>
									</div>
								</div>

				                <div class="box-wrapper">
									<div class="minutes box">
										<span class="key">36</span>
										<span class="value">MINS</span>
									</div>
								</div>

				                <div class="box-wrapper hidden-md">
									<div class="seconds box">
										<span class="key">60</span>
										<span class="value">SEC</span>
									</div>
								</div>
							</div>
						</div><!-- /.hot-deal-wrapper -->

						<div class="product-info text-left m-t-20">
							<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
							<div class="rating rateit-small"></div>

							<div class="product-price">	
								<span class="price">
									$600.00
								</span>
									
							    <span class="price-before-discount">$800.00</span>					
							
							</div><!-- /.product-price -->
							
						</div><!-- /.product-info -->

						<div class="cart clearfix animate-effect">
							<div class="action">
								
								<div class="add-cart-button btn-group">
									<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
										<i class="fa fa-shopping-cart"></i>													
									</button>
									<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
															
								</div>
								
							</div><!-- /.action -->
						</div><!-- /.cart -->
					</div>	
					</div>		        
						
	    
    </div><!-- /.sidebar-widget -->
</div>
<!-- ============================================== HOT DEALS: END ============================================== -->					
				</div>
			</div><!-- /.sidebar -->
	<div class='col-md-9'>
	<?php
		if($product['status'] == true){
			$data = $product['data'];
	?>
		<div class="detail-block">
			<div class="row  wow fadeInUp">
				 <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
			<div class="product-item-holder size-big single-product-gallery small-gallery">

        <div id="owl-single-product">
		<?php
		$j=1;
		if($data->product_images != ''){
			$images =$data->product_images;
			foreach($images as $i){
			?>
				<div class="single-product-gallery-item" id="slide<?=$j;?>">
					<a data-lightbox="<?=$data->product_id;?>" data-title="<?=$data->product_name;?>" href="<?=base_url();?>/files/<?=$i;?>" class="">
					<span class='zoom' id='ex1'>
						<img class="img-responsive" alt="" src="<?=base_url();?>/files/<?=$i;?>" data-echo="<?=base_url();?>/files/<?=$i;?>" />
					</span>
					</a>
				</div><!-- /.single-product-gallery-item -->
			<?php
				$j++;
			}
		}else{
			if($data->product_image == ''){
				$image = base_url().'files/default.png';
			}else{
				$image = $data->product_image;
			}
			?>
			<div class="single-product-gallery-item" id="slide<?=$j;?>">
				<a data-lightbox="image-1" data-title="Gallery" href="<?=$image;?>">
					<img class="img-responsive" alt="" src="<?=$image;?>" data-echo="<?=$image;?>" />
				</a>
			</div><!-- /.single-product-gallery-item -->
			<?php
		}
		?>

        </div><!-- /.single-product-slider -->


        <div class="single-product-gallery-thumbs gallery-thumbs">

            <div id="owl-single-product-thumbnails">
			
			<?php
			if($data->product_images != ''){
				$images = $data->product_images;
				$k=1;
				foreach($images as $i){
			?>
				<div class="item">
                    <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="<?=$k;?>" href="#slide<?=$k;?>">
                        <img class="img-responsive" width="85" alt="" src="<?=base_url();?>/files/<?=$i;?>" data-echo="<?=base_url();?>/files/<?=$i;?>" />
                    </a>
                </div>
			<?php
				$k++;
				}
			}else{
				?>
				<div class="item">
                    <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="1" href="#slide1">
                        <img class="img-responsive" width="85" alt="" src="<?=base_url() ?>assets/frontend/images/blank.gif" data-echo="<?=$data->product_image;?>" />
                    </a>
                </div>
				<?php
			}
			?>
            </div><!-- /#owl-single-product-thumbnails -->

            

        </div><!-- /.gallery-thumbs -->

    </div><!-- /.single-product-gallery -->
</div><!-- /.gallery-holder -->    
					<form id="variations">
					<div class='col-sm-6 col-md-7 product-info-block'>
						<div class="product-info">
							<h1 class="name"><?=$data->product_name;?></h1>
							
							<div class="rating-reviews m-t-20">
								<div class="row">
									<div class="col-sm-3">
										<div class="rating rateit-small" data-rateit-value="<?=$data->average_rating;?>" ></div>
									</div>
									<div class="col-sm-8">
										<div class="reviews">
											<a href="" onclick="$('#reviews_all').click();return false;" class="lnk">(<?=$data->total_reviews;?> Reviews)</a>
										</div>
									</div>
								</div><!-- /.row -->		
							</div><!-- /.rating-reviews -->

							<div class="stock-container info-container m-t-10">
								<div class="row">
									<div class="col-sm-2">
										<div class="stock-box">
											<span class="label">Availability :</span>
										</div>	
									</div>
									<div class="col-sm-9">
										<div class="stock-box">
											<span class="value"><?=$data->availability;?></span>
										</div>	
									</div>
								</div><!-- /.row -->	
							</div><!-- /.stock-container -->
							<style>
							.btn span.glyphicon {    			
								opacity: 0;				
							}
							.btn.active span.glyphicon {				
								opacity: 1;				
							}
							</style>
							
							<div class="stock-container info-container m-t-10">
								<div class="row">
								<?php
								$variations = $data->product_variations;
								if(isset($variations['types']) && $variations != ''){
									$all_variations = '';
									foreach($variations['types'] as $v){
								?>
									<div class="col-sm-2 m-t-10">
										<div class="stock-box">
											<span class="label"><?=$v;?> :</span>
										</div>	
									</div>
									
									<div class="col-sm-9 m-t-10">
									
										<div class="btn-group" data-toggle="buttons">
										<?php
										$i=0;
										foreach($variations['options'][$v] as $param){
										?>
										<label class="btn btn-info <?=$i==0 ? 'active' : '';?>" onclick="$('input[name=<?=$v;?>]').removeAttr( 'checked');$('#<?=$i.$v;?>').attr( 'checked', 'checked' );">
											<input type="radio" name="<?=$v;?>" id="option2" value="<?=$param;?>" autocomplete="off" <?=$i==0 ? 'checked' : '';?>>
											<span class="glyphicon glyphicon-check"></span> <?=$param;?>
											
											<input type="hidden" id="variationtype" name="variationtype" value="<?=$v;?>">
											<input type="radio" id="<?=$i.$v;?>" name="<?=$v;?>" value="<?=$param;?>">
											
										</label>
										<?php
										$i++;
										}
										$all_variations .= $v."," ;
										echo "</div>";
									echo "</div>";
									}
									?>
									<input type="hidden" value="<?=substr($all_variations, 0, -1);?>" name="all_vars" />
									<?php
								}
									?>
								</div><!-- /.row -->	
							</div><!-- /.stock-container -->
							

							<div class="description-container m-t-20">
								<?=substr($data->product_description, 0,150);?>
							</div><!-- /.description-container -->

							<div class="price-container info-container m-t-20">
								<div class="row">
									

									<div class="col-sm-6">
										<div class="price-box">
											<span class="price">$<?=$data->discounted_price;?></span>
											
											<?php
											if($data->is_deal_active == 1){
											?>
											<span class="price-strike">$<?=$data->product_price;?></span>
											<?php
												}
											?>
										</div>
									</div>

									<?php
										$flag = $data->added_to_wish == '0' ? '1' : '0';
										$button = $data->added_to_wish == '0' ? 'btn btn-primary' : 'btn btn-info';
									?>
									<div class="col-sm-6">
										<div class="favorite-button m-t-10">
											<a class="<?=$button;?>" id="inwishlist<?=$data->product_id;?>" data-toggle="tooltip" data-placement="right" title="<?php echo $data->added_to_wish == '0' ? 'Add to wishlist' : 'Remove from wishlist';?>" onclick="addtowish(<?=$flag;?>,<?=$data->product_id;?>)">
											    <i class="fa fa-heart"></i>
											</a>

											
											<a class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Add to Compare" href="<?= site_url() ?>information/product_comparison/<?= base64_encode(1);?>">											   <i class="fa fa-signal"></i>
											</a>
											<a class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="E-mail" href="detail.html#">
											    <i class="fa fa-envelope"></i>
											</a>
										</div>
									</div>

								</div><!-- /.row -->
							</div><!-- /.price-container -->
							<?php
								$disable_addcart = '';
								$availabiltyto_addcart = $data->product_quantity;
								if($availabiltyto_addcart==0)
								{
									$disable_addcart = 'disabled';
									$btntext_addcart = 'Out Of Stock';
								}
								
							?>
							<div class="quantity-container info-container">
							  
								<?php
								  if($availabiltyto_addcart<5)
									{
										echo'<div class="row"><div class="col-md-12"><div class="alert alert-warning" style="    padding: 5px 10px;
    font-size: 14px;">
													<small class="text-warning">'.$availabiltyto_addcart.' unit(s)  left the in stock</small>
												</div>
												</div>
											</div>';
									}
								  ?>
								<div class="row">
									<div class="col-sm-2">
										<span class="label">Qty :</span>
									</div>
									
									<div class="col-sm-2">
										<div class="cart-quantity">
											<div class="quant-input">
								                <!--div class="arrows">
								                  <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
								                  <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
								                </div-->
								                <input type="number" class="text-center" id="inputquantity" name="inputquantity" min="1" max="<?php echo $availabiltyto_addcart;?>" value="1" style="padding:5px;">
							              </div>
							            </div>
									</div>
									<input type="hidden" name="cart_productid" value="<?php echo $data->product_id;?>">
									<div class="col-sm-7">
										<button id="createcart" type="button" onclick="add_to_cart('<?php echo $data->product_id;?>')" class="btn btn-primary" <?php echo $disable_addcart;?>><i class="fa fa-shopping-cart inner-right-vs"></i> <?php if(isset($btntext_addcart)){ echo $btntext_addcart;} else { echo 'ADD TO CART';}?></button>
									</div>								
								</div><!-- /.row -->
							</div><!-- /.quantity-container -->

						</div><!-- /.product-info -->
					</div><!-- /.col-sm-7 -->
					</form>
				</div><!-- /.row -->
                </div>
				
				<div class="product-tabs inner-bottom-xs  wow fadeInUp">
					<div class="row">
						<div class="col-sm-3">
							<ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
								<li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
								<li><a data-toggle="tab" href="#review">REVIEW</a></li>
								<li><a data-toggle="tab" href="#tags">TAGS</a></li>
								<li><a data-toggle="tab" href="#shipping">Shipping Policies</a></li>
							</ul><!-- /.nav-tabs #product-tabs -->
						</div>
						<div class="col-sm-9">

							<div class="tab-content">
								
								<div id="description" class="tab-pane in active">
									<div class="product-tab">
										<p class="text"><?php echo html_entity_decode($data->product_description) == '' ? $data->product_description : html_entity_decode($data->product_description);?></p>
									</div>	
								</div><!-- /.tab-pane -->

								<div id="review" class="tab-pane">
									<div class="product-tab">
																				
										<div class="product-reviews">
											<h4 class="title">Customer Reviews 
												<span id="reviews_all" style="cursor:pointer" class="pull-right label label-info" onclick="see_more('reviews', '<?=md5($data->product_id);?>', 'reviews')">View All</span>
											</h4>
											<div id="status_rev">
												
											</div>
											
											<div class="reviews">
												<?php print_r($data->reviews);?>
											
											</div><!-- /.reviews -->
										</div><!-- /.product-reviews -->
										

										<div class="">
											<div class="">
												<p class="btn btn-block btn-info" onclick="$('#add_review').show();$(this).hide();">Add review</p>
											</div>
											<div class="product-add-review" id="add_review" onload="$(this).hide();">
												<h4 class="title">Write your own review</h4>
												<div class="review-table">
													<div class="table-responsive">
														<table class="table">	
															<thead>
																<tr>
																	<th class="cell-label">&nbsp;</th>
																	<th>1 star</th>
																	<th>2 stars</th>
																	<th>3 stars</th>
																	<th>4 stars</th>
																	<th>5 stars</th>
																</tr>
															</thead>	
															<tbody>
																<tr>
																	<td class="cell-label">Rate this</td>
																	<td><input type="radio" name="rev_rate" class="radio" value="1"></td>
																	<td><input type="radio" name="rev_rate" class="radio" value="2"></td>
																	<td><input type="radio" name="rev_rate" class="radio" value="3"></td>
																	<td><input type="radio" name="rev_rate" class="radio" value="4"></td>
																	<td><input type="radio" name="rev_rate" class="radio" value="5"></td>
																</tr>
															</tbody>
															<p class="text text-danger err_class" id="rating_err"></p>
														</table><!-- /.table .table-bordered -->
													</div><!-- /.table-responsive -->
												</div><!-- /.review-table -->
												
												<div class="review-form">
													<div class="form-container">
														<form role="form" class="cnt-form">
															
															<div class="row">
																<div class="col-sm-6">
																	<div class="form-group">
																		<label for="exampleInputName">Your Name <span class="astk">*</span></label>
																		<input type="text" class="form-control txt" id="rev_name" placeholder="">
																		<p class="text text-danger err_class" id="name_err"></p>
																	</div><!-- /.form-group -->
																	<div class="form-group">
																		<label for="exampleInputSummary">Summary <span class="astk">*</span></label>
																		<input type="text" class="form-control txt" id="rev_summ" placeholder="">
																		<p class="text text-danger err_class" id="summary_err"></p>
																	</div><!-- /.form-group -->
																</div>
																<input name="product_id" value="<?=$data->product_id;?>" id="product_id" type="hidden" />
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="exampleInputReview">Review <span class="astk">*</span></label>
																		<textarea class="form-control txt txt-review" id="review_comment" rows="4" placeholder=""></textarea>
																		<p class="text text-danger err_class" id="head_err"></p>
																	</div><!-- /.form-group -->
																</div>
															</div><!-- /.row -->
															
															<div class="action text-right">
																<button class="btn btn-primary btn-upper" id="add_review_btn">SUBMIT REVIEW</button>
															</div><!-- /.action -->

														</form><!-- /.cnt-form -->
													</div><!-- /.form-container -->
												</div><!-- /.review-form -->

											</div><!-- /.product-add-review -->										
										</div>
							        </div><!-- /.product-tab -->
								</div><!-- /.tab-pane -->

								<div id="tags" class="tab-pane">
									<div class="product-tag">
										
										<h4 class="title">Product Tags</h4>
										<div class="">
										<?php
											$tags= explode(',', $data->search_tags);
											foreach($tags as $t){
										?>
											<span style="font-size: 12px;" class="label label-success"><?=$t;?></span>
										<?php
											}
										?>
										</div><!-- /.form-container -->

									</div><!-- /.product-tab -->
								</div><!-- /.tab-pane -->

								<div id="shipping" class="tab-pane">
									<div class="product-tag">
										
										<h4 class="title">Shipping policies</h4>
										<div class="table-responsive">
											<table class="dataTable stripe">
												<thead>
							            <tr>
							              <th>Shipping destination</th>
														<th>Charges</th>
														<th>Charges with other items</th>
							            </tr>
								        </thead>
												<tbody>
													<?php
													if($data->shipping_charges != ''){
														
														foreach($data->shipping_charges as $s){
													?>
							            <tr>
						                <td><?=$s['country_name'];?></td>
														<td>$<?=$s['shipping_cost'] == '' ? 0 : $s['shipping_cost'];?></td>
														<td>$<?=$s['shipping_cost_with'] == '' ? 0 : $s['shipping_cost_with'];?></td>
							            </tr>
												<?php
													}
												}
												?>
												</tbody>
											</table>
										</div><!-- /.form-container -->

									</div><!-- /.product-tab -->
								</div><!-- /.tab-pane -->

							</div><!-- /.tab-content -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.product-tabs -->
<?php
	}
?>
				<!-- ============================================== UPSELL PRODUCTS ============================================== -->
<section class="section featured-product wow fadeInUp">
	<h3 class="section-title">upsell products</h3>
	<div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
	    	
		<div class="item item-carousel">
			<div class="products">
				
	<div class="product">		
		<div class="product-image">
			<div class="image">
				<a href="detail.html"><img  src="<?=base_url() ?>assets/frontend/images/products/p1.jpg" alt=""></a>
			</div><!-- /.image -->			

			            <div class="tag sale"><span>sale</span></div>            		   
		</div><!-- /.product-image -->
			
		
		<div class="product-info text-left">
			<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
			<div class="rating rateit-small"></div>
			<div class="description"></div>

			<div class="product-price">	
				<span class="price">
					$650.99				</span>
										     <span class="price-before-discount">$ 800</span>
									
			</div><!-- /.product-price -->
			
		</div><!-- /.product-info -->
					<div class="cart clearfix animate-effect">
				<div class="action">
					<ul class="list-unstyled">
						<li class="add-cart-button btn-group">
							<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
								<i class="fa fa-shopping-cart"></i>													
							</button>
							<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
													
						</li>
	                   
		                <li class="lnk wishlist">
							<a class="add-to-cart" title="Wishlist">
								 <i class="icon fa fa-heart"></i>
							</a>
						</li>
						<li class="lnk">
							<a class="add-to-cart" href="detail.html" title="Compare">
							    <i class="fa fa-signal"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.action -->
			</div><!-- /.cart -->
			</div><!-- /.product -->
      
			</div><!-- /.products -->
		</div><!-- /.item -->
	
		<div class="item item-carousel">
			<div class="products">
				
	<div class="product">		
		<div class="product-image">
			<div class="image">
				<a href="detail.html"><img  src="<?=base_url() ?>assets/frontend/images/products/p2.jpg" alt=""></a>
			</div><!-- /.image -->			

			            <div class="tag sale"><span>sale</span></div>            		   
		</div><!-- /.product-image -->
			
		
		<div class="product-info text-left">
			<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
			<div class="rating rateit-small"></div>
			<div class="description"></div>

			<div class="product-price">	
				<span class="price">
					$650.99				</span>
										     <span class="price-before-discount">$ 800</span>
									
			</div><!-- /.product-price -->
			
		</div><!-- /.product-info -->
					<div class="cart clearfix animate-effect">
				<div class="action">
					<ul class="list-unstyled">
						<li class="add-cart-button btn-group">
							<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
								<i class="fa fa-shopping-cart"></i>													
							</button>
							<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
													
						</li>
	                   
		                <li class="lnk wishlist">
							<a class="add-to-cart" href="detail.html" title="Wishlist">
								 <i class="icon fa fa-heart"></i>
							</a>
						</li>

						<li class="lnk">
							<a class="add-to-cart" href="detail.html" title="Compare">
							    <i class="fa fa-signal"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.action -->
			</div><!-- /.cart -->
			</div><!-- /.product -->
      
			</div><!-- /.products -->
		</div><!-- /.item -->
	
		<div class="item item-carousel">
			<div class="products">
				
	<div class="product">		
		<div class="product-image">
			<div class="image">
				<a href="detail.html"><img  src="<?=base_url() ?>assets/frontend/images/products/p3.jpg" alt=""></a>
			</div><!-- /.image -->			

			                        <div class="tag hot"><span>hot</span></div>		   
		</div><!-- /.product-image -->
			
		
		<div class="product-info text-left">
			<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
			<div class="rating rateit-small"></div>
			<div class="description"></div>

			<div class="product-price">	
				<span class="price">
					$650.99				</span>
										     <span class="price-before-discount">$ 800</span>
									
			</div><!-- /.product-price -->
			
		</div><!-- /.product-info -->
					<div class="cart clearfix animate-effect">
				<div class="action">
					<ul class="list-unstyled">
						<li class="add-cart-button btn-group">
							<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
								<i class="fa fa-shopping-cart"></i>													
							</button>
							<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
													
						</li>
	                   
		                <li class="lnk wishlist">
							<a class="add-to-cart" href="detail.html" title="Wishlist">
								 <i class="icon fa fa-heart"></i>
							</a>
						</li>

						<li class="lnk">
							<a class="add-to-cart" href="detail.html" title="Compare">
							    <i class="fa fa-signal"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.action -->
			</div><!-- /.cart -->
			</div><!-- /.product -->
      
			</div><!-- /.products -->
		</div><!-- /.item -->
	
		<div class="item item-carousel">
			<div class="products">
				
	<div class="product">		
		<div class="product-image">
			<div class="image">
				<a href="detail.html"><img  src="<?=base_url() ?>assets/frontend/images/products/p4.jpg" alt=""></a>
			</div><!-- /.image -->			

			<div class="tag new"><span>new</span></div>                        		   
		</div><!-- /.product-image -->
			
		
		<div class="product-info text-left">
			<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
			<div class="rating rateit-small"></div>
			<div class="description"></div>

			<div class="product-price">	
				<span class="price">
					$650.99				</span>
										     <span class="price-before-discount">$ 800</span>
									
			</div><!-- /.product-price -->
			
		</div><!-- /.product-info -->
					<div class="cart clearfix animate-effect">
				<div class="action">
					<ul class="list-unstyled">
						<li class="add-cart-button btn-group">
							<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
								<i class="fa fa-shopping-cart"></i>													
							</button>
							<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
													
						</li>
	                   
		                <li class="lnk wishlist">
							<a class="add-to-cart" href="detail.html" title="Wishlist">
								 <i class="icon fa fa-heart"></i>
							</a>
						</li>

						<li class="lnk">
							<a class="add-to-cart" href="detail.html" title="Compare">
							    <i class="fa fa-signal"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.action -->
			</div><!-- /.cart -->
			</div><!-- /.product -->
      
			</div><!-- /.products -->
		</div><!-- /.item -->
	
		<div class="item item-carousel">
			<div class="products">
				
	<div class="product">		
		<div class="product-image">
			<div class="image">
				<a href="detail.html"><img  src="<?=base_url() ?>assets/frontend/images/blank.gif" data-echo="<?=base_url() ?>assets/frontend/images/products/p5.jpg" alt=""></a>
			</div><!-- /.image -->			

			                        <div class="tag hot"><span>hot</span></div>		   
		</div><!-- /.product-image -->
			
		
		<div class="product-info text-left">
			<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
			<div class="rating rateit-small"></div>
			<div class="description"></div>

			<div class="product-price">	
				<span class="price">
					$650.99				</span>
										     <span class="price-before-discount">$ 800</span>
									
			</div><!-- /.product-price -->
			
		</div><!-- /.product-info -->
					<div class="cart clearfix animate-effect">
				<div class="action">
					<ul class="list-unstyled">
						<li class="add-cart-button btn-group">
							<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
								<i class="fa fa-shopping-cart"></i>													
							</button>
							<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
													
						</li>
	                   
		                <li class="lnk wishlist">
							<a class="add-to-cart" href="detail.html" title="Wishlist">
								 <i class="icon fa fa-heart"></i>
							</a>
						</li>

						<li class="lnk">
							<a class="add-to-cart" href="detail.html" title="Compare">
							    <i class="fa fa-signal"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.action -->
			</div><!-- /.cart -->
			</div><!-- /.product -->
      
			</div><!-- /.products -->
		</div><!-- /.item -->
	
		<div class="item item-carousel">
			<div class="products">
				
	<div class="product">		
		<div class="product-image">
			<div class="image">
				<a href="detail.html"><img  src="<?=base_url() ?>assets/frontend/images/blank.gif" data-echo="assets/images/products/p6.jpg" alt=""></a>
			</div><!-- /.image -->			

			<div class="tag new"><span>new</span></div>                        		   
		</div><!-- /.product-image -->
			
		
		<div class="product-info text-left">
			<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
			<div class="rating rateit-small"></div>
			<div class="description"></div>

			<div class="product-price">	
				<span class="price">
					$650.99				</span>
										     <span class="price-before-discount">$ 800</span>
									
			</div><!-- /.product-price -->
			
		</div><!-- /.product-info -->
					<div class="cart clearfix animate-effect">
				<div class="action">
					<ul class="list-unstyled">
						<li class="add-cart-button btn-group">
							<button class="btn btn-primary icon" data-toggle="dropdown" type="button">
								<i class="fa fa-shopping-cart"></i>													
							</button>
							<button class="btn btn-primary cart-btn" type="button">Add to cart</button>
													
						</li>
	                   
		                <li class="lnk wishlist">
							<a class="add-to-cart" href="detail.html" title="Wishlist">
								 <i class="icon fa fa-heart"></i>
							</a>
						</li>

						<li class="lnk">
							<a class="add-to-cart" href="detail.html" title="Compare">
							    <i class="fa fa-signal"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.action -->
			</div><!-- /.cart -->
			</div><!-- /.product -->
      
			</div><!-- /.products -->
		</div><!-- /.item -->
			</div><!-- /.home-owl-carousel -->
</section><!-- /.section -->
<!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
			
			</div><!-- /.col -->
			<div class="clearfix"></div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.body-content -->
<script src='<?= base_url() ?>assets/frontend/js/jquery.zoom.js'></script>
<script>
$(document).ready(function(){
	$('#ex1').zoom();
	$('#add_review_btn').on('click', function(e){
		e.preventDefault();
		var form = new FormData();
		form.append('name', $('#rev_name').val());
		form.append('summary', $('#rev_summ').val());
		form.append('rating', $('input[name=rev_rate]:checked').val());
		form.append('product_id', $('#product_id').val());
		form.append('comment', $('#review_comment').val());
		$.ajax({
			url: '<?=site_url();?>Products/add_review',
			processData:false,
			contentType:false,
			cache:false,
			type:"POST",
			data:form,
			beforeSend:function(){
				$('#add_review_btn').attr('disabled', 'disabled').html('<i class="fa fa-spinner fa-pulse fa-fw"></i><span class="sr-only">Loading...</span>Please wait..');
			}, 
			success:function(response){
				if(response.status == true){
					$('#status_rev').html('<div class="override alert alert-success"><i class="fa fa-check"></i> Review added</div>');
					$('#add_review').hide();
				}
			},
			error:function(response){
				var data = $.parseJSON(response.responseText);
				if(data.status == false){
					
					var errors = data.errorMsg;
					$.each(errors, function(key, value){
						$('#'+key+"_err").text(value);
					});
					
					$('#status_rev').prepend('<div class="override alert alert-success"><i class="fa fa-times"></i> Snap! Something went wrong, please try again!</div>');
					$('#add_review_btn').html('<i class="fa fa-times"></i> Try again!');
				}
			},
			complete:function(){
				$('.text-danger').text();
				$('#add_review_btn').removeAttr('disabled').html('<i class="fa fa-check"></i> Review Added');
				//setTimeout($('#status_rev').hide(), 3000);
			}
		});
	})
});
	
</script>
<div id="render-modal">

</div>

<p class="alert alert-info">Var Dump</p>
<style>
<style>
		/* styles unrelated to zoom */
		* { border:0; margin:0; padding:0; }
		p { position:absolute; top:3px; right:28px; color:#555; font:bold 13px/1 sans-serif;}

		/* these styles are for the demo, but are not required for the plugin */
		.zoom {
			display:inline-block;
			position: relative;
		}
		
		/* magnifying glass icon */
		.zoom:after {
			content:'';
			display:block; 
			width:33px; 
			height:33px; 
			position:absolute; 
			top:0;
			right:0;
			background:url(icon.png);
		}

		.zoom img {
			display: block;
		}

		.zoom img::selection { background-color: transparent; }

		#ex2 img:hover { cursor: url(grab.cur), default; }
		#ex2 img:active { cursor: url(grabbed.cur), default; }
	</style>
</style>
<?php echo $layout['footer'];?>