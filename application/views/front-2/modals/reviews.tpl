<!-- Large modal -->
<style>
.review{    margin-bottom: 20px;
    font-family: 'Open Sans', sans-serif, sans-serif;
    text-transform: none;
    background: #f8f8f8;
    padding: 20px;
}
	.product-tabs .tab-content .tab-pane .product-reviews .reviews .review .review-title {
    margin-bottom: 5px;
}
.summary{
	    font-weight: 600;
    font-size: 18px;
}
.product-tabs .tab-content .tab-pane .product-reviews .reviews .review .review-title .summary {
    color: #666666;
    font-size: 14px;
    font-weight: normal;
    margin-right: 10px;
    font-style: italic;
}
.product-tabs .tab-content .tab-pane .product-reviews .reviews .review .review-title .date {
    font-size: 12px;
}
.modal-footer i{
	text-align: center;
    display: block;
    cursor: pointer;
    color: #108bea;
}
.modal-body{
	    max-height: 725px;
    overflow-y: scroll;
    overflow-x: hidden;
}
</style>

<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
	
		<div class="modal-header"> 
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button> <h4 class="modal-title" id="exampleModalLabel">All reviews</h4> 
		</div>
		
		<div class="modal-body"> 
			<div class="reviews">
				<?php print_r($reviews['html']);?>									
			</div>
		</div>
		
		<div class="modal-footer">
			<i id="load_click" onclick="load_more('<?=$reviews['data']['last_id'];?>', '<?=$reviews['data']['product_id'];?>')" class="fa fa-ellipsis-h fa-2x"></i>
			
		</div>
    </div>
	
	<script>
	var load_more = function(last_id, product_id){
		var form = new FormData();
		form.append('lreview_id', last_id);
		form.append('product_id', product_id);
		form.append('option','reviews');
		$.ajax({
			url:'<?=site_url();?>Products/load_modal',
			data: form,
			type: "POST",
			processData:false,
			contentType:false,
			cache:false,
			beforeSend:function(){
				$('.modal-body').append('<div class="loader"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
				$('.modal-body').animate({
					scrollTop: $(".modal-body:last").offset().top},
					'slow');
			},
			success:function(response){
				var load_more = 'load_more('+response.data.last_id+', "'+response.data.product_id+'")';
				$('#load_click').attr('onclick', load_more);
				$('.modal-body').append(response.html);
				$(function () { $('.rating').rateit();});
				
			}, 
			error:function(response){
				var data = $.parseJSON(response.responseText);
				$('#load_click').remove();
				$('.modal-body').append(data.html);

			}, 
			complete:function(){
				$('.loader').remove();
				$('.modal-body').animate({
					scrollTop: $(".modal-body:last").offset().top},
					'slow');
			}
		})
	}
	</script>
  </div>
</div>

