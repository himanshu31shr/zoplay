
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
<!-- Large modal -->
<style>
.review{    margin-bottom: 20px;
    font-family: 'Open Sans', sans-serif, sans-serif;
    text-transform: none;
    background: #f8f8f8;
    padding: 20px;
}
	.product-tabs .tab-content .tab-pane .product-reviews .reviews .review .review-title {
    margin-bottom: 5px;
}
.summary{
	    font-weight: 600;
    font-size: 18px;
}
.product-tabs .tab-content .tab-pane .product-reviews .reviews .review .review-title .summary {
    color: #666666;
    font-size: 14px;
    font-weight: normal;
    margin-right: 10px;
    font-style: italic;
}
.product-tabs .tab-content .tab-pane .product-reviews .reviews .review .review-title .date {
    font-size: 12px;
}
.modal-footer i{
	text-align: center;
    display: block;
    cursor: pointer;
    color: #108bea;
}
.modal-body{
	  max-height: 725px;
    overflow-y: scroll;
    overflow-x: hidden;
    min-height: 700px;
    background: rgba(204, 204, 204, 0.44);
}
ul.reviews{
    margin: 0;
}
ul.reviews li {
	  padding: 2em 0;
    background: #fff;
    margin-bottom: 1em;
}
ul.reviews li :last-child{margin-bottom: 0;}
.scrollbar
{
     background: #F5F5F5;
    margin-bottom: 25px;
}
#style-8::-webkit-scrollbar-track
{

}
#style-8::-webkit-scrollbar
{
    width: 3px;
    background-color: #F5F5F5;
}

#style-8::-webkit-scrollbar-thumb
{
    background-color: #108bea;
}

.pull-times{
    position: relative;
    bottom: 21px;
    right: 9px;
}
a{cursor: pointer;}
</style>
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
	
		<div class="modal-header"> 
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="exampleModalLabel">Your wishlist</h4> 
		</div>
		
		<div class="modal-body scrollbar" id="#style-8"> 
            <div id="status">

            </div>
			<div class="">
				<ul class="reviews row">
					<?php print_r($wishlist['html']);?>
				</ul>
			</div>
		</div>
		
		<!-- <div class="modal-footer">
			<i id="load_click" onclick="load_more('', '')" class="fa fa-ellipsis-h fa-2x"></i>
		</div> -->
    </div>
  </div>
<script>
var remove_wish = function(product_id){
    var form = new FormData();
    form.append('product_id', product_id);
    $.ajax({
        url: '<?=site_url();?>products/wishlist/0',
        data: form,
        processData:false,
        cache:false,
        type:"POST",
        contentType: false,
        beforeSend:function(){
            $('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
        },
        success: function(response){
            var data = response;
            if(data.status == true){
                $('#status').html('<p class="alert alert-success"><i class="fa fa-check"></i> '+data.message+'</p>');
                $('#wish_'+product_id).remove();
            }
        },
        error: function(response){ 
            var data = response.responseText;
            data = $.parseJSON(data);
            if(data.status == false){
                 $('#status').html('<p class="alert alert-warning"><i class="fa fa-times"></i> '+data.message+'</p>');
            }
        },
        complete:function(){
            $('.overlay').remove();
            $('#status').fadeOut(3000);
        }
    });
}    
</script>
</div>