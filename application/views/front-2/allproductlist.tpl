<?php
		echo	$layout['header'];		
?> 
<div class="container">
<?php 
		if($allpro == "new-product")
		{
			$most_recent = $most_recent;
			$productsname = "NEW PRODUCT";
			$procount = count($most_recent);
		}else if($allpro == "featured-product"){
			$most_recent = $featured_products;
			$productsname = "FEATURED PRODUCT";
			$procount = count($featured_products);
		}else if($allpro == "best-seller"){
			$most_recent = $most_viewed;
			$productsname = "BEST SELLER";
			$procount = count($most_viewed);
		}else if($allpro == "most-popular"){
			$most_recent = $most_ordered;
			$productsname = "MOST POPULAR";
			$procount = count($most_ordered);
		}
		else if($allpro == "popular-shops"){			
			$most_recent = $popular_shops;
			$productsname = "POPULAR SHOPS";
			$procount = count($popular_shops);
?>
<div class="search-result-container "> 
          <div id="myTabContent" class="tab-content category-list">		  
		  <div class="more-info-tab clearfix text-center">
           <small> <a href="<?= site_url() ?>" style="color:#555;">Home</a> > <?= $productsname; ?></small></br></br>
			<h2 class="section-title"><?= $productsname; ?> </br></br>
			<small><?= $procount; ?> Items</small>
			</h2>
          </div>		  
            <div class="tab-pane active " id="grid-container">
              <div class="category-product">
                <?php if(empty(($most_recent))){ ?>
                <div class="row" id="load_more">
                  <div class="col-sm-12 wow fadeInUp extrapadd">
                    <p class="text text-center"><i class="fa fa-5x fa-info-circle"></i></p>
                    <h4 class="text text-center"></h4>
                  </div>
                <?php } else {
					echo '<div class="row" id="load_more">';
                  foreach($most_recent as $ps){					
                ?>
				<form id="variations">
                  <div class="col-sm-2 col-md-2 wow fadeInUp height">
                    <div class="products">
						  <div class="product-image">
							<div class="image items"> <a href="<?=site_url();?>shop/<?=$ps->shop_id;?>"><img src="<?=image_url($ps->featured_image);?>" alt=""></a> </div>
						  </div>
						  <!-- /.blog-post-image -->
						  <?php
							  $dtime = new DateTime($ps->updatetime);
							  $timestamp = $dtime->getTimestamp();


						  ?>
						  <div class="blog-post-info text-left">
							<h3 class="name"><a href="<?=site_url();?>shop/<?=$ps->shop_id;?>"><?=$ps->shop_name;?></a></h3>
							<span class="info">By <?=$ps->user_name;?> &nbsp;|&nbsp; <?=time_elapsed_string($timestamp);?> </span>
							<p class="text"><?=$ps->description;?></p>
						  <!-- /.blog-post-info --> 
						  
						</div>
						<!-- /.blog-post --> 
					  </div>                  
                  </div>                  
				</form>
                  <?php } ?>
					</div>
					<input type="hidden" name="last_id" value="<?= $ps->shop_id;?>" />
					<input type="hidden" name="category_id" value="" />
					<div class="row">
						<div class="col-md-2 col-md-offset-5">	
							<a id="load_products" class="btn extra" ><i class="fa fa-ellipsis-h fa-2x"></i></a>
						</div>
					</div>
					<div id="outer">					
					</div>
				<?php } ?>                
              </div>              
             
            </div>
          </div>
        </div>
      </div> 
<?php exit; }  ?>

 <div class="search-result-container"> 
          <div id="myTabContent" class="tab-content category-list">		  
		  <div class="more-info-tab clearfix text-center">
           <small> <a href="<?= site_url() ?>" style="color:#555;">Home</a> > <?= $productsname; ?></small></br></br>
			<h2 class="section-title"><?= $productsname; ?> </br></br>
			<small><?= $procount; ?> Items</small>
			</h2>
          </div>		  
            <div class="tab-pane active " id="grid-container">
              <div class="category-product">
                <?php if(empty(($most_recent))){ ?>
                <div class="row" id="load_more">
                  <div class="col-sm-12 wow fadeInUp extrapadd">
                    <p class="text text-center"><i class="fa fa-5x fa-info-circle"></i></p>
                    <h4 class="text text-center"></h4>
                  </div>
                <?php } else {
					echo '<div class="row" id="load_more">';
                  foreach($most_recent as $p){
					  $url_prod_id = site_url()."product/".md5($p->product_id);
                  if($p->product_image == ''){
                    $image = base_url().'files/default.png';
                  }else if(substr($p->product_image, 0 , 4) == 'http'){
                    $image = $p->product_image;
                  }else{
                    $image = base_url().'files/'.$p->product_image;
                  }
                ?>
				<form id="variations">
                  <div class="col-sm-2 col-md-2 wow fadeInUp height">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image items"> <a href="<?=$url_prod_id;?>">
						  <img src="<?=$image;?>" alt=""></a> </div>                         
						  </div>
                        <div class="product-info text-left">
                          <h3 class="name"><a href="<?=$url_prod_id;?>"><?=substr($p->product_name,0, 80);?></a></h3>
                          <div class="rating rateit-small" data-rateit-value="<?=$p->average_rating == 'NAN' ? 0 : $p->average_rating;?>" ></div>
                          <div class="description"></div>
                          <div class="product-price"> <span class="price"> <?=$p->product_price-$p->product_discount;?></span> <span class="price-before-discount">$<?=$p->product_price;?></span> </div><?php
                            $flag = $p->added_to_wish == '0' ? '1' : '0';
                            $button = $p->added_to_wish == '0' ? 'btn btn-primary' : 'btn btn-info';
                          ?>
                        </div>
					 <div class="row hide">
						<?php 
						$variations = $p->product_variations;
						if(isset($variations['types']) && $variations != ''){
							$all_variations = '';
							foreach($variations['types'] as $v){
						?>
							<div class="col-sm-2 m-t-10">
								<div class="stock-box">
									<span class="label"><?=$v;?> :</span>
								</div>	
							</div>
							<div class="col-sm-9 m-t-10">							
								<div class="btn-group" data-toggle="buttons">
								<?php
								$i=0;
								foreach($variations['options'][$v] as $param){
								?>
								<label class="btn btn-info <?=$i==0 ? 'active' : '';?>" onclick="$('input[name=<?=$v;?>]').removeAttr( 'checked');$('#<?=$i.$v;?>').attr( 'checked', 'checked' );">
									<input type="radio" name="<?=$v;?>" id="option2" value="<?=$param;?>" autocomplete="off" <?=$i==0 ? 'checked' : '';?>>
									<span class="glyphicon glyphicon-check"></span> <?=$param;?>
									<input type="hidden" id="variationtype" name="variationtype" value="<?=$v;?>">
									<input type="radio" id="<?=$i.$v;?>" name="<?=$v;?>" value="<?=$param;?>">
								</label>
								<?php
								$i++;
								}
								$all_variations .= $v."," ;
								echo "</div>";
							echo "</div>";
							}
							?>
							<input type="hidden" value="<?=substr($all_variations, 0, -1);?>" name="all_vars" />
							<?php } ?>
						</div>
						<?php
							$disable_addcart = '';
							$availabiltyto_addcart = $p->product_quantity;
							if($availabiltyto_addcart==0)
							{
								$disable_addcart = 'title="Product Out Of Stock!" disabled';
							}
						?>
						<input type="hidden" name="cart_productid" value="<?php echo $p->product_id;?>">
						<input type="hidden" id="inputquantity" value="1">
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                          <div class="action">
                            <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                <button onclick="add_to_cart('<?php echo $p->product_id;?>')" class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                              </li>
                              <li class="lnk wishlist"> 
                                <a class="add-to-cart" id="inwishlist<?=$p->product_id;?>" onclick="addtowish('<?=$flag;?>','<?=$p->product_id;?>')" > 
                                  <i class="icon fa fa-heart"></i>
                                </a> 
                              </li>
                              <li class="lnk"> <a class="add-to-cart" href="detail.html" title="Compare"> <i class="fa fa-signal"></i> </a> </li>
                            </ul>
                          </div>                       
                        </div>                        
                      </div>
                    </div>                    
                  </div>                  
				</form>
                  <?php } ?>
					</div>
					<input type="hidden" name="last_id" value="<?= $p->product_id;?>" />
					<input type="hidden" name="category_id" value="<?= $p->category_id;?>" />
					<div class="row">
						<div class="col-md-2 col-md-offset-5">	
							<a id="load_products" class="btn extra" ><i class="fa fa-ellipsis-h fa-2x"></i></a>
						</div>
					</div>
					<div id="outer">					
					</div>
				<?php } ?>                
              </div>              
             
            </div>
          </div>
        </div>
      </div> 
      </div> 
<?php	echo $layout['footer']; ?>
<script>
$(document).ready(function(){
	$('#load_products').on('click', function(e){
		e.preventDefault();
		var form = new FormData();
		form.append('last_id', $('input[name=last_id]').val());
		form.append('category_id', $('input[name=category_id]').val());
		form.append('criteria', $('input[name=criteria]').val());
		console.log(form);
		$.ajax({
			url:'<?=site_url();?>/load_products',
			data:form,
			type:"POST",
			processData:false,
			contentType:false,
			cache:false,
			beforeSend:function(){
				$('#load_products').hide();
				$('#outer').html('<div class="loader"><img src="<?=base_url();?>assets/frontend/images/loader.svg" /></div>');
			},
			success:function(response){
				console.log(response);
				if(response.status == true){
					$('#outer').empty();
					$('#load_more').append(response.data);
					$('#load_products').html('<i class="fa fa-ellipsis-h fa-2x"></i>');
					$('input[name=last_id]').val(response.last_id);
					$('#load_products').show();
				}
				
				$(function () { $('.rating').rateit();});
			}, 
			error:function(response){
				var data = $.parseJSON(response.responseText);
				if(data.status == false){
					$('#load_more').append(data.message);
					$('#outer').empty();
				}
			}
		});
	});
});       
</script>
<style>
* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

.items {
  position: relative;
  
  border: 1px solid #333;
  margin: 2%;
  overflow: hidden;
  height: 240px !important;  
}
.items img {
  max-width: 100%;
  height: 100%;
  -moz-transition: all 0.3s;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
}
.items:hover img {
  -moz-transform: scale(1.1);
  -webkit-transform: scale(1.1);
  transform: scale(1.1);
}
</style>