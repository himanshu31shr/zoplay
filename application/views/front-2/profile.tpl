<?php echo $layout['header'];?> 
<div class="body-content outer-top-xs">
    <div class="container">
<?php
    if($this->session->flashdata('alerts') !=''){
        $data = $this->session->flashdata('alerts');
        echo "<p class='alert alert-danger'>".$data['message']."</p>";
    }

?>
        <div class="row single-product">
            <!-- /.sidebar -->
            <div class="col-md-12">
                <div class="product-tabs inner-bottom-xs  wow fadeInUp animated maxheight" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="row">
                        <div class="col-sm-3">
                            <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                <li class="active">
                                    <a data-toggle="tab" href="#personalDetail">Personal Information</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#ChangePassword">Change password</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#address">Address</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#orders">Orders</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#transactions">Transactions</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#update_email">Update Email</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#" onclick="see_more('wishlist', '', 'wishlist')">My Wishlist</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#deactivate_account">Deactivate Account</a>
                                </li>
                            </ul>
                            <!-- /.nav-tabs #product-tabs -->
                        </div>
                        <div class="col-sm-9">
                        
                            <div class="tab-content">
                                <div id="personalDetail" class="tab-pane active">
                                    <form class="form-horizontal" id="basic_info" data-parsley-focus="first" method="POST">
                                        <div id="update_alert"></div>
                                        <fieldset>
                                            <!-- Form Name -->
                                            <legend>Personal details</legend>
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="textinput">Full Name</label>
                                                <div class="col-md-4">
                                                    <input id="textinput" name="full_name" value="<?=$user->full_name;?>" required minlength="4" type="text" placeholder="" class="form-control input-md">
                                                    <p id="full_name_err"></p>
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <!-- Multiple Radios (inline) -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="radios">Gender</label>
                                                <div class="col-md-4">
                                                    <label class="radio-inline" for="radios-0">
                                                        <input type="radio" name="gender" id="radios-0" value="1" <?=$user->gender == '1' ? 'checked': '' ;?> > Male

                                                    </label>
                                                    <label class="radio-inline" for="radios-1">
                                                        <input type="radio" name="gender" id="radios-1" value="0" <?=$user->gender == '0' ? 'checked': '' ;?>> Female

                                                    </label>
                                                </div>
                                                <p id="gender_err"></p>
                                            </div>
                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="selectbasic">Country</label>
                                                <div class="col-md-4">
                                                    <select id="selectbasic" name="country" required class="form-control">
                                                         <?php  echo getCountries(trim($user->country)); ?>
                                                    </select>
                                                    <p id="country_err"></p>
                                                </div>
                                            </div>
                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="control-label col-md-4" for="registration-date">Date of birth:</label>
                                                <div class="col-md-4">
                                                    <div class="input-group registration-date-time">
                                                        <span class="input-group-addon" id="basic-addon1">
															<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                                        </span>
                                                        <input class="form-control" name="dob" required id="dob" value="<?=$user->dob;?>" data-provide="datepicker">
                                                    </div>
                                                    <p id="dob_err"></p>
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="textinput">City</label>
                                                <div class="col-md-4">
                                                    <input id="textinput" name="city" required type="text" placeholder="" value="<?=$user->city;?>" class="form-control input-md">
                                                    <p id="city_err"></p>
                                                </div>
                                            </div>
                                           
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="textinput">Phone</label>
                                                <div class="col-md-4">
                                                    <input id="textinput" name="contact" minlength="10" maxlength="10"  required type="number" placeholder="" class="form-control input-md" value="<?=$user->contact;?>">
                                                    <p id="contact_err"></p>
                                                </div>
                                            </div>


                                            <!-- Button -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="singlebutton"></label>
                                                <div class="col-md-4">
                                                    <button id="singlebutton" name="sbnupdate" class="btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                                <div id="ChangePassword" class="tab-pane">
                                    <form class="form-horizontal" id="update_password">
                                        <fieldset>
                                            <!-- Form Name -->
                                            <legend>Change Password</legend>
                                            <!-- Password input-->
                                            <div id="updpwd_alert"></div>
                                            <?php
                                                if($user->is_password_set == 1){
                                            ?>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="piCurrPass">Old Password</label>
                                                <div class="col-md-4">
                                                    <input id="piCurrPass" name="opwd" type="password" placeholder="Your old password" class="form-control input-md" >
                                                    <p id="opwd_err"></p>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <!-- Password input-->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="piNewPass">New Password</label>
                                                <div class="col-md-4">
                                                    <input id="piNewPass" name="npwd" type="password" placeholder="Your new password" class="form-control input-md" >
                                                    <p id="npwd_err"></p>
                                                </div>
                                            </div>
                                            <!-- Password input-->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="piNewPassRepeat">Confirm Password</label>
                                                <div class="col-md-4">
                                                    <input id="piNewPassRepeat" name="rnpwd" type="password" placeholder="Repeat new password" class="form-control input-md" >
                                                    <p id="rnpwd_err"></p>
                                                </div>
                                            </div>
                                            <!-- Button (Double) -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="bCancel"></label>
                                                <div class="col-md-8">
                                                    <button id="bGodkend" name="bGodkend" class="btn btn-success">Save</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>

                                <!-- /.tab-pane -->
                                <div id="address" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form class="form-horizontal" id="add_address">
                                                <fieldset>
                                                    <!-- Form Name -->
                                                    <legend>Address</legend>
                                                    <div id="address_alrt"></div>
                                                    <!-- Password input-->
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="piCurrPass">Full Name</label>
                                                        <div class="col-md-4">
                                                            <input id="piCurrPass" name="name" type="text" placeholder="" class="form-control input-md" parsley-trigger="change">
                                                            <p id="name_err"></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="piCurrPass">Phone</label>
                                                        <div class="col-md-4">
                                                            <input id="piCurrPass" name="phone" type="text" placeholder="" class="form-control input-md" >
                                                            <p id="phone_err"></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="piNewPass">Country</label>
                                                        <div class="col-md-4">
                                                            <select class="countries_select form-control" name="country" onchange="initAutocomplete(this);" id="selectshipcountry">
                                                                 <?php  echo getCountries('','','shipping'); ?>
                                                            </select>
                                                            <p id="country_err"></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="piNewPass">City/Town</label>
                                                        <div class="col-md-4">
                                                            <input id="autocomplete" placeholder=""
             onFocus="geolocate()" class="form-control input-md" type="text" name="city">
                                                            <p id="city_err"></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="piNewPass">Zip / Postal Code

                                                        </label>
                                                        <div class="col-md-4">
                                                            <input id="postal_code" name="zip" type="text" placeholder="" class="form-control input-md" >
                                                            <p id="zip_err"></p>
                                                        </div>
                                                    </div>
                                                    <!-- Password input-->
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="piNewPass">Address Line 1</label>
                                                        <div class="col-md-4">
                                                            <input id="piNewPass" name="line_1" type="text" placeholder="" class="form-control input-md" >
                                                            <p id="line_1_err"></p>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="piNewPass">Address Line 2</label>
                                                        <div class="col-md-4">
                                                            <input id="piNewPass" name="line_2" type="text" placeholder="" class="form-control input-md" >
                                                            <p id="line_2_err"></p>
                                                        </div>
                                                    </div>
													<div>
														<input type="hidden" name="city" id="locality">
														<input type="hidden" name="state" id="administrative_area_level_1">
														
													</div>
                                                    <!--div class="form-group">
                                                        <label class="col-md-4 control-label" for="piNewPass">State / Province / Region
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input id="piNewPass" name="state" type="text" placeholder="" class="form-control input-md" >
                                                            <p id="state_err"></p>
                                                        </div>
                                                    </div-->
                                                    <!-- Button (Double) -->
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="bCancel"></label>
                                                        <div class="col-md-8">
                                                            <input id="id" name="id" type="hidden" >
                                                            <p id="zip_err"></p>
                                                            <button id="address_btn" name="bGodkend" data-update="0" class="btn btn-primary">Save</button>
                                                            <button type="reset" id="add_btn_rst" class="btn btn-info">Add new</button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
											<input type="hidden" name="orderedgcn" id="country">
											<input type="hidden" name="orderedstreet" id="street_number">
											<input type="hidden" name="orderedroute" id="route">
                                        </div>
                                        <script type="text/javascript">
                                            
                                        </script>
                                        <div class="col-md-4 saved_address " >
                                            <legend>Saved Addresses</legend>
                                            <ul class="scrollbar" id="style-8">
                                            <?php print_r($user->all_address);?>
                                            </ul>
                                            <a class="btn btn-outline btn-block text text-center" id="adr_loadmre" onclick="load_more_address(<?=$user->last_address_id;?>)"><i class="fa fa-ellipsis-h"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="update_email" class="tab-pane">
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <!-- Form Name -->
                                            <legend>Update Email</legend>
                                            <!-- Password input-->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="piCurrPass">Email Id</label>
                                                <div class="col-md-4">
                                                    <input id="upd_email" value="<?=$user->email;?>" name="upd_email" type="text" placeholder="" class="form-control input-md" required="">
                                                    <p id="upd_email_err"></p>
                                                </div>
                                            </div>
                                            <!-- Button (Double) -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="bCancel"></label>
                                                <div class="col-md-8">
                                                    <button id="bGodkend" name="bGodkend" class="btn btn-success">Update</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                    
                                </div>
                                <!-- /.tab-pane -->
                                <div id="orders" class="tab-pane">
                                   <div class="row">
                                        <div class="col-md-12">
										<span class="text-center text-danger" id="invoiceerror"></span>
                                         <legend>All orders</legend>
                                        <table class="dataTable stripe">
                                            <thead>
                                                <tr>
                                                    <th>Order#</th>
                                                    <th>Date</th>
                                                    <th>Product</th>
                                                    <th>Shipping Status</th>
                                                    <th>Payment Status</th>
                                                    <th>Price</th>
                                                    <th>Paid by</th>
                                                    <th>Invoice</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                <?php
                                                if($user->orders){
													foreach($user->orders as $orderlist)
													{
														if($orderlist->order_shipping_status==0)
														{
															$ordershippingstats ='Pending';
														}elseif($orderlist->order_shipping_status==1)
														{
															$ordershippingstats ='Processed';
														}elseif($orderlist->order_shipping_status==2)
														{
															$ordershippingstats ='Shipped';
														}elseif($orderlist->order_shipping_status==3)
														{
															$ordershippingstats ='Delivered';
														}elseif($orderlist->order_shipping_status==4)
														{
															$ordershippingstats ='Cancelled';
														}elseif($orderlist->order_shipping_status==5)
														{
															$ordershippingstats ='Refund';
														}elseif($orderlist->order_shipping_status==6)
														{
															$ordershippingstats ='Reshipment';
														}
														
																																									if($orderlist->order_payment_status==0)
														{
															$orderpaymntstats ='Pending';
														}elseif($orderlist->order_payment_status==1)
														{
															$orderpaymntstats ='Paid';
														}
                                                ?>
												<tr>
                                                    <td><a><?php echo $orderlist->order_number;?></a></td>
                                                    <td><?php echo $orderlist->order_added;?></td>
                                                    <td>Product Name<?php //echo substr($orderlist->product_name,0,25);?></td>
                                                    <td><?php echo $ordershippingstats;?></td>
                                                    <td><?php echo $orderpaymntstats;?></td>
													<td>$<?php echo $orderlist->total_price;?></td>
                                                    <td><?php echo $orderlist->payment_type == 0 ? 'Cash on Delivery' : 'Paypal';?></td>
													<td><a href="<?php echo site_url('User/invoice/'.$orderlist->order_id);?>" class="btn btn-success btn-sm">Invoice</a></td>
												</tr>
                                                <?php
													}
												}else{
                                                 echo "<tr><td align='center' colspan='6'>No orders yet!</td></tr>";
                                                }
                                                ?>
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>

                                <div id="transactions" class="tab-pane">
                                   <div class="row">
                                        <div class="col-md-12">
                                         <legend>All transactions</legend>
                                        <table class="dataTable stripe">
                                            <thead>
                                                <tr>
                                                    <th>Transaction#</th>
                                                    <th>Date</th>
                                                    <th>Product</th>
                                                    <th>Qty</th>
                                                    <th>Price</th>
                                                    <th>Paid by</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                <?php
                                                if($user->transactions){
                                                ?>
                                                    <th>Transaction#</th>
                                                    <th>Date</th>
                                                    <th>Product</th>
                                                    <th>Qty</th>
                                                    <th>Price</th>
                                                    <th>Paid by</th>
                                                <?php
                                                }else{
                                                 echo "<td align='center' colspan='6'>No transactions yet!</td>";
                                                }
                                                ?>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>

                                <div id="deactivate_account" class="tab-pane">
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <!-- Form Name -->
                                            <legend>Deactivate Account</legend>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="bCancel"></label>
                                                <div class="col-md-8">
                                                    <a href="<?=site_url();?>user/deactivate" id="bCancel" name="bCancel" class="btn btn-danger">Deactivate Account</a>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.product-tabs -->
            </div>
            <!-- /.col -->
            <div class="clearfix"></div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
   <script type="text/javascript" src="<?=base_url();?>assets/frontend/plugins/parsley/parsley.min.js"></script>
   <script src="<?php echo base_url() ; ?>assets/frontend/js/plugins/select2/select2.full.min.js"></script>
   <script src="<?php echo base_url() ; ?>assets/frontend/js/plugins/datapicker/bootstrap-datepicker.js"></script>


   <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/frontend/plugins/parsley/parsley.css" />
   <link  rel="stylesheet" type="text/css" href="<?php echo base_url() ; ?>assets/frontend/css/plugins/datapicker/datepicker3.css">

   <link href="<?php echo base_url() ; ?>assets/frontend/css/plugins/select2/select2.min.css" rel="stylesheet" />
   <script type="text/javascript">
        $('#update_email').on('submit', function(e){
            e.preventDefault()
            var form = new FormData();
            form.append('upd_email', $('#upd_email').val());
            $.ajax({
                url: '<?=site_url();?>user/update_email',
                data: form,
                type:"POST",
                processData:false,
                cache:false,
                contentType:false,
                beforeSend:function(){
                    $('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
                },
                success:function(response){
                    $('#upd_email_err');
                    if(response.status == true){
                        swal({
                            title: "Enter the verification code!",
                            text: "A verification code has been sent to your email. Please check, and enter the text in the input box below.",
                            type: "input",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            animation: "fade-in",
                            inputPlaceholder: "Paste code here!",
                            showLoaderOnConfirm: true,
                        },
                        function(inputValue){
                            if (inputValue === false) return false;
                          
                            if (inputValue === "") {
                                swal.showInputError("You need to write something!");
                                return false
                            }

                            form.append('token', inputValue);
                            $.ajax({
                                url:'<?=site_url();?>user/update_email/update',
                                data: form,
                                type:"POST",
                                processData:false,
                                cache:false,
                                contentType:false,
                                success:function(response){
                                    if(response.status = true){
                                         swal("Email verified!");
                                    }
                                },
                                error: function(response){
                                    var data = $.parseJSON(response.responseText);
                                    if(data.status == false){
                                        swal("Invalid token!");
                                    }
                                }
                            });
                        });
                    }else{
                        swal('Something went wrong. Please try again later!');
                    }
                }, 
                error:function(response){
                    var data = response.responseText;
                    data = $.parseJSON(data);
                    console.log(data.data.upd_email);
                    if(data.status == false){
                        $('#upd_email_err').html(data.data.upd_email);
                    }
                },
                complete:function(){
                    $('.overlay').remove();
                }
            });
        })
	   var render = function(address_id){
		$.ajax({
			url:'<?=site_url();?>user/get_single_address/'+address_id,
			type:"POST",
			processData:false,
			contentType:false,
			cache:false,
			beforeSend: function(){
				$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success: function(response){
				if(response.status ==  true){
					$.each(response.data, function(key, value){
						var key = key.replace("address_", "");
						if(key == 'country'){
							$('select[name=country]').select2().val(value);
						}
						$('input[name='+key+']').val(value);
					});
					$('#address_btn').html('Update address').attr('data-update', '1'); 
				}
			},
			error: function(response){
				var data = $.parseJSON(response.responseText);
				if(data.status ==  false){
					$('#address_alrt').html('<p class="alert alert-danger">'+ data.message+'</p>').show();
				}
				setTimeout($('#address_alrt').fadeOut(10000));
			},
			complete:function(){
				$('.overlay').remove();
			}
		});
	}

        $('#add_btn_rst').on('click', function(e){
            e.preventDefault();
            $(this).closest('form').find("input, textarea, select").val("");
            $('#address_btn').attr('data-update', 0).text('Save');

        });

        var load_more_address = function(address_id){
             $.ajax({
                url:'<?=site_url();?>user/get_address/null/2/'+address_id,
                type:"POST",
                cache:false,
                beforeSend: function(){
                    $('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
                },
                success: function(response){
                    if(response.status ==  true){
                        $('.saved_address ul').append(response.html);
                        $('#adr_loadmre').attr('onclick', 'load_more_address('+response.last_id+')');
                    }
                },
                error: function(response){
                    var data = $.parseJSON(response.responseText);
                    if(data.status ==  false){
                        $('.saved_address ul ').append(data.html);
                    }
                    console.log(data);
                },
                complete:function(){
                    $('.overlay').remove();
                }
            });
        }

        var delete_address = function(address_id){
            $.ajax({
                url:'<?=site_url();?>user/delete_address/'+address_id,
                type:"POST",
                cache:false,
                beforeSend: function(){
                    $('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
                },
                success: function(response){
                    if(response.status ==  true){
                        $('#prod_'+address_id).remove();
                        $('address_alrt').text('<p class="alert alert-success"><i class="fa fa-check"></i> Address deleted</p>');
                    }
                },
                error: function(response){
                    $('address_alrt').text('<p class="alert alert-danger"><i class="fa fa-times"></i> Something went wrong!</p>');

                },
                complete:function(){
                    $('.overlay').remove();
                    setTimeout($('#address_alrt').fadeOut(10000));
                }
            });
        }
        

        $('#basic_info').parsley().on('form:submit', function() { return false;}).on('form:success', function(){

            var form  = new FormData($('#basic_info')[0]);
            $.ajax({
                url:'<?=site_url();?>user/update_profile',
                type:"POST",
                data:form,
                processData:false,
                contentType:false,
                cache:false,
                beforeSend: function(){
                    $('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
                },
                success: function(response){
                    if(response.status ==  true){
                        $('#update_alert').html('<p class="alert alert-success">'+ response.message+'</p>');
                    }
                    setTimeout($('#update_alert').fadeOut(3000), 3000);
                },
                error: function(response){
                    var data = $.parseJSON(response.responseText);
                    $.each(data.data, function(key, value){
                        $('#'+key+'_err').html(value);
                    });
                    if(data.status ==  false){
                        $('#update_alert').html('<p class="alert alert-danger">'+ data.message+'</p>');
                    }
                    setTimeout($('#update_alert').fadeOut(3000), 3000);
                },
                complete:function(){
                     $('.overlay').remove();
                }
            });
        });
    
        $('#update_password').on('submit', function(e){
            e.preventDefault();
            var form = new FormData(this);
             $.ajax({
                url:'<?=site_url();?>user/change_password',
                type:"POST",
                data:form,
                processData:false,
                contentType:false,
                cache:false,
                beforeSend: function(){
                    $('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
                },
                success: function(response){
                    if(response.status ==  true){
                        $('#updpwd_alert').html('<p class="alert alert-success">'+ response.message+'</p>').show();
                    }
                    setTimeout($('#updpwd_alert').fadeOut(10000));
                    $('.error').html();
                },
                error: function(response){
                    var data = $.parseJSON(response.responseText);
                    if(data.data != ''){
                        $.each(data.data, function(key, value){
                            $('#'+key+'_err').html(value);
                        });
                    }

                    if(data.status ==  false){
                        $('#updpwd_alert').html('<p class="alert alert-danger">'+ data.message+'</p>').show();
                    }

                    setTimeout($('#updpwd_alert').fadeOut(10000));
                },
                complete:function(){
                    $('.overlay').remove();
                }
            });
        });

        $('#add_address').on('submit', function(e){
            e.preventDefault();
            var form = new FormData(this);
            var update = $('#address_btn').attr('data-update');
            $.ajax({
                url:'<?=site_url();?>user/add_address/'+update,
                type:"POST",
                data:form,
                processData:false,
                contentType:false,
                cache:false,
                beforeSend: function(){
                    $('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
                },
                success: function(response){
                    if(response.status ==  true){
                        $('#address_alrt').html('<p class="alert alert-success">'+ response.message+'</p>').show();
                        $('.error').empty();    
                    }
                    setTimeout($('#address_alrt').fadeOut(10000));
                },
                error: function(response){
                    var data = $.parseJSON(response.responseText);
                    if(data.data != ''){
                        $.each(data.data, function(key, value){
                            $('#'+key+'_err').html(value);
                        });
                    }
                    if(data.status ==  false){
                        $('#address_alrt').html('<p class="alert alert-danger">'+ data.message+'</p>').show();
                    }
                    setTimeout($('#address_alrt').fadeOut(10000));
                },
                complete:function(){
                    $('.overlay').remove();
                }
            });
        });
        $("#selectbasic").select2();
        $(".countries_select").select2();

        $('.datepicker').datepicker('update', '<?=$user->dob == "" ? 0 : $user->dob;?>');

        </script>
		
		<!--carete order invoice-->
		<script type="text/javascript">
			$(document).ready(function() { 	
				$('#createinvoice').click(function(e){	
					e.preventDefault();
					var orderid = $(this).attr('data-id');
					var formdata = new FormData();
					formdata.append('orderid', orderid);
					//console.log(checkout_data);
					$.ajax({
					url: '<?=site_url();?>User/invoice',
					data: formdata,
					type:"POST",
					contentType: false,
					processData:false,
					cache: false,
					beforeSend:function(){
							$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
						},
					success: function(response){
						var data = response;
						console.log(data);
						if(data.status == true){
							$('.text-danger').empty();
						}
					},
					error: function(response){ 
						var data = response.responseText;
						data = $.parseJSON(data);
						if(data.status == false){
							data = data.data;
							$('#invoiceerror').html('<p class="alert alert-danger">'+response.message+'<p>');
						}
					},
					complete:function(){
					$('.overlay').remove();
					}
				});
				});
			});
		</script>
		<!--For autocomplete shipping address-->
		<script>
		  // This example displays an address form, using the autocomplete feature
		  // of the Google Places API to help users fill in the information.

		  // This example requires the Places library. Include the libraries=places
		  // parameter when you first load the API. For example:
		  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

		  var placeSearch, autocomplete;
		  var componentForm = {
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'short_name',
			country: 'long_name',
			postal_code: 'short_name'
		  };

		  function initAutocomplete() {
			// Create the autocomplete object, restricting the search to geographical
			// location types.
			autocomplete = new google.maps.places.Autocomplete(
				/** @type {!HTMLInputElement} */
				(document.getElementById('autocomplete')),
				{
				types: ['(cities)'],
				componentRestrictions: {country: $('#selectshipcountry').val()}
				
				});

			// When the user selects an address from the dropdown, populate the address
			// fields in the form.
			autocomplete.addListener('place_changed', fillInAddress);
		  }

		  function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();

			for (var component in componentForm) {
			  document.getElementById(component).value = '';
			  document.getElementById(component).disabled = false;
			}

			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			for (var i = 0; i < place.address_components.length; i++) {
			  var addressType = place.address_components[i].types[0];
			  if (componentForm[addressType]) {
				var val = place.address_components[i][componentForm[addressType]];
				document.getElementById(addressType).value = val;
			  }
			}
		  }

		  // Bias the autocomplete object to the user's geographical location,
		  // as supplied by the browser's 'navigator.geolocation' object.
		  function geolocate() {
			if (navigator.geolocation) {
			  navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
				  lat: position.coords.latitude,
				  lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
				  center: geolocation,
				  radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			  });
			}
		  }
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDv5IBJrYFB-S06EOdq6ugqNxSahQnDnh4&libraries=places&callback=initAutocomplete"
			async defer></script>
		<!--eND autocomplete shipping address-->
<pre>
    <?php //print_r($user) ;?>
</pre>

<?php
		echo	$layout['footer'];
?>