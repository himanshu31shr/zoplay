<?php echo $layout['header'];?> 
<?php 
	if(isset($erroralert))
	{?>
	<script>
		swal({
		  title: 'Dear Member',
		  text: 'We are sorry! Your last transaction was cancelled.',
		  html: true,
		  timer: 3000,
		  type: "error",
		  showConfirmButton: false
		});
	</script>
	<?php }
?>
<div class="text-center col-md-12">
	<h3 style="font-family: 'quicksandbold'; font-size:16px; color:#313131; padding-bottom:8px;">Dear Member</h3>
    <span style="color:#D70000; font-size:16px; font-weight:bold;">We are sorry! Your last transaction was cancelled.</span>
</div>

<?php
		echo	$layout['footer'];
?>