<?php
	echo $layout['header'];
?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="home.html">Home</a></li>
				<li class='active'>Compare</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content outer-top-xs">
	<div class="container">
    <div class="product-comparison">
		<div>
			<h1 class="page-title text-center heading-title">Product Comparison</h1>
			<div class="table-responsive">
			<?php if(!empty($productlist)) { ?>
				<table class="table compare-table inner-top-vs">
					<tr>
						<th>Products</th>
						<?php foreach($productlist as $key=>$plist) { ?>
							<td>
								<div class="product">
									<div class="product-image">
										<div class="image">
											<a href="#">
												<img alt="" style="height: 180px;" src="<?= $plist->product_image; ?>">
											</a>
										</div>

										<div class="product-info text-left">
											<h3 class="name">
												<a href="#" data-toggle="tooltip" data-placement="right" title="<?= $plist->product_name; ?>"><?= substr($plist->product_name,0,50); ?></a>
											</h3>
											<div class="action">
												<a class="lnk btn btn-primary" href="#">Add To Cart</a><?php if($key < 3) { ?>									
													<a class="lnk btn btn-primary" href="<?= site_url()?>information/detail">Add More</a>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</td>
						<?php } ?>					
					</tr>
					<tr>
						<th>Price</th>
						<?php foreach($productlist as $plist) { ?>
							<td>
								<div class="product-price">
									<span class="price"><?= $plist->product_price; ?></span>
									<span class="price-before-discount">$<?= ltrim($plist->product_price ,"$") + ltrim($plist->product_discount,"$"); ?></span>
								</div>
							</td>	
						<?php } ?>	
					</tr>
					<tr>
						<th>Description</th>
						<?php foreach($productlist as $plist) { ?>
							<td>
								<p class="text" data-toggle="tooltip" data-placement="right" title="<?= $plist->product_description; ?>">
									<?= substr($plist->product_description , 0 ,100); ?>
								</p>								
							</td>				
						<?php } ?>	
					</tr>
					<tr>
						 <th>Availability</th>
						 <?php foreach($productlist as $plist) { ?>
							<td><p class="in-stock"><?= ($plist->product_quantity > 0) ? "In Stock" : "Out Of Stock"  ?></p></td>	                     
						 <?php } ?>
					</tr>
					<tr >
						<th>Remove</th>
						<?php foreach($productlist as $plist) { ?>
							<td class='text-center'>
								<a href="<?= site_url() ?>Products/remove_product/<?= $plist->product_id; ?>" class="remove-icon"><i class="fa fa-times"></i></a>
							</td>
						<?php } ?>
					</tr>
				</table>
			<?php }else{ ?>
				<span>No Record Found</span>
			<?php } ?>
			</div>
            </div>
		</div>
	</div>
</div>

<?php
	echo $layout['footer'];
?>