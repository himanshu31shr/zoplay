<?php
		echo	$layout['header'];
?>
<?php 
	$myproduct_count = $carthtml['myproduct_count'];
	$mygrand_total = $carthtml['mygrand_total'];
	$mycart_products = $carthtml['mycart_products'];
?>

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="checkout.html#">Home</a></li>
				<li class='active'>Checkout</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="checkout-box ">
			<div class="row">
				<div class="col-md-12">
					<div class="panel-group checkout-steps" id="accordion">
						<!-- checkout-step-01  -->
<div class="panel panel-default checkout-step-01">

	<!-- panel-heading -->
		<div class="panel-heading">
    	<h4 id="mdheading1" class="unicase-checkout-title">
	        <a data-toggle="collapse" id="mdfirststep" class="" data-parent="#accordion" href="checkout.html#collapseOne">
	          <span id="mdcheckicon1">1</span>Before You Place Your Order > Sign in
	        </a>
	     </h4>
    </div>
    <!-- panel-heading -->

	<div id="collapseOne" class="panel-collapse collapse in">

		<!-- panel-body  -->
	    <div class="panel-body">
			<div class="row">		

				<!-- guest-login -->
				<div  id="logedinmessage">
				<?php if(!isset($checklogin))
				{ ?>
				<div class="col-md-6 col-sm-6 guest-login">
					
					<h4 class="checkout-subtitle outer-top-vs">Register and save time</h4>
					<p class="text title-tag-line ">Register with us for future convenience:</p>
					
					<ul class="text instruction inner-bottom-30">
						<li class="save-time-reg">- Fast and easy check out</li>
						<li>- Easy access to your order history and status</li>
					</ul>
					<div class="col-md-12 mt-20">
					<div class="col-md-6">
					<label class="info-title" for="exampleInputPassword1">New Registration </label>
					</br>
					<button type="submit" id="register" class="btn-upper btn btn-primary checkout-page-button checkout-continue">Register</button>
					</div>
					<div class="col-md-6">
					<label class="info-title" for="exampleInputPassword1">Already registered? </label>
					</br>
					<button type="submit" id="login" class="btn-upper btn btn-primary checkout-page-button checkout-continue ">Login</button>
					</div>
					</div>
				</div>
				

				<!-- already-registered-login -->
				<div class="col-md-6 col-sm-6 already-registered-login">
					<h4 class="unicase-checkout-title">Already registered?</h4>
					<p class="text title-tag-line">Please log in below:</p>
					<form class="register-form outer-top-xs" role="form" id="login_formcheckout">
						<div id="alert"></div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1"><b>Email Address </b><span>*</span></label>
							<input type="email" class="form-control unicase-form-control text-input" name="emaill" id="email" >
							<p class="text text-danger" id="emaill_err"></p>
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputPassword1"><b>Password </b><span>*</span></label>
							<input type="password" class="form-control unicase-form-control text-input" name="passwordl" id="password" >
							<p class="text text-danger" id="passwordl_err"></p>
						</div>
						<div class="radio outer-xs">
							<button type="submit" id="loginbtn" class="btn-upper btn btn-primary checkout-page-button">Login</button>
							<a href="" class="forgot-password pull-right">Forgot your Password?</a>
						</div>
						
					</form>
					<script>
						$(document).ready(function(){
							$('#loginbtn').on('click', function(e){
								e.preventDefault();
								var form = new FormData($('#login_formcheckout')[0]);
								$.ajax({
									url:'<?=site_url();?>/user/log_user',
									data: form,
									type:"POST",
									processData:false,
									contentType: false,
									cache:false,
									beforeSend:function(){
										$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
									},
									success:function(response){
										var data = response;
										console.log(data);
										//var data = $.parseJSON(data);
										if(data.status == true){
											$('#alert').html('<p class="alert alert-success">'+data.message+'</p>');
											$('#loginbtn').html('<i class="fa fa-check"></i> Logged In!');
											$('#logedinmessage').html('<p><h4 class="text-success text-center">You Are Logged In !</h4></p>');
											
											$('#collapseOne').collapse('hide');
											$('.checkout-step-02').removeClass('hide');
											$('#mdsecondstep').click();
											$('#mdheading1').css('background', '#dff0d8');
											$( "#mdcheckicon1" ).html( "<i class='fa fa-check text-right'><i>" );
										}
									},
									error:function(response){
										var data = response.responseText;
										var data = $.parseJSON(data);
										if(data.status == false){
											$('#alert').html('<p class="alert alert-danger">'+data.message+'</p>');
											var err = data.errorMsg;
											$.each(err, function(key, value){
												$('#'+key+'l_err').text(value);
											});
										}
									}, 
									complete:function(){
										$('.overlay').remove();
									}
								})
							})
						});
					</script>
				</div>	
					
				
				<!-- new-registration -->
				<div class="col-md-6 col-sm-6 create-new-account hide">

					<form class="register-form outer-top-xs" id="reg_formcheckout" role="form">
						<div id="status">
										
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail2">Email Address <span>*</span></label>
							<input type="email" name="email" class="form-control unicase-form-control text-input" id="exampleInputEmail2" >
							<div class="text-danger" id="email_error" ></div>
							<div class="text-danger" id="is_unique_error" ></div>
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Name <span>*</span></label>
							<input type="text" name="username" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
							<div class="text-danger" id="username_error" ></div>
						</div>
						<div class="form-group">
							<label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
							<input type="text" name="phone" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
							<div class="text-danger" id="phone_error" ></div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label class="info-title" for="exampleInputEmail1">Password <span>*</span></label>
									<input type="password" name="password" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
									<div class="text-danger" id="password_error" ></div>
								</div>
								<div class="col-md-6">
									<label class="info-title" for="exampleInputEmail1">Confirm Password <span>*</span></label>
									<input type="password" name="passconf" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
									<div class="text-danger" id="passconf_error" ></div>
								</div>
							</div>
						</div>
						</hr>
						
						<button type="submit" id="click_form" class="btn-upper btn btn-primary mt-20 checkout-page-button">Sign Up</button>
						
					</form>
					<script type="text/javascript">
						$(document).ready(function() {   
							$('#click_form').click(function(e){
								e.preventDefault();
								var formdata = new FormData($('#reg_formcheckout')[0]);
								//console.log(reg_form);
								$.ajax({
								url: '<?=site_url();?>User/register',
								data: formdata,
								type:"POST",
								contentType: false,
								processData:false,
								cache: false,
								beforeSend:function(){ 
										$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
									},
								success: function(response){
									var data = response;
									if(data.status == true){
										$('#status').html('<p class="alert alert-success">'+response.message+'<p>');
										$('.text-danger').empty();
										$('#click_form').removeAttr('disabled').html('<i class="fa fa-check"></i> Registration Successful!</p>');
										$('#reg_formcheckout')[0].reset();
										$('.error').html();
									}
								},
								error: function(response){  
									var data = response.responseText;
									data = $.parseJSON(data);
									if(data.status == false){
										data = data.data;
										$.each(data, function(key, value){
											$('#'+key+'_error').empty().html(value);
										});
										$('#click_form').removeAttr('disabled').html('<i class="fa fa-times"></i> Try again!');
									}
								}, 
								complete:function(){
									$('.overlay').remove();
								}
							});
							});
						});
					</script>

				</div>
				<?php
				} else {
					echo '<p><h4 class="text-success text-center">You Are Logged In !</h4></p>';
				}?>
				</div>
			</div>			
		</div>
		<!-- panel-body  -->

	</div><!-- row -->
</div>
<!-- checkout-step-01  -->
					    <!-- checkout-step-02  -->
					  	<div class="panel panel-default checkout-step-02 hide">
						    <div class="panel-heading">
						      <h4 id="mdheading2" class="unicase-checkout-title">
						        <a data-toggle="collapse" id="mdsecondstep" class="collapsed" data-parent="#accordion" href="checkout.html#collapseTwo">
						          <span id="mdcheckicon2">2</span> Delivery Address
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwo" class="panel-collapse collapse">
						      <div class="panel-body">
							  
								<div class="col-md-4 col-sm-6 saved_address" style="width:100%;" id="mdaddressul">
									<legend>Saved Addresses</legend>
									<ul class="scrollbar" id="style-8">
									<?php print_r($useraddress);?>
									</ul>
									<a class="btn btn-outline btn-block text text-center" id="adr_loadmre" onclick="load_more_address(<?=$userlastaddressid;?>)"><i class="fa fa-ellipsis-h"></i></a>
									
								</div>
								
								<div class="col-md-8 col-sm-6 hide" id="newaddressdiv">
								
								<legend>Add New Address</legend>
								<form class="register-form outer-top-xs" id="address_form" role="form">
									<div id="addressstatus">
													
									</div>
									<div class="form-group">
										<label class="info-title" for="exampleInputEmail2">Name <span>*</span></label>
										<input type="text" name="orderedusername" class="form-control unicase-form-control text-input" id="exampleInputEmail2" >
										<div class="text-danger" id="orderedusername_error" ></div>
									</div>
									
									<div class="form-group">
										<label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
										<input type="text" name="orederedphone" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
										<div class="text-danger" id="orederedphone_error" ></div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">Address Line1 <span>*</span></label>
												<textarea name="orderedaddress1" class="form-control unicase-form-control text-input" id="exampleInputEmail1" ></textarea>
												<div class="text-danger" id="orderedaddress1_error" ></div>
											</div>
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">Address Line2 <span>*</span></label>
												<textarea name="orderedaddress2" class="form-control unicase-form-control text-input" id="exampleInputEmail1" ></textarea>
												<div class="text-danger" id="orderedaddress2_error" ></div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">Country <span>*</span></label>
												<select class="form-control unicase-form-control selectpicker" name="orderedcountry">
													<?php echo getCountries();?>
												</select>
												<div class="text-danger" id="orderedcountry_error" ></div>
											</div>
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">State <span>*</span></label>
												<input type="text" name="orederedstate" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
												<div class="text-danger" id="orederedstate_error" ></div>
											</div>
										</div>
									</div>									
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">City <span>*</span></label>
												<input type="text" name="orderedcity" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
												<div class="text-danger" id="orderedcity_error" ></div>
											</div>
											<div class="col-md-6">
												<label class="info-title" for="exampleInputEmail1">Pincode <span>*</span></label>
												<input type="text" name="orderedaddrespincode" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
												<div class="text-danger" id="orderedaddrespincode_error" ></div>
											</div>
										</div>
									</div>
									</hr>
									
									<button type="submit" id="click_addressinfoform" class="btn-upper btn btn-primary mt-20 checkout-page-button">Save</button>
									
								</form>
								<script type="text/javascript">
									$(document).ready(function() {   
										$('#click_addressinfoform').click(function(e){
											e.preventDefault();
											var formdata = new FormData($('#address_form')[0]);
											//console.log(reg_form);
											$.ajax({
											url: '<?=site_url();?>Checkout/checkoutaddress',
											data: formdata,
											type:"POST",
											contentType: false,
											processData:false,
											cache: false,
											beforeSend:function(){ 
													$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
												},
											success: function(response){
												var data = response;
												if(data.status == true){
													$('#status').html('<p class="alert alert-success">'+response.message+'<p>');
													data = data.data;
													
													//$('ul.scrollbar li').removeClass('active');
													//$('#prod_'+data).attr('class', 'active');
													$('.text-danger').empty();
													$('#click_addressinfoform').removeAttr('disabled').html('<i class="fa fa-check"></i> Save Successful!</p>');
													$('#address_form')[0].reset();
													$('.error').html();
													$("div#mdaddressul").css('width', '100%');
													$( "#newaddressdiv" ).attr('class','hide');
													$('#addedaddrid').val('prod_'+data);
												}
											},
											error: function(response){  
												var data = response.responseText;
												data = $.parseJSON(data);
												if(data.status == false){
													data = data.data;
													$.each(data, function(key, value){
														$('#'+key+'_error').empty().html(value);
													});
													$('#click_addressinfoform').removeAttr('disabled').html('<i class="fa fa-times"></i> Try again!');
												}
											}, 
											complete:function(){
												$('.overlay').remove();
											}
										});
										});
									});
								</script>
								
								<script type="text/javascript">
									$('#add_btn_rst').on('click', function(e){
										e.preventDefault();
										$(this).closest('form').find("input, textarea, select").val("");
										$('#address_btn').attr('data-update', 0).text('Save');

									});

									var load_more_address = function(address_id){
										 $.ajax({
											url:'<?=site_url();?>user/get_address/null/2/'+address_id,
											type:"POST",
											cache:false,
											beforeSend: function(){
												$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
											},
											success: function(response){
												if(response.status ==  true){
													$('.saved_address ul').append(response.html);
													$('#adr_loadmre').attr('onclick', 'load_more_address('+response.last_id+')');
													$('#style-8 li').click(function(){
														var id = $(this).attr('id');
														$('#style-8 li').attr('class', '');
														$('#'+id).attr('class', 'active');
														$('#addedaddrid').val(id);
														console.log(id);
														
													});
													$('#style_load').remove();
													$('.remove').remove();
												}
											},
											error: function(response){
												var data = $.parseJSON(response.responseText);
												if(data.status ==  false){
													$('.saved_address ul ').append(data.html);
												}
												console.log(data);
											},
											complete:function(){
												$('.overlay').remove();
											}
										});
									}
								</script>
								</div>
																
						      </div>
							  <div class="panel-body">
								<div class="col-md-12 col-sm-12">
								<div class="col-md-6 text-left">
								
								  <button id="newaddressbtn" class="btn-upper btn btn-primary checkout-page-button checkout-continue">+ Add New Address</button>
								</div>  
								<div class="col-md-6 text-right">

								  <input type="hidden" id="addedaddrid" name="dlvraddressid">
								  <button  type="submit" id="addcountinuebtn" class="btn-upper btn btn-primary checkout-page-button checkout-continue">Continue With Selected</button>
								</div>
								</div>
							  </div>
						    </div>
					  	</div>
						
						<script type="text/javascript">
							$(document).ready(function() {   
								$('#addcountinuebtn').on('click',function(e){
									e.preventDefault();
									var addid = $('#addedaddrid').val();
									var formdata = new FormData();
									formdata.append('addid',addid);									
									$.ajax({
									url: '<?=site_url();?>Checkout/deliveryaddress',
									data: formdata,
									type:"POST",
									contentType: false,
									processData:false,
									cache: false,
									beforeSend:function(){
											$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
										},
									success: function(response){
										var data = response;
										if(data.status == true){
											$('.text-danger').empty();
											$('.overlay').remove();
											$('#collapseTwo').collapse('hide');
											$('.checkout-step-03').removeClass('hide');
											$('#mdthirdstep').click();
											$('#mdheading2').css('background', '#dff0d8');
											$( "#mdcheckicon2" ).html( "<i class='fa fa-check text-right'><i>" );
										}
									},
									error: function(response){ 
										var data = response.responseText;
										data = $.parseJSON(data);
										if(data.status == false){
											data = data.data;
											$.each(data, function(key, value){
												$('#'+key+'_error').empty().html(value);
											});
											$('#status').html('<p class="alert alert-danger">'+response.message+'<p>');
											$('.overlay').remove();
										}
									
									}
								});
								
								});
							});
						</script>
					  	<!-- checkout-step-02  -->
						
						<!-- checkout-step-03  -->
					  	<div class="panel panel-default checkout-step-03 hide">
						    <div class="panel-heading">
						      <h4 class="unicase-checkout-title">
						        <a data-toggle="collapse" id="mdthirdstep" class="collapsed" data-parent="#accordion" href="checkout.html#collapseThree">
						       		<span>3</span>Order Summary : <small><?php echo $myproduct_count;?> Items, Total $<?php echo $mygrand_total;?></small>
						        </a>
						      </h4>
						    </div>
						    <div id="collapseThree" class="panel-collapse collapse">
						      <div class="panel-body">
								<div class="row ">
									<div class="shopping-cart">
										<div class="shopping-cart-table ">
											<div class="text-center success_message text-success"></div>
												<div class="table-responsive">
												<?php 
													$myproduct_count = $allcartdata['myproduct_count'];
													$mygrand_total = $allcartdata['mygrand_total'];
													$mycart_products = $allcartdata['mycart_products'];
												?>	
													<table class="table">
														<thead>
															<tr>
																<th class="cart-description item">&nbsp;</th>
																<th class="cart-product-name item">Item</th>
																<th class="cart-qty item">Quantity</th>
																<th class="cart-sub-total item">Price</th>
																<th class="cart-total last-item">Subtotal</th>
																<th class="cart-edit item">Remove</th>
																<th class="cart-romove item">Update</th>
															</tr>
														</thead><!-- /thead -->
														<tfoot>
															<tr>
																<td colspan="7">
																	
																</td>
															</tr>
														</tfoot>
														
														<tbody>
														
														<?php echo $mycart_products;?>
															
														</tbody><!-- /tbody -->
													</table><!-- /table -->
												</div>
										</div><!-- /.shopping-cart-table -->
									</div><!-- /.shopping-cart -->
								</div> <!-- /.row -->
						      </div>
						    </div>
					  	</div>
						
						<script type="text/javascript">
							$(document).ready(function() {   
								$('.updateFromcart').click(function(e){
									
									if( !$(this).val() ) {
										  $(this).parents('p').addClass('warning');
									}
									var tr = $(this).closest('tr'),
									id = tr[0].id;
									e.preventDefault();
									var formdata = new FormData();
									var cid = $(this).attr('data-cartid');
									//alert(formdata);
									formdata.append('cart_listid', $(this).attr('data-cartid'));
									formdata.append('quantity', $('#qty_'+cid).val());
									formdata.append('price', $('#price_'+cid).val());
									//$( "#multiple" ).val();
									
									$.ajax({
									url: '<?=site_url();?>Checkout/updatecartdata',
									data: formdata,
									type:"POST",
									contentType: false,
									processData:false,
									cache: false,
									beforeSend:function(){
											$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
										},
									success: function(response){
										var data = response;
										if(data.status == true){
											$('.text-danger').empty();
											//tr.remove();
											$('.overlay').remove();
											$('#cartasyncdata').html(data.html);
											$.each(response.data, function(key, value){
												$('.'+key).text(value);
												$('#'+cid+'_'+key).text(value);
											});
										}
									},
									error: function(response){ 
										var data = response.responseText;
										data = $.parseJSON(data);
										if(data.status == false){
											data = data.data;
											$.each(data, function(key, value){
												$('#'+key+'_error').empty().html(value);
											});
											$('#status').html('<p class="alert alert-danger">'+response.message+'<p>');
											$('.overlay').remove();
										}
									
									}
								});
								
								});
							});
						</script>
						
					  	<!-- checkout-step-03  -->

						<!-- checkout-step-04  -->
					    <div class="panel panel-default checkout-step-04 hide">
						    <div class="panel-heading">
						      <h4 class="unicase-checkout-title">
						        <a data-toggle="collapse" id="mdfourthstep" class="collapsed" data-parent="#accordion" href="checkout.html#collapseFour">
						        	<span>4</span>Payment Method
						        </a>
						      </h4>
						    </div>
						    <div id="collapseFour" class="panel-collapse collapse">
							    <div class="panel-body">
							     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							    </div>
					    	</div>
						</div>
						<!-- checkout-step-04  -->				
					  	
					</div><!-- /.checkout-steps -->
				</div>
				<!--div class="col-md-4">
					
<div class="checkout-progress-sidebar ">
	<div class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading">
		    	<h4 class="unicase-checkout-title">Your Checkout Progress</h4>
		    </div>
		    <div class="">
				<ul class="nav nav-checkout-progress list-unstyled">
					<li><a href="checkout.html#">Billing Address</a></li>
					<li><a href="checkout.html#">Shipping Address</a></li>
					<li><a href="checkout.html#">Shipping Method</a></li>
					<li><a href="checkout.html#">Payment Method</a></li>
				</ul>		
			</div>
		</div>
	</div>
</div> 
			</div--> 
			</div><!-- /.row -->
		</div><!-- /.checkout-box -->
		
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->

		<script>
			$('#login').on('click', function(e){
				e.preventDefault();
				$('.already-registered-login').attr('class', 'col-md-6 col-sm-6 already-registered-login');
				$('.create-new-account').attr('class', 'col-md-6 col-sm-6 create-new-account hide');
			});
			
			$('#register').on('click', function(e){
				e.preventDefault();
				$('.create-new-account').attr('class', 'col-md-6 col-sm-6 create-new-account');
				$('.already-registered-login').attr('class', 'col-md-6 col-sm-6 already-registered-login hide');
			});
			
		</script>
		<script>
			$('#newaddressbtn').on('click', function(e){
				e.preventDefault();
				$("div#mdaddressul").removeAttr("style");
				$( "#newaddressdiv" ).removeClass( "hide" );
			});			
		</script>
		<script>
			$('#style-8 li').click(function(){
				var id = $(this).attr('id');
				$('#style-8 li').attr('class', '');
				$('#'+id).attr('class', 'active');
				$('#addedaddrid').val(id);
				console.log(id);
			});
			$(document).ready(function(){
				$('.remove').remove();
			});
		</script>
	<style>
		.saved_address ul li{
			cursor: pointer;
		}
	</style>
<?php
		echo	$layout['footer'];
?>