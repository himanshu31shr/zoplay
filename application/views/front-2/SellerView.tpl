<?php echo $layout[ 'header']; ?>

<?php

if(empty($shop)){
  ?>
  <style type="text/css">
      h1{color:#ccc;margin-top: 6%;
        line-height: 1.4em;
       }
      h2{color:#ccc;font-size: 4em;}
  </style>
  <h1 class="text-center">This shop is either disabled or doesn't exist. <br/> Please wait while we redirect you to the home page</h1>
  <h2 class="text-center"> <i class="fa fa-meh-o fa-5x"></i> </h2>
  <script type="text/javascript">
    setTimeout(window.location.replace('<?=site_url();?>home'), 3000);
  </script>
  <?php
die;
}
?>
<style type="text/css">
  .cd-hero-slider{max-height: 400px;}
  .cd-half-width{
      padding: 5% 0% !important;
    }
  .nav-tabs {
    border-bottom: none !important;
    overflow: hidden;
}
.nav>li a{
  padding: 11px 19px;
  border: none;
  border-radius: 0;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus{
  background: #0f6cb2;
  border: none;
  border-radius: 0;
  color: #fff;
  padding: 11px 19px;
}
.nav>li>a:hover, .nav>li>a:focus{
  text-decoration: none;
  background-color: #0f6cb2;
  color: #fff;
  border: none;
  border-radius: 0;
  padding: 11px 19px;
  margin: 0;
}
.img_seller_icons ul li{
    display:inline-block;
    padding:0 !important;
    background:none !important;
      width: unset !important;
    text-align:center;
    color:#fff;
}
.thumbnail {
    padding: 10px;
}
.eh1{
  margin-bottom: 2em;
    text-align: center;
    margin-top: 0;
    text-transform: uppercase;
    letter-spacing: 3px;
    color: #424242;
    font-weight: 100;
}
.caption{height: unset !important;}
.image_size{margin: 0 !important;}
.social{    width: 100%;}
.bg-cloud{    background: rgb(255, 255, 255);
    padding: 2em 1em;
    border: 1px solid rgba(204, 204, 204, 0.45);
    border-radius: 3px;}
.s_name{
  color: #fff;
  letter-spacing: 3px;
  font-weight: 100;
}
.img_seller_icons{
    position: relative !important;
    bottom: 0em !important;
}
</style>
  <div class="row image_cover">
    <div class="col-md-12">
      <div class="img-container">
       
        <img class="img-responsive" src="<?=image_url($shop->featured_image);?>"   />
        </div>
      
      	<div class="row">
      	 	<div class="col-md-12 ">
      			<div class="settle ">
      				<div class="container">
		          	<ul class="nav nav-tabs" >
		                <li class="active" >
	                    <a href="#tab_default_1" data-toggle="tab" class="tab_cover">
	                    Products </a>
		                </li>
		                <li>
	                    <a href="#tab_default_2" data-toggle="tab" class="tab_cover">
	                    About </a>
		                </li>
		                <li>
	                    <a href="#tab_default_3" data-toggle="tab" class="tab_cover">
	                    Seller Information</a>
		                </li>
		            </ul>
	          	</div>
	          </div>
         	</div>

         	 <div class="col-md-12">
         	 	<div class="settle_rev">
	         	 	<div class="container">
			          <div class="pull-left shop-owner-text">

			            <!-- some stats / reviews -->
			            <ul class="reviews-bg">
			              <li>
                    <h1 class="text-uppercase s_name"><?=$shop->shop_name;?></h1>
			                <span class="reviews ">
  										<p>
  			                <span class="glyphicon glyphicon-star "></span>
  			                <span class="glyphicon glyphicon-star"></span>
  			                <span class="glyphicon glyphicon-star"></span>
  			                <span class="glyphicon glyphicon-star"></span>
  			                <span class="glyphicon glyphicon-star"></span>
			                </p>
			                 <div class="stars small" style="width:6px !important "> </div>
			                </span>

			              </li>
			            </ul>
			          </div>
			          <div class="pull-right img_seller_icons">
				            <!-- social icons -->
                    <div class="col-xs-12 col-sm-6 no-padding social">
                      <ul class="link">
                      <?php
                      if($shop->social_links_fb != ''){ ?>
                        <li class="fb "><a target="_blank" rel="nofollow" href="http://<?=$shop->social_links_fb;?>" target="_blank" title="Facebook"></a></li>
                      <?php } if($shop->social_links_tw != ''){ ?>
                        <li class="tw "><a target="_blank" rel="nofollow" href="http://<?=$shop->social_links_tw;?>" target="_blank" title="Twitter"></a></li>
                      <?php } if($shop->social_links_gp != ''){?>
                        <li class="googleplus"><a target="_blank" rel="nofollow" href="http://<?=$shop->social_links_gp;?>" target="_blank" title="GooglePlus"></a></li>
                      <?php } ?> 
                      </ul>
                    </div>
			          	</div>
			          </div>
		          </div>
		         </div>
        </div>
    <div class="cd-hero" style="    background: #2c343b;">
      <section class="container">
        <ul class="cd-hero-slider autoplay">
          <?php
          $i = 0;
          foreach($products as $p){

          ?>
          <li <?=$i == 0? 'class="selected"' : '';?>>
            <div class="cd-half-width">
              <h2 class="text-uppercase"><?=$p->product_name;?></h2>
              <p><?=trim($p->product_description);?></p>
              <a href="#0" class="cd-btn">$<?=trim($p->product_price);?></a>
              <a href="<?=site_url();?>product/<?=md5(trim($p->product_id));?>" class="cd-btn secondary">Buy</a>
            </div> <!-- .cd-half-width -->

            <div class="cd-half-width cd-img-container">
              <img src="<?=image_url($p->product_image);?>" alt="tech 1">
            </div> <!-- .cd-half-width.cd-img-container -->
          </li>
          <?php
          $i++;
            }
          ?>
        </ul> <!-- .cd-hero-slider -->
    </section> <!-- .cd-hero -->
  </div>

  
    </div>
  </div>

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          
            <div class="tab-content">
              <div class="tab-pane active" id="tab_default_1">
              <?php
              if($products != ''){
              ?>
                    <h1 class="eh1">All products</h1>
              <div class="row" id="append_prod">
                <?php 
                	foreach($products as $p){
              	?>
                <div class="col-sm-3 col-lg-3 col-md-3">
                  <div class="thumbnail">
                   <a href="<?=site_url();?>product/<?=md5($p->product_id);?>"> <div class="img-container image_size">
                      <img class="img-responsive" src="<?php echo image_url($p->product_image);?>"/>
                    </div>
                    </a>
                  
                    <div class="caption" >
                    <div class="prosell">
                      <h4 class="text-capitalize" ><a href="<?php echo site_url();?>product/<?php echo md5($p->product_id);?>"><?php echo $p->product_name;?></a></h4>
                      <div class="rating rateit-small rateit"  data-rateit-readonly="true" data-rateit-value="<?php echo $p->average_rating;?>"></div><small style="color: #ccc;font-size: 12px;" class="pull-right">Reviews(<?php echo $p->reviews_count == '' ? 0 : $p->reviews_count;?>)</small>
                      <div>
                        <small class="text text-primary">Price: $<?php echo $p->product_price;?></small>
                      </div>
                      </div></div>
                    <a href="<?=site_url();?>product/<?=md5($p->product_id);?>" class="btn btn-primary btn-block">Buy Now!</a>
                  </div>
                </div>
                <?php 
	              		}
                   $last_id = $p->product_id;
                ?>
              </div>
              <input type="hidden" name="last_id" value="<?=$last_id;?>">
              <center>
              <div class="" title="Load more products" ><a class="btn btn-secondary" id="load_s_products" onclick=""><i class="fa fa-ellipsis-h fa-2x"></i></a></div>
              </center>
              <script type="text/javascript">
                $('#load_s_products').on('click', function(){
                  var last_id = $('input[name=last_id]').val();
                  // if(last_id == 0){
                  //   $('#load_s_products').hide();
                  // }else{

                  // }
                  var form = new FormData();
                  form.append('last_id', last_id);
                  form.append('seller_id','<?=$p->user_id;?>');
                  $.ajax({
                    url:'<?php echo site_url();?>Information/get_more_seller_products',
                    data:form,
                    processData:false,
                    contentType:false,
                    cache:false,
                    type:"POST",
                    beforeSend:function(){
                      $('body').css('z-index',99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');

                    }, 
                    success:function(response){
                      if(response.status==true){
                        $('#append_prod').append(response.data);
                        $('input[name=last_id]').val(response.last_id);
                        $('.rateit').rateit();
                      }
                    },
                    error:function(response){
                      var data = $.parseJSON(response.responseText);
                      if(data.status == false){
                        $('#append_prod').append(data.data);
                        $('#load_s_products').remove();
                      }
                    },
                    complete:function(){
                      $('.overlay').remove();
                    }
                  });
                });
              </script>
              <?php
                }else{
                    $last_id = 0;
                    ?>
                    <div class="col-sm-12 col-lg-12 col-md-12 ">
                      <div class="alert alert-dismissible alert-danger">
                      No products to display!
                       </div>
                    </div>
                    <?php
                  
                }
              ?>
                </div>
                <div class="tab-pane" id="tab_default_2">
                    <h1 class="eh1">About </h1>
                    <div class="bg-cloud row">
                      <p class="shop_name">
                        <b class="col-md-3">Shop Name:</b>
                        <p class="col-md-9"><?=$shop->shop_name;?></p>
                      </p>
                      <p class="description">
                        <b class="col-md-3">Shop Description:</b>
                        <p class="col-md-9"><?=$shop->description;?></p>
                      </p>
                      <p class="description">
                        <b class="col-md-3">Shop Categories:</b>
                        <p class="col-md-9">No categories yet!</p>
                      </p>
                    </div>
                </div>
                <div class="tab-pane" id="tab_default_3">
                    <h1 class="eh1">Seller information</h1>
                    <div class="bg-cloud row">
                      <p class="shop_name">
                        <b class="col-md-3">Seller Name:</b>
                        <p class="col-md-9"><?=$shop->shop_name;?></p>
                      </p>
                      <p class="description">
                        <b class="col-md-3">Verfied:</b>
                        <p class="col-md-9">Yes</p>
                      </p>
                      <p class="description">
                        <b class="col-md-3">Rating:</b>
                        <p class="col-md-9">4/5</p>
                      </p>
                    </div>
                </div>
            </div>

      	</div>
      </div>
    </div>
    
   <div><br></div>
   <pre>
<?php
print_r($shop);      
?>
</pre>                  
<?php echo $layout[ 'footer']; ?>