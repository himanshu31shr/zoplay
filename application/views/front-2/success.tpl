<?php echo $layout['header'];?> 
</br>
</br>
</br>
<div class="container">
  <div class="row">
    <!-- You can make it whatever width you want. I'm making it full width
on <= small devices and 4/12 page width on >= medium devices -->
	<?php if(isset($cod))
	{?>
    <div class="text-center col-md-4 col-md-offset-4">
      <!-- CREDIT CARD FORM STARTS HERE -->
      <div class="panel panel-default credit-card-box">
        <div class="panel-heading display-table bg-success text-primary" >
          <div class="row display-tr" >
            <h3 class="panel-title display-td" >Dear Member
            </h3>
            <div class="display-td" >                            
              <h4>Your Order was successful, thank you for purchase.</h4>
            </div>
          </div>                    
        </div>
        <div class="panel-body">
          <form role="form" id="payment-form">
            
            <div class="row">
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <span class="text-danger pull-left">Order Number :</span> 
                </div>
              </div>
			  <div class="col-xs-8 col-md-8">
                <div class="form-group">
					<span><strong class="text-info pull-left"><?php echo $item_number; ?></strong>
				  </span>
                </div>
              </div>
            </div>
			
			<div class="row">
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <span class="text-danger pull-left">Amount Due :</span>
                </div>
              </div>
			  <div class="col-xs-8 col-md-8">
                <div class="form-group">
					<span><strong class="text-info pull-left">$<?php echo $payment_amt; ?></strong>
				  </span>
                </div>
              </div>
            </div>
			<div class="row">
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <span class="text-danger pull-left">Payment :</span> 
                </div>
              </div>
			  <div class="col-xs-8 col-md-8">
                <div class="form-group">
					<span><strong class="text-info pull-left">Cash On Delivery</strong>
				  </span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>            
      <!-- CREDIT CARD FORM ENDS HERE -->
    </div>
	<?php
	}
	else
	{ ?>
		
	<div class="text-center col-md-4 col-md-offset-4">
      <!-- CREDIT CARD FORM STARTS HERE -->
      <div class="panel panel-default credit-card-box">
        <div class="panel-heading display-table bg-success text-primary" >
          <div class="row display-tr" >
            <h3 class="panel-title display-td" >Dear Member
            </h3>
            <div class="display-td" >                            
              <h4>Your payment was successful, thank you for purchase.</h4>
            </div>
          </div>                    
        </div>
        <div class="panel-body">
          <form role="form" id="payment-form">
            
            <div class="row">
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <span class="text-danger pull-left">Order Number :</span> 
                </div>
              </div>
			  <div class="col-xs-8 col-md-8">
                <div class="form-group">
					<span><strong class="text-info pull-left"><?php echo $item_number; ?></strong>
				  </span>
                </div>
              </div>
            </div>
			<div class="row">
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <span class="text-danger pull-left">TXN ID :</span> 
                </div>
              </div>
			  <div class="col-xs-8 col-md-8">
                <div class="form-group">
					<span><strong class="text-info pull-left"><?php echo $txn_id; ?></strong>
				  </span>
                </div>
              </div>
            </div>
			<div class="row">
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <span class="text-danger pull-left">Amount Paid :</span>
                </div>
              </div>
			  <div class="col-xs-8 col-md-8">
                <div class="form-group">
					<span><strong class="text-info pull-left">$<?php echo $payment_amt.' '.$currency_code; ?></strong>
				  </span>
                </div>
              </div>
            </div>
			<div class="row">
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <span class="text-danger pull-left">Payment Status :</span> 
                </div>
              </div>
			  <div class="col-xs-8 col-md-8">
                <div class="form-group">
					<span><strong class="text-info pull-left"><?php echo $status; ?></strong>
				  </span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>            
      <!-- CREDIT CARD FORM ENDS HERE -->
    </div>	
		
	<?php }
	?>	
  </div>
</div>

<?php
		echo	$layout['footer'];
?>