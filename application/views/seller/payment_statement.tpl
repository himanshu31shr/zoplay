<?php
		echo	$layout['header'];
?> 
<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/main.css">		
<section class="container">
<div ui-view="" class="container-fluid ng-scope">
	<!-- uiView: undefined -->
	<ui-view class="ng-scope">
		<div class="row-fluid ng-scope">
			<div class="pull-left">
				<div>
					<h3 class="span10">Statements</h3>
				</div>
			</div>
		</div>
		<div class="row-fluid ng-scope">
			<div id="payments" class="span12">
				<!-- uiView: undefined -->
				<ui-view class="ng-scope">
					<div class="ng-scope">
						<div class="row-fluid stmt-filter-row">
							<div class="stmt-filter-container span12">
								<div class="row-fluid span6">
									<div class="span12 filter-row">
										<div class="pull-left filter-radio">
											<input value="1" ng-model="filterOption" ng-click="isChecked()" class="ng-pristine ng-untouched ng-valid" name="10" type="radio">
											</div>
											<span class="span10">Enter type and date range</span>
										</div>
										<div class="filter-row">
											<div class="span5">
												<label class="pull-left stmt-filter-label">Select account</label>
												<select class="span6 ng-pristine ng-untouched ng-valid" ng-model="summaryFilter.selectedAccount" ng-disabled="filterOption ==='2'" ng-options="str.name for str in accountType" ng-change="addGAEvents()">
													<option value="0" selected="selected">All</option>
													<option value="1">Prepaid</option>
													<option value="2">Postpaid</option>
												</select>
											</div>
											<div class="span7">
												<label class="pull-left stmt-filter-label">Select Date Range</label>
												<div class="input-append">
													<input date-range-picker="" class="form-control date-picker span12 ng-pristine ng-untouched ng-valid ng-isolate-scope enable" ng-class="{enable: filterOption === '1' }" ng-model="summaryFilter.date" placeholder="From - To" ng-disabled="filterOption ==='2'" options="options" readonly="readonly" type="text">
														<span class="add-on">
															<i class="icon-calendar"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="wrapper">
											<div class="line"></div>
											<div class="wordwrapper">
												<div class="word">or</div>
											</div>
										</div>
										<div class="row-fluid span3">
											<div class="span12 filter-row">
												<div class="pull-left filter-radio">
													<input ng-model="filterOption" value="2" ng-click="isChecked()" class="ng-pristine ng-untouched ng-valid" name="12" type="radio">
													</div>
													<span class="span10">Enter settlement reference number</span>
												</div>
												<div class="filter-row">
													<div class="input-append">
														<input class="input-large ng-pristine ng-untouched ng-valid" ng-model="summaryFilter.utr_no" placeholder="UTR Number" ng-disabled="filterOption ==='1'" ng-keyup="isValidUtr()" disabled="disabled" type="text">
														</div>
													</div>
												</div>
												<div class="span2 stmtBtn-group">
													<button class="btn fk-btn-primary" ng-disabled="(filterOption ==='2' &amp;&amp; !summaryFilter.utr_no || validUtr) || (filterOption ==='1' &amp;&amp; !summaryFilter.date.startDate &amp;&amp; !summaryFilter.date.endDate)" ng-click="fetchSummary()">Generate</button>
												</div>
											</div>
										</div>
										<div class="container-fluid stmt-view">
											<table class="table loader-block">
												<tbody>
													<tr class="row-fluid ng-scope" fk-table-helper-rows="" failure="false" list-size="" is-loading="false" empty-text="There is no summary generated" failure-text="Failed to load Statements">
														<td ng-attr-colspan="{{columnSpan}}">
															<div class="horizontal-middle text-middle">
																<!-- ngIf: isLoading -->
																<!-- ngIf: failure -->
																<!-- ngIf: listLength === 0 && !isLoading && !failure -->
															</div>
														</td>
													</tr>
												</tbody>
											</table>
											<!-- ngIf: invalidUtr -->
											<!-- ngIf: !summaryLoaderManager.status.isLoading -->
											<div class="row disclaimer ng-scope" ng-if="!summaryLoaderManager.status.isLoading" ng-hide="summaryLoaderManager.status.failure">
												<b class="note-label">Note:</b>
												<div class="notes">
													<p>We have removed opening, accumulated and closing balance details from statements view. This page is under revamp and we will start showing back these details once we have validated all the balances.</p>
													<p>Statements are available only for durations after 15th April 2015.</p>
												</div>
											</div>
											<!-- end ngIf: !summaryLoaderManager.status.isLoading -->
											<!-- ngIf: !summaryLoaderManager.status.isLoading -->
											<div class="row stmt-date-block ng-scope" ng-if="!summaryLoaderManager.status.isLoading" ng-hide="summaryLoaderManager.status.failure">
												<div class="span10">
													<span>Statement for</span>
													<!-- ngIf: stmtUtrNo === '' && stmtStartDate === stmtEndDate -->
													<!-- ngIf: stmtUtrNo === '' && stmtStartDate !== stmtEndDate -->
													<span ng-if="stmtUtrNo === '' &amp;&amp; stmtStartDate !== stmtEndDate" class="ng-binding ng-scope">22-12-2016 to 28-12-2016 
														<!-- ngIf: accType -->
														<span ng-if="accType" class="ng-binding ng-scope">(PREPAID AND POSTPAID)</span>
														<!-- end ngIf: accType -->
													</span>
													<!-- end ngIf: stmtUtrNo === '' && stmtStartDate !== stmtEndDate -->
													<!-- ngIf: stmtUtrNo !== '' -->
												</div>
												<div class="span2 stmt-icon-block">
													<a class="stmt-link knowMore" target="_blank" href="LearningCenter/article-steps/statements-page-glossary-and-use-cases" analytics-on="click" analytics-event="Statement- Know More" analytics-category="Payments" analytics-label="Click know more link on statements page">Know More</a>
													<!-- ngIf: !seller.enablePaymentMismatch || !summary.isMismatched -->
													<a ng-if="!seller.enablePaymentMismatch || !summary.isMismatched" class="stmt-icon ng-scope" target="_blank" ng-href="napi/payments/downloadSellerStatements?sellerId=dc8e07b18cd84b7f&amp;start_date=2016-12-22&amp;end_date=2016-12-28&amp;settlement_type=all" analytics-on="click" analytics-event="Statement- clicked download button" analytics-category="Payments" analytics-label="Seller clicked download button" href="napi/payments/downloadSellerStatements?sellerId=dc8e07b18cd84b7f&amp;start_date=2016-12-22&amp;end_date=2016-12-28&amp;settlement_type=all">
														<i class="icon icon-download-alt"></i>
													</a>
													<!-- end ngIf: !seller.enablePaymentMismatch || !summary.isMismatched -->
													<!-- ngIf: seller.enablePaymentMismatch && summary.isMismatched -->
												</div>
											</div>
											<!-- end ngIf: !summaryLoaderManager.status.isLoading -->
											<!-- ngIf: !summaryLoaderManager.status.isLoading -->
											<table class="table table-striped stmt-view-table ng-scope" ng-if="!summaryLoaderManager.status.isLoading" ng-hide="summaryLoaderManager.status.failure">
												<tbody>
													<tr>
														<td colspan="2" class="link-row">
															<div class="stmt-link-block">
																<a class="stmt-link" ng-href="dashboard#statements/startDate/2016-12-22/endDate/2016-12-28/type/all" target="_blank" analytics-on="click" analytics-event="Statement- click VIEW SETTLEMENTS" analytics-category="Payments" analytics-label="view settlements for statements" href="dashboard#statements/startDate/2016-12-22/endDate/2016-12-28/type/all">View Settlements</a>
																<span class="divider">|</span>
																<a class="stmt-link" ng-href="dashboard#transactions/startDate/2016-12-22/endDate/2016-12-28/type/all" target="_blank" analytics-on="click" analytics-event="Statement- click VIEW TRANSACTIONS" analytics-category="Payments" analytics-label="view transactions for statements" href="dashboard#transactions/startDate/2016-12-22/endDate/2016-12-28/type/all">View Transactions</a>
															</div>
														</td>
													</tr>
													<tr class="stmt-row">
														<td colspan="2">
															<strong>Settled balance</strong>
															<i class="icon icon-question-sign ng-scope" tooltip-placement="top" tooltip="Balance for which the payment was settled in this period/neft"></i>
														</td>
													</tr>
													<tr>
														<!-- ngInclude: 'angular/views/payments/payments_block/summary_desc.html' -->
														<td class="stmt-desc-block stmt-row ng-scope" colspan="2" ng-include="'angular/views/payments/payments_block/summary_desc.html'">
															<table class="table table-striped stmt-desc-table ng-scope">
																<thead>
																	<tr>
																		<th>Description</th>
																		<th>Credits
																			<span class="symb-left">(
																				<i class="icon icon-inr"></i>)
																			</span>
																		</th>
																		<th>Debits
																			<span class="symb-left">(
																				<i class="icon icon-inr"></i>)
																			</span>
																		</th>
																		<th>Net settled amount 
																			<span class="symb-left">(
																				<i class="icon icon-inr"></i>)
																			</span>
																			<i class="icon icon-question-sign ng-scope" tooltip-placement="top" tooltip="Credits - Debits"></i>
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<!-- ngIf: summary.credits.saleAmount !== '0.00' -->
																	<!-- ngIf: summary.debits.refunds !== '0.00' -->
																	<!-- ngIf: summary.debits.offer !== '0.00' -->
																	<!-- ngIf: summary.credits.mpFees.sellingCommission !== '0.00' || summary.debits.mpFees.sellingCommission !== '0.00' -->
																	<!-- ngIf: summary.credits.mpFees.fixedFee !== '0.00' || summary.debits.mpFees.fixedFee !== '0.00' -->
																	<!-- ngIf: summary.credits.mpFees.feeDiscount !== '0.00' || summary.debits.mpFees.feeDiscount !== '0.00' -->
																	<!-- ngIf: summary.credits.shippingFee !== '0.00' || summary.debits.shippingFee !== '0.00' -->
																	<!-- ngIf: summary.credits.cancellationFee !== '0.00' || summary.debits.cancellationFee !== '0.00' -->
																	<!-- ngIf: faShow -->
																	<!-- ngIf: summary.debits.reserves !== '0.00' -->
																	<!-- ngIf: summary.credits.releaseFromReserve !== '0.00' -->
																	<!-- ngIf: summary.credits.spfPayout !== '0.00' || summary.debits.spfPayout !== '0.00' -->
																	<!-- ngIf: summary.credits.tdsPayout !== '0.00' -->
																	<!-- ngIf: summary.credits.serviceTax !== '0.00' || summary.debits.serviceTax !== '0.00' -->
																	<!-- ngIf: summary.credits.sbCessTax !== '0.00' || summary.debits.sbCessTax !== '0.00' -->
																	<!-- ngIf: summary.credits.kkCessTax !== '0.00' || summary.debits.kkCessTax !== '0.00' -->
																	<!-- ngIf: (summary.credits.nddSddFee !== undefined || summary.debits.nddSddFee !== undefined) && (summary.credits.nddSddFee !== '0.00' || summary.debits.nddSddFee !== '0.00') -->
																	<!-- ngIf: (summary.credits.nddSddAmount !== undefined || summary.debits.nddSddAmount !== undefined) && (summary.credits.nddSddAmount !== '0.00' || summary.debits.nddSddAmount !== '0.00') -->
																	<!-- ngIf: (summary.credits.walletTopup !== undefined || summary.debits.walletTopup !== undefined) && (summary.credits.walletTopup !== '0.00' || summary.debits.walletTopup !== '0.00') -->
																	<!-- ngIf: (summary.credits.walletRefund !== undefined || summary.debits.walletRefund !== undefined) && (summary.credits.walletRefund !== '0.00' || summary.debits.walletRefund !== '0.00') -->
																</tbody>
															</table>
														</td>
													</tr>
													<tr class="bottom-totals stmt-row">
														<td class="text-right">
															<b>Total settled amount</b>
														</td>
														<td>
															<b class="ng-binding">
																<i class="icon icon-inr set-icon"></i>0.00
															</b>
														</td>
													</tr>
													<!-- ngIf: seller.enablePaymentMismatch && summary.isMismatched -->
												</tbody>
											</table>
											<!-- end ngIf: !summaryLoaderManager.status.isLoading -->
										</div>
									</div>
									<statement-modal enable="statementModal" note="is different" ng-click-download-pdf="downloadPdf()" class="ng-scope ng-isolate-scope">
										<div id="pop" class="modal fade">
											<div class="payment-modal">
												<div class="payment-modal-header">
													<h4>NEFT Reconciliation</h4>
													<div class="pull-right">
														<img class="cross-btn" src="https://img1a.flixcart.com/fk-sp-static/images/modal_cross.svg" alt="Close" data-dismiss="modal">
														</div>
													</div>
													<div class="payment-modal-body">
														<p class="payment-modal-desc">Please note the amount deposited in your bank account as per NEFT(s) 
															<span ng-bind-html="note" class="ng-binding">is different</span> from the sum of the 'Settlement Value' column in the transactions report.
														</p>
														<p class="payment-modal-desc">However, please be assured that the amount due for each order item has been settled correctly and our team is investigating the reason for this difference.</p>
													</div>
													<div class="payment-modal-footer">
														<a href="LearningCenter/faqs/payments#5409" target="_blank" class="learn-link" analytics-event="Statements - Learn More link [Unreconciled statements]" analytics-category="Payments" analytics-label="Seller clicks on Learn More link inside the download modal" analytics-on="">Learn more</a>
														<button class="payment-modal-btn cancel-btn" data-dismiss="modal" analytics-event="Statement - Cancelled downloads [Unreconciled statements]" analytics-category="Payments" analytics-label="Seller clicks Cancel in the download modal" analytics-on="">Cancel</button>
														<a class="payment-modal-btn download-btn" ng-click="callbackDownloadPdf()" analytics-event="Statement - Download Anyways [Unreconciled statements]" analytics-category="Payments" analytics-label="Seller clicks on Download Anyways link inside the download modal" analytics-on="">Download</a>
													</div>
												</div>
											</div>
										</statement-modal>
									</ui-view>
								</div>
							</div>
						</ui-view>
					</div>
		</section>
<?php
		echo	$layout['footer'];
?>
