<!DOCTYPE html>
<html lang="en">
<?php

if(!isset($active_class)){
	$active_class = 'home';
}
?>
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
	    <meta name="keywords" content="MediaCenter, Template, eCommerce">
	    <meta name="robots" content="all">

	    <title>Flipmart premium HTML5 & CSS3 Template</title>

	    <!-- Bootstrap Core CSS -->
	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/bootstrap.min.css">
	    
	    <!-- Customizable CSS -->
	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/main.css">
	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/blue.css">
	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/owl.transitions.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/animate.min.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/rateit.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/bootstrap-select.min.css">
                <link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/plugCss.css">
		

		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/plugins/datatables/jquery.dataTables.min.css">
                <script>
                        var site_url = '<?php echo base_url() ?>';
                </script>

		<!-- Icons/Glyphs -->
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/font-awesome.css">

        <!-- Fonts --> 
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
               <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<script src="<?= base_URL()?>assets/frontend/js/jquery-1.11.1.min.js"></script>
                <script src="<?php echo base_url();?>assets/admin/js/jquery-validate.js"></script> 
		<script src="<?= base_URL()?>assets/frontend/js/bootstrap.min.js"></script>
		<script src="<?= base_URL()?>assets/frontend/js/seller_custom.js"></script>

	</head>

    <body class="cnt-home">
		<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">

	<!-- ============================================== TOP MENU ============================================== -->
<!--<div class="top-bar animate-dropdown">
	<div class="container">
		<div class="header-top-inner">
			<div class="cnt-account">
				<ul class="list-unstyled">
					<li><a href="<?= base_url()?>index.php/seller/profile"><i class="icon fa fa-user"></i>My Account</a></li>
					<li><a href="<?= base_url()?>index.php/seller/login"><i class="icon fa fa-lock"></i>Login</a></li>
				</ul>
			</div>--><!-- /.cnt-account -->
			<!--<div class="clearfix"></div>
		</div>--><!-- /.header-top-inner -->
	<!--</div>--><!-- /.container -->
<!--</div>--><!-- /.header-top -->
<!-- ============================================== TOP MENU : END ============================================== -->
	<div class="main-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-2 logo-holder">
					<!-- ============================================================= LOGO ============================================================= -->
<div class="logo">
	<a href="home.html">
		
		<img src="<?= base_URL()?>assets/frontend/images/logo.png" alt="">

	</a>
</div><!-- /.logo -->
<!-- ============================================================= LOGO : END ============================================================= -->				</div><!-- /.logo-holder -->

				<div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
					<!-- /.contact-row -->
<!-- ============================================================= SEARCH AREA ============================================================= -->
				</div><!-- /.top-search-holder -->

				<div class="col-xs-12 col-sm-12 col-md-10 animate-dropdown top-cart-row">
					<!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->

	<div class="dropdown dropdown-cart">
		<ul class="dropdown-menu">
			<li>
				<div class="cart-item product-summary">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="name"><a href="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </a></h3>
						</div>
						<p class="notifiication_class text text-right">Oct 20, 2016</p>
					</div>
				</div><!-- /.cart-item -->
				<div class="clearfix"></div>
		</li>
		</ul><!-- /.dropdown-menu-->
	</div><!-- /.dropdown-cart -->

<!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->				</div><!-- /.top-cart-row -->
			</div><!-- /.row -->

		</div><!-- /.container -->

	</div><!-- /.main-header -->

	<!-- ============================================== NAVBAR ============================================== -->
<div class="header-nav animate-dropdown">
    <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
	<div class="nav-outer">
		<ul class="nav navbar-nav">
			<li class="<?php if($active_class == 'home'){echo 'active';}else{} ?> dropdown yamm-fw">
				<a href="<?=site_url()?>seller" >Home</a>
			</li>
		
			<li class="dropdown">
				<a href="<?=site_url();?>seller/listings" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Listings</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/listings">My Listings</a></li>
										<li><a href="<?= site_url()?>seller/approval_request">Track approval request</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="<?php if($active_class == 'orders'){echo 'active';}?> dropdown">
				<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Orders</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/active_orders">Active orders</a></li>
										<li><a href="<?= site_url()?>seller/cancelled_orders">Cancelled orders</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="<?php if($active_class == 'returns'){echo 'active';}?> dropdown">
				<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Returns</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/returns">Returns</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>	
			<li class="<?php if($active_class == 'payments'){echo 'active';}?> dropdown">
				<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Payments</a>
			<li class="dropdown">
				<a href="<?=site_url();?>seller/payment" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Payments</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content"> 
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/payment">Overview</a></li>
										<li><a href="<?= site_url()?>seller/payment_statement">Statements</a></li>
										<li><a href="<?= site_url()?>seller/transaction">Transactions</a></li>
										<li><a href="<?= site_url()?>seller/invoices">Invoices</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>	
			<li class="<?php if($active_class == 'performance'){echo 'active';}?> dropdown">
				<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Performance</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/overview">Overview</a></li>
										<li><a href="<?= site_url()?>seller/growth">Growth Opportunities</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="<?php if($active_class == 'promotions'){echo 'active';}?> dropdown">
				<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Promotions</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/Promotions">My Promotions</a></li>
										<li><a href="<?= site_url()?>seller/org_promotions">Org Promotions</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class=" <?php if($active_class == 'advertising'){echo 'active';}?> dropdown yamm-fw">
				<a href="<?= site_url()?>seller/advertising" >Advertising</a>
			</li>
		</ul><!-- /.navbar-nav -->
		<div class="clearfix"></div>				
	</div><!-- /.nav-outer -->
</div><!-- /.navbar-collapse -->


            </div><!-- /.nav-bg-class -->
        </div><!-- /.navbar-default -->
    </div><!-- /.container-class -->

</div><!-- /.header-nav -->
<!-- ============================================== NAVBAR : END ============================================== -->
</header>