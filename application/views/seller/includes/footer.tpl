<!-- ============================================================= FOOTER ============================================================= -->
<style>
.copyright-bar{background: #0f6cb2;padding: 10px 0px;}
ul.link li{margin-right: 2em;}
</style>
<footer id="footer" class="footer color-bg">
   <!-- <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Contact Us</h4>
                    </div><!-- module-heading --

		<div class="module-body">
			<ul class="toggle-footer" style="">
				<li class="media">
					<div class="pull-left">
						 <span class="icon fa-stack fa-lg">
								<i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="media-body">
						<p>ThemesGround, 789 Main rd, Anytown, CA 12345 USA</p>
					</div>
				</li>

				  <li class="media">
					<div class="pull-left">
						 <span class="icon fa-stack fa-lg">
						  <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="media-body">
						<p>+(888) 123-4567<br>+(888) 456-7890</p>
					</div>
				</li>

				  <li class="media">
					<div class="pull-left">
						 <span class="icon fa-stack fa-lg">
						  <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="media-body">
						<span><a href="sign-in.html#">flipmart@themesground.com</a></span>
					</div>
				</li>
              
            </ul>
    </div><!-- /.module-body --
                </div><!-- /.col --

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Customer Service</h4>
                    </div><!-- /.module-heading --

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="sign-in.html#" title="Contact us">My Account</a></li>
                <li><a href="sign-in.html#" title="About us">Order History</a></li>
                <li><a href="sign-in.html#" title="faq">FAQ</a></li>
                <li><a href="sign-in.html#" title="Popular Searches">Specials</a></li>
                <li class="last"><a href="sign-in.html#" title="Where is my order?">Help Center</a></li>
                        </ul>
                    </div><!-- /.module-body --
                </div><!-- /.col --

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Corporation</h4>
                    </div><!-- /.module-heading --

                    <div class="module-body">
                        <ul class='list-unstyled'>
                          <li class="first"><a title="Your Account" href="sign-in.html#">About us</a></li>
                <li><a title="Information" href="sign-in.html#">Customer Service</a></li>
                <li><a title="Addresses" href="sign-in.html#">Company</a></li>
                <li><a title="Addresses" href="sign-in.html#">Investor Relations</a></li>
                <li class="last"><a title="Orders History" href="sign-in.html#">Advanced Search</a></li>
                        </ul>
                    </div><!-- /.module-body --
                </div><!-- /.col --

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Why Choose Us</h4>
                    </div><!-- /.module-heading --

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="sign-in.html#" title="About us">Shopping Guide</a></li>
                <li><a href="sign-in.html#" title="Blog">Blog</a></li>
                <li><a href="sign-in.html#" title="Company">Company</a></li>
                <li><a href="sign-in.html#" title="Investor Relations">Investor Relations</a></li>
                <li class=" last"><a href="http://themesground.com/flipmart-demo/HTML/contact-us.html" title="Suppliers">Contact Us</a></li>
                        </ul>
                    </div><!-- /.module-body 
                </div>
            </div>
        </div>
    </div>-->

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding social">
                <ul class="link">
                  <li class=" pull-left"><a target="_blank" href="">Privacy Policy</a></li>
                  <li class=" pull-left"><a target="_blank" href="">Terms of Use</a></li>
                  <li class=" pull-left"><a target="_blank" href="">Guidelines</a></li>
                  <li class=" pull-left"><a target="_blank" href="">Help & FAQ's</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= -->
	<!-- JavaScripts placed at the end of the document so the pages load faster -->	
	<script src="<?=base_URL();?>assets/frontend/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/owl.carousel.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/plugins/datatables/jquery.dataTables.min.js"></script>
	
	<script src="<?=base_URL();?>assets/frontend/js/echo.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/jquery.easing-1.3.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/bootstrap-slider.min.js"></script>
    <script src="<?=base_URL();?>assets/frontend/js/jquery.rateit.min.js"></script>
    <script type="text/javascript" src="<?=base_URL();?>assets/frontend/js/lightbox.min.js"></script>
    <script src="<?=base_URL();?>assets/frontend/js/bootstrap-select.min.js"></script>
    <script src="<?=base_URL();?>assets/frontend/js/wow.min.js"></script>
	<script src="<?=base_URL();?>assets/frontend/js/scripts.js"></script>
	
	
	 <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Flot -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.pie.js"></script>
    <!-- Peity -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/demo/peity-demo.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/admin/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/pace/pace.min.js"></script>
	<!-- SUMMERNOTE -->
	<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js"></script>
	<!-- Data picker -->
	<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- GITTER -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/gritter/jquery.gritter.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Sparkline demo data  -->
    <script src="<?php echo base_url();?>assets/admin/js/demo/sparkline-demo.js"></script>
    <!-- ChartJS-->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/chartJs/Chart.min.js"></script>
    <!-- Toastr -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/toastr/toastr.min.js"></script>
      <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<!-- Sweet alert  -->
	<script src="<?php echo base_url();?>assets/admin/js/plugins/sweetalert/sweetalert.min.js"></script>
</body>
</html>
<script>
$(document).ready(function(){
	$('.dataTables-example').DataTable({
		pageLength: 25,
		responsive: true,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
			{ extend: 'copy'},
			{extend: 'csv'},
			{extend: 'excel', title: 'ExampleFile'},
			{extend: 'pdf', title: 'ExampleFile'},

			{extend: 'print',
			 customize: function (win){
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');

					$(win.document.body).find('table')
							.addClass('compact')
							.css('font-size', 'inherit');
				}
			}]
	});
});
</script>
<script>
    $(document).ready(function(){

        $('.summernote').summernote();

        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

    });
</script>