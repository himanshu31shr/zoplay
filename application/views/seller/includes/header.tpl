<!DOCTYPE html>
<html lang="en">
<?php

if(!isset($active_class)){
	$active_class = 'home';
}
?>
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
	    <meta name="keywords" content="MediaCenter, Template, eCommerce">
	    <meta name="robots" content="all">

	    <title>Flipmart premium HTML5 & CSS3 Template</title>

	    <!-- Bootstrap Core CSS -->
	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/bootstrap.min.css">

	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/custom.css">
	    <!-- Customizable CSS --> <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/main.css">

	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/blue.css">
	    <link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/owl.transitions.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/animate.min.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/rateit.css">
		<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/lightbox.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/bootstrap-select.min.css">
		<link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/plugCss.css">
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/plugins/sweetalert/sweetalert.css" >

		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/plugins/datatables/jquery.dataTables.min.css">
    <script>
    var site_url = '<?php echo base_url() ?>';
    </script>

		<!-- Icons/Glyphs -->
		<link rel="stylesheet" href="<?= base_URL()?>assets/frontend/css/font-awesome.css">

        <!-- Fonts --> 
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		
		<link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url();?>assets/admin/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Gritter -->
     <link href="<?php echo base_url();?>assets/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
  
    <link href="<?php echo base_url();?>assets/admin/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
      <script>
            var site_url = '<?php echo base_url() ?>';
    </script>
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-light.min.css" />		
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
	 <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/jquery-ui/jquery-ui.css">
	<script src="<?= base_URL()?>assets/frontend/js/jquery-1.11.1.min.js"></script>
	<script src="<?= base_URL()?>assets/frontend/jquery-ui/jquery-ui.js"></script>
	<script src="<?=base_URL()?>assets/frontend/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/jquery-validate.js"></script> 
	<script src="<?= base_URL()?>assets/frontend/js/bootstrap.min.js"></script>
	<script src="<?= base_URL()?>assets/frontend/js/seller_custom.js"></script>		
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/additional-methods.min.js"></script>		
	<script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>		
	<script src="<?=base_URL();?>assets/frontend/js/jquery.base64/jquery.base64.js"></script>
	
	<script src="<?php echo base_url() ?>/assets/admin/js/jquery.datetimepicker.full.min.js"></script>
<link href="<?php echo base_url() ?>/assets/admin/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
	<style>                
		#errormsg{color: red;}
		</style>
	</head>

    <body class="cnt-home">
		<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">

	<!-- ============================================== TOP MENU ============================================== -->
<!--<div class="top-bar animate-dropdown">
	<div class="container">
		<div class="header-top-inner">
			<div class="cnt-account">
				<ul class="list-unstyled">
					<li><a href="<?= base_url()?>index.php/seller/profile"><i class="icon fa fa-user"></i>My Account</a></li>
					<li><a href="<?= base_url()?>index.php/seller/login"><i class="icon fa fa-lock"></i>Login</a></li>
				</ul>
			</div>--><!-- /.cnt-account -->
			<!--<div class="clearfix"></div>
		</div>--><!-- /.header-top-inner -->
	<!--</div>--><!-- /.container -->
<!--</div>--><!-- /.header-top -->
<!-- ============================================== TOP MENU : END ============================================== -->
	<div class="main-header">
		<div class="container" style="width:1425px !important">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-2 logo-holder">
					<!-- ============================================================= LOGO ============================================================= -->
<div class="logo">
	<a href="<?= site_url() ?>seller/dashboard">
		
		<img src="<?= base_URL()?>assets/frontend/images/logo.png" alt="">

	</a>
</div><!-- /.logo -->
<!-- ============================================================= LOGO : END ============================================================= -->				</div><!-- /.logo-holder -->

				<div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
					<!-- /.contact-row -->
<!-- ============================================================= SEARCH AREA ============================================================= -->
				</div><!-- /.top-search-holder -->

				<div class="col-xs-12 col-sm-12 col-md-10 animate-dropdown top-cart-row">
					<!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
	<div class="dropdown dropdown-cart">
	<?php $sellerdata = $this->session->userdata('seller'); 
	  if(isset($sellerdata) && !empty($sellerdata))	{ 
	?>
		<form id="logout" action="<?= site_url() ?>seller/logout" method="post">
			<div class="form-group col-md-1">
				<button id="submit" value="Logout" type="submit" class="btn btn-primary logout">Logout</button>
			</div>
		</form>
		<?php } else{ ?>
		<form id="seller_login_form" name="seller_login_form" action="" method="post"  class="form-group">                    
			<div class="form-group col-md-6">
				<input id="useremail" type="text" placeholder="Email ID" name="email" class="form-control form-text">
			</div>
			<div class="form-group col-md-5">
				<input id="userpassword" type="password" placeholder="Password" name="password" class="form-control form-text">
			</div>
			<div class="form-group col-md-1">
				<button id="submit" value="Login" type="submit" class="btn btn-primary">Login</button>
			</div>
			<div id='errormsg'></div>
		</form>
		<?php  } ?>
		<ul class="dropdown-menu">
			<li>
				<div class="cart-item product-summary">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="name"><a href="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </a></h3>
						</div>
						<p class="notifiication_class text text-right">Oct 20, 2016</p>
					</div>
				</div><!-- /.cart-item -->
				<div class="clearfix"></div>
		</li>
		</ul><!-- /.dropdown-menu-->
	</div><!-- /.dropdown-cart -->

<!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->				</div><!-- /.top-cart-row -->
			</div><!-- /.row -->

		</div><!-- /.container -->

	</div><!-- /.main-header -->

	<!-- ============================================== NAVBAR ============================================== -->
<div class="header-nav animate-dropdown">
    <div class="container" style="width:1425px !important">
        <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
	<div class="nav-outer">
		<ul class="nav navbar-nav">
			<li class="<?php if($active_class == 'home'){echo 'active';}else{} ?> dropdown yamm-fw">
				<a href="<?=site_url()?>seller/dashboard" >Home</a>
			</li>
			
			<li class=" <?php if($active_class == 'profile'){echo 'active';}?> dropdown yamm-fw">
				<a href="<?= site_url()?>seller/profile" >My Profile</a>
			</li>
			
			<li class="dropdown">
				<a href="" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Products</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/addproduct">Add New Product</a></li>
										<li><a href="<?= site_url()?>seller/productlist">Product List</a></li>
										<li><a href="<?= site_url()?>seller/approval_request">Track approval request</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Coupons</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/add_coupons">Add Coupons</a></li>
										<li><a href="<?= site_url()?>seller/coupons_list">Coupons List</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="<?php if($active_class == 'orders'){echo 'active';}?> dropdown">
				<a href="sign-in.html#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Orders</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content">
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/active_orders">Active orders</a></li>
										<li><a href="<?= site_url()?>seller/cancelled_orders">Cancelled orders</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>			
			<li class=" <?php if($active_class == 'returns'){echo 'active';}?> dropdown yamm-fw">
				<a href="<?= site_url()?>seller/returns">Returns</a>
			</li>			
			<li class="<?php if($active_class == 'payments'){echo 'active';}?> dropdown">				
			<li class="dropdown">
				<a href="<?=site_url();?>seller/payment" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Payments</a>
				<ul class="dropdown-menu pages">
					<li>
						<div class="yamm-content"> 
							<div class="row">
								<div class="col-xs-12 col-menu">
								  <ul class="links">
										<li><a href="<?= site_url()?>seller/payment">Overview</a></li>
										<li><a href="<?= site_url()?>seller/transaction">Transactions</a></li>
										<li><a href="<?= site_url()?>seller/invoices">Invoices</a></li>
								  </ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>	
			
			<li class=" <?php if($active_class=='myshop'){echo 'active';}?> dropdown yamm-fw">
				<a href="<?= site_url()?>seller/myshop" >My Shop</a>
			</li>
		</ul><!-- /.navbar-nav -->
		<div class="clearfix"></div>				
	</div><!-- /.nav-outer -->
</div><!-- /.navbar-collapse -->


            </div><!-- /.nav-bg-class -->
        </div><!-- /.navbar-default -->
    </div><!-- /.container-class -->

</div><!-- /.header-nav -->
<!-- ============================================== NAVBAR : END ============================================== -->
<style>
   body
    {
        font-family: Arial;
        font-size: 10pt;
    }
  .loadermodal 
    {
        position: fixed;
        z-index: 999;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
        -moz-opacity: 0.8;
    }
    .loadercenter
    {
        z-index: 1000;
        margin: 300px auto;
        padding: 10px;
        width: 130px;
        background-color: White;
        border-radius: 10px;
        filter: alpha(opacity=100);
        opacity: 1;
        -moz-opacity: 1;
    }
    .loadercenter img
    {
        height: 120px;
        width: 120px;
    }
	.logout{
	    background-color: #006dcc;
		background-image: linear-gradient(to bottom, #08c, #04c);
		background-repeat: repeat-x;
		border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
		color: #fff;
		text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	}
	.footer {
		position : fixed !important;
	}
 </style> 
</header>
<script>
    $(document).ready(function(){
        
     $("#seller_login_form").validate({
        rules: {
               email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                  }
         },
        messages: {
             email:{
                    required:"Please enter a email address",
                    email:"please enter a valid email"
                },
             password:{
                     required:"please enter a password",
                }
        },
        submitHandler: function (form){ 
            $(".loadermodal").show();		
            $.ajax({ 
                url:'<?php echo site_url();?>seller/sellerLogin', 
                data: $('#seller_login_form').serialize(),
                type: 'POST',
				beforeSend : function()
				{
					$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
				},
                success: function(data){
                    $(".loadermodal").hide();
                    console.log(data.status);
                    if(data.status){
                       window.location.href ="<?php echo site_url();?>seller/dashboard";
					   $('.overlay').remove();
                    }else{
                       $("#errormsg").text(data.message);
					   $('.overlay').remove();
                    }
                },
				complete : function()
				{
					$('.overlay').remove();
				}				
            }); 			
        }
    });
 });
 </script>                            