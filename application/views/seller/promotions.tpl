<?php
   echo $layout['header'];
?>
<style>
.prm_button{
	margin: 0% 40%;
    font-size: 24px;
    padding: 1em 2em;
}
</style>
<div class="container">
	<div class="fffcolor">
		<div class="row">
			<div class="height col-md-12">
				<h2 class="text text-center">You can now add promotions</h2>
				
				<p class="text text-center">Create your promotion in 2 simple steps.</p>
				<br/>
				<br/>
				<br/>
				<button class="btn btn-nig btn-warning prm_button">Add a promotion</button>
			</div>
		</div>
	</div>
</div>
<?php
   echo $layout['footer'];
?>