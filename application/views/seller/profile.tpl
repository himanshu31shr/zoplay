<?php
		echo	$layout['header'];
?> 
<style>
.single-product{ min-height: 748px;}
.pad-all{padding: 0% 38%;}
.container {width: 1415px !important; } 
#citytown {
        height: 30px;
        padding-left: 10px;
        border-radius: 4px;
        border: 1px solid rgb(186, 178, 178);
        box-shadow: 0px 0px 12px #EFEFEF;
      }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="container">
		<div class="row single-product">
			<!-- /.sidebar -->
			<div class="col-md-12">
				<div class="product-tabs inner-bottom-xs  wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
				<div class="alert alert-success success-response" style="display:none;">
				</div>
				<div class="alert alert-danger danger-response" style="display:none;">
				</div>
					<div class="row">
						<div class="col-sm-3">
							<ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
								<li class="active"><a data-toggle="tab" href="#personalDetail">Personal Information</a></li>
								<li class=""><a data-toggle="tab" href="#ChangePassword">Change password</a></li>
								<li class=""><a data-toggle="tab" href="#address">Pickup Address</a></li>
								<li class=""><a data-toggle="tab" href="#business">Business Details</a></li>
								<li class=""><a data-toggle="tab" href="#deactivate">Deactivate Account</a></li>
							</ul><!-- /.nav-tabs #product-tabs -->
						</div>
						<div class="col-sm-9">
							<div class="tab-content">							
								<div id="personalDetail" class="tab-pane active">
									<form class="form-horizontal" id="personal_detail">
										<fieldset>

										<!-- Form Name -->
										<legend>Personal details</legend>

										<!-- Text input-->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="textinput">Full Name<span style="color: red">*</span>
										  </label>  
										  <div class="col-md-4">
										  <input id="full_name" name="full_name" type="text" value="<?= ($sellerData) ? $sellerData->full_name : ""?>" placeholder="Full Name" class="form-control input-md">
											
										  </div>
										</div>


										<!-- Multiple Radios (inline) -->
										<input type="hidden" value="<?= ($sellerData) ? $sellerData->gender : "" ?>" id="selectgender" >
										<div class="form-group">
										  <label class="col-md-4 control-label" for="radios">Gender<span style="color: red">*</span></label>
										  <div class="col-md-4"> 
											<label class="radio-inline" for="radios-0">
											  <input type="radio" name="gender" id="male" value="1">
											  Male
											</label> 
											<label class="radio-inline" for="radios-1">
											  <input type="radio" name="gender" id="female" value="2">
											  Female
											</label>
										  </div>
										</div>

										<!-- Select Basic -->
										<div class="form-group">
											<label class="control-label col-md-4" for="registration-date">Date of birth<span style="color: red">*</span></label>
											<div class="col-md-4">
												<div class="input-group registration-date-time">			
													<input placeholder="Date Of Birth" class="form-control datepicker " type="text" name="dob"  id="dob" value="<?= ($sellerData) ? $sellerData->dob : ""?>"> 
												</div>
											</div>    
										</div>

										<!-- Textarea -->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="textarea">Contact address<span style="color: red">*</span></label>
										  <div class="col-md-4">                     
											<textarea class="form-control" id="contact_address" name="contact_address" placeholder="Contact Address"><?= ($sellerData) ? $sellerData->street_address : ""?></textarea>
										  </div>
										</div>

										<!-- Text input-->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="textinput">Contact No.<span style="color: red">*</span></label>  
										  <div class="col-md-4">
										  <input id="contact" name="contact" type="text" placeholder="Telephone" value="<?= ($sellerData) ? $sellerData->contact : ""?>" class="form-control input-md">
											
										  </div>
										</div>

										<!-- Text input-->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="textinput">Fax<span style="color: red">*</span></label>  
										  <div class="col-md-4">
										  <input id="fax" name="fax" type="text" value="<?= ($sellerData) ? $sellerData->pincode : ""?>" placeholder="Fax" class="form-control input-md">
											
										  </div>
										</div>

										<!-- Button -->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="singlebutton"></label>
										  <div class="col-md-4">
											<button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Save</button>
										  </div>
										</div>
										</fieldset>
										</form>

								</div><!-- /.tab-pane -->

								<div id="ChangePassword" class="tab-pane">
									<form class="form-horizontal" id="change_password">
										<fieldset>

										<!-- Form Name -->
										<legend>Change Password</legend>

										<!-- Password input-->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Old Password<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="oldpassword" name="oldpassword" type="password" placeholder="" class="form-control input-md">
											
										  </div>
										</div>

										<!-- Password input-->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">New Password<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="newpassword" name="newpassword" type="password" placeholder="" class="form-control input-md" >
											
										  </div>
										</div>

										<!-- Password input-->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPassRepeat">Confirm Password<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="cpassword" name="cpassword" type="password" placeholder="" class="form-control input-md" >
											
										  </div>
										</div>

										<!-- Button (Double) -->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="bCancel"></label>
										  <div class="col-md-8"><button type="submit" id="bGodkend" name="bGodkend" class="btn btn-primary">Save</button>
										  </div>
										</div>

										</fieldset>
										<input type="hidden" value="<?=$sellerData->id; ?>" id="user_id">
										</form>

								</div><!-- /.tab-pane -->

								<div id="address" class="tab-pane">
									<form class="form-horizontal" id="pickup_address">
										<fieldset>
										<!-- Form Name -->
										<legend>Address</legend>
										<!-- Password input-->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">Name<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="name" name="name" type="text" placeholder="Name" class="form-control input-md" required="" value="<?= (!empty($pickup_address)) ? $pickup_address[0]->address_name : "" ?>">			
										  </div>
										</div>		
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">Contact<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="contact" name="contact" type="text" placeholder="Contact" class="form-control input-md" required="" value="<?= (!empty($pickup_address)) ? $pickup_address[0]->address_phone : "" ?>">			
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">City<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="citytown" name="citytown" type="text" placeholder="City" class="form-control input-md" required=""
											value="<?= (!empty($pickup_address)) ? $pickup_address[0]->address_city : "" ?>">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">State 
										<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="spr" name="spr" type="text" placeholder="State" class="form-control input-md" required="" value="<?= (!empty($pickup_address)) ? $pickup_address[0]->address_state : "" ?>">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">Zip Code<span style="color: red">*</span>

										</label>
										  <div class="col-md-4">
											<input id="zip" name="zip" type="text" placeholder="Zip Code" class="form-control input-md" required="" value="<?= (!empty($pickup_address)) ? $pickup_address[0]->address_zip : "" ?>">
											
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">Country<span style="color: red">*</span>

										</label>
										  <div class="col-md-4">											
												<select id="country" name="country"  class="form-control country">
													<?php  echo getCountries( $pickup_address[0]->address_country)  ?>
												</select>			
											  </div>	
											</div>
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Address Line 1<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="addressline1" name="addressline1" type="text" placeholder="Address Line 1" class="form-control input-md" required=""value="<?= (!empty($pickup_address)) ? $pickup_address[0]->address_line_1 : "" ?>">			
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piNewPass">Address Line 2<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="addressline2" name="addressline2" type="text" placeholder="Address Line 2" class="form-control input-md" required=""value="<?= (!empty($pickup_address)) ? $pickup_address[0]->address_line_2 : "" ?>">			
										  </div>
										</div>
										<!-- Button (Double) -->
										<div class="form-group">
										  <label class="col-md-4 control-label" for="bCancel"></label>
										  <div class="col-md-8">
											<button id="bGodkend" name="bGodkend" class="btn btn-primary">Save</button>
										  </div>
										</div>
										</fieldset>
										</form>
								</div><!-- /.tab-pane -->

								<div id="business" class="tab-pane">
									<form class="form-horizontal" id="business_details">
										<fieldset>

										<!-- Form Name -->
										<legend>Business Details</legend>
										<?php if(empty($business_detail) ||$business_detail[0]->status == 0){ ?>
										<div id="proof">
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Business type<span style="color: red">*</span></label>
										  <div class="col-md-4">
										  <input type="hidden" value="<?= ($business_detail) ? $business_detail[0]->business_type : "" ?>" id="btype">
											<select class="form-control" id="business_type" name="business_type">
											   <option value="">Select a business type</option>
											   <option value="0">Individual</option>
											   <option value="1">Company</option> </select>
										  </div>
										</div>
										<div id="company" style="display:none;"> 
											<div class="form-group">
											  <label class="col-md-4 control-label" for="piCurrPass">Business Name<span style="color: red">*</span></label>
											  <div class="col-md-4">
												<input id="business_name" name="business_name" type="text" placeholder="Business Name" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->business_name : "" ?>">	
											  </div>
											</div>
											<div class="form-group">
											  <label class="col-md-4 control-label" for="piCurrPass">Business Description<span style="color: red">*</span></label>
											  <div class="col-md-4">
												<textarea id="business_description" name="business_description" type="text" placeholder="Business Description" class="form-control input-md" required=""><?= ($business_detail) ? $business_detail[0]->business_description : "" ?></textarea>	
											  </div>
											</div>
										</div>
										<div id="company_individual" style="display:none;"> 
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Address Proof<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="address_proof" name="address_proof" type="file" placeholder="" class="form-control input-md" required="" value="">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Cancel Cheque Proof<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="cancel_cheque" name="cancel_cheque" type="file" placeholder="" class="form-control input-md" required="" value="">
										  </div>
										</div>	
										</div>
									</div>
										<?php } ?>
								<?php if(!empty($business_detail) && $business_detail[0]->status == 1){ ?>
									<div id="after_aprove" style="display:none;">
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Bank Name<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="bank_name" name="bank_name" type="text" placeholder="Bank Name" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->bank_name : "" ?>">											
										  </div>
										</div>
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Account Holder Name<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="account_holder_name" name="account_holder_name" type="text" placeholder="Account Holder Name" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->account_holder_name : "" ?>">											
										  </div>
										</div>
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Account Number<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="account_number" name="account_number" type="text" placeholder="Account Number" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->account_number : "" ?>">											
										  </div>
										</div>
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">IFSC Code<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="ifsc_code" name="ifsc_code" type="text" placeholder="IFSC Code" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->ifsc_code : "" ?>">											
										  </div>
										</div>
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">State<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="state" name="state" type="text" placeholder="State" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->state : "" ?>">											
										  </div>
										</div>
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">City<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="city" name="city" type="text" placeholder="City" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->city : "" ?>">											
										  </div>
										</div>
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">Branch<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="branch" name="branch" type="text" placeholder="Branch" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->branch : "" ?>">											
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">PAN<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="pan" name="pan" type="text" placeholder="PAN" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->tan : "" ?>">											
										  </div>
										</div>

										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">TIN<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="tin" name="tin" type="text" placeholder="TIN" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->tin_or_vat : "" ?>">
											
										  </div>
										</div>

										<div class="form-group">
										  <label class="col-md-4 control-label" for="piCurrPass">TAN<span style="color: red">*</span></label>
										  <div class="col-md-4">
											<input id="tan" name="tan" type="text" placeholder="TAN" class="form-control input-md" required="" value="<?= ($business_detail) ? $business_detail[0]->tan : "" ?>">
											
										  </div>
										</div>
									</div>
								<?php } ?>
										
										<!-- Button (Double) -->
										<input type="hidden" value="<?= ($business_detail) ? $business_detail[0]->status : "" ?>" id="checkbusinessstatus">
										<input type="hidden" value="<?= ($business_detail) ? $business_detail[0]->account_number : "" ?>" id="check_account_number">
										<div class="form-group">
										  <label class="col-md-4 control-label" for="bCancel"></label>
										  <div class="col-md-8 bdetails">
										 <?php if(empty($business_detail) ||$business_detail[0]->status == 0){ ?>
											<button id="savebusiness" name="savebusiness" class="btn btn-primary">Save</button>
											<div class="form-group verifyaccount" style="margin-left: -32%; display:none;
">
												<h4>Your Request Has Been Send To admin.Please Wait Sometime For Verify</h4>
											</div>
										  <?php } ?>
										<?php if(!empty($business_detail) && $business_detail[0]->status == 1){ ?>
											<div class="form-group verify" style="margin-left: -32%;
">
												<h4>Your Document Has Been verified.  Please Next To Fill Details</h4>
											</div>
											<span id="next" name="next" class="btn btn-success">Next</span>
										<?php } ?>
										  </div>
										</div>
										
										</fieldset>
										</form>

								</div><!-- /.tab-pane -->
								
								<div id="deactivate" class="tab-pane">	
									
									<div class="pad-all">
									<br/><br/><br/>
										<div class="col-md-6">
											<button id="deactivate" name="deactivate" class="btn btn-danger" onclick="deactivateaccount('<?php echo $sellerData->id; ?>');" <?= ($sellerData->status == '0') ? "disabled" : "" ?>>Deactivate Account</button>
											<br/>
											<input type="hidden" value="<?=$sellerData->status; ?>" id="checkstatus">
										</div>										
									</div>


								</div><!-- /.tab-pane -->
								
							</div><!-- /.tab-content -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.product-tabs -->

				<!-- ============================================== UPSELL PRODUCTS ============================================== -->
<!-- /.section -->
<!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
			
			</div><!-- /.col -->
			<div class="clearfix"></div>
		</div><!-- /.row -->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
<!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div>   
<?php
	echo	$layout['footer'];
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDv5IBJrYFB-S06EOdq6ugqNxSahQnDnh4&libraries=places&callback=initAutocomplete" async defer></script>
<script>
var placeSearch, autocomplete;
  var componentForm = {
	street_number: 'short_name',
	route: 'long_name',
	locality: 'long_name',
	administrative_area_level_1: 'short_name',
	country: 'long_name',
	postal_code: 'short_name'
  };

function initAutocomplete() {
	autocomplete = new google.maps.places.Autocomplete(
	/** @type {!HTMLInputElement} */
	(document.getElementById('citytown')),
	{
	types: ['(cities)'],
	});
	autocomplete.addListener('place_changed', fillInAddress);
}
function fillInAddress() {
	var place = autocomplete.getPlace();
	for (var component in componentForm) {
	  document.getElementById(component).value = '';
	  document.getElementById(component).disabled = false;
	}
	for (var i = 0; i < place.address_components.length; i++) {
	  var addressType = place.address_components[i].types[0];
	  if (componentForm[addressType]) {
		var val = place.address_components[i][componentForm[addressType]];
		document.getElementById(addressType).value = val;
	  }
	}
}
  
function geolocate() {
	if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(function(position) {
		var geolocation = {
		  lat: position.coords.latitude,
		  lng: position.coords.longitude
		};
		var circle = new google.maps.Circle({
		  center: geolocation,
		  radius: position.coords.accuracy
		});
		autocomplete.setBounds(circle.getBounds());
	  });
	}
}
	  
$('#next').click(function(){
	$('#after_aprove').show();
	$('#proof').hide();
	if($('#check_account_number').val()==""){
		$('.bdetails').append("<button id='hold' name='bGodkend' class='btn btn-primary'>Save</button>");
	}
	$('.verify').hide();
	$(this).hide();
});
if($('#checkbusinessstatus').val()=== '0'){ 
	$('#proof').hide();
	$('#savebusiness').hide();
	$('.verifyaccount').show();	
}
else if($('#checkbusinessstatus').val()==1){ 
	$('.verifyaccount').hide();		
}

$('#business_type').change(function(){
	var btype = $(this).val();	
	if(btype == "") {
		$('#company').hide(); 
		$('#company_individual').hide(); 
	}else if(btype == 0) {
		$('#company').hide(); 
		$('#company_individual').show(); 
	}else{
		$('#company').show(); 
		$('#company_individual').show(); 		
	}
});

$(function () {	
var gender = $('#selectgender').val();
if(gender == 1){
	$('#male').prop('checked',true);
}else if(gender == 2){
	$('#female').prop('checked',true);
}else{
	$('#male').prop('checked',false);
	$('#female').prop('checked',false);
}
$('#business_type').val($('#btype').val());
$('.datepicker').datetimepicker();    
	$("#personal_detail").validate({
		rules: {
			full_name: "required",							
			gender: "required",							
			dob: "required",							
			contact_address: "required",							
			contact: {
                    required: true,
                    number: true,
                    minlength:9,
                    maxlength:10
                  },							
			fax: "required",														
		},
		messages: {
			firstname: "Please enter your fullname",			
			gender: "Gender require",			
			dob: "DOB require",			
			contact:{
                     required:"Please enter a phone number",
                     number:"Please enter a valid phone number",
                     minlength: "Phone number must be at least 10 digit",
                     maxlength: "Phone number must be at least 10 digit"
                },
			fax: "Fax require",			
		},
		submitHandler: function (form) { 			
			var formData = new FormData(form);			
				$.ajax({ 
					url:'<?=site_url();?>seller/update_profile', 
					data: formData,
					processData: false,
					type: 'POST',
					cache: false,	
					contentType : false,							
					beforeSend : function()
					{
						$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
					},						
					success: function(data)
					{
						if(data.status == true){
						 $(".success-response").css("display" , "block");
						 $(".success-response").html(data.msg);
						 window.location.href ="<?= site_url() ?>seller/profile"
						}else{
						 $(".danger-response").css("display" , "block");
						 $(".danger-response").html(data.msg);
						}
					},
					complete : function(){
						$('.overlay').remove();
					}					
				}); 			
          }
	});
var user_id = $('#user_id').val();	
$("#change_password").validate({
	
	rules: {		   
		   oldpassword : { 
		    required : true,
			remote : {
				url : "<?= site_url() ?>seller/checkoldpassword",
				type : "post",
				data : {user_id : user_id} ,
			},
		   
		   },
		   newpassword: {
            required: true,
            minlength: 5
			},
			cpassword: {
				required: true,
				minlength: 5,
				equalTo: "#newpassword"
			}		
	 },
	messages: {
		oldpassword :{
				required:"Old password required",
				remote:"Old password incorrect",				
			},	
		newpassword : {
			required : "New password required",
		},
		cpassword : {
			required : "Confirm password required",
		},		
	},
	submitHandler: function (form){     	 
	  var formdata = new FormData(form);
	  formdata.append('user_id',user_id);
		$.ajax({ 
			url: "<?= site_url() ?>seller/change_sellerpassword",    data: formdata,
			type: 'POST',
			processData : false,
			cache : false,
			contentType :false,
			beforeSend : function()
			{
				$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success: function(data){                   
				if(data.status==true){	
				  $(".success-response").css("display" , "block");
				  $(".success-response").html(data.msg);
				  window.location.href ="<?= site_url() ?>seller/profile";
				  $('.overlay').remove();
				}else {   
				  $(".danger-response").css("display" , "block");
				  $(".danger-response").html(data.msg);	
				  window.location.href ="<?= site_url() ?>seller/profile";					 			  
				  $('.overlay').remove();
				}
			},
			complete : function()
			{
				$('.overlay').remove();
			}					
		}); 			
	}
});
$("#pickup_address").validate({
	rules: {		   
		   addressline1 : { 
		    required : true,					   
		   },
		   addressline2: {
            required: true,            
			},
		   name: {
            required: true,            
			},
		   contact: {
            required: true,            
			},
		   citytown: {
            required: true,            
			},
		   spr: {
            required: true,            
			},
		   zip: {
            required: true,            
			},
		  country: {
            required: true,            
			},
	 },
	messages: {
		addressline1 :{
				required:"Addressline1 require",				
			},		
		addressline2 :{
				required:"Addressline2 require",				
			},		
		citytown :{
				required:"City require",				
			},		
		spr :{
				required:"State require",				
			},		
		zip :{
				required:"Zip Code require",				
			},		
		Country :{
				required:"Country require",				
			},	
		name :{
				required:"Name require",				
			},
		contact :{
				required:"Contact require",				
			},		
	},
	submitHandler: function (form){     	 
	  var formdata = new FormData(form);
	  formdata.append('user_id',user_id);
		$.ajax({ 
			url: "<?= site_url() ?>seller/pickup_address",            
			data: formdata,
			type: 'POST',
			processData : false,
			cache : false,
			contentType :false,
			beforeSend : function()
			{
				$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success: function(data){                   
				if(data.status==true){	
				  $(".success-response").css("display" , "block");
				  $(".success-response").html(data.msg);
				  window.location.href ="<?= site_url() ?>seller/profile";
				  $('.overlay').remove();
				}else {   
				  $(".danger-response").css("display" , "block");
				  $(".danger-response").html(data.msg);	
				  window.location.href ="<?= site_url() ?>seller/profile"; $('.overlay').remove();
				}
			},
			complete : function()
			{
				$('.overlay').remove();
			}					
		}); 			
	}
});
$("#business_details").validate({
	rules: {		   
		   business_type : { 
		    required : true,					   
		   },
		   address_proof : { 
		    required : true,					   
		   }, 
		   cancel_cheque : { 
		    required : true,					   
		   },		   
	 },
	messages: {
		business_type :{
				required:"Business type require",				
			},	
		address_proof :{
				required:"Address proof require",				
			},	
		cancel_cheque :{
				required:"Cancel cheque proof require",				
			},							
	},
	submitHandler: function (form){     	 
	  var formdata = new FormData(form);
	  formdata.append('user_id',user_id);
		$.ajax({ 
			url: "<?= site_url() ?>seller/business_details",            
			data: formdata,
			type: 'POST',
			processData : false,
			cache : false,
			contentType :false,
			beforeSend : function()
			{
				$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success: function(data){                   
				if(data.status==true){	
				  $(".success-response").css("display" , "block");
				  $(".success-response").html(data.msg);
				  window.location.href ="<?= site_url() ?>seller/profile";
				  $('.overlay').remove();
				}else {   
				  $(".danger-response").css("display" , "block");
				  $(".danger-response").html(data.msg);	
				  window.location.href ="<?= site_url() ?>seller/profile"; $('.overlay').remove();
				}
			},
			complete : function()
			{
				$('.overlay').remove();
			}					
		}); 			
	}
});

});

function deactivateaccount(id){
	swal({
		  title: "Are you sure?",
		  text: "You want to Deactivate",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Deactivate",
		  closeOnConfirm: false
		},
	function(){
	$.ajax({
		url  : "<?= site_url() ?>seller/deactivate_account",
		data : {id : id},
		type : "POST",
	beforeSend : function()
	{
		$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	},
	success : function(data)
	{
		if(data.status==true){				   
			$(".success-response").css("display" , "block");
			$(".success-response").html(data.msg);
			$('.overlay').remove();
		}
	},
	}).done(function(){
		setTimeout(function() {
			swal({
				title: "Delete",
				text: "Account Has Been Deactivate Successfully.",
				type: "success"
			}, function() {
				window.location.href ="<?= site_url() ?>seller/profile";
			});
		}, 500);
		$('.overlay').remove();
	}).fail(function(){
		$('.overlay').remove();
	})
});
}

   
</script>
