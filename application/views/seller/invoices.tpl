<?php
	echo	$layout['header'];
?> 
<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/main.css">		
<div class="container">
	<input id="featureConfigsDashboard" name="featureConfigsDashboard" value="{&quot;sellerListingEnabled&quot;:true,&quot;emailVerification&quot;:true,&quot;bankAccountVerification&quot;:true,&quot;invitationOnly&quot;:1,&quot;mobileVerification&quot;:true,&quot;ordersAndAccounting&quot;:true}" type="hidden">
		<div id="message-area"></div>
		<div id="outer-tab-content">
			<div class="row sticky" style="display: none;"></div>
			<div class="row">
				<div id="section-header" class="span12">
					<h3 class="span6">Invoices</h3>
					<div class="invoice-options">
						<div id="claim-tds-btn">Claim TDS</div>
					</div>
				</div>
				<div id="disclaimer-note" class="disclaimer invoice-disclaimer"></div>
			</div>
			<div id="orders-table">
				<div id="invoicesTable">
					<table id="settlements-table" class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr>
								<th colspan="4" class="orderButtonsRow">
									<div class="row">
										<div class="span12">
											<form id="invoicesDateForm" class="form-inline">
												<label></label>&nbsp;
												<div class="input-append">
													<span class="add-on">Invoice Time Period &nbsp;
														<i class="icon-caret-right"></i>&nbsp;
													</span>
													<input id="datePicker" class="input-medium" type="text">
														<i id="datePickerButton" class="add-on">
															<i class="icon-calendar"></i>
														</i>
													</div>
												</form>
											</div>
										</div>
									</th>
								</tr>
								<tr>
									<th>Invoice Date</th>
									<th>Invoice ID</th>
									<th>Download</th>
								</tr>
							</thead>
							<tbody class="rows">
								<tr>
									<td colspan="4" style="text-align:center;">No Records Found</td>
								</tr>
							</tbody>
						</table>
						<button id="showBtn" class="show-more-rows" style="display: none;">
							<img src="images/btn-primary-loader.gif">
							</button>
						</div>
					</div>
				</div>
			</div>
<?php
		echo	$layout['footer'];
?>
<style>

#orders-table thead th.orderButtonsRow {
    background-color: #069;
    border-top: medium none transparent;
    border-top-right-radius: 4px;
    padding: 10px 5px 2px 15px;
}

#orders-table thead th {
    background-color: #eaeaea;
    border-radius: 0;
}

.invoice-options {
    float: right;
    margin: 4px 0;
}

#claim-tds-btn {
    border: 1px solid #007ad5;
    border-radius: 4px;
    color: #007ad5;
    cursor: pointer;
    display: inline-block;
    font-family: Arial;
    font-size: 13px;
    font-weight: 600;
    height: 34px;
    padding: 5px 20px;
}

#section-header {
    background-color: #fff;
    border-bottom: 1px solid #ddd;
    box-shadow: 0 4px 10px -5px #ccc;
    margin-bottom: 5px;
    margin-top: 10px;
    padding-bottom: 5px;
    padding-top: 5px;
}
.span12 {
    width: 1377px;
}


#orders-table thead span.add-on {
    background: #069 none repeat scroll 0 0;
    border: medium none;
    color: #fff;
    font-weight: bold;
    text-shadow: none;
}

#datePicker {
    background-color: #eeeeee;
    border: 1px solid #ccc;
    border-radius: 4px 0 0 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 12px;
    min-height: 18px;
    min-width: 133px;
    padding: 4px 10px;
}
}
</style>