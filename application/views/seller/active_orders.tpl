<?php   echo $layout['header'] ; ?>     
	<div class="wrapper wrapper-content animated fadeInRight">           <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
					<div class="ibox-title">
						<h3>Active Orders</h3>
					</div>					
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th class="center" nowrap>Order Number</th>
                        <th class="center">Item</th>
                        <th class="center" nowrap>Item Description</th>
                        <th class="center" nowrap>Order Status</th>    
						<th class="center">Price</th>                 <th class="center" nowrap>Payment Status</th>
                        <th class="center" nowrap>Order Quantity</th>
                        <th class="center">SubTotal</th>                                                
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach($active_order as $aorder){ ?>
                    <tr class="gradeX">
                        <td class="center"><?= $aorder->order_number; ?></td>            
                        <td class="left">
						<img src="<?= $aorder->product_image; ?>" style="width:10%;float: left;">&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="tooltip1" title="<?= $aorder->product_name; ?>" >
							<?= substr($aorder->product_name,0,40);?>
						</span>		
						</td>                        
                        <td class="left">
						<span class="tooltip1" title="<?= $aorder->product_description; ?>" >
						<?= substr($aorder->product_description,0,100);?>
						</span>
						</td>                        
						<td class="center">
							<?php if($aorder->order_shipping_status==0) echo  "Pending"; else
							   if($aorder->order_shipping_status==1) echo  "Processed"; else
							   if($aorder->order_shipping_status==2) echo  "Shipped"; else  
							   if($aorder->order_shipping_status==3) echo  "Delivered"; else
							   if($aorder->order_shipping_status==5) echo  "Refund"; else
							   if($aorder->order_shipping_status==6) echo  "Reshipment";else echo "";							
							?>
						</td>
                        <td class="left"><?= $aorder->product_price; ?></td>                       
						<td class="left">
							<?= ($aorder->order_payment_status == 1) ? "Paid" : "Pending" ?>
						</td>                        
                        <td class="left"><?= $aorder->order_quantity; ?></td>                        
                        <td class="left"><?= $aorder->order_total; ?></td>                        
					</tr>
					<?php } ?>
				</tbody>
				</table>
					</div>

				</div>
                </div>
            </div>
            </div>
        </div>
<style>
.label-primary{
	background-color: #337ab7;
}
</style>		
<?php echo $layout['footer']; ?>
<script>
$(document).ready(function() {
	$('.tooltip').tooltipster();
});
</script>