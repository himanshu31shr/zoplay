<?php
		echo	$layout['header'];
?> 
<style type="text/css">
	.cnt-home{background-color: #f3f3f3 !important;	}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
		<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h3>Track Approval Requests</h3>
				</div>			
			<div class="ibox-content">
		
			<div class=" tracking-filters">
				<p>You can track the status of your approval requests here.</p>
			</div>
			<div class=" tracking-filters">
				<legend>Pending Approvals</legend>
						<table class="dataTable stripe">
							<thead class="approval-table-head">
								<tr>
									<td>
										<strong>Order ID</strong>
									</td>
									<td>
										<strong>Product Name</strong>
									</td>
									<td>
										<strong>Price</strong>
									</td>
									<td>
										<strong>Shipping Status</strong>
									</td>
									<td>
										<strong>Payment Status</strong>
									</td>
									<td>
										<strong>Created Time</strong>
									</td>
									<td>
										<strong>Action</strong>
									</td>
								</tr>
							</thead>
							<tbody>

								<?php
									if(empty($products['pending'])){
								?>

									<tr>
										<td colspan="7">
											<div class="text-middle">
												 No orders pending for approval! 
											</div>
										</td>
									</tr>
									<?php 
										}else{
										foreach($products['pending'] as $p){
										?>	
											<tr>
												<td><?=$p->order_id;?></td>
												<td><?=$p->product_name;?></td>
												<td><?=$p->total_price;?></td>
												<td><?=$p->order_shipping_status_text;?></td>
												<td><?=$p->order_payment_status_text;?></td>
												<td><?=$p->order_added;?></td>
												<td><?=$p->next;?></td>
											</tr>
										<?php
											}
										}
										?>
							</tbody>
						</table>
			</div>

			<div class=" tracking-filters">
				<legend>All approved orders</legend>
						<table class="dataTable stripe">
							<thead class="approval-table-head">
								<tr>
									<td>
										<strong>Order ID</strong>
									</td>
									<td>
										<strong>Product Name</strong>
									</td>
									<td>
										<strong>Price</strong>
									</td>
									<td>
										<strong>Shipping Status</strong>
									</td>
									<td>
										<strong>Payment Status</strong>
									</td>
									<td>
										<strong>Created Time</strong>
									</td>
									<td>
										<strong>Action</strong>
									</td>
								</tr>
							</thead>
							<tbody>

								<?php
									if(empty($products['all'])){
								?>
									<tr>
										<td colspan="7">
											<div class="text-middle">
												 No approved orders!
											</div>
										</td>
									</tr>
									<?php 
										}else{
										foreach($products['all'] as $p){
										?>	
											<tr>
												<td><?=$p->order_id;?></td>
												<td><?=$p->product_name;?></td>
												<td><?=$p->total_price;?></td>
												<td><?=$p->order_shipping_status_text;?></td>
												<td><?=$p->order_payment_status_text;?></td>
												<td><?=$p->order_added;?></td>
												<td><?=$p->next;?></td>
											</tr>
										<?php
											}
										}
										?>
							</tbody>
						</table>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			
			<script type="text/javascript">
				var flag = function(flag, order_id){
					if(flag == 0){
						swal({
						  title: "Are you sure?",
						  text: "Do you wish to reject this order?",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Yes, reject it!",
						  closeOnConfirm: true
						},function(){
							asy_scr(flag, order_id);
						});
					}else{
						asy_scr(flag, order_id);
					}
				}

				var asy_scr = function(flag, order_id){
					var form = new FormData();
					form.append('flag', flag);
					
					form.append('order_id', order_id);
					$.ajax({
						url: '<?=site_url();?>seller/tracking/flag',
						data: form,
						type:"POST",
						processData: false,
						contentType: false,
						cache: false,
						beforeSend:function(){

						}, 
						success: function(response){
							if(response.status == true){
								swal("Order updated!", response.message, "success");
								location.reload();
							}
						},
						error:function(response){
							var res = $.parseJSON(response.responseText);
							if(res.status == true){
								sweetAlert("Oops...", res.message, "error");
							}
						},
						complete:function(){

						}
					});
				}
			</script>
   <!-- <pre>
   	<?php// print_r($products); ?>
   </pre> -->
<?php
		echo	$layout['footer'];
?>