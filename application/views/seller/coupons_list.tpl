<?php   echo $layout['header'] ; ?>   
	<div class="wrapper wrapper-content animated fadeInRight">          
			<div class="alert alert-success success-response" style="display:none;">
			</div>
			<div class="alert alert-danger danger-response" style="display:none;">
			</div>
			<div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">   
					<div class="ibox-title">
						<h3>Add Coupons</h3>
					</div>				
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th class="center">Code</th>
                        <th class="center">Type</th>
                        <th class="center">Value</th>
                        <!--<th>Remain</th>-->
                         <!--<th>Purchased</th>-->
                        <th class="center">Date From</th>
                        <th class="center">Date to</th>                        
                        <th class="center">status</th>
                        <th class="center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach($coupons as $coupon) { ?>
                    <tr class="gradeX">
                        <td class="center"><?= $coupon->coupon_code; ?></td>
                        <td class="center">
							<span class="label label-primary">
							<?= ($coupon->coupon_type==1) ? "Normal" : "Percentage" ?>
							</span>
						</td>
                         <td class="center"><?= ($coupon->coupon_type==1) ? $coupon->value : $coupon->value."%" ?></td>                         
                         <td class="center"><?= $coupon->coupon_from_datetime; ?></td>
                         <td class="center"><?= $coupon->coupon_to_datetime; ?></td>                         
                         
                        <td class="center">
							<span class="label label-primary">
								<?= ($coupon->coupon_status==1) ? "Active" : "Inactive" ?>
							</span>
						</td>
                         <td class="text-center">
                        <div class="btn-group">
							<i data="<?php echo $coupon->coupon_id;?>" class="status_checks btn
							  <?php echo ($coupon->coupon_status)?
							  'btn-success': 'btn-danger'?>"><?php echo ($coupon->coupon_status)? 'Active' : 'Inactive'?>
							 </i>	
						   <!--<button class="btn btn-danger delete_coupon" onClick="delete_coupon(<?= $coupon->coupon_id; ?>)"><i class="fa fa-trash"></i></button>-->
                            <button class="btn-white btn btn-xs" onclick="edit(<?= $coupon->coupon_id; ?>)"><i class="fa fa-edit"></i></button>
                        </div>
						</td>
					</tr>
					<?php } ?>
				</tbody>
				</table>
					</div>

				</div>
                </div>
            </div>
        </div>
</div>       
<style>
.label-primary{
	background-color: #337ab7;
}
.btn-group, .btn-group-vertical{
	display : inline-flex !important;
}
</style>		
<script>
$(document).on('click','.status_checks',function(){
	var status = ($(this).hasClass("btn-success")) ? '0' : '1';
	var msg = (status=='0')? 'Deactivate' : 'Activate';	
		var current_element = $(this); 		
		url = "<?= site_url() ?>seller/update_coupons_status";
			swal({
			  title: "Are you sure?",
			  text: "You want to "+msg,
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: msg,
			  closeOnConfirm: false
			},
			function(){			
			$.ajax({
					type:"POST",
					url: url,
					data: {id:$(current_element).attr('data'),status:status},
					beforeSend : function()
					{
						$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
					},
					success: function(data)
					{
						setTimeout(function() {
							swal({
								title: msg,
								text: "Status Has Been "+msg+" Successfully.",
								type: "success"
							}, function() {
								location.reload();
							});
						}, 500);							
				  },
				  complete : function()
				  {
					  $('overlay').remove();
				  }				  
			});
		});
	});
function delete_coupon(id)
{ 
	var formData = new FormData();
	var coupon_id= id;
	formData.append('coupon_id',coupon_id);
	$.ajax({
		url  : "<?= site_url() ?>seller/delete_coupon",
		type : "POST",
		data : formData,
		processData : false,		
		contentType : false,
		cache : false,		
	beforeSend : function()
	{
		$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	},
	success : function(result)
	{	
		if(result.status == true)
		{
			$(".success-response").css("display" , "block");
			$(".success-response").html(result.message);
		}else{
			$(".danger-response").css("display" , "block");
			$(".danger-response").html(result.message);
		}
		location.reload();
	},
	error : function()
	{
		$(".danger-response").css("display" , "block");
		$(".danger-response").html('Something went wrong');
	},
	complete : function()
	{
		$('.overlay').remove();	
	}	
});
}  
function edit(id)
{
	var id = $.base64.btoa(id);
	location.replace("<?= site_url() ?>seller/add_coupons/"+id);
}
</script>
<?php echo $layout['footer']; ?>