<?php  echo $layout['header'] ; ?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li class='active'>Login</li>
			</ul>
		</div>
	</div>
</div>
<div class="body-content">
	<div class="container">
		<div class="sign-in-page">
	<h4 class="checkout-subtitle">Create your new account</h4>
	<h4 class="checkout-subtitle text-right">Shop Details</h4>
<form name="sellerfrm" id="sellerfrm" class="register-form" role="form"> 
<div class="row">
	<div class="col-sm-6 create-new-account">	
	    <div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">Name <span>*</span></label>
			<input type="text" name="seller_name" id="seller_name" class="form-control">
			<input type="hidden" name="seller_id" value="<?php echo !empty($sellerData->id) ? $sellerData->id :'';?>" id="selle_id" class="form-control">
		</div>
		<div class="form-group">
			<label class="info-title">Email ID <span>*</span></label>
			<input type="text" name="seller_email" id="seller_email" value="<?php echo !empty($sellerData->email) ? $sellerData->email :'';?>" class="form-control">
	  	</div>
		<div class="form-group">
		    <label class="info-title">Password <span>*</span></label>
		    <input type="password" name="password" id="password" class="form-control">
		</div>
		<div class="form-group">
		    <label class="info-title">Confirm Password <span>*</span></label>
		    <input type="password" name="confirm_password" id="confirm_password" class="form-control">
		</div>
		<div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
		    <input type="text" name="seller_phone" id="seller_phone" value="<?php echo !empty($sellerData->contact) ? $sellerData->contact :'';?>" class="form-control">            
		</div>
	  	<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Continue</button>	
</div>	
    <div class="col-sm-6">	
		<div class="form-group">
		    <label class="info-title">Shop Name <span>*</span></label>
		    <input type="text" name="shop_name" id="shop_name" class="form-control">
		</div>	
		<div class="form-group">
		    <label class="info-title">Featured Image <span>*</span></label>
		    <div class="fileUpload">
				<span id="before_select" style="color:#FFFFFF; cursor:pointer; font-size: large;">Browse</span>
				<span id="after_select" style="color:#FFFFFF; font-size: large;"></span>
				<input type="file" name="image" onchange="getFileData(this);" id="file_up" value="" class="upload">
			</div>
			<!--<input type="file" name="image" id="image" class="form-control" onchange="getFileData(this);">-->
		</div>	
		<div class="form-group">
		    <label class="info-title">Facebook page link <span>*</span></label>
		    <input type="text" name="facebook_page_link" id="facebook_page_link" class="form-control">
		</div>	
		<div class="form-group">
		    <label class="info-title">Google+ page link <span>*</span></label>
		    <input type="text" name="google_page_link" id="google_page_link" class="form-control">
		</div>	
		<div class="form-group">
		    <label class="info-title">Twitter link <span>*</span></label>
		    <input type="text" name="twitter_link" id="twitter_link" class="form-control">
		</div>	
		<div class="form-group">
		    <label class="info-title">Description <span>*</span></label>
		    <textarea name="description" id="description" class="form-control"></textarea>
		</div>		
	</div>  
</div>	
	</form>
   </div>
	</div>
		</div>
</div>


<?php echo $layout['footer']; ?>

<script>
$(document).ready(function(){
$('.header-nav').remove();
$("#sellerfrm").validate({
        rules: {
            seller_name:"required",
            image:"required",
            facebook_page_link:"required",
            google_page_link:"required",
            twitter_link:"required",
            decription:"required",
            shop_name:"required",
             password: "required",
             seller_email:{
                    required: true,
                    email: true,
             },
            confirm_password: {
              equalTo: "#password"
            }
         },
        messages: {
               seller_name:"Please enter a seller name",
               image:"image required",
               facebook_page_link:"Please enter a facebook page link",
               twitter_link:"Please enter a twitter link",
               google_page_link:"Please enter a google page link",
               decription:"Decription required",
               shop_name:"Please enter a shop name",
               seller_email:{
                    required:"Please enter a email address",
                    emial:"please enter a valid email"
                },
            phone:{
                     required:"please enter a phone number",
                     number:"Please enter a valid phone number"
                }
        },
        submitHandler: function (form){           
			var formdata = new FormData(form);
            $.ajax({ 
                url: "<?= site_url() ?>seller/saveseller", 
                data: formdata,
                type: 'POST',
				processData : false,
				contentType : false,
				cache : false,
                beforeSend : function()
				{
					$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
				},
				success: function(data){                  
                    if(data.status){
                       var obj=data.data.sellerData; 
                       var email =obj.seller_email;
                       var phone =obj.seller_phone;
                       window.location.href = "<?= site_url() ?>seller";
					  $('.overlay').remove();
                    }else{
						$('.overlay').remove();
                    }                 
                },
				complete : function()
				{
					$('.overlay').remove();
				}				
            }); 			
        }
    });
    });

function getFileData(myFile){
   var file = myFile.files[0];  
   var filename = file.name;
   $('#after_select').text(filename);
   $('#before_select').hide();  
}
</script>
<style>
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;   
    cursor: pointer;
    opacity: 0;  
}
.fileUpload {
    background-color: #8dc63f;
    border-radius: 6px;
    color: #ffffff;
    margin: 0;
    overflow: hidden;
    padding: 14px;
    position: relative;
    text-align: center;
}
</style>
