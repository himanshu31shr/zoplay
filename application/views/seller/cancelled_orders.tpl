<?php   echo $layout['header'] ; ?>   
<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
					<div class="ibox-title">
						<h3>Cancel Orders</h3>
					</div>				
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th class="center" nowrap>Order Number</th>
                        <th class="center">Item</th>
                        <th class="center" nowrap>Item Description</th>
                        <th class="center">Price</th>                        
                        <th class="center">Quantity</th>
                        <th class="center">SubTotal</th>                                                
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach($cancelled_orders as $corder){ ?>
                    <tr class="gradeX">
                        <td class="center"><?= $corder->order_number; ?></td>            
                        <td class="left">
						<img src="<?= $corder->product_image; ?>" style="width:10%;float: left;">&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="tooltip1" title="<?= $corder->product_name; ?>" >
							<?= substr($corder->product_name,0,40);?>
						</span>												
						</td>                        
                        <td class="left">
						<span class="tooltip1" title="<?= $corder->product_description; ?>" >
						<?= substr($corder->product_description,0,100);?>
						</span>
						</td>                        
                        <td class="left"><?= $corder->product_price; ?></td>                        
                        <td class="left"><?= $corder->total_quantity; ?></td>                        
                        <td class="left"><?= $corder->total_price; ?></td>                        
					</tr>
					<?php } ?>
				</tbody>
				</table>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>    
<style>
.label-primary{
	background-color: #337ab7;
}
</style>	
<?php echo $layout['footer']; ?>
<script>
$(document).ready(function() {
	$('.tooltip').tooltipster();
});
</script>