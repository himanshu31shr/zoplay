<?php echo $layout['header'] ; ?>  
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h3>Product List</h3>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-example" >
							<thead>
								<tr>
									<th>Product Name</th>
									<th>Image</th>
									<th>Price</th>
									<th>Added by</th>
									<th>Quantity</th>
									<th>Status</th>
									<th>Created On</th>
									<th>Action</th>
									
								</tr>
							</thead>
							<tbody>
							<?php
								if(isset($list)){
									
									$list = json_decode($list);
									foreach($list as $l){
									
							?>
								<tr class="gradeX">
									<td>
									<span class="tooltip1" title="<?= $l->product_name; ?>" >
										<?= substr($l->product_name,0,15);?>
									</span>
									</td>
									<td>
										<span class="tooltip1" title="<?= $l->product_image; ?>">
										<?php 
										if($l->product_image== ''){
											echo "No Image";
										}else{
											echo substr(site_url($l->product_image),0,20);
										}
										?>
										</span>
									</td>
									<td><?=$l->product_price;?></td>
									<td><?=$l->full_name;?></td>
									<td class="center"><?=$l->product_quantity;?></td>
									<td class="center">
										<span class="label label-primary">
											<?php
												if($l->product_status == 0){
													echo "Inactive";
													$flag = "Activate";
												}else if($l->product_status == 1){
													echo "Active";
													$flag = "Deactivate";
												}else{
													$flag = "Activate";
													echo "Deleted";
												}
											?>
										</span>
									</td>
									<td><?=$l->product_added;?></td>
									<td class="text-center">
										<div class="btn-group">
											<button class="btn btn-primary tooltip1" title="View Product" onclick="view('<?php echo base64_encode($l->product_name);?>')"><i class="fa fa-eye"></i></button>
											<button class="btn btn-white" onclick="edit('<?= base64_encode($l->product_id);?>')"><i class="fa fa-edit"></i></button>  
											<!--button class="btn btn-secondary" onclick="change_flag(<?=$l->product_id;?>, <?=$l->product_status;?>)"><i class="fa fa-flag"></i></button-->
											<button class="btn btn-danger" onclick="delete_product(this , <?=$l->product_id;?>)"><i class="fa fa-trash"></i></button>
										</div>
									</td>
								</tr>
							<?php
									}
								}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $layout['footer']; ?>
<script>
$(document).ready(function() {
	$('.tooltip').tooltipster();
});
var change_flag = function(product_id, flag){
	
}

function edit(a){
window.location.replace('<?php echo site_url();?>/seller/addproduct/'+a);	
}


 function delete_product(b , a){
	swal({
		  title: "Are you sure?",
		  text: "You want to Delete",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Delete",
		  closeOnConfirm: false
		},
		function(){	
		$.ajax({
		url :  "<?php echo site_url();?>seller/delete_product/" ,
		data : {product_id : a },
		type : "POST" ,
		beforeSend : function()
		{
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},
		success : function(data){
			if( data.status =="success")
			{
				setTimeout(function() {
							swal({
								title: "Delete",
								text: "Product Has Been Deleted Successfully.",
								type: "success"
							}, function() {
								location.reload();
							});
						}, 500);				
			}else if( data.status =="error")
			{
		
			}					
		},
		 complete : function()
		  {
			  $('overlay').remove();
		  }	
		});		
	});		
}

function view( a ){	
	 window.open('<?php echo  site_url()."search/" ;?>'+a+'/search','_blank');
}
</script>