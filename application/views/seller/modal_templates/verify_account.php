<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal">&times;</button>
   <h4 class="modal-title">Verify Bank Account</h4>
</div>
<div id="modalbox-body" style="max-height:740px" class="modal-body">

   <form class="form-horizontal" name="bank-details" autocomplete="off">
	  <div class="help-text col-sm-12 mt10 text-danger">
		 * A sample amount will be transferred to your bank account in the next 2 working days, post which a verification link will be made available in "Bank details" section. Please confirm the amount received to verify your updated bank account details.
	  </div>
	  <div class="form-fields col-sm-12">
		 <div uif-append="formFields">
			<div class="hide">
			   <div class="">
				  <div uif-append="1" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="64e96de1-a0a5-faf7-4d0c-3f626404c5fc">
						<input uif-fbtype="element" type="text" value=" " name="1" class="hide" id="1" uif-uqid="64e96de1-a0a5-faf7-4d0c-3f626404c5fc">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="1" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="">
				  <div uif-append="2" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="bc20acc3-39d0-8e70-1025-895c8ddc81e2">
						<input uif-fbtype="element" type="password" value=" " name="2" class="hide" id="2" uif-uqid="bc20acc3-39d0-8e70-1025-895c8ddc81e2">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="2" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			</div>
			<div class="">
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label required" for="account-holder-name">Account holder name</label>
				  </div>
				  <div uif-append="account-holder-name" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="0dd562fe-9ac6-d76b-36fd-40f3e1097fd8">
						<input uif-fbtype="element" type="text" name="account-holder-name" class="form-control" id="account-holder-name" uif-uqid="0dd562fe-9ac6-d76b-36fd-40f3e1097fd8" maxlength="50">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="account-holder-name" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label required" for="account-number">Account number</label>
				  </div>
				  <div uif-append="account-number" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="4733c6c0-553d-a7a9-375e-f3a940ca0cb8">
						<input uif-fbtype="element" type="password" name="account-number" class="form-control" id="account-number" uif-uqid="4733c6c0-553d-a7a9-375e-f3a940ca0cb8">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="account-number" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label required" for="retype-account-number">Retype account number</label>
				  </div>
				  <div uif-append="retype-account-number" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="4918ee86-a9aa-0f73-90a8-38cda115e50e">
						<input uif-fbtype="element" type="text" name="retype-account-number" class="form-control" id="retype-account-number" uif-uqid="4918ee86-a9aa-0f73-90a8-38cda115e50e">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="retype-account-number" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group hide">
				  <div uif-append="bank-email" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="09a2353b-d3a2-92ec-1029-19a1318d2612">
						<input uif-fbtype="element" type="text" disabled="true" value="prashushinde7@gmail.com" name="bank-email" class="form-control" id="bank-email" uif-uqid="09a2353b-d3a2-92ec-1029-19a1318d2612" hidden="true">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-email" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			</div>
		 </div>
		 <div uif-append="bankFields" class="hide" id="bankFields">
			<div class="">
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-name">Bank name</label>
				  </div>
				  <div uif-append="bank-name" class=" col-sm-8">
					 <div>
						<div class="" uif-fbtype="wrapper" uif-uqid="81a079d4-d17c-af34-c371-782f4e5c5020">
						   <select uif-fbtype="element" name="bank-name" id="bank-name" uif-uqid="81a079d4-d17c-af34-c371-782f4e5c5020" class="form-control">
							  <option value="" selected="">Select Bank</option>
							  <option value="1">ABHYUDAYA CO-OP BANK LTD</option>
							  <option value="2">ABU DHABI COMMERCIAL BANK</option>
							  <option value="3">AKOLA DISTRICT CENTRAL CO-OPERATIVE BANK</option>
							  <option value="4">ALLAHABAD BANK</option>
							  <option value="5">ALMORA URBAN CO-OPERATIVE BANK LTD</option>
							  <option value="6">ANDHRA BANK</option>
							  <option value="7">ANDHRA PRAGATHI GRAMEENA BANK</option>
							  <option value="8">APNA SAHAKARI BANK LTD</option>
							  <option value="9">AUSTRALIA AND NEW ZEALAND BANKING GROUP LIMITED.</option>
							  <option value="10">AXIS BANK</option>
							  <option value="11">BANK OF AMERICA</option>
							  <option value="12">BANK OF BAHRAIN AND KUWAIT</option>
							  <option value="13">BANK OF BARODA</option>
							  <option value="14">BANK OF CEYLON</option>
							  <option value="15">BANK OF INDIA</option>
							  <option value="16">BANK OF MAHARASHTRA</option>
							  <option value="17">BANK OF TOKYO MITSUBISHI UFJ LTD.</option>
							  <option value="18">BARCLAYS BANK PLC</option>
							  <option value="19">BASSEIN CATHOLIC CO-OP BANK LTD</option>
							  <option value="20">KOTAK MAHINDRA BANK LIMITED</option>
							  <option value="21">BNP PARIBAS</option>
							  <option value="22">CANARA BANK</option>
							  <option value="23">CAPITAL LOCAL AREA BANK LTD.</option>
							  <option value="24">CATHOLIC SYRIAN BANK  LTD</option>
							  <option value="25">CENTRAL BANK OF INDIA</option>
							  <option value="26">CHINATRUST COMMERCIAL BANK</option>
							  <option value="27">CITIBANK NA</option>
							  <option value="28">CITIZENCREDIT CO-OP BANK LTD</option>
							  <option value="29">CITY UNION BANK LTD</option>
							  <option value="30">COMMONWEALTH BANK OF AUSTRALIA</option>
							  <option value="31">CORPORATION BANK</option>
							  <option value="32">CREDIT AGRICOLE CORP N INVSMNT BANK</option>
							  <option value="33">CREDIT SUISSE AG</option>
							  <option value="34">DENA BANK</option>
							  <option value="35">DEUTSCHE BANK AG</option>
							  <option value="36">DEVELOPMENT BANK OF SINGAPORE</option>
							  <option value="37">DEVELOPMENT CREDIT BANK</option>
							  <option value="38">DHANLAXMI BANK LTD</option>
							  <option value="39">DICGC</option>
							  <option value="40">DOMBIVLI NAGARI SAHAKARI BANK LIMITED</option>
							  <option value="41">FIRSTRAND BANK LIMITED</option>
							  <option value="42">GURGAON GRAMIN BANK</option>
							  <option value="43">HDFC BANK LTD</option>
							  <option value="44">HSBC</option>
							  <option value="45">ICICI BANK</option>
							  <option value="46">IDBI BANK</option>
							  <option value="47">INDIAN BANK</option>
							  <option value="48">INDIAN OVERESEAS BANK</option>
							  <option value="49">INDUSIND BANK LIMITED</option>
							  <option value="50">ING VYSYA BANK</option>
							  <option value="51">JALGAON JANATA SAHKARI BANK LTD</option>
							  <option value="52">JANAKALYAN SAHAKARI BANK LTD</option>
							  <option value="53">JANASEVA SAHAKARI BANK LTD. PUNE</option>
							  <option value="54">JANATA SAHAKARI BANK LTD (PUNE)</option>
							  <option value="55">JP MORGAN CHASE BANK</option>
							  <option value="56">KALLAPPANNA AWADE ICH JANATA S BANK</option>
							  <option value="57">KAPOLE CO OP BANK</option>
							  <option value="58">KARNATAKA BANK LTD</option>
							  <option value="59">KARNATAKA VIKAS GRAMEENA BANK</option>
							  <option value="60">KARUR VYSYA BANK</option>
							  <option value="61">KURMANCHAL NAGAR SAHKARI BANK LTD</option>
							  <option value="62">MAHANAGAR CO OP. BANK LTD. MUMBAI</option>
							  <option value="63">MAHARASHTRA STATE CO OPERATIVE BANK</option>
							  <option value="64">MASHREQ BANK PSC</option>
							  <option value="65">MIZUHO CORPORATE BANK LTD</option>
							  <option value="66">MUMBAI DISTRICT CENTRAL CO-OP. BANK LTD.</option>
							  <option value="67">NAGPUR NAGRIK SAHAKARI BANK LTD</option>
							  <option value="68">NATIONAL AUSTRALIA BANK</option>
							  <option value="69">NEW  INDIA CO-OPERATIVE  BANK  LTD</option>
							  <option value="70">NKGSB CO-OP BANK LTD</option>
							  <option value="71">NORTH MALABAR GRAMIN BANK</option>
							  <option value="72">NUTAN NAGARIK SAHAKARI BANK LTD</option>
							  <option value="73">OMAN INTERNATIONAL BANK SAOG</option>
							  <option value="74">ORIENTAL BANK OF COMMERCE</option>
							  <option value="75">PARSIK JANATA SAHAKARI BANK LTD</option>
							  <option value="76">PRATHAMA BANK</option>
							  <option value="77">PRIME CO OPERATIVE BANK LTD</option>
							  <option value="78">PUNJAB &amp; MAHARASHTRA CO-OP BANK LTD</option>
							  <option value="79">PUNJAB AND SIND BANK</option>
							  <option value="80">PUNJAB NATIONAL BANK</option>
							  <option value="81">RABOBANK INTERNATIONAL (CCRB)</option>
							  <option value="82">RAJKOT NAGARIK SAHAKARI BANK LTD</option>
							  <option value="83">RESERVE BANK OF INDIA</option>
							  <option value="84">SBERBANK</option>
							  <option value="85">SHINHAN BANK</option>
							  <option value="86">SHRI CHHATRAPATI RAJARSHI SHAHU URBAN CO-OP BANK LTD</option>
							  <option value="87">SOCIETE GENERALE</option>
							  <option value="88">SOUTH INDIAN BANK</option>
							  <option value="89">STANDARD CHARTERED BANK</option>
							  <option value="90">STATE BANK OF BIKANER &amp; JAIPUR</option>
							  <option value="91">STATE BANK OF HYDERABAD</option>
							  <option value="92">STATE BANK OF INDIA</option>
							  <option value="93">STATE BANK OF MAURITIUS LTD</option>
							  <option value="94">STATE BANK OF MYSORE</option>
							  <option value="95">STATE BANK OF PATIALA</option>
							  <option value="96">STATE BANK OF TRAVANCORE</option>
							  <option value="97">SUMITOMO MITSUI BANKING CORPORATION</option>
							  <option value="98">SYNDICATE BANK</option>
							  <option value="99">TAMILNAD MERCANTILE BANK LTD</option>
							  <option value="100">THANE BHARAT SAHAKARI BANK LTD</option>
							  <option value="101">THE A.P. MAHESH CO-OP URBAN BANK LTD.</option>
							  <option value="102">THE AHMEDABAD MERCANTILE CO-OPERATIVE BANK LTD</option>
							  <option value="103">THE ANDHRA PRADESH STATE COOP BANK LTD</option>
							  <option value="104">THE BANK OF NOVA SCOTIA</option>
							  <option value="105">THE BHARAT CO-OPERATIVE BANK (MUMBAI) LTD</option>
							  <option value="106">THE COSMOS CO-OPERATIVE BANK LTD</option>
							  <option value="107">THE FEDERAL BANK LTD</option>
							  <option value="108">THE GADCHIROLI DISTRICT CENTRAL COOPERATIVE BANK LTD</option>
							  <option value="109">THE GREATER BOMBAY CO-OP BANK LTD</option>
							  <option value="110">THE GUJARAT STATE CO-OPERATIVE BANK LTD</option>
							  <option value="111">THE JALGAON PEOPLES CO-OP BANK</option>
							  <option value="112">THE JAMMU AND KASHMIR BANK LTD</option>
							  <option value="113">THE KALUPUR COMMERCIAL CO OP BANK LTD</option>
							  <option value="114">THE KALYAN JANATA SAHAKARI BANK LTD</option>
							  <option value="115">THE KANGRA CENTRAL CO-OP BANK LIMITED</option>
							  <option value="116">THE KANGRA COOPERATIVE BANK LTD</option>
							  <option value="117">THE KARAD URBAN CO-OP BANK LTD</option>
							  <option value="118">THE KARNATAKA STATE CO-OPERATIVE APEX BANK LTD, BANGALORE</option>
							  <option value="119">THE LAKSHMI VILAS BANK LTD</option>
							  <option value="120">THE MEHSANA URBAN COOPERATIVE BANK LTD</option>
							  <option value="121">THE MUNICIPAL CO OPERATIVE BANK LTD MUMBAI</option>
							  <option value="122">THE NAINITAL BANK LIMITED</option>
							  <option value="123">THE NASIK MERCHANTS CO-OP BANK LTD., NASHIK</option>
							  <option value="124">THE RAJASTHAN STATE COOPERATIVE BANK LTD.</option>
							  <option value="125">THE RATNAKAR BANK LTD</option>
							  <option value="126">THE ROYAL BANK OF SCOTLAND NV</option>
							  <option value="127">THE SARASWAT CO-OPERATIVE BANK LTD</option>
							  <option value="128">THE SEVA VIKAS CO-OPERATIVE BANK LTD  (SVB)</option>
							  <option value="129">The Shamrao Vithal Co-op Bank Ltd</option>
							  <option value="130">THE SURAT DISTRICT CO OPERATIVE BANK LTD.</option>
							  <option value="131">THE SURAT PEOPLES CO-OP BANK LTD</option>
							  <option value="132">THE SUTEX CO.OP. BANK LTD.</option>
							  <option value="133">THE TAMILNADU STATE APEX COOPERATIVE BANK LIMITED</option>
							  <option value="134">THE THANE DISTRICT CENTRAL CO-OP BANK LTD</option>
							  <option value="135">THE THANE JANATA SAHAKARI BANK LTD</option>
							  <option value="136">THE VARACHHA CO-OP. BANK LTD.</option>
							  <option value="137">THE VISHWESHWAR SAHAKARI BANK LTD.,PUNE</option>
							  <option value="138">THE WEST BENGAL STATE COOPERATIVE BANK LIMITED</option>
							  <option value="139">TUMKUR GRAIN MERCHANTS COOPERATIVE BANK LTD.,</option>
							  <option value="140">UBS AG</option>
							  <option value="141">UCO BANK</option>
							  <option value="142">UNION BANK OF INDIA</option>
							  <option value="143">UNITED BANK OF INDIA</option>
							  <option value="144">VASAI VIKAS SAHAKARI BANK LTD.</option>
							  <option value="145">VIJAYA BANK</option>
							  <option value="146">WESTPAC BANKING CORPORATION</option>
							  <option value="147">WOORI BANK</option>
							  <option value="148">YES BANK LIMITD</option>
							  <option value="149">BANK INTERNASIONAL INDONESIA</option>
							  <option value="150">INDUSTRIAL AND COMMERCIAL BANK OF CHINA LIMITED</option>
							  <option value="151">JANASEVA SAHAKARI BANK (BORIVLI) LTD</option>
							  <option value="152">RAJGURUNAGAR SAHAKARI BANK LTD.</option>
							  <option value="153">SOLAPUR JANATA SAHKARI BANK LTD.SOLAPUR</option>
							  <option value="154">THE DELHI STATE COOPERATIVE BANK LTD.</option>
							  <option value="155">THE SAHEBRAO DESHMUKH CO-OP. BANK LTD.</option>
							  <option value="156">UNITED OVERSEAS BANK</option>
							  <option value="157">ZILA SAHAKARI BANK LTD GHAZIABAD</option>
							  <option value="158">THE AKOLA JANATA COMMERCIAL COOPERATIVE BANK</option>
							  <option value="159">DEUTSCHE SECURITIES INDIA PRIVATE LIMITED</option>
							  <option value="160">NAGAR URBAN CO OPERATIVE BANK</option>
							  <option value="161">BHARATIYA MAHILA BANK LIMITED</option>
							  <option value="162">IDBI BANK LTD</option>
							  <option value="163">KERALA GRAMIN BANK</option>
							  <option value="164">PRAGATHI KRISHNA GRAMIN BANK</option>
							  <option value="165">THE ZOROASTRIAN COOPERATIVE BANK LIMITED</option>
							  <option value="166">SHIKSHAK SAHAKARI BANK LIMITED</option>
							  <option value="167">DOHA BANK QSC</option>
							  <option value="168">EXPORT IMPORT BANK OF INDIA</option>
							  <option value="169">THE HASTI COOP BANK LTD</option>
							  <option value="170">BANDHAN BANK LIMITED</option>
							  <option value="171">IDFC BANK LIMITED</option>
							  <option value="172">SURAT NATIONAL COOPERATIVE BANK LIMITED</option>
							  <option value="173">INDUSTRIAL BANK OF KOREA</option>
							  <option value="174">NATIONAL BANK OF ABU DHABI PJSC</option>
							  <option value="175">KEB Hana Bank</option>
							  <option value="176">SAMARTH SAHAKARI BANK LTD</option>
							  <option value="177">SHIVALIK MERCANTILE CO OPERATIVE BANK LTD</option>
							  <option value="178">THE PANDHARPUR URBAN CO OP. BANK LTD. PANDHARPUR</option>
							  <option value="179">DEOGIRI NAGARI SAHAKARI BANK LTD. AURANGABAD</option>
							  <option value="180">HIMACHAL PRADESH STATE COOPERATIVE BANK LTD</option>
							  <option value="181">MMAHARASHTRA GRAMIN BANK</option>
						   </select>
						</div>
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-name" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-state`">State</label>
				  </div>
				  <div uif-append="bank-state`" class=" col-sm-8">
					 <div>
						<div class="" uif-fbtype="wrapper" uif-uqid="77fba7a7-9252-8198-cc04-b6cbbc327a3b">
						   <select uif-fbtype="element" name="bank-state`" id="bank-state" uif-uqid="77fba7a7-9252-8198-cc04-b6cbbc327a3b" disabled="true" class="form-control">
							  <option value="" selected="">Select State</option>
						   </select>
						</div>
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-state`" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-city">City</label>
				  </div>
				  <div uif-append="bank-city" class=" col-sm-8">
					 <div>
						<div class="" uif-fbtype="wrapper" uif-uqid="758cd1c9-51f3-117c-7ffb-58e6985cda10">
						   <select uif-fbtype="element" name="bank-city" id="bank-city" uif-uqid="758cd1c9-51f3-117c-7ffb-58e6985cda10" disabled="true" class="form-control">
							  <option value="" selected="">Select City</option>
						   </select>
						</div>
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-city" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-branch">Branch</label>
				  </div>
				  <div uif-append="bank-branch" class=" col-sm-8">
					 <div>
						<div class="" uif-fbtype="wrapper" uif-uqid="2dafebba-0332-b729-4aab-3c55fbcfa777">
						   <select uif-fbtype="element" name="bank-branch" id="bank-branch" uif-uqid="2dafebba-0332-b729-4aab-3c55fbcfa777" disabled="true" class="form-control">
							  <option value="" selected="">Select Branch</option>
						   </select>
						</div>
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-branch" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			</div>
		 </div>
		 <div uif-append="ifscFields">
			<div class="">
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label required" for="bank-ifsc">IFSC Code</label>
				  </div>
				  <div uif-append="bank-ifsc" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="eeaf67cd-1340-bc06-b28c-9d6e44bbaa64">
						<input uif-fbtype="element" type="text" name="bank-ifsc" class="form-control" id="bank-ifsc" uif-uqid="eeaf67cd-1340-bc06-b28c-9d6e44bbaa64">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-ifsc" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			</div>
		 </div>
		 <div uif-append="disabledBankFields" id="disabledBankFields">
			<div class="">
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-name-text">Bank Name</label>
				  </div>
				  <div uif-append="bank-name-text" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="a4aeac42-d6a5-c9b4-fcf5-acb27be5178f">
						<input uif-fbtype="element" type="text" disabled="true" name="bank-name-text" class="form-control" id="bank-name-text" uif-uqid="a4aeac42-d6a5-c9b4-fcf5-acb27be5178f">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-name-text" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-state-text">State</label>
				  </div>
				  <div uif-append="bank-state-text" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="75524be7-ec8d-6f9e-2830-6399b631c041">
						<input uif-fbtype="element" type="text" disabled="true" name="bank-state-text" class="form-control" id="bank-state-text" uif-uqid="75524be7-ec8d-6f9e-2830-6399b631c041">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-state-text" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-city-text">City</label>
				  </div>
				  <div uif-append="bank-city-text" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="89a31a47-52fc-b6a0-9084-d02a64808bbf">
						<input uif-fbtype="element" type="text" disabled="true" name="bank-city-text" class="form-control" id="bank-city-text" uif-uqid="89a31a47-52fc-b6a0-9084-d02a64808bbf">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-city-text" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			   <div class="form-group ">
				  <div class="col-sm-4 text-left">
					 <label class="control-label " for="bank-branch-text">Branch</label>
				  </div>
				  <div uif-append="bank-branch-text" class=" col-sm-8">
					 <div class="" uif-fbtype="wrapper" uif-uqid="c8db9d66-0bb5-19c5-60ca-4f4dfb27e891">
						<input uif-fbtype="element" type="text" disabled="true" name="bank-branch-text" class="form-control" id="bank-branch-text" uif-uqid="c8db9d66-0bb5-19c5-60ca-4f4dfb27e891">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="bank-branch-text" class="help-text text-danger"></span>
					 <span class="help-text text-success" id="success-message"></span>
				  </div>
			   </div>
			</div>
		 </div>
		 <h4 class="">KYC Documents</h4>
		 <hr>
		 <div uif-append="address">
			<div class="mb20">
			   <div class="form-group file-group">
				  <div class="col-sm-4 text-left">
					 <label class="control-label required" for="address-type">Address proof</label>
				  </div>
				  <div uif-append="address-type" class="col-sm-8 address-type ">
					 <div>
						<div class="" uif-fbtype="wrapper" uif-uqid="38975507-b3ba-8541-287a-0abc2790763d">
						   <select uif-fbtype="element" oldvalue="" name="address-type" id="address-type" uif-uqid="38975507-b3ba-8541-287a-0abc2790763d" class="form-control">
							  <option value="passport">Passport</option>
							  <option value="credstm">Credit card statement</option>
							  <option value="elec">Electricity bill</option>
							  <option value="aadharcard">Aadhar card</option>
							  <option value="telmob">Telephone/ mobile bill</option>
							  <option value="ration">Ration card</option>
							  <option value="bankcred">Bank account/ credit card statement</option>
							  <option value="voterIdCard">Voter's identity card</option>
						   </select>
						</div>
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="address-type" class="help-text text-danger"></span>
				  </div>
			   </div>
			   <div class="form-group file-group">
				  <div class="col-sm-4 text-left">
				  </div>
				  <div uif-append="address-file" class="col-sm-8 address-file ">
					 <div>
						<div class="file address-attachments-wrapper" uif-fbtype="wrapper" uif-uqid="c7d97eeb-70ac-2841-ea98-3133f9504f85">
						   <div id="blinx-wrapper-61" class="fileUploader-default play-arena">
							  <div uif-id="address-uploader" class="file-uploader">
								 <div class="col-md-9 no-padding">
									<div class="file-container">
									   <span class="help-text file-name">
									   <span>
									   <span class="help-text gray-text">Accepted file formats: png, jpg &amp; pdf</span>
									   </span>
									   </span>
									</div>
									<div class="help-text text-danger err-message no-padding"></div>
								 </div>
								 <div class="col-md-3 edit-links no-padding">
									<div class=" fileinput-button col-md-12  no-padding">
									   <span class="action-links">Upload</span>
									   <input type="file" name="file" data-field="address-type" accept="image/jpeg, image/png, application/pdf">
									</div>
								 </div>
							  </div>
						   </div>
						</div>
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="address-file" class="help-text text-danger"></span>
				  </div>
			   </div>
			</div>
		 </div>
		 <div uif-append="cheque">
			<div class="mb20">
			   <div class="form-group file-group">
				  <div class="col-sm-4 text-left">
					 <label class="control-label required" for="cheque-file">Cancelled Cheque</label>
				  </div>
				  <div uif-append="cheque-file" class="col-sm-8 cheque-file ">
					 <div>
						<div class="file cheque-attachments-wrapper" uif-fbtype="wrapper" uif-uqid="3a569b63-5513-9a14-997c-39cd57e5be37">
						   <div id="blinx-wrapper-62" class="fileUploader-default play-arena">
							  <div uif-id="cheque-uploader" class="file-uploader">
								 <div class="col-md-9 no-padding">
									<div class="file-container">
									   <span class="help-text file-name">
									   <span>
									   <span class="help-text gray-text">Accepted file formats: png, jpg &amp; pdf</span>
									   </span>
									   </span>
									</div>
									<div class="help-text text-danger err-message no-padding"></div>
								 </div>
								 <div class="col-md-3 edit-links no-padding">
									<div class=" fileinput-button col-md-12  no-padding">
									   <span class="action-links">Upload</span>
									   <input type="file" name="file" data-field="cheque-hidden" accept="image/jpeg, image/png, application/pdf">
									</div>
								 </div>
							  </div>
						   </div>
						</div>
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="cheque-file" class="help-text text-danger"></span>
				  </div>
			   </div>
			   <div class="form-group file-group">
				  <div class="col-sm-4 text-left">
				  </div>
				  <div uif-append="cheque-hidden" class="col-sm-8 cheque-hidden ">
					 <div class="" uif-fbtype="wrapper" uif-uqid="8f76486d-db19-033e-920f-1692f9d56a4b">
						<input uif-fbtype="element" type="text" value="cheque" name="cheque-hidden" class="hide" id="cheque-hidden" uif-uqid="8f76486d-db19-033e-920f-1692f9d56a4b">
					 </div>
				  </div>
				  <div class="col-sm-4"></div>
				  <div class="col-sm-8">
					 <span uif-field-err-message="cheque-hidden" class="help-text text-danger"></span>
				  </div>
			   </div>
			</div>
		 </div>
	  </div>
   </form>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
   <div class="action-btn">
      <input uif-fbtype="element" type="button" value="Cancel" name="close" class="btn close-btn btn-default" >
      <input uif-fbtype="element" type="submit" value="Save" name="submit" class="btn submit-btn btn-primary" >
   </div>
</div>