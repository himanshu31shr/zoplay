<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add Store Details</h4>
</div>

<div class="modal-body">
	<form class="form-horizontal" name="personal-details" autocomplete="off">
	   <div class="form-fields">
		  <div uif-append="formFields">
			 <div class="">
				<div class="form-group ">
				   <div class="col-sm-4 text-left">
					  <label class="control-label required" for="display_name">Display Name</label>
				   </div>
				   <div uif-append="display_name" class=" col-sm-8">
					  <div class="" uif-fbtype="wrapper" uif-uqid="353047c3-49e4-feb1-9fcf-b534c45e6d25">
						 <input uif-fbtype="element" type="text" name="display_name" class="form-control" id="display_name" uif-uqid="353047c3-49e4-feb1-9fcf-b534c45e6d25" maxlength="22">
					  </div>
				   </div>
				   <div class="col-sm-4"></div>
				   <div class="col-sm-8">
					  <span uif-field-err-message="display_name" class="help-text text-danger"></span>
					  <span class="help-text text-success" id="success-message"></span>
				   </div>
				</div>
				<div class="form-group ">
				   <div class="col-sm-4 text-left">
					  <label class="control-label required" for="description">Business Description</label>
				   </div>
				   <div uif-append="description" class=" col-sm-8">
					  <div>
						 <div class="" uif-fbtype="wrapper" uif-uqid="d650eb16-2a36-b52a-784e-78fcdd6f1431">
							<textarea uif-fbtype="element" class="form-control" uif-uqid="d650eb16-2a36-b52a-784e-78fcdd6f1431" maxlength="1000" name="description" id="description"></textarea>
						 </div>
					  </div>
				   </div>
				   <div class="col-sm-4"></div>
				   <div class="col-sm-8">
					  <span uif-field-err-message="description" class="help-text text-danger"></span>
					  <span class="help-text text-success" id="success-message"></span>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</form>
</div>

<div class="modal-footer">
	<div class="action-btn">
		<input uif-fbtype="element" type="button" value="Cancel" name="close" class="btn close-btn btn-default" >
		<input uif-fbtype="element" type="submit" value="Save" name="submit" class="btn submit-btn btn-primary" >
	</div>
</div>