<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add Business Details</h4>
</div>
<div class="modal-body" >
<form class="form-horizontal" name="business-details" autocomplete="off">
   <div class="form-fields">
      <div uif-append="formFields">
         <div class="">
            <div class="form-group ">
               <div class="col-sm-4 text-left">
                  <label class="control-label required" for="business-name">Business name</label>
               </div>
               <div uif-append="business-name" class=" col-sm-8">
                  <div class="" uif-fbtype="wrapper" uif-uqid="9c06efc2-8930-c80c-e909-72ea00b711af">
                     <input uif-fbtype="element" type="text" name="business-name" class="form-control" id="business-name" uif-uqid="9c06efc2-8930-c80c-e909-72ea00b711af">
                  </div>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-8">
                  <span uif-field-err-message="business-name" class="help-text text-danger"></span>
                  <span class="help-text text-success" id="success-message"></span>
               </div>
            </div>
            <div class="form-group ">
               <div class="col-sm-4 text-left">
                  <label class="control-label " for="business-type">Business type</label>
               </div>
               <div uif-append="business-type" class=" col-sm-8">
                  <div>
                     <div class="" uif-fbtype="wrapper" uif-uqid="708f30de-54d2-78ea-f2ed-e239ffacca87">
                        <select uif-fbtype="element" name="business-type" id="business-type" uif-uqid="708f30de-54d2-78ea-f2ed-e239ffacca87" class="form-control">
                           <option value="proprietor" selected="">Proprietor</option>
                           <option value="company">Company</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="col-sm-8">
                  <span uif-field-err-message="business-type" class="help-text text-danger"></span>
                  <span class="help-text text-success" id="success-message"></span>
               </div>
            </div>
         </div>
      </div>
      <div uif-append="panField">
         <div class="mb20">
            <div class="form-group file-group">
               <div class="col-sm-4 text-left">
                  <label class="control-label required" for="pan-id">Personal PAN</label>
               </div>
               <div uif-append="pan-id" class="col-sm-8 pan-id ">
                  <div class="" uif-fbtype="wrapper" uif-uqid="381d5236-329e-e539-71b5-5ca77baaefe4">
                     <input uif-fbtype="element" type="text" name="pan-id" class="form-control" id="pan-id" >
                  </div>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-8">
                  <span uif-field-err-message="pan-id" class="help-text text-danger"></span>
               </div>
            </div>
            <div class="form-group file-group">
               <div class="col-sm-8">
                  <span uif-field-err-message="pan-file" class="help-text text-danger"></span>
               </div>
            </div>
         </div>
      </div>
      <div uif-append="vatField">
         <div class="mb20">
            <div class="form-group file-group">
               <div class="col-sm-4 text-left">
                  <label class="control-label required" for="vat-id">TIN/VAT</label>
               </div>
               <div uif-append="vat-id" class="col-sm-8 vat-id ">
                  <div class="" >
                     <input uif-fbtype="element" type="text" name="vat-id" class="form-control" id="vat-id" >
                  </div>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-8">
                  <span class="help-text text-danger"></span>
               </div>
            </div>
            <div class="form-group file-group">
               <div class="col-sm-8">
                  <span class="help-text text-danger"></span>
               </div>
            </div>
            
         </div>
      </div>
      <div uif-append="tanField">
         <div class="mb20">
            <div class="form-group file-group">
               <div class="col-sm-4 text-left">
                  <label class="control-label required" for="tan-id">TAN</label>
               </div>
               <div uif-append="tan-id" class="col-sm-8 tan-id ">
                  <div class="" uif-fbtype="wrapper" uif-uqid="5a430ffc-0f1f-c2ca-177f-c852f6c860f7">
                     <input uif-fbtype="element" type="text" name="tan-id" class="form-control" id="tan-id" uif-uqid="5a430ffc-0f1f-c2ca-177f-c852f6c860f7" oldvalue="" tocheck="tan-check">
                  </div>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-8">
                  <span uif-field-err-message="tan-id" class="help-text text-danger"></span>
               </div>
            </div>
            <div class="form-group file-group">
               
               <div class="col-sm-8">
                  <span uif-field-err-message="tan-file" class="help-text text-danger"></span>
               </div>
            </div>
         </div>
      </div>
      <div uif-append="businessPanFields" id="businessFileFields" style="display: none;">
         <div class="">
            <div class="form-group file-group">
               <div class="col-sm-4 text-left">
                  <label class="control-label required" for="business_pan">Business PAN</label>
               </div>
               <div uif-append="business_pan" class="col-sm-8 business_pan ">
                  <div class="" uif-fbtype="wrapper" uif-uqid="5042fd5a-b30b-11f0-8256-2a95b48afcd2">
                     <input uif-fbtype="element" type="text" name="business_pan" class="form-control" id="business_pan" uif-uqid="5042fd5a-b30b-11f0-8256-2a95b48afcd2" hidden="true">
                  </div>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-8">
                  <span uif-field-err-message="business_pan" class="help-text text-danger"></span>
               </div>
            </div>
            <div class="form-group file-group">
               <div class="col-sm-8">
                  <span uif-field-err-message="business_pan-file" class="help-text text-danger"></span>
               </div>
            </div>
         </div>
      </div>
  
      
   </div>
</form>
<div class="modal-footer">
 <div class="action-btn">
	
	  <input uif-fbtype="element" type="button" value="Cancel" name="close" class="btn close-btn btn-default" >
	   
	  <input uif-fbtype="element" type="submit" value="Save" name="submit" class="btn submit-btn btn-primary" >
	   
 </div>
</div>