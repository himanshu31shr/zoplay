<?php
	echo $layout['header'];	
?> 
<style type="text/css">
	.cnt-home{background-color: #f3f3f3 !important;	}
</style>
<!--<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/main.css">		-->
<div class="wrapper wrapper-content animated fadeInRight">
	<div ui-view="" class="container-fluid">	
	<div ui-view="" id="listings" class="container-fluid">
		<div class="approvals">			
			<div class="alert alert-success success-response" style="display:none;">
			</div>
			<div class="alert alert-danger danger-response" style="display:none;">
			</div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
					<div class="ibox-title">
						<h3>Add Coupons</h3>
					</div>
                        <div class="ibox-content">
                            <form id="addcoupons" class="form-horizontal" novalidate="novalidate">
                                <div class="form-group">
									<label class="col-sm-2 control-label">
										Coupon Code<span style="color: red">*</span>
									</label>
                                    <div class="col-sm-10">
										<input type="text" name="coupon_code" id="coupon_code" class="form-control" placeholder="Coupon Code" value="<?= ($coupons_data) ? $coupons_data[0]->coupon_code : "" ?>">
									</div>
                                </div> 
								<?php //echo "<pre>"; print_r($coupons_data); die;?>
								<div class="form-group">
									<label class="col-sm-2 control-label">
										Coupon Type<span style="color: red">*</span>
									</label>
                                    <div class="col-sm-10">
										<select class="form-control" id="coupon_type" name="coupon_type">
                                           <option value="">Select a coupon type</option>
										   <option value="0">Percentage</option>
										   <option value="1">Normal</option>                                        
										</select>
										<input type="hidden" value="<?= ($coupons_data)? $coupons_data[0]->coupon_type : "" ?>" id="coupontype" >
									</div>
                                </div> 
								<div class="form-group">
									<label class="col-sm-2 control-label">
										Coupon Value<span style="color: red">*</span>
									</label>
                                    <div class="col-sm-10">
										<input type="text" name="coupon_value" id="coupon_value" class="form-control" placeholder="Coupon Value" value="<?= ($coupons_data) ? $coupons_data[0]->value : "" ?>">
									</div>
                                </div>  
								<div class="form-group">
									<label class="col-sm-2 control-label">
										Order Range<span style="color: red">*</span>
									</label>
                                    <div class="col-sm-10">
										<input type="number" name="min_order" id="min_order" class="form-control" placeholder="" value="<?= ($coupons_data) ? $coupons_data[0]->order_range : "" ?>">
									</div>
                                </div>  								
								<div class="form-group">
									<label class="col-sm-2 control-label">
										Coupon Date Time<span style="color: red">*</span>
									</label>
                                    <div class="col-lg-5">
										<input placeholder="From Date Time" class="form-control datepicker " type="text" name="date_from"  id="date_from" value="<?= ($coupons_data) ? $coupons_data[0]->coupon_from_datetime : "" ?>"> 
									</div>
									<div class="col-lg-5">
										<input placeholder="To Date Time" class="form-control datepicker " type="text" name="date_to"  id="date_to" value="<?= ($coupons_data) ? $coupons_data[0]->	coupon_to_datetime : "" ?>"> 
									</div>
                                </div>                           					
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">                                      
                                        <button class="btn btn-primary" name="button" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>			
<script type="text/javascript">
$('.datepicker').datetimepicker();
$('#coupon_type').val($('#coupontype').val());
	$("#addcoupons").validate({
	rules: {
			coupon_code: {
				required: true,			
			},
			coupon_type: {
				required: true,			
			},
			coupon_value: {
				required: true,
			},
			date_from: {
				required: true,
			},
			date_to: {
				required: true,
			},
			
	},
	messages: {
		coupon_code:{
				required:"Please enter a coupon code",			
			},
		coupon_type:{
				required:"Please enter a coupon type",			
			},
		coupon_value:{
				 required:"please enter a coupon value",
			},
		date_to:{
				required:"Please enter a date",			
			},
		date_from:{
				 required:"please enter a date",
			}
	},
	submitHandler: function (form){  			
		var url = window.location.href; 
		var coupon_id = /[^/]*$/.exec(url)[0];						
		var formdata = new FormData(form);
		formdata.append('coupon_id',coupon_id);
		$.ajax({
			url  : "<?= site_url() ?>seller/save_coupons",
			type : "POST",			
			data : formdata,
			processData : false,
			contentType : false,
			cache : false,			
		beforeSend : function()
		{
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},
		success : function(result)
		{
			if(result.status == true)
			{
				$(".success-response").css("display" , "block");
				$(".success-response").html(result.msg);
				window.location.href = "<?= site_url() ?>seller/coupons_list";	
			}else{
				$(".danger-response").css("display" , "block");
				$(".danger-response").html('Something went wrong');
			}
		},
		error : function()
		{
			$(".danger-response").css("display" , "block");
			$(".danger-response").html('Something went wrong');
		},
		complete : function()
		{
			$('.overlay').remove();
		}				
	})
}
});
</script>   
<?php
	echo $layout['footer'];
?>