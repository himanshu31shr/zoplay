<?php
   echo $layout['header'];
?>
<div class="container">
   <div id="sub-app-container">
      <div id="blinx-wrapper-23" class="returnOrdersMainComposite-default play-arena">
         <section class="app-content">
            <div class="orders-header">
               <div class="order-search-container">
                  <div class="order-search">
                     <div id="blinx-wrapper-24" class="orderSearch-default play-arena">
                        <section class="input-search"><input type="text" maxlength="64" class="query search-order-itemId" placeholder="Return ID/Search Order ID/Item ID"><label class="search-input" for="search-input"><i class="fa fa-search"></i></label></section>
                        <ul class="typeahead order-search-dropdown-menu" typeahead-popup="" matches="matches" active="activeIdx" select="select(activeIdx)" query="query" position="position">
                           <li class="return-id">
                              <a tabindex="-1" href="#dashboard/returns/search?type=return_id&amp;id="><span class="id-type">Return Id:</span> <strong class="search-value"></strong></a>
                           </li>
                           <li class="order-id">
                              <a tabindex="-1" href="#dashboard/returns/search?type=order_id&amp;id="><span class="id-type">Order Id:</span> <strong class="search-value"></strong></a>
                           </li>
                           <li class="order-item-id">
                              <a tabindex="-1" href="#dashboard/returns/search?type=order_item_id&amp;id="><span class="id-type">Order Item Id:</span> <strong class="search-value"></strong></a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <span class="return-orders-images search"></span>
               </div>
               <div class="row">
                  <h3 class="col-md-6">Returns</h3>
               </div>
            </div>
            <div class="return-orders-composite-container font-rlight">
               <div id="blinx-wrapper-25" class="returnOrdersComposite-default play-arena">
                  <div class="return-orders-tab">
                     <div id="blinx-wrapper-26" class="navigation-tabs play-arena">
                        <ul class="navigation horizontal-navigation ">
                           <li class="nav-item selected" value="approved">
                              <span class="nav-span" data-label="Approved">Approved</span>
                           </li>
                           <li class="nav-item   " value="in_transit">
                              <span class="nav-span" data-label="In Transit">In Transit</span>
                           </li>
                           <li class="nav-item   " value="completed">
                              <span class="nav-span" data-label="Completed">Completed</span>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="orders-active-composite-container">
               <div id="blinx-wrapper-29" class="activeOrdersShipmentStateComposite-default play-arena">
                  <div class="active-orders-main-list-composite">
                     <div id="blinx-wrapper-31" class="activeOrdersShipmentMainListComposite-default play-arena">
                        <div class="container-fluid remove-padding">
                           <div class="row-fluid">
                              <div class="col-md-2 active-orders-filter-holder">
                                 <div class="row-fluid clearfix">
                                    <div class="font-rblue pull-right btn btn-default filter-reset">Reset</div>
                                    <div class="dummy-margin"></div>
                                    <div class="row-fluid search-container">
                                       <div class="search-title">BY SKU</div>
                                       <div class="search-holder sku">
                                          <input class="form-control" type="text" placeholder="Search SKU">
                                          <span class="search-icon sku"></span>
                                       </div>
                                    </div>
                                    <div class="row-fluid search-container">
                                       <div class="search-title">BY FSN</div>
                                       <div class="search-holder fsn">
                                          <input class="form-control" type="text" placeholder="Search FSN">
                                          <span class="search-icon fsn"></span>
                                       </div>
                                    </div>
                                    <div class="active-orders-filter-container">
                                       <div id="blinx-wrapper-32" class="filterWidget-default play-arena">
                                          <div class="cf-filter-container">
                                             <div class="cf-filters cf-filter-cont-0">
                                                <div class="cf-head-container cf-filter-title-0">
                                                   <div class="cf-title cf-title-c">
                                                      <div class="cf-float-left cf-title-c">ORDER DATE</div>
                                                      <i class="fa fa-arrow fa-angle-down cf-float-right cf-title-c"></i>
                                                      <br style="clear:both"> 
                                                   </div>
                                                </div>
                                                <div class="cf-content cf-filter-0">
                                                   <ul class="list-style-none cf-list-container">
                                                      <li class="cf-list">
                                                         <div class="daterangepicker-order_approval" name="order_approval">
                                                            <div id="blinx-wrapper-33" class="daterangepicker-default play-arena">
                                                               <div class="daterangepicker-container">
                                                                  <input class="daterangepicker form-control" type="text" placeholder="On / From-To" value="">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                             <div class="cf-filters cf-filter-cont-1">
                                                <div class="cf-head-container cf-filter-title-1">
                                                   <div class="cf-title cf-title-c">
                                                      <div class="cf-float-left cf-title-c">SHIPMENT TYPE</div>
                                                      <i class="fa fa-arrow fa-angle-down cf-float-right cf-title-c"></i>
                                                      <br style="clear:both"> 
                                                   </div>
                                                </div>
                                                <div class="cf-content cf-filter-1">
                                                   <ul class="list-style-none cf-list-container">
                                                      <li class="cf-list">
                                                         <input type="radio" id="filter-option-0-1" class="cf-radio selected" value="all_products" name="filterByShipmentType">
                                                         <label for="filter-option-0-1" class="cf-name">All</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="radio" id="filter-option-1-1" class="cf-radio" value="single_product" name="filterByShipmentType">
                                                         <label for="filter-option-1-1" class="cf-name">Single</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="radio" id="filter-option-2-1" class="cf-radio" value="multi_product" name="filterByShipmentType">
                                                         <label for="filter-option-2-1" class="cf-name">Multi</label>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                             <div class="cf-filters cf-filter-cont-2">
                                                <div class="cf-head-container cf-filter-title-2">
                                                   <div class="cf-title cf-title-c">
                                                      <div class="cf-float-left cf-title-c">URGENCY</div>
                                                      <i class="fa fa-arrow fa-angle-down cf-float-right cf-title-c"></i>
                                                      <br style="clear:both"> 
                                                   </div>
                                                </div>
                                                <div class="cf-content cf-filter-2">
                                                   <ul class="list-style-none cf-list-container">
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-0-2" class="cf-checkbox" value="breaching_already" name="filterByUrgency">
                                                         <label for="filter-option-0-2" class="cf-name">SLA Breached</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-1-2" class="cf-checkbox" value="breaching_today" name="filterByUrgency">
                                                         <label for="filter-option-1-2" class="cf-name">SLA Breaching Soon</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-2-2" class="cf-checkbox" value="breached_afterToday" name="filterByUrgency">
                                                         <label for="filter-option-2-2" class="cf-name">Others</label>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                             <div class="cf-filters cf-filter-cont-3">
                                                <div class="cf-head-container cf-filter-title-3">
                                                   <div class="cf-title cf-title-c">
                                                      <div class="cf-float-left cf-title-c">DISPATCH TYPE</div>
                                                      <i class="fa fa-arrow fa-angle-down cf-float-right cf-title-c"></i>
                                                      <br style="clear:both"> 
                                                   </div>
                                                </div>
                                                <div class="cf-content cf-filter-3">
                                                   <ul class="list-style-none cf-list-container">
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-0-3" class="cf-checkbox" value="EXPRESS" name="dispatchServiceTiers">
                                                         <label for="filter-option-0-3" class="cf-name">Express</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-1-3" class="cf-checkbox" value="REGULAR" name="dispatchServiceTiers">
                                                         <label for="filter-option-1-3" class="cf-name">Standard</label>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                             <div class="cf-filters cf-filter-cont-4">
                                                <div class="cf-head-container cf-filter-title-4">
                                                   <div class="cf-title cf-title-c">
                                                      <div class="cf-float-left cf-title-c">SPECIAL FILTERS</div>
                                                      <i class="fa fa-arrow fa-angle-down cf-float-right cf-title-c"></i>
                                                      <br style="clear:both"> 
                                                   </div>
                                                </div>
                                                <div class="cf-content cf-filter-4">
                                                   <ul class="list-style-none cf-list-container">
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-0-4" class="cf-checkbox" value="multiple_product" name="filterBySpecialFilters">
                                                         <label for="filter-option-0-4" class="cf-name">Multi Product</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-1-4" class="cf-checkbox" value="multiple_quantity" name="filterBySpecialFilters">
                                                         <label for="filter-option-1-4" class="cf-name">Multi Quantity</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-2-4" class="cf-checkbox" value="replacement" name="filterBySpecialFilters">
                                                         <label for="filter-option-2-4" class="cf-name">Replacement Order</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-3-4" class="cf-checkbox" value="forms_required" name="filterBySpecialFilters">
                                                         <label for="filter-option-3-4" class="cf-name">Forms Required</label>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                             <div></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-10 remove-padding card-gutter-left">
                                 <div class="row-fluid">
                                    <div class="active-orders-list-composite-container">
                                       <div id="blinx-wrapper-34" class="activeOrdersShipmentList-default play-arena">
                                          <div class="active-orders-list-state-container">
                                             <div id="blinx-wrapper-38" class="activeOrdersShipmentToPack-default play-arena">
                                                <div class="bulk-file-status-container" style="display: none;"></div>
                                                <div class="orders-in-processing-container">
                                                </div>
                                                <div class="orders-list-container">
                                                   <div class="orders-new-list"></div>
                                                </div>
                                                <div class="bulk-file-download-popup-container"></div>
                                                <div class="processing-label-download-popup-container"></div>
                                             </div>
                                          </div>
                                          <div class="active-orders-list-holder">
                                             <div id="blinx-wrapper-35" class="activeOrdersShipmentListView-default play-arena">
                                                <div>
                                                   <div class="active-orders-list-page-cont">
                                                      <div id="blinx-wrapper-40" class="paginator-default play-arena">
                                                         <div class="paginator">
                                                            <div class="view">
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="active-orders-list-cont">
                                                      <div id="blinx-wrapper-39" class="listView-default play-arena">
                                                         <div class="fk-list-view" style="height:;overflow:">
                                                            <ul class="fk-list"></ul>
                                                            <div style="display: none" class="fk-empty-error"><span></span></div>
                                                            <div style="display: none" class="fk-empty-text"><span></span></div>
                                                            <div class="loader-container hide">
                                                               <div class="loader-inner ball-pulse">
                                                                  <div></div>
                                                                  <div></div>
                                                                  <div></div>
                                                               </div>
                                                            </div>
                                                            <div class="no-data-container fk-list-message">
																<table id="example" class="stripe dataTable" cellspacing="0" width="100%">
																	<thead>
																		<tr>
																			<td>Date</td>
																			<td>Order Ref #</td>
																			<td>Order Amount</td>
																			<td>Comments</td>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																	</tbody>
																</table>
                                                            <div class="error-container fk-list-message hide">Failed to load</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="loader-cont"></div>
                                                   <div class="error-container"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="scan-order-continer"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="active-orders-cancel-popup-container"></div>
            <div class="active-orders-cancel-item-popup-container"></div>
            <div class="active-orders-to-pack-popup-container"></div>
            <div class="active-orders-rtd-popup-container"></div>
            <div class="active-orders-pickup-popup-container"></div>
            <div class="active-orders-interstitial-popup-container"></div>
            <div class="active-orders-proof-of-pickup-popup-container"></div>
            <div class="health-meter">
               <div id="blinx-wrapper-28" class="healthMeter-default play-arena">
                  <div class="performance-overylay"></div>
                  <div class="performance-check-notifier" style="display: none;"></div>
               </div>
            </div>
         </div>
               </div>
            </div>
            <div id="returns-joyride-container"></div>
         </section>
      </div>
   </div>
</div>
<?php
   echo $layout['footer'];
?>