<?php
   echo $layout['header'];
?>
<style>
.fk-list-message{margin:0;}
.app-content{min-height:600px;}
</style>
<div class="container">
   <div id="sub-app-container">
      <div id="blinx-wrapper-23" class="returnOrdersMainComposite-default play-arena">
         <section class="app-content">
            <div class="orders-header">
               <div class="row">
                  <h3 class="col-md-6">Advertising</h3>
               </div>
            </div>
                  <div class="active-orders-main-list-composite">
                     <div id="blinx-wrapper-31" class="activeOrdersShipmentMainListComposite-default play-arena">
                        <div class="container-fluid remove-padding">
                           <div class="row-fluid">
                              <div class="col-md-12 remove-padding card-gutter-left">
                                 <div class="row-fluid">
                                    <div class="active-orders-list-composite-container">
                                       <div id="blinx-wrapper-34" class="activeOrdersShipmentList-default play-arena">
                                          <div class="active-orders-list-state-container">
                                             <div id="blinx-wrapper-38" class="activeOrdersShipmentToPack-default play-arena">
                                                <div class="bulk-file-status-container" style="display: none;"></div>
                                                <div class="orders-in-processing-container">
                                                </div>
                                                <div class="orders-list-container">
                                                   <div class="orders-new-list"></div>
                                                </div>
                                                <div class="bulk-file-download-popup-container"></div>
                                                <div class="processing-label-download-popup-container"></div>
                                             </div>
                                          </div>
                                          <div class="active-orders-list-holder">
                                             <div id="blinx-wrapper-35" class="activeOrdersShipmentListView-default play-arena">
                                                <div>
                                                   <div class="active-orders-list-page-cont">
                                                      <div id="blinx-wrapper-40" class="paginator-default play-arena">
                                                         <div class="paginator">
                                                            <div class="view">
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="active-orders-list-cont">
                                                      <div id="blinx-wrapper-39" class="listView-default play-arena">
                                                         <div class="fk-list-view" style="height:;overflow:">
                                                            <ul class="fk-list"></ul>
                                                            <div style="display: none" class="fk-empty-error"><span></span></div>
                                                            <div style="display: none" class="fk-empty-text"><span></span></div>
                                                            <div class="loader-container hide">
                                                               <div class="loader-inner ball-pulse">
                                                                  <div></div>
                                                                  <div></div>
                                                                  <div></div>
                                                               </div>
                                                            </div>
                                                            <div class="no-data-container fk-list-message">
															<div id="active-list-header">
																<div id="active-list-actionbar">
																	<button class="btn fk-btn select-all"><input class="select-all-checkbox hand-cursor" type="checkbox"></button>
																	<button class="btn fk-btn resume-button" data-toggle="adsActiveList-tooltip" data-trigger="hover" data-animation="true" data-title="Resuming a campaign will begin serving ads. You may resume a campaign that is paused." data-placement="top" data-original-title="" title=""> <i class="action-icon fa fa-play"></i> Resume </button>
																	<button class="btn fk-btn pause-button" data-toggle="adsActiveList-tooltip" data-trigger="hover" data-animation="true" data-title="Pause will temporarily stop a campaign from serving. It can be resumed anytime before the last date of the campaign." data-placement="top" data-original-title="" title=""> <i class="action-icon fa fa-pause"></i> Pause </button>
																	<button class="btn fk-btn abort-button" data-toggle="adsActiveList-tooltip" data-trigger="hover" data-animation="true" data-title="Abort will end the campaign permanently" data-placement="top" data-original-title="" title=""> <i class="action-icon fa fa-stop"></i> Abort </button>
																</div>
															</div>
																<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
																	<thead>
																		<tr>
																			<th>Campaign Name	</th>
																			<th>Status</th>
																			<th>Start Date</th>
																			<th>End Date	</th>
																			<th>Remaining budget 	</th>
																			<th>Ad Views 	</th>
																			<th>Actions </th>
																			<th>Total Revenue	</th>
																			<th>Modify  </th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<th></th>
																			<th></th>
																			<th></th>
																			<th></th>
																			<th></th>
																			<th></th>
																			<th></th>
																			<th></th>
																			<th></th>
																		</tr>
																	</tbody>
																</table>
                                                            <div class="error-container fk-list-message hide">Failed to load</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="loader-cont"></div>
                                                   <div class="error-container"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="scan-order-continer"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="active-orders-cancel-popup-container"></div>
            <div class="active-orders-cancel-item-popup-container"></div>
            <div class="active-orders-to-pack-popup-container"></div>
            <div class="active-orders-rtd-popup-container"></div>
            <div class="active-orders-pickup-popup-container"></div>
            <div class="active-orders-interstitial-popup-container"></div>
            <div class="active-orders-proof-of-pickup-popup-container"></div>
            <div class="health-meter">
               <div id="blinx-wrapper-28" class="healthMeter-default play-arena">
                  <div class="performance-overylay"></div>
                  <div class="performance-check-notifier" style="display: none;"></div>
               </div>
            </div>
         </div>
               </div>
            </div>
            <div id="returns-joyride-container"></div>
         </section>
      </div>
   </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php
   echo $layout['footer'];
?>