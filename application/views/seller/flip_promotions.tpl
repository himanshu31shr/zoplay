<?php
   echo $layout['header'];
?>
<style>
.app-content {
	padding-bottom:0px;
    padding-top: 20px;
}
.fk-list-message {
    margin: 1em 0em 0em 1em;
    background: #fff;
    padding: 1em 2em;
    min-height: 500px;
    margin-bottom: 1em;
}
</style>
<div class="container">
   <div id="sub-app-container">
      <div id="blinx-wrapper-23" class="returnOrdersMainComposite-default play-arena">
         <section class="app-content">
            <div class="orders-header">
               <div class="row">
                  <h3 class="col-md-6">Org Promotions</h3>
               </div>
            </div>
            <div class="return-orders-composite-container font-rlight">
               <div id="blinx-wrapper-25" class="returnOrdersComposite-default play-arena">
                  <div class="return-orders-tab">
                     <div id="blinx-wrapper-26" class="navigation-tabs play-arena">
                        <ul class="navigation horizontal-navigation ">
                           <li class="nav-item selected" value="approved">
                              <span class="nav-span" data-label="Approved">Not Opted In</span>
                           </li>
                           <li class="nav-item   " value="in_transit">
                              <span class="nav-span" data-label="In Transit">Opted In</span>
                           </li>
                           <li class="nav-item   " value="completed">
                              <span class="nav-span" data-label="Completed">Opted Out</span>
                           </li> 
						   <li class="nav-item   " value="completed">
                              <span class="nav-span" data-label="Completed">Expired</span>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="orders-active-composite-container">
               <div id="blinx-wrapper-29" class="activeOrdersShipmentStateComposite-default play-arena">
                  <div class="active-orders-main-list-composite">
                     <div id="blinx-wrapper-31" class="activeOrdersShipmentMainListComposite-default play-arena">
                        <div class="container-fluid remove-padding">
                           <div class="row-fluid">
                              <div class="col-md-2 active-orders-filter-holder">
                                 <div class="row-fluid clearfix">
                                    <div class="">
										<h3>Refine</h3>
									</div>
                                    <div class="active-orders-filter-container">
                                       <div id="blinx-wrapper-32" class="filterWidget-default play-arena">
                                          <div class="cf-filter-container">
                                             <div class="cf-filters cf-filter-cont-2">
                                                <div class="cf-head-container cf-filter-title-2">
                                                   <div class="cf-title cf-title-c">
                                                      <div class="cf-float-left cf-title-c">OFFER STATUS</div>
                                                      <i class="fa fa-arrow fa-angle-down cf-float-right cf-title-c"></i>
                                                      <br style="clear:both"> 
                                                   </div>
                                                </div>
                                                <div class="cf-content cf-filter-2">
                                                   <ul class="list-style-none cf-list-container">
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-0-2" class="cf-checkbox" value="breaching_already" name="filterByUrgency">
                                                         <label for="filter-option-0-2" class="cf-name">Ongoing</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-1-2" class="cf-checkbox" value="breaching_today" name="filterByUrgency">
                                                         <label for="filter-option-1-2" class="cf-name">Upcoming</label>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                             <div class="cf-filters cf-filter-cont-3">
                                                <div class="cf-head-container cf-filter-title-3">
                                                   <div class="cf-title cf-title-c">
                                                      <div class="cf-float-left cf-title-c">OFFER TYPE</div>
                                                      <i class="fa fa-arrow fa-angle-down cf-float-right cf-title-c"></i>
                                                      <br style="clear:both"> 
                                                   </div>
                                                </div>
                                                <div class="cf-content cf-filter-3">
                                                   <ul class="list-style-none cf-list-container">
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-0-3" class="cf-checkbox" value="EXPRESS" name="dispatchServiceTiers">
                                                         <label for="filter-option-0-3" class="cf-name">Listing Discount</label>
                                                      </li>
                                                      <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-1-3" class="cf-checkbox" value="REGULAR" name="dispatchServiceTiers">
                                                         <label for="filter-option-1-3" class="cf-name">Basket Discount</label>
                                                      </li>
													  <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-1-3" class="cf-checkbox" value="REGULAR" name="dispatchServiceTiers">
                                                         <label for="filter-option-1-3" class="cf-name">Combo Discount</label>
                                                      </li>
													  <li class="cf-list">
                                                         <input type="checkbox" id="filter-option-1-3" class="cf-checkbox" value="REGULAR" name="dispatchServiceTiers">
                                                         <label for="filter-option-1-3" class="cf-name">Flat Price Discount</label>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-10 remove-padding card-gutter-left">
                                 <div class="row-fluid">
                                    <div class="active-orders-list-composite-container">
                                       <div id="blinx-wrapper-34" class="activeOrdersShipmentList-default play-arena">
                                          <div class="active-orders-list-state-container">
                                             <div id="blinx-wrapper-38" class="activeOrdersShipmentToPack-default play-arena">
                                                <div class="bulk-file-status-container" style="display: none;"></div>
                                                <div class="orders-in-processing-container">
                                                </div>
                                                <div class="orders-list-container">
                                                   <div class="orders-new-list"></div>
                                                </div>
                                                <div class="bulk-file-download-popup-container"></div>
                                                <div class="processing-label-download-popup-container"></div>
                                             </div>
                                          </div>
                                          <div class="active-orders-list-holder">
                                             <div id="blinx-wrapper-35" class="activeOrdersShipmentListView-default play-arena">
                                                <div>
                                                   <div class="active-orders-list-page-cont">
                                                      <div id="blinx-wrapper-40" class="paginator-default play-arena">
                                                         <div class="paginator">
                                                            <div class="view">
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="active-orders-list-cont">
                                                      <div id="blinx-wrapper-39" class="listView-default play-arena">
                                                         <div class="fk-list-view" style="height:;overflow:">
                                                            <div class="no-data-container fk-list-message">
																<table id="example" class="stripe dataTable" cellspacing="0" width="100%">
																	<thead>
																		<tr>
																			<td>Date</td>
																			<td>Order Ref #</td>
																			<td>Order Amount</td>
																			<td>Comments</td>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																	</tbody>
																</table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
               </div>
            </div>
            
         </section>
      </div>
   </div>
</div>
<?php
   echo $layout['footer'];
?>