<?php
echo	$layout['header'];
?> 
<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/main.css">		
<section id="content-wrapper">

    <section id="container">
        <div class="container">
            <div ui-view="" class="container-fluid">
                <!-- uiView:  -->
                <div ui-view="" id="listings" class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <!-- ngInclude: 'angular/views/listings/listingPageFilters.html' -->
                                <div class="widget-block span3" ng-controller="my_listing_category_filter" ng-include="'angular/views/listings/listingPageFilters.html'">
                                    <div>
                                        <h3>My Listings</h3>
                                    </div>
                                    <!-- ngIf: seller.enableEZListing -->
                                    <div ng-if="seller.enableEZListing">
                                        <a class="btn fk-btn-primary add-new-listing-button"  id="add_new_listings" href="<?php echo site_url();?>/seller/add_new_listing" >Add new listings</a>
                                        <span class="is-new-item">New</span>
                                    </div>
                                    <!-- end ngIf: seller.enableEZListing -->
                                    <div class="gray_border_bottom" id="category_filter">
                                        <div class="filter-header">BROWSE</div>
                                        <div class="category-list-block">
                                            <div class="horizontal-middle text-middle ng-hide" ng-show="treeloaderManager.status.isLoading" style="">
                                                <i class="icon-spinner icon-spin icon-large icon-2x"></i>
                                            </div>
                                            <div ng-hide="treeloaderManager.status.isLoading" class="" style="">
                                                <div tree="main_data.categories" tree-idfield="id" tree-textfield="text" tree-childfield="child" tree-selected-id="main_data.selected" tree-option-select-leaf="false" selected-change="changeLocation(category,depthType)" tree-depth-map="treeDepth">
                                                    <span analytics-event="img upload browse tree" analytics-label="img upload browse tree" analytics-value="All" class="fk-tree-element fk-tree-active fk-tree-selected" ng-click="treeElementClick(&quot;All&quot;, &quot;All&quot;)" ng-class="{'fk-tree-active':selected['All'],'fk-tree-selected':highlight['id']==='All'}" '="">&nbsp;
                                                        <span>All</span>
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- ngIf: treeloaderManager.status.failure -->
                                            <br>
                                        </div>
                                    </div>
                                    <div id="refiners" ng-show="routeParams.state === 'LIVE'">
                                        <div class="filter-header">REFINE</div>
                                        <div class="listing-refiner">
                                            <strong>Listing Status</strong>
                                            <div class="fk-refiner-element ng-valid" ng-model="statusRefiner.selected" refiners="statusRefiner.options" refiner-name="statusRefiner.id" ng-change="updateFilter(name,value)" id-field="'id'" label-field="'label'">
                                                <!-- ngRepeat: item in list -->
                                                <label ng-class="{radio: selectOnlyOne, checkbox: !selectOnlyOne, inline: isInline, block: !isInline}" ng-repeat="item in list" class="checkbox block">
                                                    <!-- ngIf: selectOnlyOne -->
                                                    <!-- ngIf: !selectOnlyOne -->
                                                    <input name="internal_state" value="ACTIVE" ng-checked="item.checked" ng-if="!selectOnlyOne" ng-click="toggle($index)" type="checkbox">
                                                    <!-- end ngIf: !selectOnlyOne -->Active
                                                </label>
                                                <!-- end ngRepeat: item in list -->
                                                <label ng-class="{radio: selectOnlyOne, checkbox: !selectOnlyOne, inline: isInline, block: !isInline}" ng-repeat="item in list" class="checkbox block">
                                                    <!-- ngIf: selectOnlyOne -->
                                                    <!-- ngIf: !selectOnlyOne -->
                                                    <input name="internal_state" value="INACTIVE" ng-checked="item.checked" ng-if="!selectOnlyOne" ng-click="toggle($index)" type="checkbox">
                                                    <!-- end ngIf: !selectOnlyOne -->Inactive
                                                </label>
                                                <!-- end ngRepeat: item in list -->
                                                <label ng-class="{radio: selectOnlyOne, checkbox: !selectOnlyOne, inline: isInline, block: !isInline}" ng-repeat="item in list" class="checkbox block">
                                                    <!-- ngIf: selectOnlyOne -->
                                                    <!-- ngIf: !selectOnlyOne -->
                                                    <input name="internal_state" value="INACTIVATED_BY_FLIPKART" ng-checked="item.checked" ng-if="!selectOnlyOne" ng-click="toggle($index)" type="checkbox">
                                                    <!-- end ngIf: !selectOnlyOne -->Blocked
                                                </label>
                                                <!-- end ngRepeat: item in list -->
                                                <label ng-class="{radio: selectOnlyOne, checkbox: !selectOnlyOne, inline: isInline, block: !isInline}" ng-repeat="item in list" class="checkbox block">
                                                    <!-- ngIf: selectOnlyOne -->
                                                    <!-- ngIf: !selectOnlyOne -->
                                                    <input name="internal_state" value="READY_FOR_ACTIVATION" ng-checked="item.checked" ng-if="!selectOnlyOne" ng-click="toggle($index)" type="checkbox">
                                                    <!-- end ngIf: !selectOnlyOne -->Ready
                                                </label>
                                                <!-- end ngRepeat: item in list -->
                                            </div>
                                        </div>
                                        <div class="listing-refiner">
                                            <strong>Stock Level</strong>
                                            <div class="fk-refiner-element ng-valid" ng-model="stockRefiner.selected" refiners="stockRefiner.options" refiner-name="stockRefiner.id" ng-change="updateFilter(name,value)" id-field="'id'" label-field="'label'">
                                                <!-- ngRepeat: item in list -->
                                                <label ng-class="{radio: selectOnlyOne, checkbox: !selectOnlyOne, inline: isInline, block: !isInline}" ng-repeat="item in list" class="checkbox block">
                                                    <!-- ngIf: selectOnlyOne -->
                                                    <!-- ngIf: !selectOnlyOne -->
                                                    <input name="actual_stock_size" value="LESS_THAN_5" ng-checked="item.checked" ng-if="!selectOnlyOne" ng-click="toggle($index)" type="checkbox">
                                                    <!-- end ngIf: !selectOnlyOne -->Less than 5
                                                </label>
                                                <!-- end ngRepeat: item in list -->
                                                <label ng-class="{radio: selectOnlyOne, checkbox: !selectOnlyOne, inline: isInline, block: !isInline}" ng-repeat="item in list" class="checkbox block">
                                                    <!-- ngIf: selectOnlyOne -->
                                                    <!-- ngIf: !selectOnlyOne -->
                                                    <input name="actual_stock_size" value="ZERO" ng-checked="item.checked" ng-if="!selectOnlyOne" ng-click="toggle($index)" type="checkbox">
                                                    <!-- end ngIf: !selectOnlyOne -->Out of Stock
                                                </label>
                                                <!-- end ngRepeat: item in list -->
                                                <label ng-class="{radio: selectOnlyOne, checkbox: !selectOnlyOne, inline: isInline, block: !isInline}" ng-repeat="item in list" class="checkbox block">
                                                    <!-- ngIf: selectOnlyOne -->
                                                    <!-- ngIf: !selectOnlyOne -->
                                                    <input name="actual_stock_size" value="MORE_THAN_5" ng-checked="item.checked" ng-if="!selectOnlyOne" ng-click="toggle($index)" type="checkbox">
                                                    <!-- end ngIf: !selectOnlyOne -->5 or More
                                                </label>
                                                <!-- end ngRepeat: item in list -->
                                            </div>
                                            <div>or, Enter stock count:</div>
                                            <div>
                                                <form name="stockRange" class="ng-pristine ng-valid ng-valid-min ng-valid-max ng-valid-pattern">from
                                                    <input name="from" id="stock-from" class="stockRange ng-pristine ng-untouched ng-valid ng-valid-min ng-valid-max ng-valid-pattern" min="0" max="999" ng-pattern="/^[0-9]+$/" ng-model="listing_myListingProductsModel.stockRange.low" fk-tooltip="" fk-title="Please enter a number/numeric value" fk-tooltip-is-open="false" fk-tooltip-open-on-focus="true" data-original-title="Please enter a number/numeric value" title="" type="number">to
                                                    <input name="to" id="stock-to" class="stockRange ng-pristine ng-untouched ng-valid ng-valid-min ng-valid-max ng-valid-pattern" min="0" max="999" ng-pattern="/^[0-9]+$/" ng-model="listing_myListingProductsModel.stockRange.high" fk-tooltip="" fk-title="Please enter a number/numeric value" fk-tooltip-is-open="false" fk-tooltip-open-on-focus="true" data-original-title="Please enter a number/numeric value" title="" type="number">
                                                    <button analytics-event="stock count click" ng-disabled="!enableStockRangeFilter()" class="btn fk-btn-primary" type="button" ng-click="rangeSelection()" id="submitRange" disabled="disabled">Go</button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- ngIf: seller.fbfEnabled -->
                                    </div>
                                    <!-- ngIf: routeParams.state !== 'LIVE' && routeParams.state !== 'QC_VERIFIED' && seller.enableEditCatalogue 
                                    <div class="feedback-container sticky">
                                            <a analytics-on="" analytics-category="listings" analytics-event="Listings - Feedback" learning-center="" learning-center-title="Feedback" learning-center-url="https://docs.google.com/a/flipkart.com/forms/d/1f42oMDC0p5sa_T3hz7X1hYJoZrM7cr5qRW0IHFpL5N8/viewform" class="feedback"></a>
                                    </div>-->
                                </div>
                                <div class="span9 table-block">
                                    <div class="span6">
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li id="live_listings_tab" ng-class="{active:selectedTab === 'LIVE'}" class="active">
                                                <a data-toggle="tab" ng-click="loadListingTab('/listing/myListing/live','LIVE',true)" analytics-event="Live listing" analytics-category="listings" analytics-label="Live listings tab is clicked in my listings" analytics-on="">Live (0)</a>
                                            </li>
                                            <li id="non_live_listings_tab" ng-class="{active:selectedTab !== 'LIVE'}">
                                                <a data-toggle="tab" ng-click="loadListingTab('/listing/myListing/non_live/qc_verified','QC_VERIFIED',true)" analytics-event="Non live listing" analytics-category="listings" analytics-label="non live listings tab is clicked in my listings" analytics-on="">Non Live (0)</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="span3 partners-anc-link-wrapper">
                                        <a class="partner-anc-link" target="_blank" href="index.html?sellerId=dc8e07b18cd84b7f#partner/cataloging" fk-popover="" fk-popover-template="angular/views/listings/bulk-listings/bulkListingPartnerPopover.tpl.html" fk-popover-placement="bottom" fk-popover-class="fk-popover-tooltip-theme" fk-popover-open="" analytics-event="PS_listing_myListing" analytics-category="Partner_services_discovery" analytics-label="" analytics-on="">
                                            <img class="partner-img-link partner-my-listing-img" src="https://img1a.flixcart.com/fk-sp-static/aug-20-2015/Cataloging.png" alt="Need help with Cataloging ?
                                                 Reach out to 100+ catalogue service providers across India!"> Cataloging Services
                                        </a>
                                        <div class="fk-popover fk-popover-tooltip-theme fk-popover-bottom-placement fk-popover-undefined-align" style="position: absolute; top: 0px; left: 0px; display: none;">
                                            <div class="partner-popover-container partner-container-list">
                                                <div class="triangle"></div>
                                                <div class="">
                                                    <div class="partner-heading-container">
                                                        <span class="partner-heading-txt">Need help with Cataloging ?</span>
                                                    </div>
                                                    <div class="container-fluid partner-body-text">
                                                        <div class="row-fluid l-mt10">
                                                            <span>Reach out to 100+ catalogue service providers across India!</span>
                                                        </div>
                                                        <div class="row-fluid l-mt10">
                                                            <input class="btn" value="Got it!" ng-click="isPartnerPopoverActive = false" type="button">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span3 search_block" ng-controller="my_listing_search">
                                        <fk-sku-fsn-type-ahead ng-model="userData" search-options="search_options" ng-change="onChangeAndSelect(fieldValue,searchValue)" class="ng-pristine ng-untouched ng-valid">
                                            <div class="input-append">
                                                <input ng-model="searchText" should-be-safe="" typeahead=" item as item.text+' '+searchText for item in
                                                       searchOptions" ng-attr-placeholder="{{placeholder}}" typeahead-on-select="onSearch($model.id)" maxlength="64" class="ng-pristine ng-untouched ng-valid ng-valid-maxlength" placeholder=" Search SKU ID/FSN" type="text">
                                                <ul class="typeahead dropdown-menu" ng-style="{display: isOpen()&amp;&amp;'block' || 'none', top: position.top+'px', left: position.left+'px'}" typeahead-popup="" matches="matches" active="activeIdx" select="select(activeIdx)" query="query" position="position" style="display: none;">
                                                    <!-- ngRepeat: match in matches -->
                                                </ul>
                                                <!-- ngIf: showClearSearch -->
                                            </div>
                                        </fk-sku-fsn-type-ahead>
                                    </div>
                                    <!-- uiView:  -->
                                    <div ui-view="">
                                        <div class="tab-pane" id="live">
                                            <!-- ngInclude:  -->
                                            <div ng-include="" src="'angular/views/listings/listingsJoyRide.html'">
                                                <div class="hide" fk-joy-ride="demo.startJoyRide" config="joyride.config" on-finish="onFinish()" on-skip="onFinish()" template="#template">
                                                    <div id="template">
                                                        <div class="popover fk-joyride sharp-borders">
                                                            <div class="arrow"></div>
                                                            <div class="popover-inner">
                                                                <h3 class="popover-title sharp-borders"></h3>
                                                                <div class="popover-content">
                                                                    <div class="row-fluid">
                                                                        <div id="pop-over-text" class="span12"></div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row-fluid">
                                                                        <div class="span4 center">
                                                                            <a class="skipBtn pull-left" type="button">
                                                                                <i class="icon-ban-circle"></i>&nbsp; Skip
                                                                            </a>
                                                                        </div>
                                                                        <div class="span8">
                                                                            <div class="pull-right">
                                                                                <button id="prevBtn" class="prevBtn btn btn-mini" type="button">
                                                                                    <i class="icon-chevron-left"></i>&nbsp;Previous
                                                                                </button>
                                                                                <button id="nextBtn" class="nextBtn btn btn-mini fk-btn-primary" type="button">Next&nbsp;
                                                                                    <i class="icon-chevron-right"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="titleTemplate">
                                                        <div class="fk-joyride sharp-borders intro-banner" style="">
                                                            <div class="popover-inner">
                                                                <h3 class="popover-title sharp-borders">Listings Dashboard</h3>
                                                                <div class="popover-content">
                                                                    <div class="row-fluid">
                                                                        <div id="title-text" class="span12">
                                                                            <span class="main-text">Welcome to the New Improved 
                                                                                <strong>Listings Dashboard</strong>
                                                                            </span>
                                                                            <br>
                                                                            <span class="muted small">( This demo will walk you through the New Listings Dashboard. )</span>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row-fluid">
                                                                        <div class="span4 skip-class">
                                                                            <a class="skipBtn pull-left" type="button">
                                                                                <i class="icon-ban-circle"></i>&nbsp; Skip
                                                                            </a>
                                                                        </div>
                                                                        <div class="span8">
                                                                            <div class="pull-right">
                                                                                <button disabled="disabled" class="prevBtn btn" type="button">
                                                                                    <i class="icon-chevron-left"></i>&nbsp;Previous
                                                                                </button>
                                                                                <button id="nextTitleBtn" class="nextBtn btn fk-btn-primary" type="button">Next&nbsp;
                                                                                    <i class="icon-chevron-right"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span12">
                                                <span class="l-mr25 pull-right">
                                                    <a target="blank" href="api-docs/listing-api-docs/LMAPIRef.html">Update with API</a>
                                                </span>
                                            </div>
                                            <!-- ngInclude: 'angular/views/listings/listingCountpage.html' -->
                                            <div ng-include="'angular/views/listings/listingCountpage.html'">
                                                <div class="row-fluid" ng-controller="my_listing_products_list_count">
                                                    <!-- ngIf: listing_myListingProductsModel.listingCount>0 -->
                                                </div>
                                            </div>
                                            <!-- ngInclude: 'angular/views/listings/downloadUploadTemplate.html' -->
                                            <div ng-include="'angular/views/listings/downloadUploadTemplate.html'">
                                                <div class="row-fluid" ng-controller="my_listing_download_upload" id="listingUpload">
                                                    <div class="row-fluid">
                                                        <div class="span4 filter">
                                                            <div class="btn-group ng-valid" id="selection_checkbox" ng-disabled="listingModel.getListingsCount() === 0" on-select-change="onChangeSelection()" text-list="statuses" ng-model="listingModel.selectedListings.selectedType" select-none="listingModel.getCheckedProducts() === 0" select-all="listingModel.getCheckedProducts() === listingModel.getListingsCount()" disabled="disabled">
                                                                <button class="btn dropdown-toggle" data-toggle="dropdown" ng-disabled="isDisabled" disabled="disabled">
                                                                    <i class="icon-check ng-hide" ng-show="all"></i>
                                                                    <i class="icon-check-empty" ng-show="none"></i>
                                                                    <!-- ngIf: !all && !none -->
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <!-- ngRepeat: item in textList -->
                                                                    <li ng-repeat="item in textList">
                                                                        <a ng-click="onChange(item)">All Pages</a>
                                                                    </li>
                                                                    <!-- end ngRepeat: item in textList -->
                                                                    <li ng-repeat="item in textList">
                                                                        <a ng-click="onChange(item)">None</a>
                                                                    </li>
                                                                    <!-- end ngRepeat: item in textList -->
                                                                </ul>
                                                            </div>
                                                            <download-listings ng-class="{'hide':bulkDownloadLoaderManager.status.isLoading === true}" ng-disabled="listingModel.enableDownload() === 0" selected-filter="" disabled="disabled" class="">
                                                                <button class="btn download" ng-disabled="isDisabled" ng-click="buildJson()" analytics-event="download in listing" analytics-category="listings" analytics-label="Download file in my listings" analytics-on="" disabled="disabled">Request Download</button>
                                                                <form id="getListingsInCSV" action="listing/downloadListings" method="POST" class="hide ng-pristine ng-valid">
                                                                    <input value="" name="downloadData" type="hidden">
                                                                    <input value="" name="_csrf" type="hidden">
                                                                </form>
                                                            </download-listings>
                                                            <span ng-class="{'download-msg':bulkDownloadLoaderManager.status.isLoading === true,'hide':bulkDownloadLoaderManager.status.isLoading === false}" class="hide">Generating your requested file</span>
                                                        </div>
                                                        <!-- ngIf: showLastDownload -->
                                                        <!-- ngIf: showLastUpload -->
                                                        <!-- ngIf: seller.requestType !== 'supportDashboard' -->
                                                        <div class="span2 upload-file" id="upload-file-block" ng-if="seller.requestType !== 'supportDashboard'">
                                                            <a onclick="$('#listing-file').val('');
                                                                                                                                                                                                                                                                        $('#listing-file').click()" ng-class="{'disable-link':bulkUploadLoaderManager.status.isLoading === true}" class="">
                                                                <span ng-show="bulkUploadLoaderManager.status.isLoading" analytics-event="upload in listing" analytics-category="listings" analytics-label="Upload file in my listings" analytics-on="" class="ng-hide" style="">
                                                                    <i class="icon-refresh icon-spin"></i>
                                                                </span>Upload file
                                                            </a>
                                                            <input ng-file-select="uploader($files)" id="listing-file" class="hide" type="file">
                                                            <i class="icon-upload-alt"></i>
                                                        </div>
                                                        <!-- end ngIf: seller.requestType !== 'supportDashboard' -->
                                                        <span ng-init="onLoadGetFeedStatus()"></span>
                                                        <!-- ngIf: showUploadResult -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-fluid" ng-controller="my_listing_products_list" id="listOfListings">
                                                <!-- ngIf: seller.enableEditCatalogue -->
                                                <table class="table product-details-listing edit-catalogue" fk-sticky-table-header="" ng-if="seller.enableEditCatalogue" style="padding: 0px;">
                                                    <thead class="tableFloatingHeaderOriginal">
                                                        <tr>
                                                            <th colspan="2" class="product-col">
                                                                <span id="listOfListingsHeading_productDetails">Product details</span>
                                                            </th>
                                                            <th class="stock-col text-right">
                                                                <span id="listOfListingsHeading_stock">Units in stock</span>
                                                            </th>
                                                            <th class="price-col text-right">
                                                                <span id="listOfListingsHeading_price">Selling price</span>
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <thead class="tableFloatingHeader" style="display: none;">
                                                        <tr>
                                                            <th colspan="2" class="product-col">
                                                                <span id="listOfListingsHeading_productDetails">Product details</span>
                                                            </th>
                                                            <th class="stock-col text-right">
                                                                <span id="listOfListingsHeading_stock">Units in stock</span>
                                                            </th>
                                                            <th class="price-col text-right">
                                                                <span id="listOfListingsHeading_price">Selling price</span>
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr fk-table-helper-rows="" failure="false" list-size="0" is-loading="false" empty-text="There are no listings" failure-text="Failed to load Listings" column-span="5">
                                                            <td ng-attr-colspan="{{columnSpan}}" colspan="5">
                                                                <div class="horizontal-middle text-middle">
                                                                    <!-- ngIf: isLoading -->
                                                                    <!-- ngIf: failure -->
                                                                    <!-- ngIf: listLength === 0 && !isLoading && !failure -->
                                                                    <span ng-if="listLength === 0 &amp;&amp; !isLoading &amp;&amp; !failure" fk-flip-in="" class="">
                                                                        <h4>There are no listings</h4>
                                                                    </span>
                                                                    <!-- end ngIf: listLength === 0 && !isLoading && !failure -->
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <!-- ngRepeat: product in main_data.products -->
                                                    </tbody>
                                                </table>
                                                <!-- end ngIf: seller.enableEditCatalogue -->
                                                <!-- ngIf: !seller.enableEditCatalogue -->
                                            </div>
                                            <div class="pull-right page-control" ng-controller="my_listing_page_filter" ng-hide="listing_myListingProductsModel.status.isLoading">
                                                <div class="pull-left">
                                                    <span class="dropdown page-number">Rows per page : 
                                                        <button class="dropdown-toggle btn">10&nbsp;&nbsp;
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <!-- ngRepeat: choice in pageSizeChoice track by $index -->
                                                            <li ng-repeat="choice in pageSizeChoice track by $index">
                                                                <a ng-click="updatePageSize(choice)" analytics-value="10" analytics-event="Edited Rows per page" analytics-category="listings" analytics-label="Edited Rows per page to 10 in my listings" analytics-on="">10</a>
                                                            </li>
                                                            <!-- end ngRepeat: choice in pageSizeChoice track by $index -->
                                                            <li ng-repeat="choice in pageSizeChoice track by $index">
                                                                <a ng-click="updatePageSize(choice)" analytics-value="20" analytics-event="Edited Rows per page" analytics-category="listings" analytics-label="Edited Rows per page to 20 in my listings" analytics-on="">20</a>
                                                            </li>
                                                            <!-- end ngRepeat: choice in pageSizeChoice track by $index -->
                                                            <li ng-repeat="choice in pageSizeChoice track by $index">
                                                                <a ng-click="updatePageSize(choice)" analytics-value="50" analytics-event="Edited Rows per page" analytics-category="listings" analytics-label="Edited Rows per page to 50 in my listings" analytics-on="">50</a>
                                                            </li>
                                                            <!-- end ngRepeat: choice in pageSizeChoice track by $index -->
                                                            <li ng-repeat="choice in pageSizeChoice track by $index">
                                                                <a ng-click="updatePageSize(choice)" analytics-value="100" analytics-event="Edited Rows per page" analytics-category="listings" analytics-label="Edited Rows per page to 100 in my listings" analytics-on="">100</a>
                                                            </li>
                                                            <!-- end ngRepeat: choice in pageSizeChoice track by $index -->
                                                        </ul>
                                                    </span>
                                                </div>
                                                <div class="pull-left btn-group">
                                                    <button class="btn" ng-disabled="!$parent.fkPaginator.hasPrevious()" ng-click="$parent.fkPaginator.previous();;gotToTop()" disabled="disabled">
                                                        <i class="icon-double-angle-left"></i>
                                                    </button>
                                                    <button class="btn" ng-click="$parent.fkPaginator.next();gotToTop()" ng-disabled="!$parent.fkPaginator.hasNext()" disabled="disabled">
                                                        <i class="icon-double-angle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <script src="angular/scripts/external/dtmCommon.js"></script>
                                        <script class="my-listing-type" data-id="liveListingApp" src="angular/scripts/main/listings/common/myListingDtmConfiguration.js"></script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    echo	$layout['footer'];
    ?>