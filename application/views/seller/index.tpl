<?php  echo $layout['header'] ;  ?>
<div id="banner" class="banner container-full col-md-12 track-order-page">	
	<div class="container">
	<div class="row">
		<div class="col-md-8 banner-desc">
			<h1 id="typed">Sell On Flipkart 
				<br> Aur apne sapne jee kar dekho
				</h1>
				<span class="typed-cursor">|</span>
			</div>
			<div class="col-md-4">
				<div class="form-login animated bounceIn">
					<h4>Register Today</h4>
						<form id="seller_forms" name="seller_form" action="" method="post"  class="form-group">
							<div>
								<div class="form-group">
									<input id="useremail" type="text" placeholder="Email ID" name="email" class="form-control form-text">
								</div>
								<div class="form-type-textfield form-item-phone form-item form-group">
									<input id="userphone" type="text" placeholder="Phone Number" name="phone" class="form-control form-text">
								</div>
								<button id="submit" value="Start Selling" type="submit" class="btn btn-primary">Start Selling</button>
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
</div>

<div class="col-md-12 track-order-page">
	<img src="<?php echo base_url();?>assets/frontend/images/banners/Screenshot.png" style="min-height: 350px; background-repeat: no; width: 100%;">
	
</div>

<div class="container"> 
  
</div>

<?php echo $layout['footer']; ?>

<script>
$(document).ready(function(){
	$('.header-nav').remove();
   $("#seller_forms").validate({
        rules: {
               email: {
                    required: true,
                    email: true,
                    remote: {
						url:" <?= site_url()?>seller/checkseller",
						type: "post"
					 }
                },
                phone: {
                    required: true,
                    number: true,
                    minlength:9,
                    maxlength:10
                  }
         },
        messages: {
             email:{
                    required:"Please enter a email address",
                    emial:"please enter a valid email",
                    remote:"Email already in use!"
                },
            phone:{
                     required:"please enter a phone number",
                     number:"Please enter a valid phone number",
                     minlength: "phone number must be at least 10 digit",
                     maxlength: "phone number must be at least 10 digit"
                }
        },
        submitHandler: function (form){           
		  var formdata = new FormData(form);
            $.ajax({ 
                url: "<?= site_url() ?>api/seller_api/addseller",              
                data: formdata,
                type: 'POST',
				processData : false,
				cache : false,
				contentType :false,
				beforeSend : function()
				{
					$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
				},
                success: function(data){                   
                    if(data.status){
                       var obj=data.data.sellerData; 
                       var email =obj.seller_email;
                       var phone =obj.seller_phone;
					   var sid = $.base64.btoa(data.seller_id);
                       window.location.href ="<?= site_url() ?>seller/accountCreation/"+sid;
                    }else{                      
                    }
                },
				complete : function()
				{
					$('overlay').remove();
				}					
            }); 			
        }
    });
});    
 </script>
 <style>
    .banner{
		margin-top: 100px;
		background: url("<?= base_url();?>assets/frontend/images/banners/Neutral.png") no-repeat #000;
		background-size: cover;
		padding: 4.5em 0;
		width: 100%;
		height: 40%;		
	}
	
 </style>