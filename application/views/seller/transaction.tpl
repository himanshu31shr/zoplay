<?php   echo $layout['header'] ; ?>   
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="alert alert-success success-response" style="display:none;">
	</div>
	<div class="alert alert-danger danger-response" style="display:none;">
	</div>
	<div class="row">
		<div class="col-lg-12">
		<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h3>Transactions</h3>
				</div>			
			<div class="ibox-content">
			<div class="table-responsive">
			 <table class="table table-striped table-bordered table-hover dataTables-example" >
			<thead>
			<tr>
				<th class="center" nowrap>Transaction Number</th>
				<th class="center" nowrap>Order ID</th>
				<th class="center" nowrap>Order Item Name</th>                   
				<th class="center" nowrap>Order Payment Status</th>
				<th class="center" nowrap>Order Item Value</th>
				<th class="center">Quantity</th>
				<th class="center">Total</th>
				<th class="center" nowrap>Order Date</th>    
			</tr>
			</thead>
			<tbody>
			<?php foreach($transactions as $trans) { ?>
			<tr class="gradeX">
				<td class="center"><?= $trans->txn_id; ?></td>
				<td class="center"><?= $trans->order_number; ?></td>
				<td class="center"><?= $trans->product_name; ?></td>
				<td class="center"><?= ($trans->order_payment_status==1) ? "Paid" : "pending" ?></td>
				<td class="center"><?= $trans->product_price; ?></td> 
				<td class="center"><?= $trans->order_quantity; ?></td> 
				<td class="center"><?= $trans->order_total; ?></td> 
				<td class="center"><?= $trans->order_added; ?></td> 
			</tr>
			<?php } ?>
		</tbody>
		</table>
			</div>

		</div>
		</div>
	</div>
</div>
</div>
<?php echo $layout['footer']; ?>