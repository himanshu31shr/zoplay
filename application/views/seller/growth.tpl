<?php
   echo $layout['header'];
?>
<style>
#blinx-wrapper-23{min-height:600px;}
</style>
<div class="container">
   <div id="sub-app-container">
      <div id="blinx-wrapper-23" class="returnOrdersMainComposite-default play-arena">
           <div class="insightsHome-default row ">
				<div class="col-md-12">
					<p class="text text-left">Get recommendations on your existing SKUs, and insights on growing your sales with new products that you could sell with us.</p>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-12">
					<div class="col-md-4">
						<a class="summary-widget summary-widget-newProductOpportunities" href="#dashboard/metrics/insight/product/trending" data-id="newProductOpportunities">
							<h4 class="title">New Product Opportunities</h4>
							<!--<div class="count">Worth Rs. 0</div>-->
							<div class="desc">Products that are high on demand but have either a few SKUs or no selection available.</div>
						</a>
					</div>
					
					<div class="col-md-4">
						<a class="summary-widget summary-widget-newProductOpportunities" href="#dashboard/metrics/insight/product/trending" data-id="newProductOpportunities">
							<h4 class="title">New Product Opportunities</h4>
							<!--<div class="count">Worth Rs. 0</div>-->
							<div class="desc">Products that are high on demand but have either a few SKUs or no selection available.</div>
						</a>
					</div>
					
					<div class="col-md-4">
						<a class="summary-widget summary-widget-newProductOpportunities" href="#dashboard/metrics/insight/product/trending" data-id="newProductOpportunities">
							<h4 class="title">New Product Opportunities</h4>
							<!--<div class="count">Worth Rs. 0</div>-->
							<div class="desc">Products that are high on demand but have either a few SKUs or no selection available.</div>
						</a>
					</div>
				</div>
		   </div>
	  </div>
   </div>
</div>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php
   echo $layout['footer'];
?>