<?php
	echo	$layout['header'];
?> 
	
	<div class="container">
		<div id="blinx-wrapper-22" class="onboardingSummary-default play-arena"><div class="summary-home-page">
    <div class="top-banner clearfix">
        <div class="ticker-banner clearfix">
            <div class="col-lg-8 col-md-8 col-sm-8 verify-mail">We have sent you an email with a verification link. click on the link and verify your email Id.</div>
            <div class="col-lg-4 col-md-4 col-sm-4 rgt-ticker-banner">
                <span class="resend-link" analytics-on="click" analytics-label="summary_resend_mail" analytics-category="Welcome_page" analytics-action="summary_resend_mail_click" analytics-state="tracked">Resend Link</span> <span class="resend-partition">|</span><span class="resend-dismiss" analytics-on="click" analytics-label="summary_ignore_mail" analytics-category="Welcome_page" analytics-action="summary_dismiss_mail_click" analytics-state="tracked">Dismiss</span>
            </div>
        </div>
        <div class="top-banner-header clearfix top-banner-header-margin">
            <div class="col-md-9 col-sm-8 col-lg-9 lft-title">
                <h4>Welcome to Seller Hub!</h4>
                <span>
                    Complete your profile to start selling on Flipkart
                </span>
            </div>
            <div class="col-md-3 col-sm-4 col-lg3 rgt-title">
                <div class="progress pull-right">
                    <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%">
                        <span class="sr-only">20% Complete</span>
                    </div>
                </div>
                <span class="pull-right">Your process is <span id="progressScore">20</span>% complete</span>
            </div>
        </div>
        <div class="top-banner-content clearfix">
            <div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-1">
                    <div class="business-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-business"></div>
                        <h3>Business Details</h3>
                            <span class="open-close-verify">We need your PAN, TIN, TAN details to verify your business details</span><hr>
                            <button class="add_details update-button business-detail" temp-file="business_details">Add Details</button>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-2">
                    <div class="bank-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-bank"></div>
                        <h3>Bank Details</h3>
                        <span class="open-close-verify">We need your bank account details and KYC documents to verify your bank account</span><hr>
                        <button class="add_details update-button business-detail" temp-file="verify_account">Add Details</button>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-3">
                    <div class="store-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-store"></div>
                        <h3>Store Details</h3>
                        <span class="open-close-verify">We need your display name and business description to verify your store details</span><hr>
                        <button class="add_details update-button business-detail" temp-file="verify_store">Add Details</button>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-3">
                    <div class="store-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-store"></div>
                        <h3>Add Listings</h3>
                        <span class="open-close-verify">You need to add minimum 10 listings to activate your account.</span><hr>
                        <button class="add_details update-button business-detail" temp-file="activate">Add Details</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="summary-footer-section clearfix">
        <h4>Meanwhile you can</h4>
        <div class="summary-footer-section-content clearfix">
            <div class="col-lg-4 col-md-4 col-sm-4 footer-col">
                <div class="footer-col-bgcolors">
                    <h5>Flipkart Services Hub</h5>
                    <p class="paragraph clearfix">Need help with listing your products or obtaining packaging material? Explore the range of services available and reach out from the range of service providers available to you.</p>
                    <a class="update-button pull-right" id="listing-update" target="_blank" href="/index.html#partner/home" analytics-on="click" analytics-label="view_partners_in_summaryPage" analytics-category="Partner_Homepage" analytics-action="view_partners_in_summaryPage_click" analytics-state="tracked">Explore Services Hub</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 footer-col">
                <div class="footer-col-bgcolors">
                    <h5>Read FAQ</h5>
                    <ul class="faqs list-unstyled">
                        <li>
                            <a href="/LearningCenter/article-video/getting-started/registration--onboarding/how-sell-flipkart?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="introduction_to_market_place" analytics-category="Welcome_page" analytics-action="faq1_click" analytics-state="tracked">Introduction to online marketplace</a>
                        </li>
                        <li>
                            <a href="/LearningCenter/article-video/getting-started/registration--onboarding/how-sell-flipkart?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="how_to_sell_flipkart" analytics-category="Welcome_page" analytics-action="faq2_click" analytics-state="tracked">How to sell on flipkart</a>
                        </li>
                        <li>
                            <a href="/LearningCenter/article-video/getting-started/registration--onboarding/getting-started-guide?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="getting_statrted_guide" analytics-category="Welcome_page" analytics-action="faq3_click" analytics-state="tracked">Getting started guide</a>
                        </li>
                        <li>
                            <a href="/LearningCenter/article-imagetutorial/doing-business/quick-tips--tricks/sell-genuine-products-avoid-being-blacklisted?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="selling_policy" analytics-category="Welcome_page" analytics-action="faq4_click" analytics-state="tracked">Flipkart selling policy</a>
                        </li>
                        <li>
                            <a href="/LearningCenter?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="SLC_more_link" analytics-category="Welcome_page" analytics-action="SLC_more_link_click" analytics-state="tracked">More...</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script>
	$('.add_details').on('click', function (e) {
		e.preventDefault();
		var url = $(this).attr('temp-file');
		url = '<?=site_url();?>seller/modal/'+url;
		$.ajax({
			url:url,
			processData:false,
			type:"POST",
			success:function(data){
				$('#fill').empty().html(data);
				$('#myModal').modal('show')
			}
		});
	})
</script>
<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content" id="fill">
		</div>
	   </div>
    </div>
</div>
<?php
	echo $layout['footer'];
?>