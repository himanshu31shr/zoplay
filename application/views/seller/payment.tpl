<?php
		echo	$layout['header'];
?> 
<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/main.css">		
<section id="content-container" class="pure-u-1 dashboard-payments-account-summary container">
<div id="sub-app-container">
	<div id="blinx-wrapper-27" class="paymentsAccountSummary-default play-arena">
		<div class="payment-summary-container">
			<div class="title-bar">
				<span class="title-text">Payments Overview</span>
			</div>
			<div class="summary-block">
				<div class="account-summary-details">
					<div id="blinx-wrapper-30" class="accountSummaryDetails-default play-arena">
						<div class="summary-card-block">
							<div class="general-card">
								<div class="summary-card next-payment">
									<div class="card-header">
										<h4>Next Payment</h4>
										<div class="desc">Estimated value of next payment. This may change due to returns that come in before the next payout.</div>
									</div>
									<div class="card-inner-block">
										<div class="title-note">Paid on Dec 30, 2016 </div>
										<div class="card-body">
											<div class="inner-card-scroll">
												<div class="card-row">
													<div class="card-row-title">
														<span>Postpaid</span>
														<span class="label-info"></span>
													</div>
													<div class="sub-total">
														<div class="positive">
															<i class="fa fa-inr fa-1 rupee-size"></i> 0.00
                    
														</div>
													</div>
												</div>
												<div class="card-row">
													<div class="card-row-title">
														<span>Prepaid</span>
														<span class="label-info"></span>
													</div>
													<div class="sub-total">
														<div class="positive">
															<i class="fa fa-inr fa-1 rupee-size"></i> 0.00
                    
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card-footer">
											<div class="card-row-total">
												<span class="footer-label">Total</span>
												<div class="main-total">
													<div class="positive">
														<i class="fa fa-inr fa-1 rupee-size"></i> 0.00
                    
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="summary-card settlements">
									<div class="card-header">
										<h4>Last Payment 
											<span class="check-payments">
												<a href="#dashboard/payments/settlements?inFrameSrc=dashboard%23statements" target="_blank" analytics-on="click" analytics-action="All previous payment link clicked" analytics-category="Payments" analytics-label="All previous payment link clicked" analytics-state="tracked">View all previous payments</a>
											</span>
										</h4>
										<div class="desc">These payments have been initiated and may take up to 48 hours to reflect in your bank account.</div>
									</div>
									<div class="card-inner-block">
										<div class="summaryErrorNote">No details found</div>
									</div>
								</div>
							</div>
							<div class="future-card">
								<div class="summary-card due-payment">
									<div class="card-header">
										<h4>Total Outstanding Payments</h4>
										<div class="desc">Total amount you are to receive from Flipkart for dispatched orders. It includes the 'Next Payment' amount shown above.</div>
									</div>
									<div class="card-inner-block">
										<div class="card-body">
											<div class="inner-card-scroll">
												<div class="card-row">
													<div class="card-row-title">
														<span>Postpaid</span>
														<span class="label-info"></span>
													</div>
													<div class="sub-total">
														<div class="positive">
															<i class="fa fa-inr fa-1 rupee-size"></i> 0.00
                    
														</div>
													</div>
												</div>
												<div class="card-row">
													<div class="card-row-title">
														<span>Prepaid</span>
														<span class="label-info"></span>
													</div>
													<div class="sub-total">
														<div class="positive">
															<i class="fa fa-inr fa-1 rupee-size"></i> 0.00
                    
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card-footer">
											<div class="card-row-total">
												<span class="footer-label">Total</span>
												<div class="main-total">
													<div class="positive">
														<i class="fa fa-inr fa-1 rupee-size"></i> 0.00
                    
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="summary-card order-summary">
									<div class="card-header">
										<h4>Unbilled Orders</h4>
										<div class="desc">These are orders yet to be dispatched. Once dispatched, payments will be scheduled and they will reflect in 'Total Outstanding Payments' section.</div>
									</div>
									<div class="card-inner-block">
										<div class="card-body">
											<div class="inner-card-scroll">
												<div class="card-row">
													<div class="card-row-title">
														<span>No. of Orders</span>
														<span class="label-info"></span>
													</div>
													<div class="sub-total">
														<div class="positive">
                                0
                    </div>
													</div>
												</div>
												<div class="card-row">
													<div class="card-row-title">
														<span>Total Sale Amount</span>
														<span class="label-info"></span>
													</div>
													<div class="sub-total">
														<div class="positive">
															<i class="fa fa-inr fa-1 rupee-size"></i> 0.00
                    
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card-footer">
											<div class="card-row-total">
												<span class="footer-label">
													<div class="label-block">
														<span id="order-csv-download">Download the new Orders report</span>
													</div>
												</span>
												<div class="main-total"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="sub-title-section">
								<div class="sub-title">Reports Section</div>
							</div>
							<div class="reports-card">
								<div class="summary-card fy-report">
									<div class="card-header">
										<h4>FY Report</h4>
										<div class="desc">Payments dashboard shows data only for current financial year. Refer to these annual reports for older data.</div>
									</div>
									<div class="card-inner-block">
										<div class="summaryErrorNote">No details found</div>
									</div>
								</div>
								<div class="summary-card nil-wth-report">
									<div class="card-header">
										<h4>NIL WHT Certificate</h4>
										<div class="desc">These are your NIL WHT certificates. Please use them for filing your TDS returns.</div>
									</div>
									<div class="card-inner-block">
										<div class="summaryErrorNote">No details found</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="account-summary-QR">
					<div id="blinx-wrapper-31" class="accountSummaryQR-default play-arena">
						<ul class="ref-block-area">
							<li>
								<a href="LearningCenter/faqs/payment-basics" target="_blank" class="ref-title" analytics-on="click" analytics-action="Help link to SLC clicked" analytics-category="Payments" analytics-label="When will i get paid" analytics-state="tracked">When will I get paid?</a>
								<div class="ref-desc">Read our policy to know when you'll get paid for Orders fulfilled by you.</div>
							</li>
							<hr>
								<li>
									<a href="index.html#dashboard/payments/ratecard/NON_FBF?fsn=default" target="_blank" class="ref-title" analytics-on="click" analytics-action="Help link to SLC clicked" analytics-category="Payments" analytics-label="How much will i get paid" analytics-state="tracked">How much will I get paid?</a>
									<div class="ref-desc">Check out our rate card to understand the different fees charged on Orders fulfilled by you.</div>
								</li>
								<hr>
									<li>
										<a href="LearningCenter/faqs/payment-basics" target="_blank" class="ref-title" analytics-on="click" analytics-action="Help link to SLC clicked" analytics-category="Payments" analytics-label="Other payment related FAQs" analytics-state="tracked">Have other payment related questions?</a>
										<div class="ref-desc">Find answers to the most frequently asked questions.</div>
									</li>
									<hr>
										<li>
											<div class="payment-ref-title">Payments quick reference</div>
											<ul class="payment-ref-block">
												<li>
													<a href="#dashboard/payments/invoices?inFrameSrc=dashboard%23invoices" target="_blank" class="ref-title" analytics-on="click" analytics-action="Payment quick reference links" analytics-category="Payments" analytics-label="View Invoices" analytics-state="tracked">View Invoices and TDS Details</a>
													<div class="ref-desc">List of all month end invoices with TDS details.</div>
												</li>
												<li>
													<a href="#dashboard/payments/statements?inFrameSrc=payments_management%23summary" target="_blank" class="ref-title" analytics-on="click" analytics-action="Payment quick reference links" analytics-category="Payments" analytics-label="View Statements" analytics-state="tracked">View Statement</a>
													<div class="ref-desc">Summarised payment statement for a time period.</div>
												</li>
												<li>
													<a href="#dashboard/payments/transactions?inFrameSrc=dashboard%23transactions" target="_blank" class="ref-title" analytics-on="click" analytics-action="Payment quick reference links" analytics-category="Payments" analytics-label="View Transactions" analytics-state="tracked">View all transactions</a>
													<div class="ref-desc">Details of your settled and unsettled payment transactions.</div>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
		echo	$layout['footer'];
?>