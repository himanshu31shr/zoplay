<?php
    echo  $layout['header'];
?> 
<style>
.single-product{ min-height: 748px;}
.pad-all{padding: 0% 38%;}
.pr_name{    
padding: 7px 13px;
border-radius: 3px;
font-size: 14px;
text-transform: capitalize;
font-weight: 600;}
.pr_name small{cursor: pointer;}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;  
}
.fileUpload {
    background-color: #8dc63f;
    border-radius: 6px;
    color: #ffffff;
    margin: 0;
    overflow: hidden;
    padding: 5px;
    position: relative;
    text-align: center;
}
</style>
<div class="body-content ">
  <div class="container">
 
    <div class="row single-product">
      <!-- /.sidebar -->
      <div class="col-md-12">
        <div class="product-tabs inner-bottom-xs  wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
          <div class="row">
            <div class="col-sm-3">
              <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                <li class="active"><a data-toggle="tab" href="#personalDetail">Shop Details</a></li>
                <li class=""><a data-toggle="tab" href="#ChangePassword">Featured products</a></li>
                
                
              </ul><!-- /.nav-tabs #product-tabs -->
            </div>
            <div class="col-sm-9">
              <div class="tab-content" >                             
                <div id="personalDetail" class="tab-pane active">

                    <!-- Form Name -->
                    <legend>Shop details</legend>
                   <form id="" class="form-horizontal" >
                   <div class="status"></div>
                    <fieldset>

                    <!-- Form Name -->
                  

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Shop Name</label>  
                      <div class="col-md-4">
                      <input id="textinput" name="shop_name" value="<?=isset($shop->shop_name) && $shop->shop_name != '' ? $shop->shop_name : '' ;?>" type="text" placeholder="" class="form-control input-md" >
                      <span class="text-danger"> <?php echo form_error('shop_name'); ?></span>
                   
                      </div>
                    </div>

                   <!-- Text input-->
                    
                     <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput"> Featured Image</label>  
                      <div class="col-md-4">
                      <div class="img-cont">
                        <?php
                        // if(isset($shop->featured_image)){
                          // $img_url = image_url($shop->featured_image);
                          // echo "<img src='". $img_url."' class='img-responsive'  />";
						  // echo $shop->featured_image;
                        // }else{
                          // echo "No image set!";
                        // }
                        ?>
                        </div>
						<div class="fileUpload">
							<span id="before_select" style="color:#FFFFFF; cursor:pointer; font-size: large;">
							<?= ($shop) ? $shop->featured_image : "Browse" ?>
							</span>
							<span id="after_select" style="color:#FFFFFF; font-size: large;"></span>
							<input type="file" name="featured_image" onchange="getFileData(this);" id="file_up" value="" class="upload">
						</div>
                       <!-- <input id="textinput" name="featured_image" type="file" placeholder="" class="form-control input-md">-->
                      </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="selectbasic">Facebook page link</label>
                      <div class="col-md-4">
                        <input type="text" class="form-control input-md" value="<?=isset($shop->social_links_fb) && $shop->social_links_fb != '' ? $shop->social_links_fb : '' ;?>" name="fb" />
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-md-4 control-label" for="selectbasic">Google+ page link</label>
                      <div class="col-md-4">
                        <input type="" class="form-control input-md" value="<?=isset($shop->social_links_gp) && $shop->social_links_gp != '' ? $shop->social_links_gp : '' ;?>" name="gp" />
                      </div>
                      
                    </div>
                     <div class="form-group">
                      <label class="col-md-4 control-label" for="selectbasic">Twitter link</label>
                      <div class="col-md-4">
                        <input type="" class="form-control input-md" value="<?=isset($shop->social_links_tw) && $shop->social_links_tw != '' ? $shop->social_links_tw : '' ;?>" name="tw" />
                      </div>
                      
                    </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput"> Description</label>  
                      <div class="col-md-4">
                      <textarea id="textinput" name="description" type="text" placeholder="" class="form-control input-md" ><?=isset($shop->description) && $shop->description != '' ? $shop->description : '' ;?></textarea>
                      
                      </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="singlebutton"></label>
                      <div class="col-md-4">
                      <input id="singlebutton" type="submit" name="submit" class="btn btn-primary" value="save" />
                      </div>
                    </div>

                    </fieldset>
                    </form>
                </div><!-- /.tab-pane -->
               
                <div id="ChangePassword" class="tab-pane">
               <?php
                  if(isset($shop) && $shop->featured_products != ''){
                  $inputs = 4 - count($shop->products);
                ?>
                 
                  <form class="form-horizontal" method="post" action="<?php echo site_url();?>seller/featured_product">
                    <fieldset>
                    <!-- Form Name -->
                    <legend>Featured Products</legend>
                       <div class="ui-widget">
                  <?php
                  $i = 0;
                    foreach($shop->products as $p){
                  ?>
                    <!-- text input-->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="piCurrPass"><?=$i+1;?></label>
                      <div class="col-md-10">
                        <div class="hide" id="click<?=$i;?>">
                          <input id="tags<?=$i;?>" placeholder="Search products" name="product_name<?=$i;?>" value="<?=$p->product_id;?>" type="text" placeholder="" class="tags form-control input-md" value="">
                        </div>
                          <div class="pr_name" id="clicks<?=$i;?>">
                            <a href="<?=site_url();?>product/<?=md5($p->product_id);?>"><?=$p->product_name;?></a> <small onclick="change(<?=$i;?>)"><i class="fa fa-trash"></i> Remove</small>
                          </div>
                   
                       <div class="ui-widget">
                 
                   
              
                    <!-- text input-->
                    <div class="form-group">
                      <label class="col-md-1 control-label" for="piCurrPass">1</label>
                      <div class="col-md-10">
                  
                        <input id="tag1" name="product1"  type="text" value=" " placeholder="" class="form-control input-md" 
                         value="">
               
                     <!--   <input id="tag1" name="product1"  type="text" value="<?php echo $row->product_name;?>" placeholder="" class="form-control input-md" 
                         value=""> -->
                 
                      </div>

                    </div>                    
					<?php
                      $i++;
                      }
                      while($inputs > 0){
                      ?>
                      <div class="form-group">
                      <label class="col-md-2 control-label" for="piCurrPass"><?=$i+1;?></label>
                      <div class="col-md-10">
                        <input id="tags<?=$i;?>" name="product_name<?=$i;?>"  type="text" placeholder="Search products" class="tags form-control input-md" value="">
                    <!-- text input-->
                    <div class="form-group">
                      <label class="col-md-1 control-label" for="piNewPass">2</label>
                      <div class="col-md-10">
                      <input id="tag2" name="product2" type="text" placeholder="" value="" class="form-control input-md" >
                      
                      </div>
                    </div>
                      <?php
                        $inputs--;
                        $i++;
                      }

                  ?>
                  <script type="text/javascript">
                   
                  </script>
                    <!-- text input-->
                    <div class="form-group">
                      <label class="col-md-1 control-label" for="piNewPassRepeat">3</label>
                      <div class="col-md-10">
                      <input id="tag3"  name="product3" type="text" placeholder="" value="" class="form-control input-md" >
                      
                      </div>
                    </div>
                    <!-- text input-->
                         <div class="form-group">
                      <label class="col-md-1 control-label" for="piNewPassRepeat">4</label>
                     
                      <div class="col-md-10">
                      <input id="tag4" name="product4"  type="text" placeholder="" value="" class="form-control input-md" >
                      
                      </div>
                    </div> </div>
                    <!-- Button (Double) -->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="piCurrPass"></label>
                      <div class="col-md-10">
                      <button id="bCancel" name="bCancel" class="btn btn-danger">Cancel</button>
                      <input type="submit" name="submit" class="btn btn-success" value="Save" />
                      </div>
                    </div>

                    </fieldset>
                   
                    </form>
                  <?php

                  }
                  ?>
                </div><!-- /.tab-pane -->
                    <style type="text/css"> .ui-widget { width:600px; }</style>
                             
                      <div id="address" class="tab-pane">
                  <form class="form-horizontal">
                    <fieldset>

                    <!-- Form Name -->
                    <legend>Address</legend>

                    <!-- Password input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piCurrPass">Address Line 1</label>
                      <div class="col-md-4">
                      <input id="piCurrPass" name="piCurrPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piNewPass">Address Line 1</label>
                      <div class="col-md-4">
                      <input id="piNewPass" name="piNewPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piNewPass">Address Line 2</label>
                      <div class="col-md-4">
                      <input id="piNewPass" name="piNewPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piNewPass">City/Town</label>
                      <div class="col-md-4">
                      <input id="piNewPass" name="piNewPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piNewPass">State / Province / Region
                    </label>
                      <div class="col-md-4">
                      <input id="piNewPass" name="piNewPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piNewPass">Zip / Postal Code

                    </label>
                      <div class="col-md-4">
                      <input id="piNewPass" name="piNewPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piNewPass">Country

                    </label>
                      <div class="col-md-4">
                     
                        </div>
              
              
                    </div>

                  
                    <!-- Button (Double) -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="bCancel"></label>
                      <div class="col-md-8">
                      <button id="bCancel" name="bCancel" class="btn btn-danger">Cancel</button>
                      <button id="bGodkend" name="bGodkend" class="btn btn-success">Save</button>
                      </div>
                    </div>

                    </fieldset>
                    </form>
                </div><!-- /.tab-pane -->

                <div id="business" class="tab-pane">
                  <form class="form-horizontal">
                    <fieldset>

                    <!-- Form Name -->
                    <legend>Business Details</legend>
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piCurrPass">Business type</label>
                      <div class="col-md-4">
                      <input id="piCurrPass" name="piCurrPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piCurrPass">PAN</label>
                      <div class="col-md-4">
                      <input id="piCurrPass" name="piCurrPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piCurrPass">TIN</label>
                      <div class="col-md-4">
                      <input id="piCurrPass" name="piCurrPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="piCurrPass">TAN</label>
                      <div class="col-md-4">
                      <input id="piCurrPass" name="piCurrPass" type="text" placeholder="" class="form-control input-md" required="">
                      
                      </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="bCancel"></label>
                      <div class="col-md-8">
                      <button id="bCancel" name="bCancel" class="btn btn-danger">Cancel</button>
                      <button id="bGodkend" name="bGodkend" class="btn btn-success">Save</button>
                      </div>
                    </div>

                    </fieldset>
                    </form>

                </div><!-- /.tab-pane -->
                
                <div id="deactivate" class="tab-pane">
                  <h3 class="text text-center">Are you sure?</h3>
                  
                  <br/>
                  <h4 class="text text-center">Do you wish to deactivate your seller account?</h4>
              
                  <br/>
                  <p class="text text-center">This cannot be undone!</p>
                  
                  <br/>
                  
                  <div class="pad-all">
                    <div class="col-md-6">
                      <button class="btn btn-block btn-large btn-danger">Yes</button>
                    </div>
                    <div class="col-md-6">
                      <button class="btn btn-block btn-large btn-outline">No</button>
                    </div>
                  </div>


                </div><!-- /.tab-pane -->
                
              </div><!-- /.tab-content -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.product-tabs -->

        <!-- ============================================== UPSELL PRODUCTS ============================================== -->
<!-- /.section -->
<!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
      
      </div><!-- /.col -->
      <div class="clearfix"></div>
    </div><!-- /.row -->
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
<!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->  </div><!-- /.container -->
</div>

<?php echo  $layout['footer']; ?>
<script type="text/javascript">
 // var change = function(id){
    // $('#click'+id).attr('class', '');
    // $('#tags'+id).removeAttr('val');
    // $('#clicks'+id).val('').attr('class', 'pr_name hide');
  // }
  
// $( '[type="text"]').autocomplete({          
  // source: function( request, response ) {
    // $.ajax({
      // url: "<?php echo site_url();?>seller/searchproduct",
      // dataType: "json",
      // data: request,
      // beforeSend:function(){
        // var id = $(this).attr('id');
        // $("#"+id).html("<li id='ldl'>Please wait! Loading..</li>" );
      // },
      // success: function(data){
        // if(data.response == 'true') {          
            // response( $.each( data.message, function(key, value) {
              // var id = $(this).attr('id');
               // $("#"+id).html("<li>"+ key.value + "</li>" ); 
            // }));           
          // }
        // },
        // error: function(data) {
          // $('[type="text"]').removeClass('ui-autocomplete-loading');  
        // },
        // complete:function(response){
          // $('#ldl').remove();
        // }
    // });
  // },
  // select: function(event, ui) {
    // console.log();
    // var tag = "#"+$(this).attr('id');
    // $(tag).hide();
     // var id= tag.substring(tag.length,  tag.length-1);
    // $(tag).closest('div.col-md-10').append("<div class='pr_name' id='clicks"+id+"'><?=$p->product_name;?> <small onclick='unhider("+id+")'><i class='fa fa-trash'></i> Remove</small></div>");
    // $(tag).val(ui.item.value);

  // }
// });

// var unhider = function(id){
  // $('#tags'+id).show();
  // $('#clicks'+id).remove();

// }

$(document).ready(function(){ alert('test');
$("#shop_detailss").validate({
        rules: {
            shop_name:"required",
            featured_image:"required",
            fb:"required",
            gp:"required",
            tw:"required",
            description:"required",                         
         },
        messages: {
               shop_name:"Please enter a seller name",
               featured_image:"image required",
               fb:"Please enter a facebook page link",
               tw:"Please enter a twitter link",
               gp:"Please enter a google page link",
               description:"Decription required",             
        },
        submitHandler: function (form){           
			var form = new FormData(form);
			  $.ajax({
				url: '<?php echo site_url();?>seller/shopdetails',
				type:'POST',
				data:form,
				cache:false,
				processData:false,
				contentType:false,
				beforeSend:function(){				
					$('body').css('z-index',99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
				},
				success:function(res){
				  if(res.status ==  true){
					$('.status').html('<p class="msg alert alert-success">'+res.message+'</p>');
					$('.img-cont').empty().html("<img src='<?=base_url();?>files/"+$(res.data.img_url).val()+"' class='img-responsive' />");
				  }
				}, 
				error:function(res){
				  var response = $.parseJSON(res.responseText);
				  if(response.status ==  false){
					$('.status').html('<p class="alert alert-danger">'+response.message+'</p>');
					var data = response.data;
					if(response.data != ''){
					  $.each(data, function(key, value){
						$('input[name='+key+']').closest('div').append(value);
					  });
					}
				  }
				},
				complete:function(res){
				  $('.overlay').remove();
				}
			  });			
        }
    });
    });
	
	
	
// $('#shop_details').on('submit', function(e){  alert('test');
  // e.preventDefault();
  // var form = new FormData(form);
  // $.ajax({
    // url: '<?php echo site_url();?>seller/shopdetails',
    // type:'post',
    // data:form,
    // cache:false,
    // processData:false,
    // contentType:false,
    // beforeSend:function(){
      // $('.error').remove();
   // $('body').css('z-index',99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');

    // },
    // success:function(res){
      // if(res.status ==  true){
        // $('.status').html('<p class="msg alert alert-success">'+res.message+'</p>');
        // $('.img-cont').empty().html("<img src='<?=base_url();?>files/"+$(res.data.img_url).val()+"' class='img-responsive' />");
      // }
    // }, 
    // error:function(res){
      // var response = $.parseJSON(res.responseText);
      // if(response.status ==  false){
        // $('.status').html('<p class="alert alert-danger">'+response.message+'</p>');
        // var data = response.data;
        // if(response.data != ''){
          // $.each(data, function(key, value){
            // $('input[name='+key+']').closest('div').append(value);
          // });
        // }
      // }
    // },
    // complete:function(res){
      // $('.overlay').remove();
    // }
  // });
// });


// setTimeout(function() {
// $(".msg").remove();
// }, 5000);
</script>
<script>
function getFileData(myFile){
   var file = myFile.files[0];  
   var filename = file.name;
   $('#after_select').text(filename);
   $('#before_select').hide();  
}
</script>