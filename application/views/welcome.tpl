<section id="content-container" class="pure-u-1 dashboard-listings">
        <div id="sticky-container"><div id="blinx-wrapper-3" class="stickyMessage-default play-arena"></div></div>
        <div id="sub-app-container"><div id="blinx-wrapper-24" class="onboardingSummary-default play-arena"><div class="summary-home-page">
    <div class="top-banner clearfix">
        <div class="ticker-banner clearfix">
            <div class="col-lg-8 col-md-8 col-sm-8 verify-mail">We have sent you an email with a verification link. click on the link and verify your email Id.</div>
            <div class="col-lg-4 col-md-4 col-sm-4 rgt-ticker-banner">
                <span class="resend-link" analytics-on="click" analytics-label="summary_resend_mail" analytics-category="Welcome_page" analytics-action="summary_resend_mail_click" analytics-state="tracked">Resend Link</span> <span class="resend-partition">|</span><span class="resend-dismiss" analytics-on="click" analytics-label="summary_ignore_mail" analytics-category="Welcome_page" analytics-action="summary_dismiss_mail_click" analytics-state="tracked">Dismiss</span>
            </div>
        </div>
        <div class="top-banner-header clearfix top-banner-header-margin">
            <div class="col-md-9 col-sm-8 col-lg-9 lft-title">
                <h4>Welcome to Seller Hub!</h4>
                <span>
                        Complete your profile to start selling on Flipkart
                </span>
            </div>
            <div class="col-md-3 col-sm-4 col-lg3 rgt-title">
                <div class="progress pull-right">
                    <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%">
                        <span class="sr-only">20% Complete</span>
                    </div>
                </div>
                <span class="pull-right">Your process is <span id="progressScore">20</span>% complete</span>
            </div>
        </div>
        <div class="top-banner-content clearfix">
            <div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-1">
                    <div class="business-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-business"></div>
                        <h3>Business Details</h3>
                            <span class="open-close-verify">We need your PAN, TIN, TAN details to verify your business details</span><hr>
                            <button class="update-button business-detail" data-id="businessDetailInfo" data-status="Pending" analytics-on="click" analytics-label="add_business_details_pending" analytics-category="Welcome_page" analytics-action="add_business_details_pending_click" analytics-state="tracked">Add Details</button>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-2">
                    <div class="bank-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-bank"></div>
                        <h3>Bank Details</h3>
                        <span class="open-close-verify">We need your bank account details and KYC documents to verify your bank account</span><hr>
                        <button class="update-button bank-detail " data-id="bankDetailInfo" data-status="Pending" analytics-on="click" analytics-label="add_bank_details_pending" analytics-category="Welcome_page" analytics-action="add_bank_details_pending_click" analytics-state="tracked">Add Details</button>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-3">
                    <div class="store-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-store"></div>
                        <h3>Store Details</h3>
                        <span class="open-close-verify">We need your display name and business description to verify your store details</span><hr>
                        <button class="update-button store-detail" data-id="accountDetailsDisplayInfo" data-status="Pending" analytics-on="click" analytics-label="add_store_details_pending" analytics-category="Welcome_page" analytics-action="add_store_details_pending_click" analytics-state="tracked">Add Details</button>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 content-box content-box-3">
                    <div class="store-details col-bgcolors">
                        <div class="ribbon-container open-ribbon-container">
                            <span class="ribbon open-ribbon">Pending</span>
                        </div>
                        <div class="onboarding-icon-store"></div>
                        <h3>Add Listings</h3>
                        <span class="open-close-verify">You need to add minimum 10 listings to activate your account.</span><hr>
                        <a class="view-button text-center" href="#dashboard/listings/addListing/dashboard/recentCategories" analytics-on="click" analytics-label="add_new_listing" analytics-category="Welcome_page" analytics-action="add_new_listing_click" analytics-state="tracked">Add a new Listing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="qa-section clearfix">
        <h4>Help us know you better</h4>
        <span class="qa-count"><span class="current-count">2</span>/<span class="total-count">11</span></span>
        <div class="questionnaires"><div id="blinx-wrapper-26" class="carousel-default play-arena"><div class="my-carousel carousel slide" id="questionnaires-carousel" data-ride="carousel" style="height:100%" data-interval="false" data-wrap="false">
    <!-- Indicators -->

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
            <div class="item">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Who will handle Flipkart account/business ?</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio6" qid="6" aid="23" qversion="1" checked="" type="radio">Self/family</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio6" qid="6" aid="24" qversion="1" type="radio">Business partner</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio6" qid="6" aid="25" qversion="1" type="radio">Employee</label>
    </div>
</div>

            </div>
            <div class="item active">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Nature of Business:</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio1" qid="1" aid="1" qversion="1" type="radio">Manufacturer</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio1" qid="1" aid="2" qversion="1" type="radio">Distributor/wholesaler</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio1" qid="1" aid="3" qversion="1" type="radio">Retailer</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio1" qid="1" aid="4" qversion="1" type="radio">Online only retailer</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio1" qid="1" aid="5" qversion="1" type="radio">Importer</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Do you wish to sell products of your own brand?</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio11" qid="11" aid="46" qversion="1" type="radio">Yes</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio11" qid="11" aid="47" qversion="1" type="radio">No</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Age of owner:</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio7" qid="7" aid="26" qversion="1" type="radio">Less than 20 years</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio7" qid="7" aid="27" qversion="1" type="radio">21-25</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio7" qid="7" aid="28" qversion="1" type="radio">26-30</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio7" qid="7" aid="29" qversion="1" type="radio">31-45</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio7" qid="7" aid="30" qversion="1" type="radio">46-50</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio7" qid="7" aid="31" qversion="1" type="radio">&gt;50 years</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">What kind of help would you need to grow your business on flipkart:</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio9" qid="9" aid="35" qversion="1" type="radio">Cataloging help for listing</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio9" qid="9" aid="36" qversion="1" type="radio">Employee training</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio9" qid="9" aid="37" qversion="1" type="radio">Manpower</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio9" qid="9" aid="38" qversion="1" type="radio">Working capital help</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio9" qid="9" aid="39" qversion="1" type="radio">Insights on what to sell/list</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio9" qid="9" aid="40" qversion="1" type="radio">Other</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Are you already selling online</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio2" qid="2" aid="6" qversion="1" type="radio">Yes</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio2" qid="2" aid="7" qversion="1" type="radio">No</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Number of products available to sell on Flipkart now</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio3" qid="3" aid="8" qversion="1" type="radio">&lt;20</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio3" qid="3" aid="9" qversion="1" type="radio">21-50</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio3" qid="3" aid="10" qversion="1" type="radio">51-100</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio3" qid="3" aid="11" qversion="1" type="radio">101-500</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio3" qid="3" aid="12" qversion="1" type="radio">501-1000</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio3" qid="3" aid="13" qversion="1" type="radio">&gt;1001</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Where are you operating from?</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio5" qid="5" aid="20" qversion="1" type="radio">Home</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio5" qid="5" aid="21" qversion="1" type="radio">Retail store</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio5" qid="5" aid="22" qversion="1" type="radio">Warehouse</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Price range of products that you wish to sell:</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio4" qid="4" aid="14" qversion="1" type="radio">&lt;500</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio4" qid="4" aid="15" qversion="1" type="radio">501-1000</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio4" qid="4" aid="16" qversion="1" type="radio">1001-5000</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio4" qid="4" aid="17" qversion="1" type="radio">5001-10,000</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio4" qid="4" aid="18" qversion="1" type="radio">10,001-50,000</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio4" qid="4" aid="19" qversion="1" type="radio">&gt;50,000</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Gender of Owner:</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio8" qid="8" aid="32" qversion="1" type="radio">Male</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio8" qid="8" aid="33" qversion="1" type="radio">Female</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio8" qid="8" aid="34" qversion="1" type="radio">Others</label>
    </div>
</div>

            </div>
            <div class="item ">
                    <div class="carousel-caption">
    <div class="col-xs-12 question">Expected business from Flipkart in next 3 months.</div>
    <div class="answers">
        <label class="radio-inline"><input class="qaanswers" name="qaradio10" qid="10" aid="41" qversion="1" type="radio">0 - 50K</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio10" qid="10" aid="42" qversion="1" type="radio">50K-2L</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio10" qid="10" aid="43" qversion="1" type="radio">2L - 10L</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio10" qid="10" aid="44" qversion="1" type="radio">10L - 1cr</label>
        <label class="radio-inline"><input class="qaanswers" name="qaradio10" qid="10" aid="45" qversion="1" type="radio">&gt;1 cr</label>
    </div>
</div>

            </div>
    </div>

        <a class="left carousel-control" href="#questionnaires-carousel" role="button" data-slide="prev">

        </a>
        <a class="right carousel-control" href="#questionnaires-carousel" role="button" data-slide="next">

        </a>
</div></div></div>
    </div>
    <div class="summary-footer-section clearfix">
        <h4>Meanwhile you can</h4>
        <div class="summary-footer-section-content clearfix">
            <div class="col-lg-4 col-md-4 col-sm-4 footer-col">
                <div class="footer-col-bgcolors">
                    <h5>Flipkart Services Hub</h5>
                    <p class="paragraph clearfix">Need help with listing your products or obtaining packaging material? Explore the range of services available and reach out from the range of service providers available to you.</p>
                    <a class="update-button pull-right" id="listing-update" target="_blank" href="/index.html#partner/home" analytics-on="click" analytics-label="view_partners_in_summaryPage" analytics-category="Partner_Homepage" analytics-action="view_partners_in_summaryPage_click" analytics-state="tracked">Explore Services Hub</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 footer-col">
                <div class="footer-col-bgcolors">
                    <h5>Read FAQ</h5>
                    <ul class="faqs list-unstyled">
                        <li>
                            <a href="/LearningCenter/article-video/getting-started/registration--onboarding/how-sell-flipkart?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="introduction_to_market_place" analytics-category="Welcome_page" analytics-action="faq1_click" analytics-state="tracked">Introduction to online marketplace</a>
                        </li>
                        <li>
                            <a href="/LearningCenter/article-video/getting-started/registration--onboarding/how-sell-flipkart?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="how_to_sell_flipkart" analytics-category="Welcome_page" analytics-action="faq2_click" analytics-state="tracked">How to sell on flipkart</a>
                        </li>
                        <li>
                            <a href="/LearningCenter/article-video/getting-started/registration--onboarding/getting-started-guide?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="getting_statrted_guide" analytics-category="Welcome_page" analytics-action="faq3_click" analytics-state="tracked">Getting started guide</a>
                        </li>
                        <li>
                            <a href="/LearningCenter/article-imagetutorial/doing-business/quick-tips--tricks/sell-genuine-products-avoid-being-blacklisted?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="selling_policy" analytics-category="Welcome_page" analytics-action="faq4_click" analytics-state="tracked">Flipkart selling policy</a>
                        </li>
                        <li>
                            <a href="/LearningCenter?utm_source=sp_slc_link&amp;utm_campaign=slc_sp_link&amp;utm_medium=sp" target="_blank" analytics-on="click" analytics-label="SLC_more_link" analytics-category="Welcome_page" analytics-action="SLC_more_link_click" analytics-state="tracked">More...</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 footer-col">
                <div class="footer-col-bgcolors">
                    <h5>Grow your business. Sell on Flipkart.</h5>
                    <iframe src="https://www.youtube.com/embed/2z3nFGGXDLM?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="edit-modal-container"><div id="blinx-wrapper-25" class="manageProfileUpdateModal-default play-arena"><div id="modal-content"></div></div></div></div></div>
    </section>