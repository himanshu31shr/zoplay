<?php echo $layout['header'] ; ?>  
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Payment Gateway</h2>
	</div>
</div>
<style>
	th,td{vertical-align:middle !important;}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12" id="mode_show" >
			<div class="panel panel-primary">
				<div class="panel-heading" id="pmode">
					Add a payment mode
				</div>
				<div class="panel-body" >
					<div class="">
						<p class="alert alert-warning hide" id="err"></p>
						<p class="text text-info">Enter alias for the payment gateway.</p>
						<form role="add_gateway" class="form-inline" id="add_gateway">
							<div class="form-group">
								<label for="" class="sr-only">Name</label>
								<input type="text" name="alias" placeholder="Enter gateway alias" id="alias" class="form-control">
							</div>
							<div class="form-group">
								<button class="btn btn-white" type="submit">Add gateway</button>
							</div>
						</form>
					</div>				
				</div>
			</div>
		</div>
		
		<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
			<p class="" id="suc"></p>
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover dataTables-example" id="example">
				<thead>
				<tr>
					<th>Gateway name</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
					$gateways = json_decode($gateways);
					foreach($gateways as $g){
						if($g->payment_gateway_status == 0){
							$text= "Disabled";
							$ctext= "Enable";
							$flag= 1;
						}else if($g->payment_gateway_status == 1){
							$text= "Enabled";
							$ctext= "Disable";
							$flag= 0;
						}else{
							$text= "Deleted" ;
							$ctext= "Undelete" ;
							$flag= 1;
						}
				?>
				<tr class="gradeX" id="mode-<?=$g->payment_gateway_id;?>">
					<td class="center"><?=$g->payment_gateway_name;?></td>
					<td class="center"><span class="label label-primary" type="button">
					<?=$text?>
					</span></td>
					<td class="center"><button class="btn btn-primary" onclick="change(<?=$g->payment_gateway_id;?>, <?=$flag;?>)" type="button"><?=$ctext?></button> <button class="btn btn-danger" onclick="del(this, <?=$g->payment_gateway_id;?>)" type="button">Delete</button></td>
				</tr>
				<?php
					}
				?>
				</tbody>
			</table>
				<div class="pull-right"><button class="btn btn-primary" id="add_mode">Add new mode</button></div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<?php echo $layout['footer']; ?>
<script>
	var del = function(elem, id){
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			$.ajax({
				url:'<?=site_url();?>admin/del_gateway/'+id,
				type:"POST",
				processData:false,
				contentType:false,
				success:function(data){
					var d = $.parseJSON(data);
					if(d.status == true){
						$(elem).closest('tr').addClass('hide').remove();
						swal("Deleted!", "Selected payment mode deleted.", "success");
						toastr.success(d.message);
					}else{
						toastr.error(d.message);
						swal("Failed!", "Unable to delete the payment mode.", "failed");
					}
				}
			});
		});
	}
	
	var change = function(id, flag){
		$.ajax({
			url:'<?=site_url();?>admin/change_gateway_status/'+id+'/'+flag,
			type:"POST",
			processData:false,
			contentType:false,
			success:function(data){
				var t = $.parseJSON(data);
				if(t.status == true){
					toastr.success(t.message);
					location.reload();
				}else{
					toastr.warning(d.message);
					location.reload();
				}
			}
		});
	}
	
	$(document).ready(function(){
		$('#add_gateway').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				url:'<?=site_url();?>admin/add_gateway',
				type:"POST",
				data: new FormData($('#add_gateway')[0]),
				processData:false,
				contentType:false,
				success:function(data){
					var p = $.parseJSON(data);
					if(p.status == true){
						var t = $('#example').DataTable();
						if(p.data[0].payment_gateway_status == 0){
							var status = '<span class="badge badge-info" type="button">Disabled</span>';
						}else if(p.data[0].payment_gateway_status == 1){
							var status = '<span class="badge badge-info" type="button">Enabled</span>';
						}else{
							var status = '<span class="badge badge-info" type="button">Deleted</span>';
						}
						t.row.add([
							p.data[0].payment_gateway_name,
							status,
							'<button class="btn btn-primary" onclick="change('+ p.data[0].payment_gateway_id +',1)" type="button">Enable</button> <button class="btn btn-danger" onclick="del('+'this,' +p.data[0].payment_gateway_id +')" type="button">Delete</button>',
						]).draw( false );
						
						// Automatically add a first row of data
						$('#addRow').click();
						toastr.success(p.message);
					}else{
						toastr.warning(p.message);					
					}
				}
			})
		});
		$('#pmode').css('cursor', 'pointer');
		$('#mode_show').toggle('fast');
		$('#add_mode').click(function(){
			$('#mode_show').toggle('fast');
		});
	});
	
</script> 