<?php   echo $layout['header'] ; 
$seller = json_decode($seller);
?>  

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Site Earnings</h2>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Vendor Email</th>
                        <th>Order</th>
                        <th>Earning</th>
                        <th>Dispute Amt</th>
                        <th>Other Detections </th>
                        <th> Site Earned</th>
                        <th>vendor Earned</th>
                        <th>Paid</th>
                        <th>Balance</th>
                        <th>option</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
						if($seller->status == 1){
							$data = $seller->data;
							$i = 0;
							foreach($data as $d){
					?>
                    <tr class="gradeX">
                        <td><?=$d->full_name;?></td>
                        <td><?=$d->email;?></td>
                        <td><?=$d->total_orders;?></td>
                        <td class="center">$<?=$d->tp_ord;?></td>
                        <td class="center">NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>$<?=$d->tp_txn;?> </td>
                        <td class="center">NA</td>
                        <td class="center">NA</td>
                        <td class="center">
                         <div class="btn-group">
                             <a href="<?php echo  base_url();?>admin/paiddetails/"   onclick="update_modal(<?php echo $d->id ; ?>)" class="btn btn-primary" data-toggle="modal" data-target="#myModal5">View</a>
							 <input type="hidden"  value='<?php print_R(json_encode($d->transactions)) ; ?>'  id="hidden-<?php echo $d->id ; ?>" />
                        </div>
                        </td>
                    </tr>
                   <?php
							}
						}else{
							echo "<tr><td colspan='11'>".$seller->messsage."</td></tr>";
						}
				   ?>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>  
        

		
      <div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog" style="width:70%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Vendor payment details</h4>
				</div>
				<div class="modal-body">
                   <div class="table-responsive">
				   <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                    <tr role="row">
						<th>Transaction Id</th>
						<th>Transaction Date</th>
						<th>Payment Mode</th>
						<th>Amount</th>
						<th>Status</th></tr>
                    </thead>
                    <tbody>
					<script>
					var update_modal = function(i){
						var data = $("#hidden-"+i).val();
						$('#modal_in').empty().html("<td colspan='5'>No transactions yet</td>");
						if(typeof(data) !== "undefined" && data !== null && data !== ''){
							data = $.parseJSON(data)[0];
							console.log(data);
							var html = "<td>"+data.transaction_number+"</td><td>"+data.payment_added+"</td><td>"+data.payment_gateway_id+"</td><td>"+data.total_price+"</td><td><span class='badge badge-primary'>"+data.payment_status+"</span></td>";
						}else{
							var html = "<td colspan='5'>No transactions yet</td>";
						}
						$('#modal_in').empty().html(html);
						data = '';
					}
					</script>
                    <tr id="modal_in">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><span class="label label-primary"></span></td>
                    </tr>
					
					</tbody>
					</table>
					</div>

                    </div>
			</div>
		</div>
	</div>  
       
<?php echo $layout['footer']; ?>