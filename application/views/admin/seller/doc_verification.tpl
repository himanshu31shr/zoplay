<?php  echo $layout['header'] ; ?>  
<div class="loadermodal" style="display: none">
		<div class="loadercenter">
			<img alt="" src="<?= base_url() ?>assets/admin/img/loader.gif" />
		</div>
	</div
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Document Verification</h2>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                   <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Address Proof</th>                        
                        <th>Cancel Cheque Proof</th>                        
						<th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
						if(!empty($seller_details)){	
							foreach($seller_details as $s){ ?>
                    <tr class="gradeX">
                        <td><?=$s->full_name;?></td>
                        <td><?=$s->email;?></td>
                        <td class="center">
							<a href="#" class="pop">
								<img src="<?php echo base_url();?>assets/uploads/<?php if($s->address_proof_image == ""){echo "a5.jpg";}else{echo $s->address_proof_image;} ?>" class="thumbnail" width="100px" height="50px">
							</a>
						</td>
						<td class="center">
							<a href="#" class="pop">
								<img src="<?php echo base_url();?>assets/uploads/<?php if($s->cancelled_cheque_image == ""){echo "a5.jpg";}else{echo $s->cancelled_cheque_image;} ?>" class="thumbnail" width="100px" height="50px">
							</a>
						</td>
                        <td class="center">
						<?= ($s->status==1) ? "Verified" : "Not Verify" ?>
						</td>
                        <td class="center">                  
						<span style="cursor:pointer;" data="<?= $s->seller_id; ?>" class="label status_checks btn <?= ($s->status) ? "label-primary" : "label-danger" ?>">
						<?= ($s->status) ? "Verified" : "Not Verified" ?>
						</span>
                        </td>
                    </tr>
					<?php
							}
						}else{
							echo "<tr><td colspan=5>No sellers yet!</td></tr>";
						}
					?>
                    </tfoot>
                    </table>
						<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog" data-dismiss="modal">
							<div class="modal-content"  >              
							  <div class="modal-body">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<img src="" class="imagepreview" style="width: 100%;" >
							  </div> 
							  </div>
							</div>
						  </div>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </div>  
<?php echo $layout['footer']; ?>
<script>
$(function() {
	$('.pop').on('click', function() {
		$('.imagepreview').attr('src', $(this).find('img').attr('src'));
		$('#imagemodal').modal('show');   
	});		
});

$(document).on('click',".status_checks",function(){
	var status = ($(this).hasClass('label-primary') ? "0" : "1");
	var msg = (status == '0' ? "Not Verified" : "Verified");	
		var id = $(this);		
		swal({
		  title: "Are you sure?",
		  text: "You want to "+msg,
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: msg,
		  closeOnConfirm: false
		},
		function(){
		$.ajax({
			type : "POST",
			url:'<?=site_url();?>api/admin_api/verified_document', 
			data : {id : $(id).attr('data') , status : status},
			cache : false,			
		beforeSend : function()
		{
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},
		success : function(result)
		{
			setTimeout(function() {
							swal({
								title: msg,
								text: "Document Has Been "+msg+" Successfully.",
								type: "success"
							}, function() {
								location.reload();
							});
						}, 500);	
			$('.overlay').remove();
			toastr.success('Document Has Been '+msg+' Successfully', 'Success');			
		},
		error :function()
		{
			
		},
		complete :function()
		{
			$('.overlay').remove();
		}
	});		
});
});
</script>
 