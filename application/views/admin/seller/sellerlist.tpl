<?php  echo $layout['header'] ; ?>  
<div class="loadermodal" style="display: none">
		<div class="loadercenter">
			<img alt="" src="<?= base_url() ?>assets/admin/img/loader.gif" />
		</div>
	</div
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Seller List</h2>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                   <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Thumnails</th>
                        <th>User name</th>
                        <th>Payment Mode</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
						if(!empty($all_sellers)){
							//$all_sellers = json_decode($all_sellers);
							foreach($all_sellers as $s){
							
					?>
                    <tr class="gradeX">
                        <td><?=$s->full_name;?></td>
                        <td><?=$s->email;?></td>
                        <td><img src="<?php echo base_url();?>assets/admin/img/<?php if($s->picture == ""){echo "a5.jpg";}else{echo $s->picture;} ?>" class="thumbnail" width="40px" height="40px"></td>
                        <td class="center"><?=$s->user_name;?></td>
                        <td class="center">No Defined</td>
                        <td class="center"><?php echo $s->admin_approved_status==1?  '<span class="label label-primary" style="cursor:pointer;" onclick="changestatus('.$s->id.',\'0\')">Active</span>':  '<span class="label label-danger" style="cursor:pointer;" onclick="changestatus('.$s->id.',\'1\')">Inactive</span>';?></td>
                        <td class="center">
                        <div class="btn-group">
                            <a href="<?=site_url();?>admin/viewseller/<?=$s->id;?>" class="btn-primary btn "><i class="fa fa-eye"></i></a>
                        </div>
                            
                        </td>
                    </tr>
					<?php
							}
						}else{
							echo "<tr><td colspan=5>No sellers yet!</td></tr>";
						}
					?>
                    </tfoot>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>  
<?php echo $layout['footer']; ?>
<script>
function changestatus(id,status){
	$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	$.ajax({ 
		url:'<?=site_url();?>api/admin_api/sellerstatus', 
		data: {'id':id,'status':status},
		datatype: 'json',
		type: 'POST',
		success: function(res)
		{
			 $(".overlay").remove();
			 //res = $.parseJSON(res);
			 //console.log(res.status);
			 if(res.status == true){
				toastr.success(res.message); 
				window.location.reload();
			 }else{
				 toastr.error(res.message);
			 }
			 
		}			
	}); 
}

</script>
 