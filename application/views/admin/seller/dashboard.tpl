<?php   echo $layout['header'] ; ?>  

	    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Seller Dashboard</h2>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
				<div class="col-lg-9">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5> Pending Requests </h5>
						</div>
						<div class="ibox-content">
							<div class="alert alert-info alert-dismissable hide" id="message">
                                <span id="msg">A wonderful serenity has taken possession.</span>
                            </div>
							<table class="table table-striped dataTables-example">
								<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Create Time</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$pending_users = json_decode($pending_users);
								if(!empty($pending_users )){
									
									foreach($pending_users as $p)
									{
								?>
								
								<tr>
									<td><?=$p->full_name;?></td>
									<td><?=$p->email;?></td>
									<td><?=$p->street_address.", ".$p->city.", ".$p->state.", ".$p->country;?></td>
									<td><?=time_elapsed_string(strtotime($p->created));?></td>
									<td><button onclick="seller_upd(this, 1, <?=$p->id;?>)" class="btn btn-primary"><i class="fa fa-check"></i></button> <button class="btn btn-warning" onclick="seller_upd(this, 0, <?=$p->id;?>)"><i class="fa fa-close"></i></button> </td>
								</tr>
								<?php
									}
								}else{
									echo $pending_users;
									echo "<tr><td colspan=5>No pending requests.</td></tr>";
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

                <div class="col-lg-3">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Sellers Dashboard</h5>
						</div>
						<div class="ibox-content">
						<div class="table-responsive">
						 <table class="table table-striped table-bordered table-hover " >
						<thead>
						</thead>
						<tbody>
							<tr><span><?php echo $total_seller;?> sellers registered in this site</span></tr>
						<tr class="gradeX">
							<td>Top Seller </td>
							<td>james </td>
						 </tr>
						<tr class="gradeX">
							<td>Top Amount </td>
							<td>$ 901000.00 USD </td>
						 </tr>
						</tbody>
						</table>
							</div>

						</div>
					</div>
				</div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>
       <script>
		//$(document).ready(function(){
			var seller_upd = function(el, flag, id){
				$.ajax({
					url:'<?=site_url();?>/admin/seller_upd/'+flag+'/'+id,
					processData:false,
					type:"POST",
					success:function(data){
						if(data == true){
							$(el).closest("tr").hide();
							$('#message').removeClass('hide');
							$('#msg').text('Status updated.');
							setTimeout(function() {
								$('#message').addClass('hide');
							}, 1000);
						}else{
							$('#message').removeClass('hide');
							$('#msg').text('Unable to update current status.');
							setTimeout(function() {
								$('#message').addClass('hide');
							}, 1000);
						}
					}
				});
			}
		//});
	   </script>