<?php   echo $layout['header'] ; 
$user_data = json_decode($seller);
?>  
   <div class="loadermodal" style="display: none">
		<div class="loadercenter">
			<img alt="" src="<?= base_url() ?>assets/admin/img/loader.gif" />
		</div>
	</div>

<div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-lg-10">
      <h2>Seller Details</h2>
   </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
      <div class="col-lg-12">
         <div class="ibox float-e-margins">
            <div class="ibox-title">
               <h5><?= $user_data->full_name; ?></h5>
            </div>
            <div class="ibox-content">
               <div class="table-responsive">
                  <table class="table table-striped table-bordered">
                     <thead>
                     </thead>
                     <tbody>
                        <tr></tr>
                        <tr class="gradeX">
                           <td>Full Name</td>
                           <td><?= $user_data->full_name; ?></td>
                        </tr>
                        <tr class="gradeX">
                           <td>User Name</td>
                           <td><?= $user_data->user_name; ?></td>
                        </tr>
                        <tr class="gradeX">
                           <td>Email</td>
                           <td><?= $user_data->email; ?></td>
                        </tr>
                        <tr class="gradeX">
                           <td>Create Date</td>
                           <td><?= gettime4db($user_data->user_created); ?></td>
                        </tr>
                        <tr class="gradeX">
                           <td>Status</td>
                           <td><?= ($user_data->status=='1') ? '<div class="label label-primary">Active</div>' : '<div class="label label-danger">Inactive</div>' ?></td>
                        </tr>
                        <tr class="gradeX">
                           <td>User Image</td>
                           <td><img src="<?php echo base_url();?>assets/admin/img/<?php if($user_data->picture == ""){echo "a5.jpg";}else{echo $user_data->picture;} ?>" class="thumbnail" width="40px" height="40px"></td>
                        </tr>
                        <tr class="gradeX">
                           <td colspan="2"><a href="<?= site_url() ?>admin/sellerlist" class="btn btn-primary">Back</a></td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>   
       
<?php echo $layout['footer']; ?>
