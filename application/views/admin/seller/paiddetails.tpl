<?php   echo $layout['header'] ; ?>  

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Tables</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Tables</a>
                        </li>
                        <li class="active">
                            <strong>Data Tables</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5> Vendor payment details</h5>
                    </div>
                    <div class="ibox-content">
                   <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Transaction Id </th>
                        <th>Transaction Date </th>
                        <th>Transaction Method </th>
                        <th>Amount</th>
                        <th>Status</th>
                       
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="gradeX">
                        <td>#1447780808 </td>
                        <td>2015-11-17 17:20:08 </td>
                        <td class="center">paypal </td>
                        <td class="center">20.00 </td>
                        <td class="center"><span class="btn-primary">success </span></td>
                    </tr>
                    <tr class="gradeX">
                        <td>#1447780808 </td>
                        <td>2015-11-17 17:20:08 </td>
                        <td class="center">paypal </td>
                        <td class="center">20.00 </td>
                        <td class="center"><span class="btn-primary">success </span></td>
                    </tr>
                   
                    </tfoot>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>  
        

       
       
<?php echo $layout['footer']; ?>

 <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
