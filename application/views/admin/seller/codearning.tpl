<?php   echo $layout['header'] ; 
$seller = json_decode($seller);
?>  
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>COD Earnings</h2>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Vender Name</th>
                        <th>Vendor Email</th>
                        <th>COD Order</th>
                        <th>COD Amount</th>
                        <th>COD Earning</th>
                        <th>Receive Amount</th>
                        <th>Balance</th>
                        <th>Update</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
						if($seller->status == 1){
							$data = $seller->data;
							$i = 0;
							foreach($data as $d){
					?>
                    <tr class="gradeX">
                        <td><?=$d->full_name;?></td>
                        <td><?=$d->email;?></td>
                        <td><?=$d->total_orders;?></td>
                        <td class="center">$<?=$d->tp_ord;?></td>
                        <td class="center">NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>$<?=$d->tp_txn;?> </td>
                        <td class="center">
                         <div class="btn-group">
                             <a href="<?php echo  base_url();?>admin/paiddetails/"   onclick="update_modal(<?php echo $d->id ; ?>)" class="btn btn-primary" data-toggle="modal" data-target="#myModal5">View</a>
							 <input type="hidden"  value='<?php print_R(json_encode($d->transactions)) ; ?>'  id="hidden-<?php echo $d->id ; ?>" />
                        </div>
                        </td>
                    </tr>
                   <?php
							}
						}else{
							echo "<tr><td colspan='11'>".$seller->messsage."</td></tr>";
						}
				   ?>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
          
        </div>
        

        </div>
       
<?php echo $layout['footer']; ?>

 
