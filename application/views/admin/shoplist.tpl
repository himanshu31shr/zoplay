<?php   echo $layout['header'] ; ?>  

           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Shop List</h2>
                   
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Shop List</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>shop name</th>
                        <th>owner name</th>
                        <th>create date</th>
                     	
                        <th>status</th>
                       
						
                    </tr>
                    </thead>
                    <tbody>
					<?php if(!empty($shoplist)){ ?>
					<?php foreach($shoplist as $sl){ ?>
                    <tr class="gradeX">
                        <td><?php echo $sl->shop_name; ?></td>
                        <td><?php echo $sl->full_name; ?></td>
                        <td><?php echo gettime4db($sl->createtime) ; ?></td>
                      
                        <td class="center"><?php echo ($sl->status == 1) ? '<span class="label label-success" style="cursor: pointer;" onclick="changestatus('.$sl->shop_id.',2)">Active</span>' : '<span class="label label-danger" style="cursor: pointer;" onclick="changestatus('.$sl->shop_id.',1)">Enactive</span>' ;  ?></td>
                         
                    </tr>
                    <?php } } ?>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>
<script>
function changestatus(id,status){
	$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	$.ajax({ 
		url:'<?=site_url();?>api/admin_api/shopstatus', 
		data: {'id':id,'status':status},
		datatype: 'json',
		type: 'POST',
		success: function(res)
		{
			 $(".overlay").remove();
			 //res = $.parseJSON(res);
			 //console.log(res.status);
			 if(res.status == true){
				toastr.success(res.message); 
				window.location.reload();
			 }else{
				 toastr.error(res.message);
			 }
			 
		}			
	}); 
}

</script>
 