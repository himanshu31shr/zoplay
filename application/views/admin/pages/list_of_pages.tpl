<?php   echo $layout['header'] ; ?>  

            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Tables</h2>
                   
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Basic Data Tables example with responsive plugin</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
					
                        <th><input type="checkbox" ></th>
                        <th>Page Name</th>
                        <th>Page Url</th>
                        <th>Category</th>
                     	<th>Hidden page</th>
						<th>status</th>
						<th>Action </th>
						
						
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="gradeX">
                        <td><input type="checkbox" ></td>
                        <td>Trident</td>
                        <td>http://etsyclone.zoplay.com/pages/your-threads</td>
                        <td>Win 95+</td>
                        <td class="center"></td>
                       
                        <td class="center">X</td>
						<td class="center">X</td>
                    </tr>
                    <tr class="gradeX">
                        <td><input type="checkbox" ></td>
                        <td>Trident</td>
                        <td>http://etsyclone.zoplay.com/pages/your-threads</td>
                        <td>Win 95+</td>
                        <td class="center"></td>
                       
                        <td class="center">X</td>
						<td class="center">X</td>
                    </tr>
                    <tr class="gradeX">
                        <td><input type="checkbox" ></td>
                        <td>Trident</td>
                        <td>http://etsyclone.zoplay.com/pages/your-threads</td>
                        <td>Win 95+</td>
                        <td class="center"></td>
                       
                        <td class="center">X</td>
						<td class="center">X</td>
                    </tr>
                    <tr class="gradeX">
                        <td><input type="checkbox" ></td>
                        <td>Trident</td>
                        <td>http://etsyclone.zoplay.com/pages/your-threads</td>
                        <td>Win 95+</td>
                        <td class="center"></td>
                       
                        <td class="center">X</td>
						<td class="center">X</td>
                    </tr>
                   
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>

 <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>