<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>vZoPlay | Login</title>

    <link href="<?=base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?=base_url();?>assets/admin/css/animate.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/admin/css/style.css" rel="stylesheet">
	<script src="<?=base_url();?>assets/admin/js/jquery-2.1.1.js"></script>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
	
		<div id="spinner" class="ibox-content hide">
			<div class="spiner-example">
			<div class="sk-spinner sk-spinner-chasing-dots">
				<div class="sk-dot1"></div>
				<div class="sk-dot2"></div>
			</div>
				</div>
		</div>
        <div id="lcont">
            <div>

                <h1 class="logo-name">VZP</h1>

            </div>
            <h3>Welcome to vZoPlay</h3>
            <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Login in</p>
			<div class="alert alert-warning hide" id="false"></div>
			<div class="alert alert-info hide" id="true"></div>
                        <form class="m-t" role="form" id="login">
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Username" required="">
					<div class="text text-danger text-left" id="email"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
					<div class="text text-danger text-left" id="password"></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" id="sbn_login">Login</button>

                <a href="#"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
            </form>
        </div>
    </div>
	
	<script>
		$(document).ready(function(){
			$('#login').on('submit', function(e){
				e.preventDefault();
				var data = new FormData($('#login')[0]);
				$.ajax({
					url: '<?=site_url();?>adminLogin/doLogin',
					type:"POST",
					data: data,
					contentType: false,
					processData:false,
					beforeSend:function(){
						$('#spinner').attr('class', 'ibox-content');
						$('#spinner').css('background', 'transparent');
						$('#spinner').css('padding', '100% 0%');
						$('#lcont').attr('class', 'hide');
					},
					success:function(data){
						if(data.status == false){
							$('.text-danger').empty();
							if(data.errorMsg!=''){
							var error = data.errorMsg;
							$.each(error, function(key, value){
								$('#'+key).text(value);
							});
							}
							$('#true').attr('class', 'alert alert-success hide').empty().html(data.message);	
							$('#'+data.status).removeClass('hide').empty().html(data.message);	
							$('#spinner').attr('class', 'ibox-content hide');
							$('#lcont').attr('class', '');
						}
						if(data.status == true){
							$('#false').attr('class', 'alert alert-success hide').empty().html(data.message);	
							$('#'+data.status).removeClass('hide').empty().html(data.message);	
							$('.text-danger').empty();
							$('#spinner').attr('class', 'ibox-content hide');
							$('#lcont').attr('class', '');
							$('#sbn_login').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Please wait!!').attr('disabled', 'disabled');
							window.location.replace('<?=site_url();?>admin');
						}
						$('#'+data.status).empty().html(data.message);	
					},
					error:function(data){
						
					}
				})
			});
		});
	</script>

    <!-- Mainly scripts -->
	
    <script src="<?=base_url();?>assets/admin/js/bootstrap.min.js"></script>

</body>

</html>
