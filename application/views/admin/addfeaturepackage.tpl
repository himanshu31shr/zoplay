<?php   echo $layout['header'] ; ?>  
<div class="loadermodal" style="display: none">
		<div class="loadercenter">
			<img alt="" src="<?= base_url() ?>assets/admin/img/loader.gif" />
		</div>
	</div>
           <div class="row wrapper border-bottom white-bg page-heading">
               <div class="col-lg-10">
                    <h2>Add New Package</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a>Forms</a>
                        </li>
                        <li class="active">
                            <strong>Basic Form</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add New Package</small></h5>
                        </div>
                        <div class="ibox-content">
                            <form method="post" name="addpackagefrm" id="addpackagefrm" class="form-horizontal">
                                <div class="form-group"><label class="col-sm-2 control-label">Package Name<span style="color: red">*</span></label>
                                 <div class="col-sm-10">
                                     <input type="text" name="package_name" class="form-control">
                                 </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Number of Days<span style="color: red">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="package_day" class="form-control"> 
                                    </div>
                                </div>
                                 <div class="form-group"><label class="col-sm-2 control-label">Amount<span style="color: red">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="package_amount" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                      <div class="switch">
                                        <div class="onoffswitch">
                                                <input type="checkbox" checked class="onoffswitch-checkbox" name="status" id="example1">												 
                                                <label class="onoffswitch-label" for="example1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                </label>
                                        </div>
                                           <input type="hidden" value="1" name="package_staus" id="status">
                                   </div> 
                                    </div>
                                </div>
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
 <?php echo $layout['footer']; ?>
