<?php   echo $layout['header'] ;
$userData = json_decode($userData);
$userlist = $userData->data;
?>            
	   <div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>User List</h2>
			</div>
			<div class="col-lg-2">
			</div>		
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Full name</th>
                        <th>Username</th>
                        <th>Email</th>
                     	<th>Thumbnail</th>
                        <th>Date</th>
                        <th>Ip Address</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($userlist)) { foreach($userlist as $user) { 
                        
                            $userImg ='';
                            if (!empty($user->picture) && file_exists('assets/uploads/'.$user->picture)) {
                                 $userImg= base_url().'assets/uploads/'.$user->picture;    
                            }else{
                              $userImg= base_url().'assets/admin/img/userdefault_img.png';
                            }
                       ?>
                            <tr class="gradeX">
                                    <td><?php echo !empty($user) ? $user->full_name : '' ?></td>
                                    <td><?php echo !empty($user) ? $user->user_name : '' ?></td>
                                    <td><?php echo !empty ($user) ? $user->email  : '' ?></td>
                                    <td>
                                        <div class="text-center">
                                             <img src="<?php echo $userImg; ?>" style="margin-left: 10px;" class="thumbnail" width="50px" height="40px">
                                        </div>
                                    </td>
                                    <td class="center"><?php echo !empty ($user->user_created) ? date("d-m-Y", strtotime($user->user_created)): '' ?></td>
                                    <td class="center"><?php echo !empty ($user) ? $user->ip_address : '' ?></td>
                                    <td class="center">
										<span style="cursor:pointer;" data="<?= $user->id; ?>" class="label  status_checks <?= ($user->status) ? "label-primary" : "label-danger" ?>">
											<?= ($user->status) ? "Active" : "Inactive" ?>
										</span>
										
                                    </td>
                                    <td class="text-center">
                                      <div class="btn-group">										  
										<a href="<?= site_url() ?>admin/viewUser/<?= base64_encode($user->id); ?>" title="View" >
											<button class=" btn btn-primary"><i class="fa fa-eye"></i></button>
										</a>									
										<button class=" btn btn-danger" onclick="deleteuser(<?= $user->id; ?>)">
											<i class="fa fa-trash"></i>
										</button>
										
										<button class=" btn btn-white" onclick="edit(<?= $user->id; ?>)">
											<i class="fa fa-edit"></i>
									  </button>										
                                        </div>
                                    </td>
                            </tr>  
                    <?php } } ?>							
                    </tbody>
                    </table>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>
<script>
function deleteuser(id)
{
	swal({
		  title: "Are you sure?",
		  text: "You want to delete",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Delete",
		  closeOnConfirm: false
		},
	function(){	
	$.ajax({
		url  : "<?= site_url() ?>admin/deleteuser",
		data : {userid : id},
		type : "POST",
		cache: false,				
	beforeSend : function()
	{
		$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	},
	success : function(res)
	{
		if(res.status == true)
		{
			$('.overlay').remove();
			toastr.success('User has been successfully deleted!', 'success');
			location.reload();
		}else{
			toastr.error('Somethings went wrong!', 'Error');
		}		
	}		
	}).done(function(res){
		swal("Deleted", "User has been successfully deleted!", "success");	
		$('.overlay').remove();
	}).error(function(){
		swal("Oops", "Somethings went wrong!", "error");
		toastr.error('Somethings went wrong!', 'Error');
		$('.loadermodal').hide();
	});
});
}

$(document).on('click','.status_checks',function(){
	var status = ($(this).hasClass('label-primary') ? '0' : '1');
	var msg = (status=='0' ? "Inactive" : "Active");
	var userid = $(this);	
	swal({
      title: "Are you sure?", 
      text: "Are you sure you want to "+msg, 
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Yes",
      confirmButtonColor: "#ec6c62"
    },function() {	
	$.ajax({
			url  : "<?= site_url() ?>admin/update_userstatus",
			data : {userid : $(userid).attr('data') , status : status},
			type : "POST",
			cache: false,
		beforeSend : function(){
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},	
		success  : function(response){						
			location.reload();
		}	 
	}).done(function(response){
		swal(msg, "Status has been successfully "+msg+"!", "success");		
        $('.overlay').remove();
	}).error(function(response){
		swal("Oops Not "+msg+" ", "Somethings went wrong!", "error");
	});
 });
});

function edit(id)
{
	var id = $.base64.btoa(id); 
	location.replace("<?= site_url() ?>admin/adduser/"+id)
}
</script>
