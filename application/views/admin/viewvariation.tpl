<?php   echo $layout['header'] ; ?>  
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>View Attribute</h2>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">						
						<div class="ibox-content">
						<div class="table-responsive">                                                
                        <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0">
						<thead>
						</thead>
						<tbody>
							<tr></tr>
						<tr class="gradeX">
							<td>Attribute Name</td>
							<td><?php echo !empty($variation_data[0]->attribute_name)? $variation_data[0]->attribute_name :' - '; ?></td>
						 </tr>
						<tr class="gradeX">
							<td>Attribute Option</td>
							<td><?php echo !empty($variation_data[0]->attribute_options)? $variation_data[0]->attribute_options : ' - ' ; ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Status</td>
							<td>
					<?php 	if($variation_data[0]->variation_status==0) 
									echo  "Inactive";else 
										if($variation_data[0]->variation_status==1) 
											echo "Active"; else 
												if($variation_data[0]->variation_status==2) 
													echo "Delete"; 
													else echo "-";
								  
							//echo !empty($variation_data[0]->variation_status)? //$variation_data[0]->variation_status : ' - '; ?>
							</td>
						 </tr>						
						 <tr class="gradeX">
							<td colspan="2"><a href="<?= site_url() ?>admin/variationslist" class="btn btn-primary">Back</a></td>
						 </tr>
						 
						</tbody>
						</table>					 
						</div>
					</div>
					</div>
				</div>
            </div>
        </div>
<?php echo $layout['footer']; ?>
