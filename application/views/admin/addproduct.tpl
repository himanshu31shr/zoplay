<?php   echo $layout['header'] ; ?> 

<?php
       $assets_url =  base_url()."/assets/admin/";
error_reporting( 0 );
		 $shipping_charges = unserialize($product_data['shipping_charges']);
												/* 	 echo "<pre>";
													 print_R($shipping_charges ); */
?>

		<link rel="stylesheet" href="<?php echo $assets_url ; ?>css/style.css">
		<!-- blueimp Gallery styles -->
		<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="<?php echo $assets_url ; ?>css/jquery.fileupload.css">
		<link rel="stylesheet" href="<?php echo $assets_url ; ?>css/jquery.fileupload-ui.css">
		 <link href="<?php echo $assets_url ; ?>css/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $assets_url ; ?>css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
		<!-- CSS adjustments for browsers with JavaScript disabled -->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<noscript><link rel="stylesheet" href="<?php echo $assets_url ; ?>css/jquery.fileupload-noscript.css"></noscript>
		<noscript><link rel="stylesheet" href="<?php echo $assets_url ; ?>css/jquery.fileupload-ui-noscript.css"></noscript>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css" rel="stylesheet" />
 <link href="<?php echo $assets_url ; ?>css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
      
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Basic Form</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Forms</a>
                        </li>
                        <li class="active">
                            <strong>Basic Form</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add New Product</h5>
                        </div>
                        <div class="ibox-content">
                          <form id="fileupload" action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group"><label class="col-sm-2 control-label">Title 
                                        <span></span></label>
								<input type="hidden" name="product_id"  id="product_id" class="form-control" value="<?php echo empty($product_data['product_id'])? "" : $product_data['product_id'] ;  ?>" >
                                    <div class="col-sm-10"><input type="text" name="product_title" class="form-control" value="<?php echo empty($product_data['product_name'])? "" : $product_data['product_name'] ;  ?>"  required ></div>
                                </div>
                           
                                <div class="form-group"><label class="col-sm-2 control-label">About this listing</label>
                                    <div class="col-md-2 col-sm-3 col-xs-12">
                                        <select  class="form-control" name="product_listing_maker">
                                           <?php   echo getListingMaker($product_data['product_listing_maker']); ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12" id="What_is" >
						<select  id="what_item" name="product_listing_use" class="form-control">
							<?php   echo getListingUse($product_data['product_listing_use']); ?>
						</select>
					</div>
					<div class="col-sm-2 col-xs-12" id="When_is" >
						<select  id="product_listing_order" name="product_listing_order" class="form-control">
								
									<?php   echo getListingOrder($product_data['product_listing_order']); ?>
						</select>
					</div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Categories</label>
                                    <div class="col-sm-10">
                                        <input type="text"  id="multi_select"  name="product_categories" multiple="multiple"  value="<?php echo empty($product_data['category_id'])? "" : $product_data['category_id'] ;  ?>"  />
                                      
                                    </div>
								</div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Pricing Format</label>
                                    <div class="col-sm-10">
                                          <label class="checkbox-inline"> 
                                        <input type="radio" name="pricing_format" data-pricing="fixed" id="type_fixed" value="1" <?php echo $product_data['product_pricing_format']==1? "checked" :"" ;  if(empty(trim($product_data['product_pricing_format']))){  echo "checked";}?>> Fixed </label> 
                                        <label class="checkbox-inline">
                                            <input type="radio" name="pricing_format" data-pricing="auction" id="type_auction" value="2" <?php echo $product_data['product_pricing_format']==2? "checked" :"" ;  ?> > Auction  
											</label>
                                    </div>
                                </div>
                                <div class="form-group">
									<label id="PriceLab" for="price" class="col-sm-2 control-label">Price   ($)</label>
									<div class="col-sm-2" id="price_div_disp">
										<input type="text" class="form-control" placeholder="$ :" value="<?php echo empty($product_data['product_price'])? "" : $product_data['product_price'] ;  ?>" id="price" name="price">
									</div>
									<div  class="list_inner_right" id="price_div_hid" style="display:none;">
										<span class="price-input-variations-notice">
											Price has been set by variation.<br>
											To set a general price <a href="javascript:void(0)" id="remove_attr_pricing">turn off pricing by variation</a>
										</span>
									</div>
									<div class="col-md-3 col-sm-12 right P8">
										<p>Factor in the costs of materials and labor, plus any related business expenses.</p>
									</div>
									<input type="hidden" name="price_status"  id="price_status" value="1" />
								</div>
				
				<div class="form-group auction" data-pricing-type="2" style="display:none">
					<label id="startingPrice" for="auction_starting_price" class="col-sm-2 control-label">Starting Price </label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="auction_starting_price" name="auction_starting_price" value="<?php echo empty($product_data['starting_price'])? "" : $product_data['starting_price'] ;  ?>">
					</div>
					<div class="col-sm-5"></div>
				</div>
				<div class="form-group auction" data-pricing-type="2" style="display:none">
					<label id="auctionDuration" for="auction_duration" class="col-sm-2 control-label">Duration(Days)</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="auction_duration" name="auction_duration" value="1">
					</div>
					<div class="col-sm-5"></div>
				</div>	
                                <div class="form-group"><label class="col-sm-2 control-label">Item type</label>
                                   <div class="col-sm-10">
                                          <label class="checkbox-inline"> 
                                        <input type="radio" name="item_type" id="physical_item" value="1" onclick="return dis_val('variation_wrapper')" <?php echo $product_data['item_type']==1? "checked" :"" ; if(empty(trim($product_data['item_type']))){  echo "checked";} ?>>Physical item </label> 
                                        <label class="checkbox-inline">
                                            <input type="radio" name="item_type" id="digital_item" value="2" onclick="return dis_val('file_wrapper')"  <?php echo $product_data['item_type']==2? "checked" :"" ;  ?>>Digital file</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label id="QuantityLab" for="quantity" class="col-sm-2 control-label">Quantity</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="product_quantity" name="quantity" value="1">
                                    </div>
                                    <div class="col-sm-5"></div>
                                    <div class="col-md-3 col-sm-12" id="qty_text_div">
                                            <p>For quantities greater than one, this listing will renew automatically until it sells out. You'll be charged a listing fee each time.₹ 324.85 INR</p>
                                    </div>
                                </div>
                            
                                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-6 border">
                                        <textarea id="summernote" name="product_desc" class="form-control summernote"><?php echo empty($product_data['product_description'])? "" : html_entity_decode( $product_data['product_description'] ) ;  ?></textarea>
                                    </div>
                                </div>
                                
                                <div class="row">
                                     <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <label class="control-label">
                                                    Deal Of The Day
                                                </label>
									
										<div class="form-group"><label class="col-lg-2 control-label">Deal Date Time</label>
											<div class="col-lg-5">
												<input placeholder="From Date Time" class="form-control datepicker " type="text" name="deal_from"  id="deal_from" value="<?php echo empty($product_data['deal_datetime_from'])? "" : $product_data['deal_datetime_from'] ;  ?>"> 
											</div>
											<div class="col-lg-5">
												<input placeholder="To Date Time" class="form-control datepicker" type="text" name="deal_to" value="<?php echo empty($product_data['deal_datetime_to'])? "" : $product_data['deal_datetime_to'] ;  ?>"> 
											</div>
										</div>
										<div class="form-group"><label class="col-lg-2 control-label">Discount</label>
											<div class="col-lg-5">
												<input placeholder="%:" class="form-control" type="decimal" name="deal_discount" value="<?php echo empty($product_data['product_discount'])? "" : $product_data['product_discount'] ;  ?>">
											</div>
											<div class=""></div>
										</div>
															
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												 <div class="col-lg-12">
													<div class="ibox float-e-margins">
														<div class="ibox-content">
															  <label class="control-label">
															   Images
															 </label>
															  
												<!-- Redirect browsers with JavaScript disabled to the origin page -->
												<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
												<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
												<div class="row fileupload-buttonbar">
													<div class="col-lg-7">
														<!-- The fileinput-button span is used to style the file input field as button -->
														<span class="btn btn-success fileinput-button">
															<i class="glyphicon glyphicon-plus"></i>
															<span>Add files...</span>
															<input type="file" name="files[]" multiple>
														</span>
														<button type="submit" class="btn btn-primary start">
															<i class="glyphicon glyphicon-upload"></i>
															<span>Start upload</span>
														</button>
														<button type="reset" class="btn btn-warning cancel">
															<i class="glyphicon glyphicon-ban-circle"></i>
															<span>Cancel upload</span>
														</button>
														<!--button type="button" class="btn btn-danger delete">
															<i class="glyphicon glyphicon-trash"></i>
															<span>Delete</span>
														</button-->
													  
														<span class="fileupload-process"></span>
													</div>
													<!-- The global progress state -->
													<div class="col-lg-5 fileupload-progress fade">
														<!-- The global progress bar -->
														<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
															<div class="progress-bar progress-bar-success" style="width:0%;"></div>
														</div>
														<!-- The extended global progress state -->
														<div class="progress-extended">&nbsp;</div>
													</div>
												</div>
												<!-- The table listing the files available for upload/download -->
												<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
										
   

													<!-- The blueimp Gallery widget -->
													<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
														<div class="slides"></div>
														<h3 class="title"></h3>
														<a class="prev">‹</a>
														<a class="next">›</a>
														<a class="close">×</a>
														<a class="play-pause"></a>
														<ol class="indicator"></ol>
													</div>
													<!-- The template to display files available for upload -->
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                  <label class="control-label">Variations</label>
                                             <div class="variations-div">
											 <?php 
											 error_reporting(0);
											    $variations = unserialize($product_data['product_variations']);
												
												if( !empty(  $variations ))
												{   foreach( $variations['options'] as $key=>$type)
													  {
														  ?>
															<div class="form-group single-variation">
                                                   <label class="col-lg-2 control-label">Add Variations</label>
                                                        <div class="col-lg-5">
                                                            <select class="form-control addvariations"  name="variations[types][]"  >
																<?php 
																  echo getVariations( $key );
																?>
															</select>
															 <a class="remove-variations" ><i class="fa fa-trash"></i> Remove </a>	
                                                        </div>
														 <div class="col-lg-5">
															<div class="options-div">
															
															<?php 
																foreach( $type as $single  )	
																{		
															?>
																<div class="form-group">
																	<div class="col-lg-12">
																		<input placeholder="" class="form-control variation-options" type="text"  name="variations[options][<?php echo  $key; ?>][]" value="<?php echo $single ; ?>">
																	</div> 
																</div>
															<?php 
																}
															?>		
															</div>	
															<div class="form-group">
																<a class="btn btn-danger  pull-right"  onclick="addOptions(this)" ><i class="fa fa-plus"></i> Add Options </a>
															</div>
                                                        </div>
                                               </div>

													
												<?php	  }
													
													
													?>
													  
                                             
											<?php 		
												}
											 ?>
												  <div class="form-group single-variation">
                                                   <label class="col-lg-2 control-label">Add Variations</label>
                                                        <div class="col-lg-5">
                                                            <select class="form-control addvariations"  name="variations[]"  >
																<?php 
																  echo getVariations();
																?>
															</select>
															 <a class="remove-variations" ><i class="fa fa-trash"></i> Remove </a>	
                                                        </div>
														 <div class="col-lg-5">
															<div class="options-div">
																<div class="form-group">
																	<div class="col-lg-12">
																		<input placeholder="" class="form-control variation-options" type="text"  name="variations[options][]">
																	</div> 
																</div>
															</div>	
															<div class="form-group">
																<a class="btn btn-danger  pull-right"  onclick="addOptions(this)" ><i class="fa fa-plus"></i> Add Options </a>
															</div>
                                                        </div>
                                               </div>
                                             
											</div>      
                                               
												
												<button class="btn btn-primary" id="add-variations"><i class="fa fa-plus"></i> Add Variations </button>
												
                                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                     <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                  <label class="control-label">Shipping</label>
                                          
												<div class="form-group">
											   
														<label class="col-lg-2 control-label">Delivery/Collection Options</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control" name="shipping_options" id="shipping_options">
															  <?php echo getDeliveryOptions() ;?>
															</select>
                                                        </div>
                                                </div>
                                                <div class="form-group">
													<label class="col-lg-2 control-label">Processing time</label>
                                                    <div class="col-lg-10">
                                                       <select class="form-control" name="shipping_process_time"  id="shipping_process_time">
													   <?php  echo getProcessingTime() ;?>
													   </select>
                                                    </div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-2 control-label">Ships from</label>
                                                    <div class="col-lg-10">
                                                        
														<select  class="form-control country" name="shipping_from">
                                                                <?php  echo getCountries( $product_data['shipping_from'])  ?>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="form-group"><label class="col-lg-2 control-label">Ships Cost</label>
                                                    <div class="col-lg-10">
                                                                            <table class="table table-bordered" id="shipping-table">
                                                    <thead>
                                                    <tr>
                                                    <th>Ships to (Country Name)</th>
                                                    <th>By itself</th>
                                                    <th>With another item</th>
                                                    <th>Remove</th>
                                                  
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                  
                                                     <tr>
                                                        <td>Everywhere Else
															<input type="hidden" value="0" name="shipping_to[]" />
														
														</td>
                                                        <td>
                                                            <input class="form-control shipping_txt_bax" value="<?php  echo !empty($shipping_charges[0]['shipping_cost'])?$shipping_charges[0]['shipping_cost'] : "" ;  ?>" name="shipping_cost[]" placeholder="$:" type="text">
                                                        </td>
                                                        <td><input class="form-control shipping_txt_bax" value="<?php  echo !empty($shipping_charges[0]['shipping_cost'])?$shipping_charges[0]['shipping_cost'] : "" ;  ?>" name="shipping_cost_with[]" placeholder="$:" type="text"></td>
														<td></td>
                                                      
                                                    </tr>
													 <?php 
													
												      for($i=1 ; $i < count($shipping_charges) ; $i++)
													  {
														  $shipping_charge = $shipping_charges[$i];
														
														?>
														         <tr>
                                                        <td>
                                                            <select  class="form-control country" name="shipping_to[]">
                                                                <?php  echo getCountries( $shipping_charge['country_id'] );  ?>
                                                            </select>
                                                        </td>
                                                        <td><input class="form-control shipping_txt_bax" value="<?php  echo !empty($shipping_charge['shipping_cost'])?$shipping_charge['shipping_cost'] : "" ;  ?>" name="shipping_cost[]" placeholder="$:" type="text"></td>
                                                        <td><input class="form-control shipping_txt_bax" value="<?php  echo  !empty($shipping_charge['shipping_cost_with'])? $shipping_charge['shipping_cost_with'] : "";  ?>" name="shipping_cost_with[]" placeholder="$:" type="text"></td>
														<td>
														  <a class="remove-shipping" ><i class="fa fa-trash"></i> Remove </a>
														
														</td>

                                                    </tr>
                                              
														  
												<?php	  }
												   ?>
                                                    </tbody>
                                                </table>
                                                        <button type="button" class=" btn btn-primary" id="add-location">Add Location</button>   
                                                    </div>
                                                </div>
                                          
                                            </div>
                                        </div>
                                    </div>
                                </div>
							   <div class="row">
                                     <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <label class="control-label">Search terms<br/><small>Help more people discover your listing by using accurate and descriptive words or phrases.</small></label>
												<br>
												<br>
												<br>
                                                  
											<div class="form-group">
                                                   <label class="col-lg-2 control-label">Tags</label>
                                                        <div class="col-lg-10">
															<input placeholder="" class="form-control" type="text" id="product_tags" name="product_tags" value="<?php echo empty($product_data['search_tags'])? "" : $product_data['search_tags'] ;  ?>">
                                                        </div>
											</div>
											<div class="form-group">
												<label class="col-lg-2 control-label">Materials</label>
                                                    <div class="col-lg-10">
                                                        <input id="material_tags" class="form-control" type="text" name="material_tags" value="<?php echo empty($product_data['search_materials'])? "" : $product_data['search_materials'] ;  ?>">
													</div>
                                                  
											</div>
                                          
                                            </div>
                                        </div>
                                    </div>
								</div>
                                <div class="alert alert-success success-response" style="display:none;">
								</div>
								<div class="alert alert-danger danger-response" style="display:none;">
								</div>
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">Cancel</button>
                                        <button class="btn btn-primary" id="save_product">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php echo $layout['footer']; ?>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({height: 300});
        
        $("#type_auction").on("click",function (){
            $(".auction").css('display','block');
        });

        $("#type_fixed").on("click",function (){
            $(".auction").css('display','none');
        });
    });
 </script>  
 <script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="hidden" name="filer[]"  value="{%=file.name%}">
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<script src="<?php echo $assets_url ; ?>js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo $assets_url ; ?>js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo $assets_url ; ?>js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo $assets_url ; ?>js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo $assets_url ; ?>js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo $assets_url ; ?>js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo $assets_url ; ?>js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo $assets_url ; ?>js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo $assets_url ; ?>js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->

<script src="<?php echo $assets_url ; ?>js/main.js"></script>
<script src="<?php echo $assets_url ; ?>js/select2.js"></script>
<script src="<?php echo $assets_url ; ?>js/tag-it.min.js"></script>
<script src="<?php echo $assets_url ; ?>js/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
 $(".country").select2();
	$('body').on( "focus" , ".country", function(){
		 $(this).select2();
		
	}); 
	$('.datepicker').datetimepicker();
	   $('#product_tags').tagit();
	   $('#material_tags').tagit();
	   
		$("#multi_select").select2({
			maximumSelectionLength: 1,
			initSelection: function(element, callback) {
				var data = <?php  echo getThisData( $product_data['category_id'])  ;  ?>;
				callback(data);
			},
			closeOnSelect: false,
			allowClear: true,
			maximumSelectionSize: 4,
			multiple: true,
			ajax: {
				 url: '<?php echo site_url();?>/Admin/get_categories',
				 dataType: 'json',
				 delay: 250,
				 data: function (params) {
				   return  "q="+ params ;
				  },
				processResults: function(data, params) {
					return data;
					console.log(data);
				},
				cache: false
			}
		});

		setTimeout(function() {
			$('#multi_select').select2('val', $('#multi_select').val());
		}, 3000);
		
		$("#multi_select").on("select2-removed", function(obj) {
			var fs = "";
			console.log(obj.val);
			console.log($("#multi_select").val());
			for (var i = 0; i < $("#multi_select").select2("val").length; i++) {

				if ($("#multi_select").select2("val")[i].trim() == obj.val.trim()) {

				} else {
					fs = $("#multi_select").select2("val")[i] + "," + fs;
				}
			}
			fs = fs.replace(/(^\s*,)|(,\s*$)/g, '');
			console.log(fs);
			$("#multi_select").val(fs);
		});
		
	</script>
	
<script>
  $("#save_product").on("click" , function(){
				$("#fileupload").on("submit" , function( e ){
					e.preventDefault();
					var formdata = new FormData( this );
					$.ajax({
						url 	 : "<?php echo site_url() ;?>/Admin/new_product" ,
						data     : formdata ,
						type :"POST" ,
						processData : false ,
						contentType : false ,
						success  : function( data ){
						 var obj = $.parseJSON( data );
						
						if(obj.status == "success"){
							console.log( obj );
							$(".success-response").css("display" , "block");
							$(".success-response").html(obj.msg);
							 window.location.replace('<?php echo site_url(); ?>/admin/addproduct/' + obj.data.id); 			
						}
else{
	
	$(".danger-response").css("display" , "block");
							$(".danger-response").html(obj.msg);
}						
						}
					});					
					
				});
			
  });
  
  $("#add-location").on("click" , function(){
	  var html = `<tr>
                                                        <td>
                                                            <select  class="form-control country" name="shipping_to[]" >
                                                                 <?php echo getCountries() ; ?>
                                                            </select>
                                                        </td>
                                                        <td><input class="form-control shipping_txt_bax" value="" name="shipping_cost[]" placeholder="$:" type="text"></td>
                                                        <td><input class="form-control shipping_txt_bax" value="" name="shipping_cost_with[]" placeholder="$:" type="text"></td>
														<td>
														    <a class="remove-shipping" ><i class="fa fa-trash"></i> Remove </a>
														</td>
                                                    </tr>`;
	  $("#shipping-table tbody").append(html);
	  
	  
  });
  
  var $variation = ` <div class="form-group single-variation">
                                                   <label class="col-lg-2 control-label">Add Variations</label>
                                                        <div class="col-lg-5">
                                                            <select class="form-control addvariations" name="variations[types][]">
																<?php 
																  echo getVariations();
																?>
															</select>
														  <a class="remove-variations" ><i class="fa fa-trash"></i> Remove </a>	
                                                        </div>
														 <div class="col-lg-5">
															 <div class="options-div">
																<div class="form-group">
																	<div class="col-lg-12">
																		<input placeholder="" class="form-control variation-options" type="text"  name="variations[options][]" >
																	</div>
																</div>
																
															 </div>
																 
															 <div class="form-group">
																<a class="btn btn-danger add-options pull-right" onclick="addOptions(this)" ><i class="fa fa-plus"></i> Add Options </a>
															 </div>
															
                                                        </div>
					</div>
                                               `;
  var $var_options = `	<div class="form-group">
								<div class="col-lg-12">
									<input placeholder="" class="form-control variation-options" type="text" name="variations[options][]">
								</div>
						</div> `; 
						
   function addOptions(e)  {
	   
		$(e).parent().parent().children('.options-div').append( $var_options);  
	  
  }
  $("#add-variations").on("click" ,  function(e){
	  e.preventDefault();
	  $(".variations-div").append($variation);
	  
  });
/* $('body').on("change" , ".addvariations" , function(){
	 console.log($(this).parent().parent().find(".variation-options" ).attr("name" , "variations[options]["+$(this).val()+"][]"));
	
}); */
  
$("body").on("click" , ".variation-options" , function(){
   var name = $(this).parents(".single-variation").children().find(".addvariations").val();
$(this).attr("name" , "variations[options]["+name+"][]" );
});  


$("body").on("click" , "a.remove-variations" , function(){ $(this).parents(".single-variation").remove(); }); 
$("body").on("click" , "a.remove-shipping" , function(){ $(this).parents("tr").remove(); });

$("#product_listing_order").val('<?php echo !empty($product_data['product_listing_order'])?$product_data['product_listing_order']: "" ?>');
$("#shipping_process_time").val('<?php echo !empty($product_data['shipping_process_time'])?$product_data['shipping_process_time']: "" ?>');
$("#shipping_options").val('<?php echo !empty($product_data['shipping_options'])?$product_data['shipping_options']: "" ?>');

</script>

<style>
 .select2-input{width:100% !important;}
 .select2-container{ width : 100% !important ;}
</style>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
