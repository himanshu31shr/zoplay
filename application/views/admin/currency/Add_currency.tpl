<?php   echo $layout['header'] ; ?>     
<div class="loadermodal" style="display: none">
    <div class="loadercenter">
        <img alt="" src="<?php echo base_url() ?>assets/admin/img/loader.gif" />
    </div>
</div>
	   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                        <h2>Add New currency</h2>
                </div>
                <div class="col-lg-2">
                </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
         <div class="row">
            <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <form method="POST"  name="addcurrencyfrm" id="addcurrencyfrm" class="form-horizontal" >                               
                                <div class="form-group"><label class="col-lg-2 control-label">Currency Name<span style="color: red">*</span></label>
                                    <div class="col-lg-10">
					<input type="text" placeholder="Currency Name" id="currency_name" name="currency_name" class="form-control"> 
                                    </div>
                                </div> 
				<div class="form-group"><label class="col-lg-2 control-label">Currency Code<span style="color: red">*</span></label>
                                    <div class="col-lg-10">
					<input type="text" placeholder="Currency Code" id="currency_code" name="currency_code" class="form-control"> 
                                    </div>
                                </div>
				<div class="form-group"><label class="col-lg-2 control-label">Currency Symbol<span style="color: red">*</span></label>
                                    <div class="col-lg-10">
					<input type="text" placeholder="Currency Symbol" id="currency_symbol" name="currency_symbol" class="form-control"> 
                                    </div>
                                </div>
				<div class="form-group"><label class="col-lg-2 control-label">Currency Value<span style="color: red">*</span></label>
                                    <div class="col-lg-10">
					<input type="text" placeholder="Currency Value" id="currency_value" name="currency_value" class="form-control"> 
                                    </div>
                                </div>
                               <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                            <div class="switch">
                                                     <div class="onoffswitch">
                                                             <input type="checkbox" checked class="onoffswitch-checkbox" name="status" id="example1">												 
                                                             <label class="onoffswitch-label" for="example1">
                                                                     <span class="onoffswitch-inner"></span>
                                                                     <span class="onoffswitch-switch"></span>
                                                             </label>
                                                     </div>
                                                     <input type="hidden" value="1" name="userstatus" id="userstatus">
                                            </div>           
                                    </div>
                            </div>                               
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>                
			</div>              
         </div>            
<?php echo $layout['footer']; ?>
