<?php   echo $layout['header'];
		$currencyData = json_decode($currencyData);
		$currencylist = $currencyData->data;
 ?>  

	   <div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Add New Currency</h2>
			</div>
			<div class="col-lg-2">
			</div>
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">                   
                    <div class="ibox-content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Currency Name</th>
                        <th>Symbol</th>
                        <th>Currency Code</th>
                     	<th>Currency value</th>
						<th>Status</th>
						<th>Defualt Currency</th>
                        <th>Action</th>
						
                    </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($currencylist)) {
                        foreach($currencylist as $clist) { 
                        ?>
                    <tr class="gradeX">
                        <td><?= $clist->currency_name; ?></td>
                        <td><?= $clist->currency_code; ?></td>
                        <td><?= $clist->currency_symbol; ?></td>
                        <td class="center"><?= $clist->currency_value; ?></td>
                        <td class="center"><?= ($clist->currency_status=='1') ? 'Active' : 'Inactive' ?></td>
                            <td class="center">$</td>                       
                            <td class="center">
                                    <a class="action-icons c-suspend" href="<?= site_url() ?>admin/view_currency/<?= base64_encode($clist->currency_id);?>" title="View">
                                       <button class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                    </a>
                            </td>
                     </tr> 	
			<?php } } ?>					
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>       
<?php echo $layout['footer']; ?>