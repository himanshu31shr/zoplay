<?php   echo $layout['header'];
		
 ?>  
 <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>View Currency</h2>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">						
						<div class="ibox-content">
						<div class="table-responsive">
                                                <?php if(!empty($currency_data)){ 
                                                
                                                    $currencyData = json_decode($currency_data);
                                                    $currencylist = $currencyData->data;
                                                ?>
                                                    <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0">
						<thead>
						</thead>
						<tbody>
							<tr></tr>
						<tr class="gradeX">
							<td>Currency Name</td>
							<td><?php echo  !empty($currencylist[0]->currency_name)? $currencylist[0]->currency_name : ' - '; ?></td>
						 </tr>
						<tr class="gradeX">
							<td>Currency Code</td>
							<td><?php  echo !empty($currencylist[0]->currency_code)? $currencylist[0]->currency_code:' - ';  ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Currency Symbol</td>
							<td><?php echo !empty($currencylist[0]->currency_symbol)? $currencylist[0]->currency_symbol :' - '; ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Currency Value</td>
							<td><?php echo !empty($currencylist[0]->currency_value)? $currencylist[0]->currency_value :' - '; ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Currency Status</td>
							<td><?php echo  !empty($currencylist[0]->currency_status) ? 'Active' : 'Inactive' ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Currency Added Date</td>
							<td><?php echo !empty($currencylist[0]->currency_added)? $currencylist[0]->currency_added :' - ';  ?></td>
						 </tr>
						 <tr class="gradeX">
							<td colspan="2"><a href="<?= site_url() ?>admin/currency_list" class="btn btn-primary">Back</a></td>
						 </tr>
						</tbody>
						</table>
                                                 <?php }else{ ?>
                                                 <div class="alert alert-danger">No data found </div>
                                                <?php } ?>   
                                                	</div>
						</div>
					</div>
				</div>
            </div>
        </div>    
<?php echo $layout['footer']; ?>