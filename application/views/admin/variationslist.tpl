<?php   echo $layout['header'] ; ?>               
	   <div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Product Attribute Details</h2>				
			</div>
			<div class="col-lg-2">
			</div>
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5> Product Attribute Details</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Attribute Name</th>
                        <th>Attribute Options</th>
                        <th>Scaling Options</th>
                        <th>status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach($variation_data as $key=>$vdata) { ?>
                    <tr class="gradeX">
                        <td><?= $vdata->attribute_name; ?></td>
                        <td><?= $vdata->attribute_options;?></td>
                        <td><?= ($vdata->scaling_options=='1') ? "Yes" : "No" ?></td>
                        <td class="center"> 
							<span class="label label-primary">
							<?php if($vdata->variation_status==0) echo  "Inactive";else 
								  if($vdata->variation_status==1) echo "Active"; else 
								  if($vdata->variation_status==2) echo "Delete"; 
						    ?>
							</span>
						</td>
                         <td class="text-right">
                        <div class="btn-group">
                            <button class="btn-white btn btn-xs" onclick="view_variation(<?=$vdata->variation_id; ?>)">View</button>
                            <button class="btn-white btn btn-xs" onclick="edit_variation(<?=$vdata->variation_id; ?>)">Edit</button>
                        </div>
                    </td>			
                    </tr>
					<?php } ?>					
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>       
<?php echo $layout['footer']; ?>
<script>
function view_variation(id)
{
	var id = $.base64.btoa(id);
	location.replace('<?= site_url() ?>admin/view_variation/'+id);
}

function edit_variation(id)
{
	var id = $.base64.btoa(id);	
	location.replace('<?= site_url() ?>admin/addvariations/'+id);
}
</script>