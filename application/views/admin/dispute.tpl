<?php   echo $layout['header'] ; ?>  
 <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Tables</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Tables</a>
                        </li>
                        <li class="active">
                            <strong>Data Tables</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Dispute Managment</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Days</th>
                        <th>Thumbnail</th>
                        <th>Amount</th>
                        <th>status</th>
                        <th>Action</th>
						
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="gradeX">
                        <td>Trident</td>
                        <td>Internet Explorer 4.0</td>
                         <td class="center"><img src="<?php echo base_url();?>assets/admin/img/a5.jpg" class="thumbnail" width="40px" height="40px"></td>
                        <td>Win 95+</td>
                        <td class="center"> <span class="label label-primary">Enable</span></td>
                         <td class="text-right">
                        <div class="btn-group">
                            <button class="btn-white btn btn-xs">View</button>
                            <button class="btn-white btn btn-xs">Edit</button>
                        </div>
                    </td>
			
                    </tr>
                    <tr class="gradeX">
                        <td>Trident</td>
                        <td>Internet Explorer 4.0</td>
                         <td class="center"><img src="<?php echo base_url();?>assets/admin/img/a5.jpg" class="thumbnail" width="40px" height="40px"></td>
                        <td>Win 95+</td>
                        <td class="center"> <span class="label label-primary">Enable</span></td>
                          <td class="text-right">
                            <div class="btn-group">
                                <button class="btn-white btn btn-xs">View</button>
                                <button class="btn-white btn btn-xs">Edit</button>
                            </div>
                        </td>
			
                    </tr> <tr class="gradeX">
                        <td>Trident</td>
                        <td>Internet Explorer 4.0</td>
                         <td class="center"><img src="<?php echo base_url();?>assets/admin/img/a5.jpg" class="thumbnail" width="40px" height="40px"></td>
                        <td>amit@argalon.net</td>
                      <td class="center"><span class="label label-danger">Disabled</span></td>
			  <td class="text-right">
                            <div class="btn-group">
                                <button class="btn-white btn btn-xs">View</button>
                                <button class="btn-white btn btn-xs">Edit</button>
                            </div>
                        </td>
                    </tr> <tr class="gradeX">
                        <td>Trident</td>
                        <td>Internet Explorer 4.0</td>
                        <td>Win 95+</td>
                      <td class="center"><span class="label label-danger">Disabled</span></td>
			  <td class="text-right">
                            <div class="btn-group">
                                <button class="btn-white btn btn-xs">View</button>
                                <button class="btn-white btn btn-xs">Edit</button>
                            </div>
                        </td>
                    </tr> <tr class="gradeX">
                        <td>Trident</td>
                        <td>Internet Explorer 4.0</td>
                         <td class="center"><img src="<?php echo base_url();?>assets/admin/img/a5.jpg" class="thumbnail" width="40px" height="40px"></td>
                          <td>Win 95+</td>
                     <td class="center"><span class="label label-danger">Disabled</span></td>
			  <td class="text-right">
                            <div class="btn-group">
                                <button class="btn-white btn btn-xs">View</button>
                                <button class="btn-white btn btn-xs">Edit</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>

 <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>