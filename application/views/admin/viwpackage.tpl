<?php   echo $layout['header'] ; ?>  
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>View Package</h2>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">						
						<div class="ibox-content">
						<div class="table-responsive">
                                                    <?php 
                                                      if(!empty($user_data)){
                                                        $user_data = json_decode($user_data); ?>
                                                        <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0">
                                                        <thead>
                                                        </thead>
                                                        <tbody>
                                                                <tr></tr>
                                                        <tr class="gradeX">
                                                                <td>Package Name</td>
                                                                <td><?php echo !empty($user_data[0]->package_name)? $user_data[0]->package_name  : ' - '  ?></td>
                                                         </tr>
                                                        <tr class="gradeX">
                                                                <td>Number of Days</td>
                                                                <td><?php echo !empty($user_data[0]->package_days)? $user_data[0]->package_days : ' - '; ?></td>
                                                         </tr>
                                                         <tr class="gradeX">
                                                                <td>Amount</td>
                                                                <td><?php echo !empty($user_data[0]->package_amount)? $user_data[0]->package_amount :' - ' ; ?></td>
                                                         </tr>
                                                         <tr class="gradeX">
                                                                <td>Status</td>
                                                                <td><?php echo !empty($user_data[0]->package_status) ? 'Active' : 'Inactive' ?></td>
                                                         </tr>
                                                         <tr class="gradeX">
                                                            <td colspan="2">
                                                                <a href="<?php echo site_url(); ?>admin/featurepackagelist" class="btn btn-primary">Back</a>
                                                            </td>
                                                         </tr>
                                                        </tbody>
                                                        </table>
                                                     <?php }else{ ?>
                                                     <div class="aler alert-danger"> No Data found </div>
                                                     <?php } ?>
                                                   	</div>
						</div>
					</div>
				</div>
            </div>
        </div>
   <?php echo $layout['footer']; ?>
