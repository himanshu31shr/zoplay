<?php   echo $layout['header'] ; ?>  
 <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Basic Form</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Forms</a>
                        </li>
                        <li class="active">
                            <strong>Basic Form</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit User</h5>
                        </div>
                        <div class="ibox-content">
                            <form method="post" action="<?= site_url() ?>admin/edituser" id="editUser" class="form-horizontal">
                                <div class="form-group"><label class="col-sm-2 control-label">Full Name</label>
                                    <div class="col-sm-10"><input type="text" name="full_name" class="form-control"></div>
                                </div>
                           
                                <div class="form-group"><label class="col-sm-2 control-label">User Name</label>
                                    <div class="col-sm-10"><input type="text" name="user_name" class="form-control"> <span class="help-block m-b-none">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10"><input type="text" name="email" class="form-control"> <span class="help-block m-b-none">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                                    </div>
                                </div>                            
                                <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                <div class="switch">
                                 <div class="onoffswitch">
                                     <input type="checkbox" checked class="onoffswitch-checkbox" name="status" id="example1">
                                     <label class="onoffswitch-label" for="example1">
                                         <span class="onoffswitch-inner"></span>
                                         <span class="onoffswitch-switch"></span>
                                     </label>
                                 </div>
                             </div>           
                             </div>
                                </div>
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">Cancel</button>
                                        <button class="btn btn-primary" name="submit" type="submit">Update changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>     			
<script>
$(function () {
	// validate signup form on keyup and submit
	$("#editUser").validate({
		rules: {
			full_name: "required",				
			user_name: {
				required: true,
				minlength: 5
			},
			email: {
				required: true,
				email: true
			},				
		},
		messages: {
			firstname: "Please enter your fullname",
			user_name: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 5 characters"
			},			
			email: "Please enter a valid email address",
		},
		submitHandler: function (form) {                 
              toastr.success('User Update Successfull', 'Success');
			  form.submit();
              }
	});
}); 
</script>		
<?php echo $layout['footer']; ?>
