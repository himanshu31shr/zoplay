<?php   echo $layout['header'] ; ?>  

            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>SUBSCRIBERS LIST</h2>
                   
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>SUBSCRIBERS LIST</h5>
                      
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th><input type="checkbox" ></th>
                        <th>Symbol</th>
                        <th>Email Address</th>
                     	
						<th>Status</th>
					
                        <th>Action</th>
						
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="gradeX">
                        <td><input type="checkbox" ></td>
                        <td>LUCKYargalon@gmail.com</td>
                        <td>LUCKYargalon@gmail.com</td>
                        <td class="center">Active</td>
						<td class="center">YES</td>
                    </tr> 
					<tr class="gradeX">
                        <td><input type="checkbox" ></td>
                        <td>Akhilesh tenguriya7@gmail.com</td>
                        <td>Akhilesh tenguriya7@gmail.com</td>
                        <td class="center">Active</td>
						<td class="center">YES</td>
                    </tr>
					<tr class="gradeX">
                        <td><input type="checkbox" ></td>
                        <td>abc@gmail.com</td>
                        <td>abc@gmail.com</td>
                        <td class="center">Active</td>
						<td class="center">YES</td>
                    </tr> 
				
                   
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>

 <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>