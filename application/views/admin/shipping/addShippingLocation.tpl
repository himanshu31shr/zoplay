<?php   echo $layout['header'] ; ?>   
	
	 <div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Add Shipping Location</h2>
			</div>
			<div class="col-lg-2">

			</div>
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <form id="addlocation" class="form-horizontal" method="post">
                                <div class="form-group"><label class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-10">
										<select name="country" id="country" class="form-control">
											<option value="0">Select Country</option>
											<?php if(!empty($countries)){
												foreach($countries as $country){
													echo '<option value="'.$country->id.'">'.$country->name.'</option>';
												}
											} ?>
										</select>
									</div>
                                </div>
                           
                                <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
										<div class="switch">
											 <div class="onoffswitch">
												 <input type="checkbox" checked class="onoffswitch-checkbox" name="status1" id="example1">												 
												 <label class="onoffswitch-label" for="example1">
													 <span class="onoffswitch-inner"></span>
													 <span class="onoffswitch-switch"></span>
												 </label>
											 </div>
											 <input type="hidden" value="1" name="status" id="status">
										</div>           
									</div>
								</div>
								
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" name="button"  type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>     					
<script>
$(document).ready(function() {
    $("#example1").click(function() {
        if (this.checked) {
           $('#status').val('1');
        }else{
			$('#status').val('2');
		}
    });
});
$(function () {
	// validate signup form on keyup and submit
	$("#addlocation").validate({
		rules: {
			country: "required",				
						
		},
		messages: {
			country: "Please Select Country",
			
		},
		submitHandler: function (form) { 
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');		
			
					$.ajax({ 
						url:'<?=site_url();?>api/admin_api/addShippingLocation', 
						data: $("#addlocation").serialize(),
						datatype: 'json',
						type: 'POST',
						success: function(res)
						{
							 $('.overlay').remove();
							 //res = $.parseJSON(res);
							 //console.log(res.status);
							 if(res.status == true){
								toastr.success(res.message); 
								window.location.href ="<?= site_url() ?>admin/adminShipping"
							 }else{
								 toastr.error(res.message);
							 }
							 
						}			
					}); 			
          }
	});
}); 

</script>		
<?php echo $layout['footer']; ?>