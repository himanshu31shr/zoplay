<?php   echo $layout['header'] ; ?> 
<div class="loadermodal" style="display: none">
		<div class="loadercenter">
			<img alt="" src="<?= base_url() ?>assets/admin/img/loader.gif" />
		</div>
	</div> 
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Shipping Location</h2>
                   
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Shipping Location</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Country</th>
                        <th>Status</th>
                        <th>Action</th>
						
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($locations)){ ?>
						<?php foreach($locations as $loc){ ?>
					<tr class="gradeX">
                        <td><?php echo $loc->name ;  ?></td>
						<td class="center"><?php echo ($loc->status == 1) ? '<span class="label label-success" style="cursor: pointer;" onclick="changestatus('.$loc->sl_id.',2)">Active</span>' : '<span class="label label-danger" style="cursor: pointer;" onclick="changestatus('.$loc->sl_id.',1)">Enactive</span>' ;  ?></td>
						<td class="text-right">
                            <div class="btn-group">
                                <button class="btn-danger btn" onclick="deleteLocation(<?= $loc->sl_id ; ?>)"><i class="fa fa-trash"></i></button>
                            </div>
                        </td>
                    </tr>
					<?php } }  ?>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
    
<?php echo $layout['footer']; ?>
<script>
function changestatus(id,status){
	$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	$.ajax({ 
		url:'<?=site_url();?>api/admin_api/shippinglocationstatus', 
		data: {'id':id,'status':status},
		datatype: 'json',
		type: 'POST',
		success: function(res)
		{
			 $(".overlay").remove();
			 //res = $.parseJSON(res);
			 //console.log(res.status);
			 if(res.status == true){
				toastr.success(res.message); 
				window.location.reload();
			 }else{
				 toastr.error(res.message);
			 }
			 
		}			
	}); 
}
var did = '';
function deleteLocation(id){
	did = id;
	swal({
		  title: "Are you sure?",
		  text: "You want to delete",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Yes",
		  closeOnConfirm: true
		},
	function(){	
		$.ajax({
			url:'<?=site_url();?>api/admin_api/deleteshippinglocation', 
			data: {'id':did},
			datatype: 'json',
			type: 'POST',			
		beforeSend : function()
		{
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},
		
	}).done(function(res){
		$('.overlay').remove();
		if(res.status == true){
				toastr.success(res.message); 
				window.location.reload();
			 }else{
				 toastr.error(res.message);
			 }
	}).error(function(){
		swal("Oops", "Somethings went wrong!", "error");
		toastr.error('Somethings went wrong!', 'Error');
		$('.overlay').remove();
	});
});
}


</script>