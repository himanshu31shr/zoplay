<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="<?php echo base_url();?>assets/admin/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Gritter -->
     <link href="<?php echo base_url();?>assets/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/admin/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
      <script>
            var site_url = '<?php echo base_url() ?>';
    </script>
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="<?=base_URL()?>assets/frontend/js/plugins/sweetalert/sweetalert.min.js"></script>
	<link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-light.min.css" />
	<link rel="stylesheet" href="<?=base_URL();?>assets/frontend/css/lightbox.css">	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
	<script src="<?php echo base_url();?>assets/admin/js/jquery-2.1.1.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/jquery-validate.js"></script>    
	<script src="<?php echo base_url();?>assets/admin/js/additional-methods.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>
	<script src="<?=base_url();?>assets/frontend/js/jquery.base64/jquery.base64.js"></script>
	<link rel="stylesheet" href="<?=base_url();?>assets/frontend/custom.css">
	<script src="<?= base_url()?>assets/frontend/js/seller_custom.js"></script>
	<script>
		var base_url='<?php echo base_url() ?>';
	</script> 

	<script src="<?php echo base_url() ?>/assets/admin/js/jquery.datetimepicker.full.min.js"></script>
	<link href="<?php echo base_url() ?>/assets/admin/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">	
</head>
<body> 
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?php echo base_url() ;?>assets/admin/img/profile_small.jpg" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="profile.html">Profile</a></li>
                                <li><a href="contacts.html">Contacts</a></li>
                                <li><a href="mailbox.html">Mailbox</a></li>
                                <li class="divider"></li>
                                <li><a href="login.html">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    <li class="">
                        <a href="index.html"><i class="fa fa-user"></i> <span class="nav-label">User</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="userdashboard"><a href="<?php echo site_url();?>admin/userdashboard">Dashboard</a></li>
                            <li id="userlist" ><a href="<?php echo site_url();?>admin/userlist">User List</a></li>
                            <li id="adduser"><a href="<?php echo site_url();?>admin/adduser">Add New User</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="layouts.html"><i class="fa fa-user"></i> <span class="nav-label">Seller</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="dashboard"><a href="<?php echo site_url();?>admin/dashboard">Dashboard </a></li>
                            <li id="sellerlist"><a href="<?php echo site_url();?>admin/sellerlist">Seller List</a></li>
                            <li id="siteearning"><a href="<?php echo site_url();?>admin/siteearning">Site Earning</a></li>
                            <li id="codearning"><a href="<?php echo site_url();?>admin/codearning">COD Earning</a></li>
							<li id="codearning"><a href="<?php echo site_url();?>admin/doc_varification">Document Varification</a></li>
                        </ul>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-file"></i> <span class="nav-label">Shop Details</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                           <li class="active" id="shoplist"><a href="<?php echo site_url()?>admin/shoplist">List of shops </a></li>  
                        </ul>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-diamond"></i> <span class="nav-label">Category</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="category"><a href="<?php echo site_url();?>admin/category">Category List</a></li>
                            
                        </ul>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-folder"></i> <span class="nav-label">Product</span><span class="fa arrow"></span></a>

						<ul class="nav nav-second-level">
                            <li id="productlist"><a href="<?php echo site_url();?>admin/productlist">Product List </a></li>
                            <li id="recentproductlist"><a href="<?php echo site_url();?>admin/recentproductlist">Recent Product List</a></li>
                            <li id="addproduct"><a href="<?php echo site_url();?>admin/addproduct">Add New Product</a></li>
                            <li id="deleteproductlist"><a href="<?php echo site_url();?>admin/deleteproductlist">Deleted Product List</a></li>
                           
			
                        </ul>
                    </li>                
					<li>
                        <a href="layouts.html"><i class="fa fa-diamond"></i> <span class="nav-label">Affiliate Settings</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="affiliatesettings"><a href="<?php echo site_url();?>admin/affiliatesettings">Settings</a></li>
                            
                           
                            
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo site_url();?>admin/adminShipping"><i class="fa fa-folder"></i> <span class="nav-label"> Shipping</span><span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
                            <li id="shippinglocation"><a href="<?php echo site_url();?>admin/adminShipping">Location</a></li>
                            <li id="addShippingLocation"><a href="<?php echo site_url();?>admin/addShippingLocation">Add Location</a></li>
                          
                        </ul>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-folder"></i> <span class="nav-label"> Orders</span><span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
                            <li id="paidpayment"><a href="<?php echo site_url();?>admin/paidpayment">Paid Payment</a></li>
                            <li id="failedpayment"><a href="<?php echo site_url();?>admin/failedpayment">Failed Payment</a></li>
                            <li id="cashdelivery"><a href="<?php echo site_url();?>admin/cashdelivery">Cash On Delivery Payment</a></li>
                            <li id="transferpayment"><a href="<?php echo site_url();?>admin/transferpayment">Wire Transfer Payment</a></li>
                            <li id="unionpayment"><a href="<?php echo site_url();?>admin/unionpayment">Western Union Payment</a></li>
                            <li id="requestcancel"><a href="<?php echo site_url();?>admin/requestcancel">Request for Cancel</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="layouts.html"><i class="fa fa-folder"></i> <span class="nav-label"> Deals</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="deals"><a href="<?php echo site_url();?>admin/deals">Deal of the day list</a></li>
                                                     
                        </ul>
                    </li>					                    					
					<li>
                        <a href="layouts.html"><i class="fa fa-desktop"></i> <span class="nav-label">Banner</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="bannerlist"><a href="<?php echo site_url();?>admin/bannerlist">View banner</a></li>
                            <li id="addbanner"><a href="<?php echo site_url();?>admin/addbanner">Add banner</a></li>
                                                   
                        </ul>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-desktop"></i> <span class="nav-label">   Variations </span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="variationslist"><a href="<?php echo site_url();?>admin/variationslist"> Variations List</a></li>
                            <li id="addvariations"><a href="<?php echo site_url();?>admin/addvariations">Add Variation</a></li>
                            
                                                   
                        </ul>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-diamond"></i> <span class="nav-label">Coupon Codes </span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="couponcodelist"><a href="<?php echo site_url();?>admin/couponcodelist"> Coupon Code List</a></li>
							<li id="addcoupon"><a href="<?php echo site_url();?>admin/add_coupon"> Add Coupon </a></li>                       
                        </ul>
                    </li>										
					<li>
                        <a href="layouts.html"><i class="fa fa-globe"></i> <span class="nav-label">     Currency</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="add_currency"><a href="<?php echo site_url();?>admin/add_currency">Add Currency </a></li>
                            <li id="currency_list"><a href="<?php echo site_url();?>admin/currency_list">Currency List</a></li>
                        </ul>
                    </li>
					
					<li>
                        <a href="layouts.html"><i class="fa fa-dropbox"></i> <span class="nav-label">  Complaints</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="product_complaints_list"><a href="<?php echo site_url();?>admin/product_complaints_list">Product Complaints List</a></li>
                            <li id="shop_complaints_list"><a href="<?php echo site_url();?>admin/shop_complaints_list">Shop Complaints List</a></li>
                        </ul>
                    </li>					
					<li id="payment_gateway">
                        <a href="<?php echo site_url();?>admin/payment_gateway"><i class="fa fa-shopping-cart"></i> <span class="nav-label">  Payment Gateway</span><span class="fa arrow"></span></a>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-envelope"></i> <span class="nav-label"> Contacts</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="display_contact_seller"><a href="<?php echo site_url();?>admin/display_contact_seller">Contact Shop Owner</a></li>
                            <li id="display_askquestion_seller"><a href="<?php echo site_url();?>admin/display_askquestion_seller">Ask Question</a></li>
                            <li id="display_chat_offer"><a href="<?php echo site_url();?>admin/display_chat_offer">Ask for Offer</a></li>                        
                        </ul>
                    </li>
					<li>
                        <a href="layouts.html"><i class="fa fa-envelope"></i> <span class="nav-label">Reviews </span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
                            <li id="product_feedback"><a href="<?php echo site_url();?>admin/product_feedback">Product Feedback</a></li>
                            <li id="shop_feedback_report"><a href="<?php echo site_url();?>admin/shop_feedback_report">Shop Feedback Report</a></li>                         
                        </ul>
                    </li>					
					<input type="hidden"  value="" id="show_active">                 
                </ul>
            </div>
        </nav>
	<div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <!--li>
                    <span class="m-r-sm text-muted welcome-message">Visit Site</span>
                </li>
                <li>
                    <span class="m-r-sm text-muted welcome-message">Visit Blog</span>
                </li-->
                <li>
                    <a href="<?php echo  site_url('Admin/logout') ?>">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </nav>
		
	<style>
   body
    {
        font-family: Arial;
        font-size: 10pt;
    }
  .loadermodal 
    {
        position: fixed;
        z-index: 999;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
        -moz-opacity: 0.8;
    }
    .loadercenter
    {
        z-index: 1000;
        margin: 300px auto;
        padding: 10px;
        width: 130px;
        background-color: White;
        border-radius: 10px;
        filter: alpha(opacity=100);
        opacity: 1;
        -moz-opacity: 1;
    }
    .loadercenter img
    {
        height: 120px;
        width: 120px;
    }
 </style>  	
 <script>
 $( document ).ready(function() {
	var url = window.location.href;
    var result = url.substring(url.lastIndexOf("/") + 1);
	if(result=='userdashboard'){		
		$('#userdashboard').parent().addClass('in');
		$('#userdashboard').addClass('active');
	}else if(result=='userlist'){
		$('#userlist').parent().addClass('in');
		$('#userlist').addClass('active');
	}else if(result=='adduser'){
		$('#adduser').parent().addClass('in');
		$('#adduser').addClass('active');
	}else if(result=='dashboard'){
		$('#dashboard').parent().addClass('in');
		$('#dashboard').addClass('active');
	}else if(result=='sellerlist'){
		$('#sellerlist').parent().addClass('in');
		$('#sellerlist').addClass('active');
	}else if(result=='siteearning'){
		$('#siteearning').parent().addClass('in');
		$('#siteearning').addClass('active');
	}else if(result=='codearning'){
		$('#codearning').parent().addClass('in');
		$('#codearning').addClass('active');
	}else if(result=='shoplist'){
		$('#shoplist').parent().addClass('in');
		$('#shoplist').addClass('active');
	}else if(result=='category'){
		$('#category').parent().addClass('in');
		$('#category').addClass('active');
	}else if(result=='productlist'){
		$('#productlist').parent().addClass('in');
		$('#productlist').addClass('active');
	}else if(result=='recentproductlist'){
		$('#recentproductlist').parent().addClass('in');
		$('#recentproductlist').addClass('active');
	}else if(result=='addproduct'){
		$('#addproduct').parent().addClass('in');
		$('#addproduct').addClass('active');
	}else if(result=='deleteproductlist'){
		$('#deleteproductlist').parent().addClass('in');
		$('#deleteproductlist').addClass('active');
	}else if(result=='featurepackagelist'){
		$('#featurepackagelist').parent().addClass('in');
		$('#featurepackagelist').addClass('active');
	}else if(result=='addfeaturepackage'){
		$('#addfeaturepackage').parent().addClass('in');
		$('#addfeaturepackage').addClass('active');
	}else if(result=='affiliatesettings'){
		$('#affiliatesettings').parent().addClass('in');
		$('#affiliatesettings').addClass('active');
	}else if(result=='paidpayment'){
		$('#paidpayment').parent().addClass('in');
		$('#paidpayment').addClass('active');
	}else if(result=='failedpayment'){
		$('#failedpayment').parent().addClass('in');
		$('#failedpayment').addClass('active');
	}else if(result=='cashdelivery'){
		$('#cashdelivery').parent().addClass('in');
		$('#cashdelivery').addClass('active');
	}else if(result=='transferpayment'){
		$('#transferpayment').parent().addClass('in');
		$('#transferpayment').addClass('active');
	}else if(result=='unionpayment'){
		$('#unionpayment').parent().addClass('in');
		$('#unionpayment').addClass('active');
	}else if(result=='requestcancel'){
		$('#requestcancel').parent().addClass('in');
		$('#requestcancel').addClass('active');
	}else if(result=='deals'){
		$('#deals').parent().addClass('in');
		$('#deals').addClass('active');
	}else if(result=='dispute'){
		$('#dispute').parent().addClass('in');
		$('#dispute').addClass('active');
	}else if(result=='constantcontact'){
		$('#constantcontact').parent().addClass('in');
		$('#constantcontact').addClass('active');
	}else if(result=='mailchimp'){
		$('#mailchimp').parent().addClass('in');
		$('#mailchimp').addClass('active');
	}else if(result=='mailchimp'){
		$('#mailchimp').parent().addClass('in');
		$('#mailchimp').addClass('active');
	}else if(result=='currentauctions'){
		$('#currentauctions').parent().addClass('in');
		$('#currentauctions').addClass('active');
	}else if(result=='outdatedauctions'){
		$('#outdatedauctions').parent().addClass('in');
		$('#outdatedauctions').addClass('active');
	}else if(result=='event'){
		$('#event').parent().addClass('in');
		$('#event').addClass('active');
	}else if(result=='teams'){
		$('#teams').parent().addClass('in');
		$('#teams').addClass('active');
	}else if(result=='communitynews'){
		$('#communitynews').parent().addClass('in');
		$('#communitynews').addClass('active');
	}else if(result=='bannerlist'){
		$('#bannerlist').parent().addClass('in');
		$('#bannerlist').addClass('active');
	}else if(result=='addbanner'){
		$('#addbanner').parent().addClass('in');
		$('#addbanner').addClass('active');
	}else if(result=='variationslist'){
		$('#variationslist').parent().addClass('in');
		$('#variationslist').addClass('active');
	}else if(result=='addvariations'){
		$('#addvariations').parent().addClass('in');
		$('#addvariations').addClass('active');
	}else if(result=='couponcodelist'){
		$('#couponcodelist').parent().addClass('in');
		$('#couponcodelist').addClass('active');
	}else if(result=='giftcarddasboard'){
		$('#giftcarddasboard').parent().addClass('in');
		$('#giftcarddasboard').addClass('active');
	}else if(result=='giftcardssetting'){
		$('#giftcardssetting').parent().addClass('in');
		$('#giftcardssetting').addClass('active');
	}else if(result=='giftcardslist'){
		$('#giftcardslist').parent().addClass('in');
		$('#giftcardslist').addClass('active');
	}else if(result=='subscribers_list'){
		$('#subscribers_list').parent().addClass('in');
		$('#subscribers_list').addClass('active');
	}else if(result=='subscribers_list'){
		$('#subscribers_list').parent().addClass('in');
		$('#subscribers_list').addClass('active');
	}else if(result=='email_template_list'){
		$('#email_template_list').parent().addClass('in');
		$('#email_template_list').addClass('active');
	}else if(result=='add_email_template'){
		$('#add_email_template').parent().addClass('in');
		$('#add_email_template').addClass('active');
	}else if(result=='add_currency'){
		$('#add_currency').parent().addClass('in');
		$('#add_currency').addClass('active');
	}else if(result=='currency_list'){
		$('#currency_list').parent().addClass('in');
		$('#currency_list').addClass('active');
	}else if(result=='theme_list'){
		$('#theme_list').parent().addClass('in');
		$('#theme_list').addClass('active');
	}else if(result=='category'){
		$('#category').parent().addClass('in');
		$('#category').addClass('active');
	}else if(result=='product_complaints_list'){
		$('#product_complaints_list').parent().addClass('in');
		$('#product_complaints_list').addClass('active');
	}else if(result=='shop_complaints_list'){
		$('#shop_complaints_list').parent().addClass('in');
		$('#shop_complaints_list').addClass('active');
	}else if(result=='list_pages'){
		$('#list_pages').parent().addClass('in');
		$('#list_pages').addClass('active');
	}else if(result=='payment_gateway'){
		$('#payment_gateway').parent().addClass('in');
		$('#payment_gateway').addClass('active');
	}else if(result=='display_contact_seller'){
		$('#display_contact_seller').parent().addClass('in');
		$('#display_contact_seller').addClass('active');
	}else if(result=='display_askquestion_seller'){
		$('#display_askquestion_seller').parent().addClass('in');
		$('#display_askquestion_seller').addClass('active');
	}else if(result=='display_chat_offer'){
		$('#display_chat_offer').parent().addClass('in');
		$('#display_chat_offer').addClass('active');
	}else if(result=='product_feedback'){
		$('#product_feedback').parent().addClass('in');
		$('#product_feedback').addClass('active');
	}else if(result=='shop_feedback_report'){
		$('#shop_feedback_report').parent().addClass('in');
		$('#shop_feedback_report').addClass('active');
	}else if(result=='multilanguage'){
		$('#multilanguage').parent().addClass('in');
		$('#multilanguage').addClass('active');
	}else if(result=='adminShipping'){
		$('#shippinglocation').parent().addClass('in');
		$('#shippinglocation').addClass('active');
	}else if(result=='addShippingLocation'){
		$('#addShippingLocation').parent().addClass('in');
		$('#addShippingLocation').addClass('active');
	}else if(result=='shoplist'){
		$('#shoplist').parent().addClass('in');
		$('#shoplist').addClass('active');
	}
});
 </script>