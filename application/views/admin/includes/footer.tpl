               <div class="footer">
            <div>
                <strong>Copyright</strong> Filpmart.com &copy; 2017
            </div>
        </div>
</div>
    <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/dataTables/datatables.min.js"></script>
 <!-- Flot -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/flot/jquery.flot.pie.js"></script>
   <!-- Peity -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/demo/peity-demo.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url();?>assets/admin/js/inspinia.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/plugins/pace/pace.min.js"></script>
	<!-- SUMMERNOTE -->
	<script src="<?php echo base_url();?>assets/admin/js/plugins/summernote/summernote.min.js"></script>
	<!-- Data picker -->
	<script src="<?php echo base_url();?>assets/admin/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- GITTER -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/gritter/jquery.gritter.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Sparkline demo data  -->
    <script src="<?php echo base_url();?>assets/admin/js/demo/sparkline-demo.js"></script>
    <!-- ChartJS-->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/chartJs/Chart.min.js"></script>
    <!-- Toastr -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/toastr/toastr.min.js"></script>
      <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/admin/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<!-- Sweet alert  -->
	<script src="<?php echo base_url();?>assets/admin/js/plugins/sweetalert/sweetalert.min.js"></script>
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
<script>
    $(document).ready(function(){

        $('.summernote').summernote();

        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

    });
</script>
</body>
</html>