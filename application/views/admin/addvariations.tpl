<?php   echo $layout['header']; ?>              		
<?php $assets_url =  base_url()."/assets/admin/"; ?>				
		<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add New Attribute</h2>                   
                </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add New Attribute</h5>
                        </div>
                        <div class="ibox-content">
                            <form id="attribute" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Attribute Name<span style="color: red">*</span></label>
                                    <div class="col-sm-10">
										<input type="text" name="attribute_name" id="attribute_name" value="<?= ($variation_data) ? $variation_data[0]->attribute_name : "" ?>" class="form-control">
									</div>
                                </div>
                               <div class="form-group"><label class="col-sm-2 control-label">You need scaling options?</label>
                                    <div class="col-sm-10">
                                        <label class="checkbox-inline"> 
                                        <input value="1" id="inlineCheckbox2" name="scaling_option" type="radio"  > Yes </label> 
                                        <label class="checkbox-inline">
										<input value="0" id="inlineCheckbox3" name="scaling_option" type="radio" > No </label>
										 <span style="margin-left:150px;">Example:   Cm,Mm,L,Ml,inches,pounds,Grams,etc...</span>
										 <input type="hidden" value="<?= ($variation_data) ? $variation_data[0]->scaling_options : "" ?>" id="soption">
                                    </div>
                                 </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Attribute Options</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="attribute_option" id="attribute_option" value="<?= ($variation_data) ? $variation_data[0]->attribute_options : "" ?>" class="form-control">                                        
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Status <span style="color: red">*</span></label>
                                <div class="col-sm-10">
                                <div class="switch">
                                 <div class="onoffswitch">
                                     <input type="checkbox" 	
									 class="onoffswitch-checkbox" id="status" name="status">
                                     <label class="onoffswitch-label" for="status">
                                         <span class="onoffswitch-inner"></span>
                                         <span class="onoffswitch-switch"></span>
                                     </label>
									 <input type="hidden" value="<?= ($variation_data) ? $variation_data[0]->variation_status : "" ?>" id="vstatus">
                                 </div>
                             </div>                                       
                             </div>
                                </div>
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php echo $layout['footer']; ?>
<link href="<?php echo $assets_url ; ?>css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?php echo $assets_url ; ?>css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<script src="<?php echo $assets_url ; ?>js/tag-it.min.js"></script>
<script>
$(document).ready(function(){
	var status = $('#vstatus').val();
	if(status == '1'){
		$('#status').prop('checked',true);
	}else if(status == ""){
		$('#status').prop('checked',true);
	}else{ 
		$('#status').prop('checked',true);
	}
	var soption = $('#soption').val();
	if(soption == '1'){
		$('#inlineCheckbox2').prop('checked',true);
	}else if(soption == '0'){
		$('#inlineCheckbox3').prop('checked',true);
	}else{ 
		$('#inlineCheckbox2').prop('checked',true);
	}
});
$('#attribute_option').tagit();
$("#attribute").validate({
	rules: {
			attribute_name: {
				required: true,			
			},
			attribute_option: {
				required: true,			
			}			
	},
	messages: {
			attribute_name: {
				required: "attribute name required",			
			},
			attribute_option: {
				required: "attribute option required",			
			}	
	},
	submitHandler :function(form)
	{
		var str = window.location.href;
		var n = str.lastIndexOf('/'); 
		var variation_id = str.substring(n + 1); 		
		var formdata=new FormData(form);	
		formdata.append('variation_id',variation_id);
		$.ajax({
			type : "POST",
			url  : "<?= site_url() ?>admin/savevariations",
			data : formdata,
			processData : false,
			contentType : false,
			cache : false,
		beforeSend : function()
		{
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},
		success : function(response)
		{
			if(response.status==true)
			{
				toastr.success(response.msg, 'Success');
				window.location.href = "<?= site_url() ?>admin/variationslist";	
			}
			$('.overlay').remove();
		},
		complete : function()			
		{
			$('.overlay').remove();
		},
	});
  }
});	
</script>  

