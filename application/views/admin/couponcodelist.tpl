<?php echo $layout['header'] ; ?>   			
	   <div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Coupon Codes List</h2>			   
			</div>
			<div class="col-lg-2">
			</div>
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Coupon Codes List</h5>                     
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
						<tr>
							<th>Code</th>
							<th>Type</th>
							<th>Value</th>                        
							<th>Date From</th>
							<th>Date to</th>
							<th>Card Status</th>
							<th>status</th>
							<th>Action</th>
						</tr>
						</thead>
							<tbody>
								<?php foreach($coupon_data as $coupon) { ?>
								<tr class="gradeX">
									<td><?= $coupon->coupon_code; ?></td>
									<td>
										<span class="label label-primary">
										<?= ($coupon->coupon_type==1) ? "Normal" : "Percentage" ?>
										</span>
									</td>
									 <td><?= $coupon->value; ?></td>                        
									 <td><?= $coupon->coupon_from_datetime; ?></td>
									 <td><?= $coupon->coupon_to_datetime; ?></td>                         
									 <td>
									 <span class="label label-primary">
										<?= ($coupon->card_status==1) ? "Active" : "Expired" ?>
									 </span>	
									 </td>
									 <td class="center"> 
										
										<span style="cursor:pointer;" data="<?= $coupon->coupon_id; ?>" class="label status_checks btn <?= ($coupon->coupon_status) ? "label-primary" : "label-danger" ?>">
									   <?= ($coupon->coupon_status) ? "Active" : "Inactive" ?>
									   </span>
									 </td>
									 <td class="text-center">
									  	
										<button class="btn btn-danger delete_coupon" onClick="delete_coupon(<?= $coupon->coupon_id; ?>)"><i class="fa fa-trash"></i></button>
										<button class="btn-white btn btn-xs" onclick="edit(<?= $coupon->coupon_id; ?>)"><i class="fa fa-edit"></i></button>									   
									 </td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>     
<script>
$(document).on('click',".status_checks",function(){
	var status = ($(this).hasClass('label-primary') ? "0" : "1");
	var msg = (status == '0' ? "Deactivate" : "Activate");	
		var coupon = $(this);
		console.log(coupon);
		swal({
		  title: "Are you sure?",
		  text: "You want to "+msg,
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: msg,
		  closeOnConfirm: false
		},
		function(){
		$.ajax({
			type : "POST",
			url : "<?= site_url() ?>admin/change_coupon_status",
			data : {coupon_id : $(coupon).attr('data') , status : status},
			cache : false,			
		beforeSend : function()
		{
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},
		success : function(result)
		{
			setTimeout(function() {
							swal({
								title: msg,
								text: "Status Has Been "+msg+" Successfully.",
								type: "success"
							}, function() {
								location.reload();
							});
						}, 500);	
			$('.overlay').remove();
			toastr.success('Status Change Successfully', 'Success');			
		},
		error :function()
		{
			
		},
		complete :function()
		{
			$('.overlay').remove();
		}
	});		
});
});

function delete_coupon(id)
{ 
	var formData = new FormData();
	var coupon_id= id;
	formData.append('coupon_id',coupon_id);
	swal({
		  title: "Are you sure?",
		  text: "You want to delete!!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Delete",
		  closeOnConfirm: false
		},
	function(){
	$.ajax({
		url  : "<?= site_url() ?>admin/delete_coupon",
		type : "POST",
		data : formData,
		processData : false,		
		contentType : false,
		cache : false,		
	beforeSend : function()
	{
		$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	},
	success : function(result)
	{	
		if(result.status == true)
		{
			setTimeout(function() {
					swal({
						title: "Deleted",
						text: "Coupon Has Been Delete Successfully!!",
						type: "success"
					}, function() {
						location.reload();
					});
				}, 500);
			toastr.success(result.message, 'Success');
		}else{
			toastr.success(result.message, 'Success');			
		}
		location.reload();
	},
	error : function()
	{
		toastr.success(result.message, 'Success');	
	},
	complete : function()
	{
		$('.overlay').remove();
	}	
});
});
}

function edit(id)
{
	var id = $.base64.btoa(id);
	location.replace("<?= site_url() ?>admin/add_coupon/"+id);
}
</script>  
<?php echo $layout['footer']; ?>