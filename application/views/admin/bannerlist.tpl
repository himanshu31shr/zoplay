<?php   echo $layout['header'] ; ?>  
 
            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Banners</h2>
                    
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Banner List</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Tag Line</th>
                        <th>Heading</th>
                        <th>Description</th>
                        <th>Thumb</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php if(!empty($banners)){
						foreach($banners as $banner){ ?>
						
                    <tr class="gradeX">
						<td><?php echo $banner->tagline; ?></td>
						<td><?php echo $banner->heading; ?></td>
						<td><?php echo $banner->description; ?></td>
						<td class="center">
						<?php 
						if(!empty($banner->image) && file_exists(FCPATH.'assets/uploads/banner/'.$banner->image)){
							$image = base_url('assets/uploads/banner/'.$banner->image);
						}else{
							$image = base_url('assets/admin/img/a5.jpg');
						} ?>
						<a href="<?php echo $image; ?>" target="_blank"><img src="<?php echo $image; ?>" class="thumbnail" width="40px" height="40px"></a></td>
						<td class="center"><?php echo ($banner->status == 1) ? '<span class="label label-success" style="cursor: pointer;" onclick="changestatus('.$banner->banner_id.',2)">Active</span>' : '<span class="label label-danger" style="cursor: pointer;" onclick="changestatus('.$banner->banner_id.',1)">Inactive</span>' ;  ?></td>
						<td class="text-right">
							<div class="btn-group">
								<button class="btn-danger btn " onclick="deletebanner(<?php echo $banner->banner_id ; ?>)"><i class="fa fa-trash"></i></button> &nbsp; 
								<a class="btn-primary btn " href="<?php echo site_url('admin/addbanner?banner='.$banner->banner_id); ?>"><i class="fa fa-pencil"></i></a>
							</div>
						</td>
					</tr>
						<?php }
					} ?>
                  
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>

 <script>
 function changestatus(id,status){
	$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
	$.ajax({ 
		url:'<?=site_url();?>api/admin_api/bannerstatus', 
		data: {'id':id,'status':status},
		datatype: 'json',
		type: 'POST',
		success: function(res)
		{
			 $(".overlay").remove();
			 //res = $.parseJSON(res);
			 //console.log(res.status);
			 if(res.status == true){
				toastr.success(res.message); 
				window.location.reload();
			 }else{
				 toastr.error(res.message);
			 }
			 
		}			
	}); 
}
var did = '';
function deletebanner(id){
	did = id;
	swal({
		  title: "Are you sure?",
		  text: "You want to delete",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Yes",
		  closeOnConfirm: true
		},
	function(){	
		$.ajax({
			url:'<?=site_url();?>api/admin_api/deletebanner', 
			data: {'id':did},
			datatype: 'json',
			type: 'POST',			
		beforeSend : function()
		{
			$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
		},
		
	}).done(function(res){
		$('.overlay').remove();
		if(res.status == true){
				toastr.success(res.message); 
				window.location.reload();
			 }else{
				 toastr.error(res.message);
			 }
	}).error(function(){
		swal("Oops", "Somethings went wrong!", "error");
		toastr.error('Somethings went wrong!', 'Error');
		$('.overlay').remove();
	});
});
}
</script>