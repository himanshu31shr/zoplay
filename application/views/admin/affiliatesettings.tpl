<?php   echo $layout['header'] ; //print_r($affiliatesettings);  ?>  
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>General Settings</h2>
                </div>
                <div class="col-lg-2">
               </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>General Settings</h5>
                        </div>
                        <div class="ibox-content">
                            <form method="post" id="Affiliatesettingsfrm" name=">Affiliatesettingsfrm" class="form-horizontal">
                                 <div class="form-group"><label class="col-sm-2 control-label">Affiliate settings <span style="color: red">*</span></label>
                                    <div class="col-sm-10">
                                <div class="switch">
                                 <div class="onoffswitch">
                                     <input type="checkbox"  class="onoffswitch-checkbox" id="example1" <?php if(isset($affiliatesettings->general_setting_status) && $affiliatesettings->general_setting_status == '1') { ?> checked <?php } ?>>
                                     <label class="onoffswitch-label" for="example1">
                                         <span class="onoffswitch-inner"></span>
                                         <span class="onoffswitch-switch"></span>
                                     </label>
                                 </div>
                                      <input type="hidden" value="1" name="status" id="status">
                             </div>
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-sm-2 control-label">One Point = <span style="color: red">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="one_point" id="one_point" value="<?php echo !empty($affiliatesettings->one_point)? $affiliatesettings->one_point : '' ?>" class="form-control">
                                    </div>
                                    <div class="col-sm-2">
                                            <span>$ USD</span>
                                    </div>
                                </div>
                           
                                <div class="form-group"><label class="col-sm-2 control-label">One Referral = <span style="color: red">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="one_referral" id="one_referral" value="<?php echo !empty($affiliatesettings->one_referral)? $affiliatesettings->one_referral : '' ?>" class="form-control"> 
                                    </div>
                                    <div class="col-sm-2">
                                       <span>points</span> 
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Cookie Expiration duration <span style="color: red">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="expiration_duration" id="expiration_duration" value="<?php echo !empty($affiliatesettings->cookie_expiration_duration)? $affiliatesettings->cookie_expiration_duration : '' ?>" class="form-control">
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="cookie_period" id="cookie_period" class="form-control">
                                            <option value="mins" <?php if(isset($affiliatesettings->cookie_period) && $affiliatesettings->cookie_period=='mins'){ ?> selected="selected" <?php } ?>>mins</option>
                                                <option value="hrs" <?php if(isset($affiliatesettings->cookie_period) && $affiliatesettings->cookie_period=='hrs'){ ?> selected="selected" <?php } ?>>hrs</option>
                                                <option value="days" <?php if(isset($affiliatesettings->cookie_period) && $affiliatesettings->cookie_period=='days'){ ?> selected="selected" <?php } ?>>days</option>
                                                <option value="months" <?php if(isset($affiliatesettings->cookie_period) && $affiliatesettings->cookie_period=='months'){ ?> selected="selected" <?php } ?>>months</option>
                                                <option value="years" <?php if(isset($affiliatesettings->cookie_period) && $affiliatesettings->cookie_period=='years'){ ?> selected="selected" <?php } ?>>years</option>
                                        </select>
                                    </div>
                                </div>
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php echo $layout['footer']; ?>
