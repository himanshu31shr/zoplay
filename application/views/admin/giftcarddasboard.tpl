<?php   echo $layout['header'] ; ?>  
 <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Tables</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Tables</a>
                        </li>
                        <li class="active">
                            <strong>Data Tables</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Gift Cards Dashboard</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    </thead>
                    <tbody>
                    <tr class="gradeX">
                        <td>Redeemed Cards </td>
                        <td>3</td>
                     </tr>
                    <tr class="gradeX">
                        <td>Redeemed Cards </td>
                        <td>6</td>
                     </tr>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
                    <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Recent Gift Cards</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Recipient Name </th>
                        <th>Sender Name</th>
                        <th>status</th>
                        <th>Ammount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="gradeX">
                        <td>0idZecOpb4  </td>
                         <td>check</td>
                         <td>test</td>
                        <td class="center"> <span class="label label-primary">redeemed</span></td>
                         <td>125 </td>
		     </tr>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>

 <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>