<?php   echo $layout['header'] ; ?>   
	<div class="loadermodal" style="display: none">
		<div class="loadercenter">
			<img alt="" src="<?= base_url() ?>assets/admin/img/loader.gif" />
		</div>
	</div>
	 <div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Add User</h2>
			</div>
			<div class="col-lg-2">

			</div>
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <form id="addUser" class="form-horizontal">
                                <div class="form-group"><label class="col-sm-2 control-label">Full Name</label>
                                    <div class="col-sm-10"><input type="text" name="full_name" id="full_name" class="form-control" value="<?= ($user_data) ? $user_data[0]->full_name : "" ?>"></div>
                                </div>
                           
                                <div class="form-group"><label class="col-sm-2 control-label">User Name</label>
                                    <div class="col-sm-10"><input type="text" name="user_name" id="user_name" class="form-control" value="<?= ($user_data) ? $user_data[0]->user_name : "" ?>">
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10"><input type="text" name="email" id="email" class="form-control" value="<?= ($user_data) ?$user_data[0]->email : "" ?>"> 
                                    </div> 
                                </div>
                            <?php if(!isset($user_data) && empty($user_data)) { ?>
                                <div class="form-group"><label class="col-sm-2 control-label">Password</label>

                                    <div class="col-sm-10"><input type="password" class="form-control" id="password" name="password"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label"> Re-Type Password</label>

                                    <div class="col-sm-10"><input type="password"  class="form-control" name="confirm_password"></div>
                                </div>
							<?php } ?>
                                <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
										<div class="switch">
											 <div class="onoffswitch">
												 <input type="checkbox"  
												class="onoffswitch-checkbox"name="status" id="example1" checked>				 
												 <label class="onoffswitch-label" for="example1">
													 <span class="onoffswitch-inner"></span>
													 <span class="onoffswitch-switch"></span>
												 </label>
												 <input type="hidden" value="<?= ($user_data) ? $user_data[0]->status : "" ?>" id="checkstatus">
											 </div>
											 <input type="hidden" value="1" name="userstatus" id="userstatus">
										</div>           
									</div>
								</div>
								<div class="form-group"><label class="col-sm-2 control-label">User Image</label>
                                    <div class="col-sm-10">
										<input type="file" name="image" id="image">
									</div>
                                </div>
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a class="btn btn-white" href="<?php echo site_url('admin/userlist'); ?>">Cancel</a>
                                        <input class="btn btn-primary" name="button"  type="submit" value="Save changes" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>     					
<script>
$(document).ready(function() {
	var status = $("#checkstatus").val();
	if(status == 1)
	{
		$("#example1").prop('checked',true);	
	}else if(status == ""){
		$('#example1').prop('checked',true);
	}else{
		$('#example1').prop('checked',false);
	}
    $("#example1").click(function() {
        if (this.checked) {
           $('#userstatus').val('1');
        }else{
			$('#userstatus').val('0');
		}
    });
});
$(function () {
	// validate signup form on keyup and submit
	$("#addUser").validate({
		rules: {
			full_name: "required",				
			image: {
				required : true,
				accept:"jpg,png,jpeg,gif"			
			},
			user_name: {
				required: true,
				minlength: 5
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			email: {
				required: true,
				email: true
			},				
		},
		messages: {
			firstname: "Please enter your fullname",
			user_name: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 5 characters"
			},
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address",
			image : {
				required:"Image required",
				accept: "Only image type jpg/png/jpeg/gif is allowed"
			}
		},
		submitHandler: function (form) { 			
			var formData = new FormData();
			var fullname = $('#full_name').val();
			var userstatus = $('#userstatus').val(); 
			var username = $('#user_name').val();
			var email = $('#email').val();
			var password = $('#password').val();
			var image = $('#image').val();
				formData.append("full_name",fullname);
				formData.append("user_name",username);
				formData.append("status",userstatus);
				formData.append("email",email);
				formData.append("password",password);
				formData.append("image", $('#image')[0].files[0]);
					$.ajax({ 
						url:'<?=site_url();?>admin/adduser', 
						data: formData,
						processData: false,
						type: 'POST',
						cache: false,	
						contentType : false ,	
							
						beforeSend : function()
						{
							$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
						},						
						success: function(data)
						{
							 toastr.success('Add User Successfully', 'Success');
							 window.location.href ="<?= site_url() ?>admin/userlist"
						},
						complete : function(){
							$('.overlay').remove();
						}
						
					}); 			
          }
	});
}); 
$('#email').focusout(function () {
var email = $("#email").val();
$.ajax(
	{
		type:"post",
		url: "<?php echo base_url(); ?>admin/mail_exists",
		data:{ email:email},
		success:function(response)
		{
			if (response == 1) 
			{		
				$('#email').after('<label id="email-error" class="error" for="user_name">Email already exist</label>');
			}
			else 
			{ 
				$('#email-error').remove();
			}  
		}
	});
});


</script>		
<?php echo $layout['footer']; ?>