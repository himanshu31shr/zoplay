<?php   echo $layout['header'] ; ?> 
<link href="<?php echo base_url(); ?>assets/admin/css/plugins/jsTree/style.min.css" rel="stylesheet">			
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Categories List</h2>
                </div>
                <div class="col-lg-2">
           </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                         <div id="jstree1">
                                <ul>
                                <li class="jstree-open">Categories <span style="margin-left:20px;">
								<a title="add subcategory" style="color:green;" onclick="add_category('0')">
								<i class="fa fa-plus-circle"></i>
								</a></span>								
                                <?php echo jsCategories($jscategories); ?>
                                </li>
								</ul>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
    
<div id="add_category" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3>Add subcategory</h3>
      </div>
      <div class="modal-body" >
		  <form id="addCategoryFrm" >
			  <div class="form-group">
				<label>Category Name</label>
				<input type="text" name="category_name" class="form-control">
				<input type="hidden" name="parent_id" id="parent_id" value="">
			  </div>
			  <div class="form-group">
				<button type="button" class="btn btn-primary" onclick="add_cat('<?php echo site_url('admin/add_category'); ?>');" >Add</button>
			  </div>
		  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>  

<div id="edit_category" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3>Edit Category</h3>
      </div>
      <div class="modal-body" >
		  <form id="editCategoryFrm" >
			  <div class="form-group">
				<label>Category Name</label>
				<input type="text" name="category_name" id="cat_name" value="" class="form-control">
				<input type="hidden" name="category_id" id="parent_id_category" value="">				
			  </div>
			  <div class="form-group">
				<button type="button" class="btn btn-primary" onclick="edit_cat('<?php echo site_url('admin/edit_category'); ?>');" >Update</button>
			  </div>
		  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>     
<?php echo $layout['footer']; ?>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins/jsTree/jstree.min.js"></script>
 <script>
        $(document).ready(function(){
            $('#categorytable').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });
			$('#jstree1').jstree();

        });

    </script>
	<style> 
	#jstree1{
		font-size:17px;
	}
	.loadermodal{
		z-index: 999999 !important;
	}
	
	</style>
	<script>
	function add_category(parent_id){
		$('#parent_id').val(parent_id);
		$('#add_category').modal({show:true});
	}
	function edit_category(category_id,category_name){
		$('#parent_id_category').val(category_id);		
		$('#cat_name').val(category_name);				
		$('#edit_category').modal({show:true});
	}
	function delete_category(category_id){			
		swal({
		  title: "Are you sure?",
		  text: "You want to Delete",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Delete",
		  closeOnConfirm: false
		},
		function(){
		$.ajax({
			url:'<?= site_url() ?>admin/delete_category',
			type:'post',
			data:{'category_id':category_id},
			beforeSend : function()
			{
				$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success:function(result){								
				if(result == 1){
					setTimeout(function() {
							swal({
								title: "Delete",
								text: "Category Has Been Deleted Successfully.",
								type: "success"
							}, function() {
								window.location.href ="<?= site_url() ?>admin/category";
							});
						}, 500);
					toastr.success('Category Has Been Deleted Successfully!!', 'Success');
					$('.overlay').remove();					
				}else{
					toastr.success('Something Went Wrong!!', 'Success');
					window.location.href ="<?= site_url() ?>admin/category";
					$('.overlay').remove();						
				}
			},
			complete : function()
			{
				$('.overlay').remove();
			}
		});
	});
}	
	
	function add_cat(url){		
		$.ajax({
			url: url, 
			dataType: "json", 
			type: "post", 
			data: $('#addCategoryFrm').serialize(), 
			beforeSend : function()
			{
				$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success: function(result){								
				if(result == 1){
					toastr.success('Category Has Been Added Successfully!!', 'Success');
					window.location.href ="<?= site_url() ?>admin/category";	$('.overlay').remove();				
				}else{
					toastr.success('Something Went Wrong!!', 'Success');
					window.location.href ="<?= site_url() ?>admin/category";
					$('.overlay').remove();						
				}
			},
			complete : function()
			{
				$('.overlay').remove();
			}
		});
	}	
	
	function edit_cat(url){		
		$.ajax({
			url:url,
			type:"post",
			data:$('#editCategoryFrm').serialize(),
			beforeSend : function()
			{
				$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success: function(result){								
				if(result == 1){
					toastr.success('Category Has Been Update Successfully!!', 'Success');
					window.location.href ="<?= site_url() ?>admin/category"											;
					$('.overlay').remove();
				}else{
					toastr.success('Something Went Wrong!!', 'Success');
					window.location.href ="<?= site_url() ?>admin/category";
					$('.overlay').remove();					
				}
			},
			complete : function()
			{
				$('.overlay').remove();
			}
		});
	}
	</script>