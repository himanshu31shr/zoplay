<?php   echo $layout['header'] ; 
		$usercount = json_decode($usercount);
		$activeUser = json_decode($activeUser);
		$inactiveUser = json_decode($inactiveUser);
		$userlist = json_decode($userlist);
		?>  
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>User Dashboard</h2>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>User Dashboard</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    </thead>
                    <tbody>
                        <tr><span><?= $usercount; ?> users registered in this site</span></tr>
                    <tr class="gradeX">
                        <td>Active Users </td>
                        <td><?= $activeUser; ?></td>
                     </tr>
                    <tr class="gradeX">
                        <td>Inactive Users </td>
                        <td><?= $inactiveUser; ?></td>
                     </tr>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
             <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Recent Users</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>User Name </th>
                        <th>Email</th>
                        <th>Thumbnail </th>
                    </tr>
                    </thead>
                    <tbody>
		<?php foreach($userlist as $user) {
                        
                    $userImg ='';
                    if (!empty($user->picture) && file_exists('assets/uploads/'.$user->picture)) {
                         $userImg= base_url().'assets/uploads/'.$user->picture;    
                    }else{
                      $userImg= base_url().'assets/admin/img/userdefault_img.png';
                    }
                ?>
                    <tr class="gradeX">
                        <td><?= $user->full_name; ?></td>
                         <td><?= $user->user_name; ?></td>
                         <td><?= $user->email; ?></td>                         
                        <td >
                            <div class="center">
                                 <img src="<?php echo  $userImg; ?>" class="thumbnail" width="40px" height="40px">
                            </div>
                        </td>
                     </tr>
		<?php } ?>
                    </tbody>
                    </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>