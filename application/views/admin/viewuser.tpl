<?php   echo $layout['header'] ; ?>  
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>View User</h2>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">						
						<div class="ibox-content">
						<div class="table-responsive">
                                                <?php 
                                                if(!empty($user_data)){
                                                    $user_data = json_decode($user_data);
                                                    $userImg ='';
                                                    if (!empty($user_data[0]->picture) && file_exists('assets/uploads/'.$user_data[0]->picture)) {
                                                         $userImg= base_url().'assets/uploads/'.$user_data[0]->picture;    
                                                    }else{
                                                      $userImg= base_url().'assets/admin/img/userdefault_img.png';
                                                    }
                                                 ?>   
                                                <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0">
						<thead>
						</thead>
						<tbody>
							<tr></tr>
						<tr class="gradeX">
							<td>Full Name</td>
							<td><?php echo !empty($user_data[0]->full_name)? $user_data[0]->full_name :' - '; ?></td>
						 </tr>
						<tr class="gradeX">
							<td>User Name</td>
							<td><?php echo !empty($user_data[0]->user_name)? $user_data[0]->user_name : ' - ' ; ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Email</td>
							<td><?php echo !empty($user_data[0]->email)? $user_data[0]->email : ' - '; ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Create Date</td>
							<td><?php echo !empty($user_data[0]->user_created)? date('d-m-Y',strtotime($user_data[0]->user_created)) : ' - '; ?></td>
						 </tr>
						 <tr class="gradeX">
							<td>Status</td>
							<td><?php echo !empty($user_data[0]->status) ? 'Active' : 'Inactive' ?></td>
						 </tr>
						 <tr class="gradeX">
                                                    <td>User Image</td>
                                                    <td>
                                                      <div class="text-center">
                                                            <img src="<?php echo $userImg; ?>" class="thumbnail" width="60px" height="45px">
                                                      </div>
                                                    </td>
						 </tr>
						 <tr class="gradeX">
							<td colspan="2"><a href="<?= site_url() ?>admin/userlist" class="btn btn-primary">Back</a></td>
						 </tr>
						 
						</tbody>
						</table>
                                                <?php }else{ ?>
                                                <div class='alert alert-danger'>No data found </div>
                                                 <?php } ?>    
                                                	</div>
                                                </div>
					</div>
				</div>
            </div>
        </div>
<?php echo $layout['footer']; ?>
