<?php   echo $layout['header'] ; ?>  
            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Team</h2>
                   
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
         <div class="row">
            <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add Team</h5>
                           
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="POST" id="addTeam">
                                
                                <div class="form-group"><label class="col-lg-2 control-label">TEAM NAME</label>

                                    <div class="col-lg-10"><input type="text" name="teamname"  class="form-control"> 
                                    </div>
                                </div> 
								<div class="form-group"><label class="col-lg-2 control-label">DESCRIPTION</label>

                                    <div class="col-lg-10"><textarea name="teamshortDescription" id="teamshortDescription"   original-title="Please enter short description"  class="form-control"> </textarea>
                                    </div>
                                </div>
								<div class="form-group"><label class="col-lg-2 control-label">WHO CAN JOIN?</label>

                                  <div class="col-lg-10"><textarea  name="who_can_join" id="who_can_join"  class="form-control"> </textarea> 
                                  </div>
								  
								
                                </div>
								
								<div class="form-group"><label class="col-lg-2 control-label">TEAM LOGO</label>

                                    <div class="col-lg-10"><input type="file" name="logo"  > 
                                    </div>
                                </div>
								<div class="form-group"><label class="col-lg-2 control-label">TAGS
</label>

                                    <div class="col-lg-10"><textarea  name="tags" id="tags"  class="form-control"> </textarea>
                                    </div>
                                </div>
								
								
								
                                <div class="form-group"><label class="col-lg-2 control-label">Status</label>

                                    <div class="col-lg-10"><select class="form-control" name="status"  id="status" >
									<option value="enable">Enable</option>
									<option value="disable">Disable</option>
											</select>									
                                    </div>
                                </div>
                                
                               
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
                
            </div>
              
            </div>
<script>
$(function () {
 // validate signup form on keyup and submit
 $("#addTeam").validate({
  rules: {
   teamname: "required",    
   logo: {
    required : true,
    accept:"jpg,png,jpeg,gif"   
   },
   teamshortDescription: {
    required: true,
    
   },
  
   tags: {
    required: true
   },    
  },
  messages: {
   teamname: "Please enter your team name",
   
   teamshortDescription: {
    required: "Please enter short description",
    
   },
   tags: {
    required: "Please enter tags",
   
   },
   
   logo : {
    required:"logo required",
    accept: "Only logo type jpg/png/jpeg/gif is allowed"
   }
  },
  submitHandler: function (form) {                 
   var formData = new FormData(form);
   
     $.ajax({ 
      url:'<?=site_url();?>api/admin_api/addTeam', 
      data: formData,
      processData: false,
      type: 'POST',
      cache: false, 
      contentType : false , 
       success: function(data)
       {
       if(data.status==true){
			
			toastr.success(data.message, 'Success');
			window.location.href='<?=site_url('admin/teams');?>';
		} if(data.status==false){
			
			toastr.success('Failed Create Event ', 'Failed');
		}
       }   
     });    
          }
 });
});
  
</script>
<?php echo $layout['footer']; ?>


