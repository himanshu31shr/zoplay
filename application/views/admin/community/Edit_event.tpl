<?php   echo $layout['header'] ; ?>  
            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Event</h2>
                   
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
         <div class="row">
            <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add Event</h5>
                           
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="POST" id="addEvent">
                                
                                <div class="form-group"><label class="col-lg-2 control-label">EVENT TITLE</label>

                                    <div class="col-lg-10"><input type="text" name="title"  class="form-control"> 
                                    </div>
                                </div> 
								<div class="form-group"><label class="col-lg-2 control-label">DESCRIPTION</label>

                                    <div class="col-lg-10"><input type="text" name="description"   class="form-control"> 
                                    </div>
                                </div>
								
								<div class="form-group">
									<label class="col-lg-2 control-label">EVENT TYPE</label>

                                    <div class="col-lg-10">
									<select class="form-control" name="event_type"  id="event_type" >
										<option value="Special">Special</option>
										<option value="Normal">Normal</option>
									</select>									
                                    </div>
                                </div>
                               
								<div class="form-group"><label class="col-lg-2 control-label">EVENT DATE</label>

                                    <div class="col-lg-10"><input type="text"  name="event_date"   class="form-control"> 
                                    </div>
                                </div>
								<div class="form-group"><label class="col-lg-2 control-label">EVENT TIME</label>

                                    <div class="col-lg-10"><input type="text" name="event_time"  class="form-control"> 
                                    </div>
                                </div>
								<div class="form-group"><label class="col-lg-2 control-label">EVENT LINK</label>

                                    <div class="col-lg-10"><input type="text"  name="event_link"  class="form-control"> 
                                    </div>
                                </div>
								<div class="form-group"><label class="col-lg-2 control-label">EVENT BUTTON NAME</label>

                                    <div class="col-lg-10"><input type="text"  name="event_button_name"  class="form-control"> 
                                    </div>
                                </div>
								<div class="form-group"><label class="col-lg-2  control-label">LOCATION</label>

                                    <div class="col-lg-10"><input type="text" name="location"   class="form-control"> 
                                    </div>
                                </div>
								
                                <div class="form-group"><label class="col-lg-2 control-label">Status</label>

                                    <div class="col-lg-10"><select class="form-control" name="status"  id="status" >
									<option value="enable">Enable</option>
									<option value="disable">Disable</option>
											</select>									
                                    </div>
                                </div>
                                
                               
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
                
            </div>
              
            </div>
     <script>
       $(function () {
 // validate signup form on keyup and submit
 $("#addEvent").validate({
  rules: {
   title: "required",    
   description: "required",    
   event_type: "required",    
   event_date: "required",    
   event_time: "required",    
   event_link: "required",    
   event_button_name: "required",    
   location: "required",    
   status: "required",    
  
  
  },
  messages: {
   title: "Please enter your title",
   description: "Please enter your description",
   event_type: "Please enter your event type",
   event_time: "Please enter your event time",
   event_link: "Please enter your event link",
   event_button_name: "Please enter your event button name ",
   location: "Please enter your location",
  
  },
  submitHandler: function (form) {                 
   var formData = new FormData(form);
  
     $.ajax({ 
      url:'<?=site_url();?>api/admin_api/addEvent', 
      data: formData,  
      processData: false,
      type: 'POST',
      cache: false, 
      contentType : false , 
       success: function(data)
       {
       
		if(data.status==true){
			
			toastr.success(data.message, 'Success');
			window.location.href='<?=site_url('admin/event');?>';
		} if(data.status==false){
			
			toastr.success('Failed Create Event ', 'Failed');
		}
       }   
     });    
          }
		  });
});

</script>
<?php echo $layout['footer']; ?>


