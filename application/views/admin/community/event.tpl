<?php   echo $layout['header'];
		$eventData = json_decode($eventList);
		$eventlist = $eventData->data;
 ?>  
	    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                     <a type="buttton" href="<?php echo site_url(); ?>admin/add_event" name="addevent" class="btn btn-primary ">Add Event</a>
                   
                     
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Events List</h5>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
					
                    <tr>
                        <th>HostedBy</th>
                        <th>Event</th>
                        <th>Event TYPE</th>
                        <th>Event Description</th>
                        <th>Date & TIME</th>                        
                        <th>status</th>
                        <th>Action</th>						
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach($community_Event as $key=>$event) { ?>
                    <tr class="gradeX">
                        <td><?php ?></td>
                        <td><?= isset($event['community_event_title']) ? $event['community_event_title'] :""; ?></td>
                        <td><?= isset($event['community_event_type']) ? $event['community_event_type'] :""; ?></td>
                        <td><?= isset($event['community_event_start_datetime']) ? $event['community_event_start_datetime'] :""; ?></td>
                        <td><?= isset($event['community_event_start_datetime']) ? $event['community_event_start_datetime'] :""; ?></td>
                        
                       
                        <td><span class="label label-primary"><?= isset($event['community_event_status']) ? $event['community_event_status'] :""; ?></span></td>
                        
                         
                         <td class="text-right">
                        <div class="btn-group">
                            
                            <button class="btn-white btn btn-xs"><a href="<?php echo base_url(); ?>admin/editEvent/<?php echo $event['community_event_id'] ?>">Edit</button>
                        </div>
                    </td>
			
                    </tr>
					<?php } ?>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

<?php echo $layout['footer']; ?>
