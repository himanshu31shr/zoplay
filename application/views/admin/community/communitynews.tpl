<?php   echo $layout['header'] ; ?>  
 
            
           <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Tables</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Tables</a>
                        </li>
                        <li class="active">
                            <strong>Data Tables</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Community News</h5>
                         <button type="buttton" name="addevent" style="margin-top:5px;" class="btn btn-primary pull-right">Add News</button>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>User Name</th>
                        <th>News Title</th>
                        <th>Added Date</th>
                        <th>Comment</th>
                        <th>status</th>
                        <th>Action</th>
						
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="gradeX">
                        <td>Alejandro Ramirez </td>
                        <td>Hello There! </td>
                       <td class="center">2016-05-30 23:09:49 </td>
                        <td>0</td>
                        <td class="center"> <span class="label label-primary">unpublish</span></td>
                         <td class="text-right">
                        <div class="btn-group">
                            <button class="btn-white btn btn-xs">View</button>
                            <button class="btn-white btn btn-xs">Edit</button>
                        </div>
                    </td>
		     </tr>
                    <tr class="gradeX">
                        <td>Alejandro Ramirez </td>
                        <td>Hello There! </td>
                       <td class="center">2016-05-30 23:09:49 </td>
                        <td>0</td>
                        <td class="center"> <span class="label label-primary">unpublish</span></td>
                         <td class="text-right">
                        <div class="btn-group">
                            <button class="btn-white btn btn-xs">View</button>
                            <button class="btn-white btn btn-xs">Edit</button>
                        </div>
                    </td>
		     </tr>
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
<?php echo $layout['footer']; ?>

 <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>