<?php   echo $layout['header'] ; ?>  
 
            
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2> Banner</h2>
                    
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?php echo  !empty($bannerinfo)  ? 'Update' : 'Add'; ?> Banner</h5>
                        </div>
                        <div class="ibox-content">
                            <form id="bannerform" class="form-horizontal">
							<input type="hidden" id="banner_id" value="<?php echo  !empty($bannerinfo) && isset($bannerinfo->banner_id) ? $bannerinfo->banner_id : ''; ?>">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tag Line<span style="color: red">*</span></label>
                                    <div class="col-sm-10"><input type="text" name="tagline" id="tagline" class="form-control" value="<?php echo  !empty($bannerinfo) && isset($bannerinfo->tagline) ? $bannerinfo->tagline : ''; ?>"></div>
                                </div>
								<div class="form-group">
                                    <label class="col-sm-2 control-label">Headding <span style="color: red">*</span></label>
                                    <div class="col-sm-10"><input type="text" name="headding" id="headding" class="form-control" value="<?php echo  !empty($bannerinfo) && isset($bannerinfo->heading) ? $bannerinfo->heading : ''; ?>"></div>
                                </div>
								<div class="form-group">
                                    <label class="col-sm-2 control-label">Description <span style="color: red">*</span></label>
                                    <div class="col-sm-10"><input type="text" name="description" id="description" class="form-control" value="<?php echo  !empty($bannerinfo) && isset($bannerinfo->description) ? $bannerinfo->description : ''; ?>"></div>
                                </div>
                               <div class="form-group"><label class="col-sm-2 control-label">Banner Image <span style="color: red">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" name="image" id="image" accept="image/*">
                                    </div>
                                </div>
                               <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
										<div class="switch">
											 <div class="onoffswitch">
												 <input type="checkbox"  
												class="onoffswitch-checkbox"name="status" id="example1" checked>				 
												 <label class="onoffswitch-label" for="example1">
													 <span class="onoffswitch-inner"></span>
													 <span class="onoffswitch-switch"></span>
												 </label>
												 <input type="hidden" value="<?php echo  !empty($bannerinfo) && isset($bannerinfo->status) ? $bannerinfo->status : ''; ?>" id="checkstatus">
											 </div>
											 <input type="hidden" value="1" name="userstatus" id="userstatus">
										</div>           
									</div>
								</div>
                               <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
<?php echo $layout['footer']; ?>
<script>
$(document).ready(function() {
	var status = $("#checkstatus").val();
	if(status == 1)
	{
		$("#example1").prop('checked',true);	
	}else if(status == ""){
		$('#example1').prop('checked',true);
	}else{
		$('#example1').prop('checked',false);
	}
    $("#example1").click(function() {
        if (this.checked) {
           $('#userstatus').val('1');
        }else{
			$('#userstatus').val('0');
		}
    });
});
$(function () {
	// validate signup form on keyup and submit
	$("#bannerform").validate({
		rules: {
			tagline: {
				required: true,
				maxlength: 255
			},
			headding: {
				required: true,
				maxlength: 255
			},
			description: {
				required: true,
				maxlength: 255
				
			},
			
			image: {
				<?php if(empty($bannerinfo)){ ?>
				required : true,
				<?php } ?>
				accept:"jpg,png,jpeg,gif"			
			},
							
		},
		messages: {
			
			tagline: {
				required: "Please enter a taglaine",
				maxlength: "taglaine must consist of almost 255 characters"
			},
			headding: {
				required: "Please enter a headding",
				maxlength: "headding must consist of almost 255 characters"
			},
			description: {
				required: "Please enter a description",
				maxlength: "description must consist of almost 255 characters",
			},
			
			image : {
				<?php if(empty($bannerinfo)){ ?>
				required:"Image required",
				<?php } ?>
				
				accept: "Only image type jpg/png/jpeg/gif is allowed"
			}
		},
		submitHandler: function (form) { 			
			var formData = new FormData();
			var tagline = $('#tagline').val();
			var headding = $('#headding').val(); 
			var description = $('#description').val();
			var userstatus = $('#userstatus').val();
			var banner_id = $('#banner_id').val();
			
			var image = $('#image').val();
				formData.append("tagline",tagline);
				formData.append("headding",headding);
				formData.append("status",userstatus);
				formData.append("description",description);
				formData.append("banner_id",banner_id);
				
				formData.append("image", $('#image')[0].files[0]);
					$.ajax({ 
						url:'<?=site_url();?>api/admin_api/savebanner', 
						data: formData,
						processData: false,
						type: 'POST',
						datatype: 'json',
						cache: false,	
						contentType : false ,	
							
						beforeSend : function()
						{
							$('body').css('z-index', 99999999999).append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
						},						
						success: function(res)
						{
							if(res.status == true){
								toastr.success('Data saved Successfully');
								window.location.href ="<?= site_url() ?>admin/bannerlist";
							}else{
								toastr.error(res.message);
							}
						},
						complete : function(){
							$('.overlay').remove();
						}
						
					}); 			
          }
	});
}); 



</script>		