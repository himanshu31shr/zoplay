<?php   echo $layout['header'] ; ?>  
 <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Basic Form</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Forms</a>
                        </li>
                        <li class="active">
                            <strong>Basic Form</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Gift Cards Settings</h5>
                            <button type="button" class="pull-right">Image</button>
                            <button type="button" class="pull-right">content</button>
                        </div>
                        <div class="ibox-content">
                            <form method="get" class="form-horizontal">
                                <div class="form-group"><label class="col-sm-2 control-label">Title</label>

                                    <div class="col-sm-10"><input type="text" class="form-control"></div>
                                </div>
                           
                                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea name="description" class="form-control"></textarea>
                                      
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Amount</label>
                                    <div class="col-sm-10">
                                        <input class="tagsinput form-control" type="text" value="10,20,30,40,50"/>
                                      
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Default Amount</label>
                                    <div class="col-sm-10"><input type="text" class="form-control">
                                       
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Expairy Days</label>
                                    <div class="col-sm-10"><input type="text" class="form-control">
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
<?php echo $layout['footer']; ?>
<script>
      $(document).ready(function(){
            $('.tagsinput').tagsinput({
                tagClass: 'label label-primary'
            });
      });
</script>