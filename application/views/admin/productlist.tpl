<?php echo $layout['header'] ; ?>  
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Products List</h2>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Product List</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-example" >
							<thead>
								<tr>
									<th>Product Name</th>
									<th>Image</th>
									<th>Price</th>
									<th>Added by</th>
									<th>Quantity</th>
									<th>Status</th>
									<th>Created On</th>
									<th>Action</th>
									
								</tr>
							</thead>
							<tbody>
							<?php
								if(isset($list)){
									
									$list = json_decode($list);
									foreach($list as $l){
									
							?>
								<tr class="gradeX">
									<td><?=$l->product_name;?></td>
									<td><?php 
										if($l->product_image== ''){
											echo "No Image";
										}else{
											echo site_url($l->product_image);
										}
										?>
									</td>
									<td><?=$l->product_price;?></td>
									<td><?=$l->full_name;?></td>
									<td class="center"><?=$l->product_quantity;?></td>
									<td class="center">
										<span class="label label-primary">
											<?php
												if($l->product_status == 0){
													echo "Inactive";
													$flag = "Activate";
												}else if($l->product_status == 1){
													echo "Active";
													$flag = "Deactivate";
												}else{
													$flag = "Activate";
													echo "Deleted";
												}
											?>
										</span>
									</td>
									<td><?=$l->product_added;?></td>
									<td class="text-center">
										<div class="btn-group">
											<button class="btn btn-primary tooltip1" title="View Product" onclick="view('<?php echo md5($l->product_id);?>')"><i class="fa fa-eye"></i></button>
											<button class="btn btn-white" onclick="edit(<?=$l->product_id;?>)"><i class="fa fa-edit"></i></button>
											<!--button class="btn btn-secondary" onclick="change_flag(<?=$l->product_id;?>, <?=$l->product_status;?>)"><i class="fa fa-flag"></i></button-->
											<button class="btn btn-danger" onclick="delete_product(this , <?=$l->product_id;?>)"><i class="fa fa-trash"></i></button>
										</div>
									</td>
								</tr>
							<?php
									}
								}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $layout['footer']; ?>
<style>
 .tooltip1[title]:hover:after {
  content: attr(title);
  position: absolute;
  back
}
</style>
<script>
$(document).ready(function() {
	$('.tooltip').tooltipster();
});
var change_flag = function(product_id, flag){
	
}

function edit(a){
	window.location.replace('<?php echo site_url();?>/admin/addproduct/'+a);	
}


 function delete_product(b , a){
		$.ajax({
		url :  "<?php echo site_url();?>/admin/delete_product/" ,
		data : "product_id=" + a ,
		type : "GET" ,
		processData : false ,
		contentType : false ,
		success : function(data){
			if( data.status =="success")
			{
				$(b).parents("tr").hide();
				toastr.success(data.msg , 'Server Response ')
			}else if( data.status =="error")
			{
				toastr.error(data.msg , 'Server Response')
			}	
				
				
		}
		});		
}

function view( a ){
	
	 window.open('<?php echo  site_url()."product/" ;?>'+a,'_blank');
}

</script>