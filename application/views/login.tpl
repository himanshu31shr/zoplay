<?php  echo $layout['header'] ; ?>

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="home.html">Home</a></li>
				<li class='active'>Login</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="sign-in-page">
			<div class="row">
				<!-- Sign-in -->			
<div class="col-md-6 col-sm-6 sign-in">
	<h4 class="">Sign in</h4>
	<p class="">Hello, Welcome to your account.</p>
	<div class="social-sign-in outer-top-xs">
	<?php 
	if(!isset($data['fb_login_url'])){

	}else{
		echo '<a href="'.$data["fb_login_url"].'" class="facebook-sign-in"><i class="fa fa-facebook"></i> Sign In with Facebook</a>';
	}

	if(isset($data['google_login_url'])){
	?>
		<a href="<?=$data['google_login_url'];?>" class="twitter-sign-in"><i class="fa fa-google-plus"></i> Sign In with Google</a>
	<?php
		}
	?>
	</div>
	<form class="register-form outer-top-xs" role="form" id="login">
		<div id="alert"></div>
		<input type="hidden" name="backurl" id="backurl" value="<?php if(isset($_GET['returnurl'])){ echo $_GET['returnurl'];}?>">
		<div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">Email Address <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input" name="emaill" id="email" >
			<p class="text text-danger" id="emaill_err"></p>
		</div>
	  	<div class="form-group">
		    <label class="info-title" for="exampleInputPassword1">Password <span>*</span></label>
		    <input type="password" class="form-control unicase-form-control text-input" name="passwordl" id="password" >
			<p class="text text-danger" id="passwordl_err"></p>
		</div>
		<div class="radio outer-xs">
		  	<!--<label>
		    	<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Remember me!
		  	</label>-->
		  	<a href="" class="forgot-password pull-right">Forgot your Password?</a>
		</div>
	  	<button type="submit" id="loginbtn" class="btn-upper btn btn-primary checkout-page-button">Login</button>
	</form>		

<script>
if (typeof(Storage) !== "undefined") {
    console.log("Code for localStorage/sessionStorage.");
} else {
     console.log(" Sorry! No Web Storage support..");
}
	$(document).ready(function(){
		$('#loginbtn').on('click', function(e){
			e.preventDefault();
			var form = new FormData($('#login')[0]);
			$.ajax({
				url:'<?=site_url();?>/user/log_user',
				data: form,
				type:"POST",
				processData:false,
				contentType: false,
				cache:false,
				beforeSend:function(){
					$('#loginbtn').attr('disabled', 'disabled').html('<i class="fa fa-spinner fa-pulse fa-fw"></i><span class="sr-only">Loading...</span>Please wait..');
				},
				success:function(response){
					var data = response;
					console.log(data);
					//var data = $.parseJSON(data);
					if(data.status == true){
						// Store
									
						$('#alert').html('<p class="alert alert-success">'+data.message+'</p>');
						$('#loginbtn').html('<i class="fa fa-check"></i> Logged In!');
						setTimeout(function() {
							swal({
								title: "Congrates!",
								text: "Logged successfully!",
								type: "success"
							}, function() {
								window.location.replace(data.data);
							});
						}, 500);
					}
				},
				error:function(response){
					var data = response.responseText;
					var data = $.parseJSON(data);
					console.log(data);
					if(data.status == false){
						$('#alert').html('<p class="alert alert-danger">'+data.message+'</p>');
						var err = data.errorMsg;
						$.each(err, function(key, value){
							$('#'+key+'l_err').text(value);
						});
						$('#loginbtn').removeAttr('disabled').html('<i class="fa fa-check"></i> Try again!');
					}
				}
			})
		})
	});
</script>	
</div>
<!-- Sign-in -->

<!-- create a new account -->
<div class="col-md-6 col-sm-6 create-new-account">
	
	<h4 class="checkout-subtitle">Create a new account</h4>
	<p class="text title-tag-line">&nbsp;</p>
	<form class="register-form outer-top-xs" id="reg_form" role="form">
		<div id="status">
                        
        </div>
		<div class="form-group">
	    	<label class="info-title" for="exampleInputEmail2">Email Address <span>*</span></label>
	    	<input type="email" name="email" class="form-control unicase-form-control text-input" id="exampleInputEmail2" >
			<div class="text-danger" id="email_error" ></div>
			<div class="text-danger" id="is_unique_error" ></div>
	  	</div>
        <div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">Name <span>*</span></label>
		    <input type="text" name="username" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
			<div class="text-danger" id="username_error" ></div>
		</div>
        <div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
		    <input type="text" name="phone" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
			<div class="text-danger" id="phone_error" ></div>
		</div>
        <div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">Password <span>*</span></label>
		    <input type="password" name="password" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
			<div class="text-danger" id="password_error" ></div>
		</div>
         <div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">Confirm Password <span>*</span></label>
		    <input type="password" name="passconf" class="form-control unicase-form-control text-input" id="exampleInputEmail1" >
			<div class="text-danger" id="passconf_error" ></div>
		</div>
	  	<button type="submit" id="click_form" class="btn-upper btn btn-primary checkout-page-button">Sign Up</button>
	</form>
</div>	

	<script type="text/javascript">
	$(document).ready(function() {   
		$('#click_form').click(function(e){
			e.preventDefault();
			var formdata = new FormData($('#reg_form')[0]);
			$.ajax({
			url: '<?=site_url();?>User/register',
			data: formdata,
			type:"POST",
			contentType: false,
			processData:false,
			cache: false,
			beforeSend:function(){ 
				$('body').append('<div class="overlay"><img src="<?=base_url();?>/assets/frontend/images/loader.svg" /></div>');
			},
			success: function(response){
				var data = response;
				if(data.status == true){
					$('#status').html('<p class="alert alert-success">'+response.message+'<p>');
					$('.text-danger').empty();
					$('#click_form').removeAttr('disabled').html('<i class="fa fa-check"></i> Registration Successful!</p>');
					$('#reg_form')[0].reset();
					$('.error').html();
				}
			},
			error: function(response){  
				var data = response.responseText;
				data = $.parseJSON(data);
				console.log(data);
				if(data.status == false){
					data = data.data;
					$.each(data, function(key, value){
						$('#'+key+'_error').html(value);
					});
					$('#click_form').removeAttr('disabled').html('<i class="fa fa-times"></i> Try again!');
				}
			}, 
			complete:function(){
				$('.overlay').remove();
			}
		});
		});
	});
	</script>
<!-- create a new account -->			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
	</div><!-- /.container -->
</div><!-- /.body-content -->

<?php
if($this->session->flashdata('alerts')){
$alert = $this->session->flashdata('alerts');
	?>
	<script type="text/javascript">
		swal("<?=$alert['message'];?>");
	</script>

	<?php
}

?>
<?php echo $layout['footer']; ?>