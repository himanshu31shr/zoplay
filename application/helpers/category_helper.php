<?php 
	function jsCategories($categories,$html = null) {
		
		if(!empty($categories)){
			$html='<ul>';
			foreach ($categories as $row)
			{
				
				$html.="<li>".$row['category_name'] . " <span style=\"margin-left:20px;\">";
				
				$html.="<a title=\"add subcategory\" style=\"color:green;\" onclick=\"add_category(".$row['category_id'].")\"><i class=\"fa fa-plus-circle \" ></i></a> &nbsp; &nbsp;";
				
				$html.="<a title=\"delete\" style=\"color:red;\" onclick=\"delete_category(".$row['category_id'].")\"><i class=\"fa fa-trash-o\" ></i></a>&nbsp; &nbsp;";
				
				$html.="<a title=\"edit category\" style=\"\" onclick=\"edit_category('".$row['category_id']."','".$row['category_name']."')\"><i class=\"fa fa-pencil\"></i></a></span>";
				if(!empty($row['data'])){
					$html.=jsCategories($row['data'],$html);
				}
				$html.="</li>";
			}
			$html.='</ul>';
			
			
		}
		return $html;
    }
    
      function getCategoryParentId($cid) {
        $CI =& get_instance();
        $categories = array();
        $CI->db->from('categories');
        $CI->db->where('parent_id', $cid);
        $result = $CI->db->get()->result();
        return $result;
    }

    
?>