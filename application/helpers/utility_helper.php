<?php 

     function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if(!isset($is_logged_in) || $is_logged_in != true)
        {
          return false; 
        }else{
            return true;   
         }       
    }

	/* function langIn($msg=null)
		{
			$obj = &get_instance();
		
				if(isset($_COOKIE['language']))
					{
						$obj->lang->load($my_lang , $my_lang);
					} else {
						$obj->lang->load('english' , 'english');	
					}
						$newlang  = $obj->lang->line($msg);			
			return $newlang ;
		} */

	function gettime4db($date)
		{
			if(empty($date) || $date == '0000-00-00 00:00:00'){
				return '';
			
			}else{
				return date('Y-m-d h:i:s',strtotime($date)); 	
			}
		}
		
	function isselected($value1,$value2)
		{
			if($value2==$value1)
				{
					echo "selected='selected'";	
				}
		}
	function ischecked($value1,$value2)
		{
			if($value2==$value1)
				{
					echo "checked='checked'";	
				}
		}
		
	function time_elapsed_string($datetime, $full = false) 
	{
		$now = new DateTime;
		$ago = new DateTime();
		$ago =$ago->setTimestamp($datetime);;
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
		
	function getCountries( $param = null , $format= null, $shipping=null){
		
			
		
		$CI = & get_instance();
		$CI->load->model("CommonModel");
	
		 $sql="SELECT id ,  nicename  From countries ";
		
		if($shipping!=null)
		{
		  $sql="SELECT b.id ,  b.nicename, b.iso  From shippinglocation as a join countries as b on a.country_id = b.id where a.status = 1";
		}
		 $query=$CI->db->query($sql);
		 $r = $query->result_array();
		 $options = "";
		if($format != null){
			return $r[0]['nicename'];
		}
		 foreach( $r as $single )
		 {
			$value = $single['id'];
			if($shipping!=null)
			{
				$value = $single['iso'];
			}
			  if( $param == $single['id'] ){  $selected = "selected";}else{ $selected = ""; }
			$options .= "<option value='{$value}'   $selected >{$single['nicename']}</option>";
			 
		 }
		 
		 return $options ;
	}	
	
	
	
	function getVariations( $param = null  ){
		$types = array("color" , "size" , "flavor" , "weight" , "length" , "height" , "breadth" );
			$return =  '<option value="">Select available property...</option>';	
	foreach( $types as $type )
		{
			if($param == $type){ $selected = "selected";}else{ $selected = "";} 
			$return .= "<option value='$type'  $selected >$type</option>";
			
		}
	
		return  $return ;
	}
	
	function getThisData( $params=null ){
		/*
		  returns $result_set
		*/
		$result_set = array();
		$CI = & get_instance();
			if($params != null)
			{
				$data = explode(",",$params);
				
				foreach( $data as $single ){
					if(!empty($single))
					{
						$r = $CI->db->query("SELECT category_id , category_name FROM categories WHERE category_id = $single");
						$s = $r->result_array();
								if( !empty( $s ))
								{
									$result_set[] = array("id" => $s[0]['category_id'] , "text"=> $s[0]['category_name']  );
								}
					}	
				}
				
				
			}
			
			return json_encode( $result_set );
			
			
		}
		
		function getListingOrder( $params = null ){
			
			$return = '<optgroup label="Not yet made">
								<option value="made_to_order">Made to order</option>
							</optgroup>
							<optgroup label="Recently">
								<option value="2010,2017">2010 - 2017</option>
								<option value="2000,2009">2000  - 2009</option>
								<option value="1995,1999">1995  - 1999</option>
							</optgroup>
							<optgroup label="Vintage">
								<option value="0,1994">Sometime before 1995</option>
								<option value="1990,1994">1990  - 1994</option>
								<option value="1980,1989">1980s</option>
								<option value="1970,1979">1970s</option>
								<option value="1960,1969">1960s</option>
								<option value="1950,1959">1950s</option>
								<option value="1940,1949">1940s</option>
								<option value="1930,1939">1930s</option>
								<option value="1920,1929">1920s</option>
								<option value="1910,1919">1910s</option>
								<option value="1900,1909">1900 - 1909</option>
								<option value="1800,1899">1800s</option>
								<option value="1700,1799">1700s</option>
								<option value="0,1699">Before 1700</option>
							</optgroup>';
							
			return $return ;				
			/* $types = array("1" => "A finished product" , "2" => "A supply or tool to make things" );
			$return =  '<option value="">Select a use...</option>';	
	foreach( $types as  $key => $type )
		{
			if($param == $type){ $selected = "selected";}else{ $selected = "";} 
			$return .= "<option value='$key'  $selected >$type</option>";
			
		}
	
		return  $return ; */
			
			
			
		}
		
		
		function getListingUse( $param = null ){
			
			$types = array("1" => "A finished product" , "2" => "A supply or tool to make things" );
			$return =  '<option value="">Select a use...</option>';	
	foreach( $types as  $key => $type )
		{
			if($param == $key){ $selected = "selected";}else{ $selected = "";} 
			$return .= "<option value='$key'  $selected >$type</option>";
			
		}
	
		return  $return ; 
			
			
		}
		
		function getListingMaker( $param = null ){
			
			$types = array("1" => "I did" , "2" => "A member of my shop" , "3" => "Another company or person"			);
			$return =  '<option value="">Select a maker...</option>';	
	foreach( $types as  $key => $type )
		{
			if($param == $key){ $selected = "selected";}else{ $selected = "";} 
			$return .= "<option value='$key'  $selected >$type</option>";
			
		}
	
		return  $return ;
			
		}
		
		
		function getProcessingTime(){
			return '
			<option value="">Ready to ship in...</option>
                                    <optgroup label="----------------------------"></optgroup>
                                    <option class="range-1-1">1 business day</option>
                                    <option class="range-1-2">1-2 business days</option>
                                    <option class="range-1-3">1-3 business days</option>
                                    <option class="range-3-5">3-5 business days</option>
                                    <option class="range-5-10">1-2 weeks</option>
                                    <option class="range-10-15">2-3 weeks</option>
                                    <option class="range-15-20">3-4 weeks</option>
                                    <option class="range-20-30">4-6 weeks</option>
                                    <option class="range-30-40">6-8 weeks</option>
                                    <option value="custom">Custom range</option>
			';
			
		}
	
	function getDeliveryOptions(){
		
		return '<option value="Delivery and Collection">Delivery and Collection</option>
                                   
                                    <option value="Delivery Only">Delivery Only </option>
                                    <option value="Collection Only">Collection Only</option>';
	}
	
	
        function image_url($product_image){
            if($product_image == ''){
                $image = base_url().'files/default.png';
            }else if(substr($product_image, 0, 4) == 'http'){
                $image = $product_image;
            }else{
                $image = base_url()."files/".$product_image;
            }
            return $image;
        }
?>