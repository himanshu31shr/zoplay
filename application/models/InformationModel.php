<?php

	Class InformationModel extends CI_Model
		{
	
			public function checkpostinput($index)
				{
						$return = $this->input->post($index);
						$return = $this->security->xss_clean($return);
					return trim($return); 
				}

			public function checkgetinput($index)
				{
						$return = $this->input->get($index);
						$return = $this->security->xss_clean($return);
					return trim($return); 
				}
								
			public function checkloggedin()
				{
					if($this->session->userdata('token') && $this->session->userdata('username') && $this->session->userdata('logintype'))
						{
							$token			=	$this->session->userdata('token');
							$username		=	$this->session->userdata('username');
							$logintype		=	$this->session->userdata('logintype');
								if(trim($logintype)=="admin")
									{
										$temp = array();
											$temp['token']		=	base64_decode($token);
											$temp['username']	=	$username;
											$temp['logintype']	=	$logintype;
										return $temp;
									} else {
										return 0;
									}
						} else {
							return 0;
						}
				}
	
			public function dologin() 
				{	
					$data = array();
						$data['refresh'] = 0;
							$email 	   =  $this->checkpostinput('email');
							$password  =  $this->checkpostinput('password');
						
						if($email=="")
							{
								$data['status']		=	0;
								$data['message']	=	"email is not correct";
								return json_encode($data);
							}
						if($password=='')
							{
								$data['status']		=	0;
								$data['message']	=	"password is not correct";
								return json_encode($data); 
							}
						if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
							{
								$data['status']		=	0;
								$data['message']	=	"Invalid email format";
								return json_encode($data);
							}
						if(trim($email != "") && trim($password != ""))				
							{
									$password = md5($password);
									$this->db->select('`userid`,`username`,`email`,`lastlogin`');
									$this->db->from('admin');
									
									//	$where	=	
									
									$this->db->where('email',$email);
									$this->db->where('password',$password);
									$query	=	$this->db->get();
									$result =	$query->result();

								if(!empty($result))
									{
										$token 		= base64_encode($result[0]->userid); 
										$username 	= $result[0]->username;
										$logintype	= "admin";
											/* setting session data */
											$session = array(
																'token' 		=>	$token,
																'username'		=>	$username,
																'logintype' 	=>	$logintype
															);
												$this->session->set_userdata($session);
												$data['refresh']	=	1;
											/* setting session data */
										$data['status']		=	1;
										$data['message']	= 	" Login Succesfully.";
										return json_encode($data);
									} else {
										$data['status']		=	0;
										$data['message']	=	"Wrong Email or Password.";
										return json_encode($data);
									}
									
							} else {
									$data['status']		=	0;
									$data['message']	=	"Please check the entered details.";
								return json_encode($data);
							}
									$data['status']		=	0;
									$data['message']	=	"Something went Wrong.";
								return json_encode($data);
				}


			public function most_viewed(){
				return $data  = $this->db->from('products')
						->where('product_status', 1)			
						->where('product_quantity > ', 0)														
						->order_by('product_views', 'DESC')
						->group_by('product_id')
						->limit(12)
						->get()->result();
			}

			public function most_recent(){
				return $data  = $this->db->from('products')
						->where('product_status', 1)														
						->where('product_quantity > ', 0)														
						->order_by('product_added', 'DESC')
						->group_by('product_id')
						->limit(12)
						->get()->result();
			}

			public function most_ordered(){
				return $this->db->query('SELECT products.*, COUNT( order_detail.order_product_id ) AS sales_count
						FROM  `order_detail` 
						LEFT JOIN  `products` ON order_detail.order_product_id = products.product_id
						GROUP BY order_detail.order_detail_id
						ORDER BY sales_count DESC')->result();
			}

			public function most_popular_shops(){
				return $this->db->select('a.*, sum(b.product_views) as total_view, c.user_name')
							->from('shops a')
							->join('products b', 'b.user_id = a.seller_id')
							->join('users c', 'c.id = a.seller_id')
							->limit(4)
							->group_by('shop_id')
							->order_by('total_view', 'ASC')
							->get()->result();
			}

			public function suggested_posts($cat_id){
				return $this->db->from('products')
						->like('category_id', $cat_id)
						->order_by('rand()')
						->group_by('product_id')
						->limit(8)
						->get()->result();
			}
		}

?>