<?php
Class ProductModel extends CI_Model
{
	
	public function all_products($category, $last_id = null, $filter = null)
	{
		if($category){
			if(!is_numeric($category)){ 
				$this->db->from('products a')
						->where('a.product_name',$category);
			}else{
				$this->db->from('products a')
					->join('categories b', 'a.category_id = b.category_id')
					->like('a.category_id', $category);
			}
			if($last_id != null){
				$this->db->where('a.product_id > ', $last_id);
			}
			
			if($filter == 'plh'){
				$this->db->order_by('a.product_price', 'ASC');
			}else if($filter == 'phl'){
				$this->db->order_by('a.product_price', 'DESC');
			}else if($filter == 'name'){
				$this->db->order_by('a.product_name', 'ASC');
			}else{
				$this->db->order_by('a.product_id', 'ASC')		;		
			}
			
			//echo $filter;die;
			if($filter == ''){
				$this->db->limit(12);	
			}else{
				$this->db->limit(12);
			}
			$products = $this->db->get()->result();
			
			$i=0;
			foreach($products as $p){
				$reviews = $this->reviews($p->product_id);
				if($reviews){
					$num = $reviews[0]->total_reviews;
					$sum = $reviews[0]->total_rating;
					$average = $num != 0 ? (float) $sum/$num : 0;
					$products[$i]->average_rating = $average;
					$products[$i]->total_reviews = $num;
					
				}else{
					$products[$i]->average_rating = 0;
					$products[$i]->total_reviews = 0;
				}
				$i++;
			}
			
			return $products;
		}else{
			return false;
		}
	}
	
	public function reviews($product_id, $limit = null, $last_id =null){
		if(strlen($product_id) > 5){
			$p = $this->db->select('product_id')->from('products')->where('md5(product_id) = "'.$product_id.'"', null, false)->get()->result();
			$product_id = $p[0]->product_id;
		}
		$this->db->select('count(review_id) as total_reviews, sum(rating) as total_rating')
			->from('reviews')
			->where('product_id', $product_id);
		$return	= $this->db->get()->result();
		
		if($limit != null){
			$return	= $return[0];
			$this->db->from('reviews')
				->where('product_id', $product_id);
				if($last_id != null){
					$this->db->where('review_id > ', $last_id);
				}
			$return->reviews = $this->db->order_by('review_id', 'asc')
					->limit($limit)
					->get()->result();
		}
	
		return $return;
	}
	
	public function single_product($category, $product_id){
		$product =  $this->db->from('products a')
				->where('md5(a.product_id) = "'.$product_id.'"', null, false)
				->get()->result();
		
		return $product;	
	}


	public function wishlist($user_id, $last_id = null){
		$this->db->from('wishlist a')
			->join('products b', 'a.product_id = b.product_id')
			->where('a.user_id', $user_id)
			->where('a.wishlist_status', '1');
			if($last_id != null){
				$this->db->where('a.wishlist_id > ', $last_id);
			}
			$return = $this->db->order_by('wishlist_id', 'ASC')->get()->result();

			return $return;
	}
        public function serach_product($subcategoryid,$categoryid,$minprice,$maxprice) {
            
                        if($subcategoryid){
                          $like = $categoryid.','.$subcategoryid;
                        }else{
                              $like = $categoryid;
                        }
                        $sql= "SELECT * FROM  products WHERE category_id LIKE  '%$like%' AND (product_price >= $minprice AND product_price<=$maxprice)";
                         $query = $this->db->query($sql);
                        $products = $query->result();
                        if(!empty($products)){
                            $i=0;
                            foreach($products as $p){
                                    $reviews = $this->reviews($p->product_id);
                                    if($reviews){
                                            $num = $reviews[0]->total_reviews;
                                            $sum = $reviews[0]->total_rating;
                                            $average = $num != 0 ? (float) $sum/$num : 0;
                                            $products[$i]->average_rating = $average;
                                            $products[$i]->total_reviews = $num;

                                    }else{
                                            $products[$i]->average_rating = 0;
                                            $products[$i]->total_reviews = 0;
                                    }
                                    $i++;
                            }

                            return $products;
                        }else{
                         return false;
                       }
            
        }
  public function getFeaturedProducts(){
			$this->db->order_by("product_id", "DESC");
		   $products = $this->db->get_where('products', array('is_featured'=>1,'product_status'=>1), 15, 0)->result();
		if($products){
			$i=0;
			foreach($products as $p){
				$reviews = $this->reviews($p->product_id);
				if($reviews){
					$num = $reviews[0]->total_reviews;
					$sum = $reviews[0]->total_rating;
					$average = $num != 0 ? (float) $sum/$num : 0;
					$products[$i]->average_rating = $average;
					$products[$i]->total_reviews = $num;
					$products[$i]->added_to_wish = 0;
					
				}else{
					$products[$i]->average_rating = 0;
					$products[$i]->total_reviews = 0;
				}
				$i++;
			}
			
			return $products;
		}else{
			return false;
		}
	} 
        
        
        
}