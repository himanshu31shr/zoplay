<?php
Class Seller_model extends CI_Model
{
    public function sellerLogin($email,$password){
        $this->db->select("*");
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $this->db->where('role','seller');
        $result = $this->db->get('users');
        return $result->row();
    }
	

    public function product_display($seller_id, $last_id = null){
	  	$this->db->select("*")
	  			->from('products as t1')
	  			->join('categories as t2','t1.category_id=t2.category_id', 'LEFT')
	  			->where('t1.user_id', $seller_id);
      if($last_id != null){
        $this->db->where('t1.product_id > ', $last_id);
      }
      $this->db->order_by('t1.product_id', 'ASC')
          ->limit(8);
      	$query=$this->db->get();
      	return $query->result();             
    }
	public function productList()
	{	
		$data = $this->db->select('a.product_id, a.user_id, a.product_name, a.product_price, a.product_quantity, a.product_image, a.is_featured, a.product_status, a.product_added, b.full_name')
			->from('products a')
			->join('users b', 'a.user_id = b.id')
			->where('a.user_id = 4')
			->get()->result();		
		return $data;
	}
  
  public function shopdetails($data)
  {
     $this->db->insert("shops",$data);
        
    
  }
     function search_products($id)
     {
        $product=trim($this->input->get('term')); //get term parameter sent via text field. Not sure how secure get() is
        $this->db->select('product_name, product_id');
        $this->db->from('products');
        $this->db->where('user_id',$id);
        $this->db->like('product_name',$product);

      
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            $data['response'] = 'true'; 
            $data['message'] = array();

            foreach ($query->result() as $row)
            {
                $data['message'][] = array(
                    'label' => $row->product_name,
                    'value' => $row->product_id
                );
            }

           
        }

           
        else
        {
            $data['response'] = 'false'; 
        }

        return json_encode($data);
}

}

?>