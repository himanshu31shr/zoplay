<?php

Class UserModel extends CI_Model
{	
	public function email_verification_get($token = null, $user_id = null){
		if($token != null){
			return $this->db->query('SELECT * FROM users WHERE email_verification_token = "'.$token.'" LIMIT 1')->result();
		}
		
		if($user_id != null){
			$this->db->update('users', array('is_email_verified'=> 1), array('id'=>$user_id));
			
			return $this->db->affected_rows() > 0 ? true : false;
		}
	}
	public function select_orderinvoice_data($order_id)
	{
		$this->db->select('*');
		$this->db->from('order_detail a');
		$this->db->join('products b','a.order_product_id = b.product_id','left');
		$this->db->where(array('a.order_id' => $order_id));
		$query = $this->db->get();
		return $query;
	}
	public function select_orderaddress_data($orderaddressid)
	{
		$this->db->select('*');
		$this->db->from('user_addresses a');
		$this->db->join('countries b','a.address_country = b.id','left');
		$this->db->where(array('a.address_id' => $orderaddressid));
		$query = $this->db->get();
		return $query;
	}

}