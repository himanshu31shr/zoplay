<?php

Class AdminModel extends CI_Model
{	
	/**
	*	Author: Himanshu
	*
	*	updates seller status
	*	@param int flag, id 
	*	@return boolean
	*/
	public function seller_upd($flag, $id)
	{
		$stat = $this->db->update('users', array('admin_approved_status' => $flag), array('id'=>$id));
		if ($this->db->affected_rows() == 1) {
			
			return true;
		} else {
			return false;
		}
	}
	
	/**
	*	Author: Himanshu
	*
	*	Fetches transactions of seller 
	*	@param int payment_mode
	*	@return mixed
	*/
	public function sitearnings($payment_mode = null){
		$data = $this->db->from('users a')
				->where('a.role', 'seller')->get()->result();
				
		$i = 0;
		foreach($data as $d)
		{
			$this->db->from('transactions')->where('user_id',$d->id);
			if($payment_mode != null){
				$this->db->where('payment_gateway_id',$payment_mode);
			}
			$data[$i]->transactions = $this->db->get()->result();
						
			$this->db->from('orders')->where('user_id', $d->id);
			if($payment_mode != null){
				$this->db->where('payment_gateway_id',$payment_mode);
			}
			$data[$i]->orders = $this->db->get()->result();
					
			$i++;
		}	
		return $data;
	}
	
	public function productList( $param = null )
	{		
		if( $param =="recent"){
				$data = $this->db->select('a.product_id, a.user_id, a.product_name, a.product_price, a.product_quantity, a.product_image, a.is_featured, a.product_status, a.product_added, b.full_name')
				->from('products a')
				->join('users b', 'a.user_id = b.id')
				->order_by('a.product_added')
				->get()->result();			
		}
		else if(  $param == "deleted"){
				$data = $this->db->select('a.product_id, a.user_id, a.product_name, a.product_price, a.product_quantity, a.product_image, a.is_featured, a.product_status, a.product_added, b.full_name')
			->from('products a')
			->join('users b', 'a.user_id = b.id')
			->where('a.product_status = 2')
			->order_by('a.product_added')
			->get()->result();
			
		}else{
			$data = $this->db->select('a.product_id, a.user_id, a.product_name, a.product_price, a.product_quantity, a.product_image, a.is_featured, a.product_status, a.product_added, b.full_name')
			->from('products a')
			->join('users b', 'a.user_id = b.id')
			->get()->result();
		}					
		return $data;
	}
	
	public function getDealsOfTheDay( $id=null  ){
	  $date =  new DateTime();
	  $date = $date->getTimestamp();
	  if( $id != null)
	  { $conds = " AND product_id = $id" ;
	   $sql = "SELECT *  FROM products WHERE unix_timestamp(deal_datetime_from) < $date  AND unix_timestamp(deal_datetime_to)  > $date   ";
	   $r = $this->db->query( $sql ); 
	   if(!empty( $r->result_array()))
	   {
	    $return = array("status"=>"success" , "msg" => "Deal is activated " );
	   }else{
	    $return = array("status"=>"error" , "msg" => "No Deal" );
	   } 
	    return $return ;
	  
	  }else{
	   $sql = "SELECT *  FROM products WHERE unix_timestamp(deal_datetime_from) < $date  AND unix_timestamp(deal_datetime_to)  > $date ";
	     $r = $this->db->query( $sql ); 
	   $conds = "";
	  }
	  //$sql = "SELECT * , unix_timestamp(deal_datetime_from) AS from_date , unix_timestamp(deal_datetime_to) AS to_date FROM products  ";
	  
	  
	  return $r->result();

	 }
	 
	public function getUserById( $user_id )
	{
		
		
	}

	// public function getUser()
	// { 
 // 	$m = $this->session->userdata("user");
	// 	if( $m['loggedIn']== true)
	// 	{
			
	// 		 $u = $m['user_id'] ;
	// 		 if($u)
	// 		 {
	// 				$p = $this->db->query("SELECT * FROM users WHERE id =  $u ");	
	// 				$p = $p->result_array()[0];
	// 				if( $p )
	// 				{
	// 					return array("status"=>"success" , "data" => $p );
					 
	// 				}	
						
					
	// 		 }
	// 		 else
	// 		{
	// 			$arr = array(
	// 						"loggedIn"=>false
	// 						);
	// 				$this->session->userdata("user" , $arr );
	// 				return array("status"=>"error" , "data"=>array("redirect"=>site_url()."/login"));
	// 				//echo "<script>window.location.replace('".site_url()."/?r=USER_NOT_FOUND');</script>"; 
	// 		}
			
			
	// 	}
 //       else{
	// 	   return array("status"=>"error" , "data"=>array("redirect"=>site_url()."/login"));
		   
	//    }		
	// }
	

}