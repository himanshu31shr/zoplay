<?php
Class CommonModel extends CI_Model
{

	public function Categories($id,$name = null) {
        $sql="SELECT category_id, parent_id, category_status ,".($name != null ? 'CONCAT("'.$name.' > "  , category_name) as name' : 'category_name as name' ) ." From categories 
			WHERE parent_id ={$id} ";
		$query=$this->db->query($sql);		
		if($query->num_rows()>0){
			 $results=array();
			foreach ($query->result_array() as $row)
			{
			  $results[]=$row;
			  if ($sub_categories=$this->Categories($row['category_id'],$row['name']))
				$results=array_merge($results,$sub_categories);
			}			
			return $results;			
		}else{
			return false;
		}
    }
	public function jsCategories($id,$data = null) {
		
        $sql="
			SELECT category_id, parent_id, category_status ,category_name From categories 
			WHERE parent_id ={$id} ";
			
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			$results=array();
			foreach ($query->result_array() as $row)
			{
				$row['data']=$this->jsCategories($row['category_id'],$results);
				$results[]=$row;
			}			
			return $results;
			
		}else{
			return false;
		}
		
    }
	public function addCategory() {
		$category_name = $this->input->post('category_name');
		$parent_id = $this->input->post('parent_id');
		$return_array = array();
		if(empty($category_name) || empty($parent_id)){
			$return_array['status']= false;
			$return_array['message']= 'Required field empty';
		}else{
			$this->db->insert('categories',array('category_name'=>$category_name,'parent_id'=>$parent_id));
			$return_array['status']= true;
			$return_array['message']= 'Category added successfully';
		}
		return $return_array;
			
    }	
	public function insert_data($tablename,$data, $return_last_id = null)
	{
		$this->db->insert($tablename,$data);
		
		if($return_last_id != null){
			return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id();
		}

		return ($this->db->affected_rows() != 1) ? false : true;
	}		
	public function select_data_where($table,$where, $orderby = null)
	{	
		if($orderby!=null)
		{
		$this->db->order_by($orderby, 'ASC');
		}
		$query=$this->db->get_where($table , $where);
		return $query;
	}
	
	public function select_order_data($user_id)
	{	
		$this->db->select('*');
		$this->db->from('orders');
		$this->db->where(array('user_id' => $user_id));
		$query=$this->db->get();
		return $query;
	}
	
	public function select_all($table)
	{
		$query = $this->db->get($table);
		return $query;
	}
	public function getShippingLocations()
	{
		$this->db->join('countries', 'shippinglocation.country_id = countries.id', 'inner'); 
		$query = $this->db->get('shippinglocation');
		return $query->result();
	}
	public function get_shops()
	{
		$this->db->select('users.full_name,shops.shop_name,shops.shop_id,shops.createtime,shops.status,');
		$this->db->join('users', 'shops.seller_id = users.id', 'left'); 
		$query = $this->db->get('shops');
		return $query->result();
	}
	
	public function delete_data($tablename,$data)
	{
		$this->db->delete($tablename, $data); 
		return ($this->db->affected_rows() != 1) ? false : true;
	}	
	
	public function update_data($tablename,$data, $where)
	{
		$this->db->update($tablename, $data, $where); 
		return ($this->db->affected_rows() != 1) ? false : true;
	}	
	/**
	*	Author: Himanshu
	*
	*	Check for password match
	*	@param string email, password, role 
	*	@return mixed
	*/
	public function doLogin($email, $password, $role)
	{
		$data = $this->db->select('id, role')
				->from('users')
				->where(array('email'=>$email, 'password'=>$password, 'role'=>$role))
				->get()->result();

		if ($data) {
			return $data[0];
		} else {
			return false;
		}
	}	
	
	
	public function check_admin_approval($email, $role){
		return $this->db->get_where('users', array('email'=>$email, 'role'=>$role, 'admin_approved_status'=>1))->result();
	}
	
	/**
	*	Author: Himanshu
	*
	*	Gets list of all sellers 
	*	@param array where (optional)
	*	@return mixed
	*/
	public function sellerList($where = array())
	{
		$this->db->from('users');
		if(!empty($where)){
			$this->db->where($where);
		}
		$this->db->order_by('user_created', 'asc');
		$data = $this->db->get()->result();
		
		if($data){
			return $data;
		}else{
			return null;
		}
	}
	 public function get_record($tablename,$where,$orderby=false)
		{
			if(!empty($where)){
				$this->db->where($where);
			}
			if(!empty($orderby) ){
				if(is_array($orderby)){
					foreach($orderby as $k=>$v){
						$this->db->order_by($k, $v);
					}
				}else{
					$this->db->order_by($orderby);
				}
				
			}
			 $query = $this->db->get($tablename);
			if($query && $query->num_rows() > 0)
			{
				return  $query->result();
			}
		 }
	public function get_single_record($tablename,$where)
		{
			if(!empty($where)){
				$this->db->where($where);
			}
			if(!empty($orderby) ){
				if(is_array($orderby)){
					foreach($orderby as $k=>$v){
						$this->db->order_by($k, $v);
					}
				}else{
					$this->db->order_by($orderby);
				}
			}
			$query = $this->db->get($tablename);
			if($query && $query->num_rows() > 0)
			{
				return  $query->row();
			}
		}
	public function sellerDetails($where = array()){
		$this->db->from('users');
		if(!empty($where)){
			$this->db->where($where);
		}
		$data = $this->db->get()->result();
		
		if($data){
			return $data[0];
		}else{
			return null;
		}
	}
	
	public function getCategories( $cat = null ){
		$cat = strtolower( $cat );
		   $sql="SELECT category_id,  category_name  From categories 
			WHERE LOWER(category_name) LIKE '%{$cat}%' ";
			
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			$return = $query->result_array();
			
				$i = 0;
			foreach($return as $cat){
				$cats[$i] = array(
					'id'	=>	$cat['category_id'],
					'text'	=>	$cat['category_name']
				);
				$i++;
			}
			return array("results" => $cats);
				
		}else{
			return false;
		}
	}
	
	
	/* public function getCategory( $category_id ){
		$sql = " SELECT category_name , category_id FROM ";
	} */
    public function getPackageList(){
        
        $keywords='';
        
        $aColumns = array('feature_package_id','package_name','package_amount','package_days','feature_package_added','package_status');

         $sTable = 'feature_packages';
         $iDisplayStart = $this->input->get_post('start', true);
         $iDisplayLength = $this->input->get_post('length', true);
         $columns = $this->input->get_post('columns', true);
         $order = $this->input->get_post('order', true);
         $search = $this->input->get_post('search', true);
         $sEcho = $this->input->get_post('draw', true);
         // Paging
         if(isset($iDisplayStart) && $iDisplayLength != '-1')
         {
          $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
         }

         // Ordering
         if(isset($order[0]))
         {
          $this->db->order_by($aColumns[$order[0]['column']], $order[0]['dir']);
         }
         //Filtering
         if(isset($search['value']) && !empty($search['value']))
         {
          for($i=0; $i<count($columns); $i++)
          {
           $bSearchable = $columns[$i]['searchable'];
           // Individual column filtering
           if(isset($bSearchable) && $bSearchable == 'true')
           {
            $this->db->or_like($aColumns[$i], $this->db->escape_like_str($search['value']));
           }
          }
         }
         // Select Data
         $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
         $rResult = $this->db->get($sTable);

         // Data set length after filtering
         $this->db->select('FOUND_ROWS() AS found_rows');
         $iFilteredTotal = $this->db->get()->row()->found_rows;

         // Total data set length
         $iTotal = $this->db->count_all($sTable);

         // Output
         $output = array(
          'draw' => intval($sEcho),
          'recordsTotal' => $iTotal,
          'recordsFiltered' => $iFilteredTotal,
          'data' => array()
         );

            foreach($rResult->result_array() as $aRow)
            {
                  $row = array();
                foreach($aColumns as $col)
                {
                    if($col== 'package_status') {
                          if($aRow[$col]==1) {
                              $row[] = "<span class='label label-primary'>Active</span>";
                          } else {
                               $row[] = "<span class='label label-danger'>Inactive</span>";
                         }   
                     }else if($col== 'feature_package_id'){
                         $feature_package_id= $aRow[$col];
                      }else {
                           $row[] = $aRow[$col];
                      }
                }
                $row[] ="<div class='btn-group'>										
                      <a href=".base_url()."admin/viwpackage/".base64_encode($feature_package_id)." title='View'>
                          <button class='btn btn-primary'><i class='fa fa-eye'></i></button>
                      </a>										
                     </div>";
                  $output['data'][] = $row;
            }
              echo json_encode($output);
              die;
        
//        $this->db->select('*');    
//        $this->db->from('feature_packages');
//        $this->db->where("package_name LIKE '%$keywords%' OR package_amount LIKE '%$keywords%' OR package_days LIKE '%$keywords%'");
//        $this->db->where(array('package_status !=' =>2));
//        $query = $this->db->get();
//        $rowcount = $query->num_rows();
//        $list=  $query->result();
//        $data = array();
//        $table_row_count = $this->db->count_all('feature_packages');
//       
//        foreach ($list as $packages) {
//            $row = array();
//            $row[] = $packages->package_name;
//            $row[] = $packages->package_days;
//            $row[] = $packages->package_amount;
//            $row[] = date('d-m-Y',strtotime($packages->feature_package_added));
//            if($packages->package_status) {
//                $str="<span class='label label-primary'>Active</span>";
//              } else {
//                $str="<span class='label label-danger'>Inactive</span>";
//             } 
//            $row[] = $str;
//             $row[] ="<div class='btn-group'>										
//                        <a href='.site_url().'admin/viewUser'.base64_encode($packages->feature_package_id)' title='View'>
//                            <button class='btn btn-primary'><i class='fa fa-eye'></i></button>
//                        </a>										
//                    </div>";
//            $data[] = $row;
//        }
//        $output = array("draw" => $_POST['draw'],
//                        "recordsTotal" => $table_row_count,
//                        "recordsFiltered" => $rowcount,
//                        "data" => $data,
//                    );
//        echo json_encode($output);
      //  die;
    }
    
   function getAll($table,$where){
		$return = $this->db->get_where($table,$where);
		if($return->num_rows()){
			return $return->result();
		}else{
			return false;
		} 
      }
        
   
    
        
	
}

?>