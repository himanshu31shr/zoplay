<?php

Class BillingModel extends CI_Model
{	
	public function get_dropdowncart_data($limit=null)
	{
		$cart_check = $this->cart->contents();
		$grand_total = 0;
		$countproducts = 0;
		$html = '';
		foreach ($cart_check as $item){
			$cartproduct_id = $item['id'];
			$productdetail=$this->Product_detail($cartproduct_id);

			//$pname=$pname=substr($productdetail['product_name'],0,10).'...';
			//$pprice=$productdetail['product_price'];
			//$pimage=$productdetail['product_image'];
			//$pid=$productdetail['product_id'];
			$pname=$item['name'];
			$pprice=$item['price'];
			
			if($item['image'] == ''){
				$pimage = base_url().'files/default.png';
			  }else if(substr($item['image'], 0 , 4) == 'http'){
				$pimage = $item['image'];
			  }else{
				$pimage = base_url().'files/'.$item['image'];
			  }
			
			//$pimage=$item['image'];
			$pid=$item['id'];
			$pqty=$item['qty'];
			$url_prod_id = site_url()."product/".md5($pid);

			$html .= '<div id="'.$item['rowid'].'" class="forcartrow cart-item product-summary">
				<div class="row">
					<div class="col-xs-4">
						<div class="image">
							<a href="'.$url_prod_id .'"><img src="'.$pimage.'" alt="" style="width:55px;height:70px;"></a>
						</div>
						
					</div>
					<div class="col-xs-7">
						
						<h3 class="name"><a href="'.$url_prod_id .'">'.$pname.'</a></h3>
						<div class="price">$'.number_format($pprice,2).' <span>x '.$item['qty'].'</span></div>
						<div class="price">Total : $'.number_format($item['subtotal'],2).'</div>
						
					</div>
					<div class="col-xs-1 action">
						
						<a data-id="'. $item['rowid'].'" data-cartid="'. $item['rowid'].'" title="remove" class="removeFromcart"><i class="fa fa-trash"></i></a>
					</div>
				</div>
			</div>';
			$itemincart=$countproducts++;
			if($itemincart==$limit)
			{
				break;
			}
		} ;
		return $html;
	}
	
	public function get_cartpagedata()
	{
		$cart_check = $this->cart->contents();
		
			$grand_total = 0;
			$countproducts = 0;
			$html = '';
			
			foreach ($cart_check as $item){
				$rowid = $item['rowid'];
				$cartproduct_id = $item['id'];
				$productdetail=$this->Product_detail($cartproduct_id);
	
				//$pname=$pname=substr($productdetail['product_name'],0,10).'...';
				//$pprice=$productdetail['product_price'];
				//$pimage=$productdetail['product_image'];
				//$pid=$productdetail['product_id'];
				$pname=$item['name'];
				$pprice=$item['price'];
				
				if($item['image'] == ''){
					$pimage = base_url().'files/default.png';
				  }else if(substr($item['image'], 0 , 4) == 'http'){
					$pimage = $item['image'];
				  }else{
					$pimage = base_url().'files/'.$item['image'];
				  }
				//$pimage=$item['image'];
				$pid=$item['id'];
				$pqty=$item['qty'];
				$url_prod_id = site_url()."product/".md5($pid);
				
				$optionvalue = '';
			
				 if(!empty($item['options'])){ 

				   foreach ($this->cart->product_options($item['rowid']) as $option_name => $option_value){ 

						$optionvalue .= '<div class=""><span class="label label-custom1">'. $option_name.': <span>'. $option_value.'<span></span></div>';

				   } 

				 }
				
				$html .= '<tr id="cartrow_'.$item['rowid'].'" class="style1">
				<form id="checkout_data">
					<td class="col-sm-8 col-md-6">
					<div class="media">
						<a class="thumbnail pull-left" href="'.$url_prod_id.'"> <img class="media-object" src="'. $pimage.'" style="width: 72px; height: 72px;"> </a>
						<div class="media-body">
							<h4 class="media-heading"><a href="'.$url_prod_id.'">'. $pname.'</a></h4>
							<p>'.$optionvalue.'</p>
							
						</div>
					</div></td>
					
					<td class="cart-product-quantity">
						<div class="quant-input">
				                <input type="number" class="form-control text-center" id="qty_'.$rowid.'" name="cart_quantity" value="'. $pqty.'" style="padding:5px;">
								<input type="hidden" id="price_'.$rowid.'" value="'. $pprice.'">
								<input type="hidden" id="pid_'.$rowid.'" value="'. $pid.'">
			              </div>
		            </td>
					<td class="cart-product-sub-total"><span class="cart-sub-total-price">$'. number_format($pprice,2).'</span></td>
					<td class="cart-product-grand-total"><span class="cart-grand-total-price" id="'.$rowid.'_updatedprice">$'. number_format($item['subtotal'],2).'</span></td>
					<td class="romove-item"><button data-id="'. $item['rowid'].'" data-cartid="'. $item['rowid'].'" title="remove" class="removeFromcart btn btn-danger btn-sm icon"><i class="fa fa-trash-o"></i></button></td>
					<td class="cart-product-edit"><button class="updateFromcart product-edit btn btn-success btn-sm icon" data-cartid="'. $item['rowid'].'" title="Update">Update</button></td>
				</form>
				</tr>
				'; 
				
			} ;
		
		return $html;
	}
	
	public function get_productlist()
	{
		$this->db->select('*');
		$this->db->from('products'); 
		$this->db->order_by("product_id", "desc");
		$this->db->limit('10');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function get_allProduct()
	{
		$this->db->select('* ,a.product_quantity as cartpquantity');
		$this->db->from('carts a'); 
		$this->db->join('users b', 'b.id=a.user_id', 'left');
		$this->db->join('products c', 'c.product_id=a.product_id', 'left');
		$this->db->order_by("a.cart_id", "desc");
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function Product_detail($product_id)
	{
		$sql = "SELECT * FROM products WHERE product_id = $product_id";	
		$result = $this->db->query($sql);
		
		$res = $result->result_array() ;
		if($res)
		{
			return $res[0];
			
		}
		else{
			return 0;
		}
		
	}
    
    // Insert customer details in "customer" table in database.
	public function insert_customer($data)
	{
		$this->db->insert('customers', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;		
	}
	
        // Insert order date with customer id in "orders" table in database.
	public function insert_order($data)
	{
		$this->db->insert('orders', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}
	
        // Insert ordered product detail in "order_detail" table in database.
	public function insert_order_detail($data)
	{
		$this->db->insert('order_detail', $data);
	}
	
	//For payment(Paypal)
	public function getRows($id = ''){
        $this->db->select('product_id,product_name,product_image,product_price');
        $this->db->from('products');
        if($id){
            $this->db->where('product_id',$id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('product_name','asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }
    //insert transaction data
    public function insertTransaction($data = array()){
        $insert = $this->db->insert('transactions',$data);
        return $insert?true:false;
    }
	//For payment(Paypal)

}