<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'language', 'url'));
        $this->load->model('BillingModel');
        $this->load->model('InformationModel');
		$this->load->model('CommonModel');
        $this->load->library('cart');
        $this->load->library('curl');
		$this->load->library('paypal_lib');

        if (isset($_COOKIE['language'])) {
            $this->lang->load($_COOKIE['language'] . "_landing", $_COOKIE['language']);
        } else {
            $this->lang->load('english_landing', 'english');
        }
    }
	
	//protected $user_id = 7;
	
    public function index_get() 
	{
		
		$data = array();
        $seo = array();
        
		$data['allcartdata']	= $this->get_cartpagedata();
        $seo['url'] = site_url("Information");
        $seo['title'] = lang('logintext') . " - " . WEBSITENAME;
        $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
        $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
        $data['data']['seo'] = $seo;
		$data['layout'] = $this->frontLayout2($data);
        $data['layout'] = $this->frontLayout($data);
		$data['products'] = $this->BillingModel->get_allProduct();
		$this->load->view("front-2/shopping_cart.tpl", $data);
	}
	
	public function place_order_get()
	{
		$currentUrl = str_replace(site_url(),'',current_url());
		$userinfo = $this->session->userdata("user");
		
		if(isset($userinfo)) 
		{ 
			$user_id = $this->is_logged_in($currentUrl)['user_id'];	 
			//userdata
			$user_data = $this->CommonModel->select_data_where('users', array('id' => $user_id, 'role'=>'user'))->result();
			$data['userdata'] = $user_data[0];
			$data['allcartdata'] = $this->get_cartpagedata();
			$addresses = $this->get_address_post($user_id);
			$data['useraddress'] = $addresses['html'];
		}
		$data['payment_gateways'] = $this->CommonModel->select_all('payment_gateways')->result();
		$data['cartdetail'] = $this->BillingModel->get_dropdowncart_data();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/checkout.tpl", $data);
	}
	public function checkoutaddress_post() 
	{			
		$currentUrl = str_replace(site_url(),'',current_url());
		$user_id = $this->is_logged_in($currentUrl)['user_id'];	
		
	   $this->form_validation->set_rules('orderedusername', 'Username', 'trim|required');
       $this->form_validation->set_rules('orederedphone','Phone Number','required|numeric|min_length[10]|max_length[10]');
       $this->form_validation->set_rules('orderedaddrespincode','Pincode','required|numeric');
       $this->form_validation->set_rules('orderedaddress1','Address','required');
       $this->form_validation->set_rules('orderedcity','City','required');
       $this->form_validation->set_rules('orderedstate','State','required');

       if($this->form_validation->run() == false){
			$errors = array(
				'orderedusername'	=> form_error('orderedusername', '<p class="text text-danger error">','</p>'),
				'orederedphone'	=> form_error('orederedphone', '<p class="text text-danger error">','</p>'),
				'orderedaddrespincode'	=> form_error('orderedaddrespincode', '<p class="text text-danger error">','</p>'),
				'orderedaddress1'	=> form_error('orderedaddress1', '<p class="text text-danger error">','</p>'),
				'orderedcity'	=> form_error('orderedcity', '<p class="text text-danger error">','</p>'),
				'orderedstate'	=> form_error('orderedstate', '<p class="text text-danger error">','</p>')
			);
			
			
			$cat = array(
				'status'	=> 	false,
				'message'	=>	'<i class="fa fa-times"></i> Validation Errors',
				'data'		=>	$errors
			);
			
			
			$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
		}else{	
			//$salt	= $this->store_salt ? $this->salt() : false;
			$username=$this->input->post('orderedusername');
			$phone=$this->input->post('orederedphone');
			$pincode=$this->input->post('orderedaddrespincode');
			$address1=$this->input->post('orderedaddress1');
			$address2=$this->input->post('orderedaddress2');
			$city=$this->input->post('orderedcity');
			$country=$this->input->post('orderedcountry');
			$state=$this->input->post('orderedstate');
			
			$country_data = $this->CommonModel->select_data_where('countries', array('iso' => $country))->result();
			$country_id = $country_data[0]->id;
			
			$insert = array(
						'address_user_id'	=> $user_id,
						'address_name'	=> $username,
						'address_line_1'	=> $address1,
						'address_line_2'	=> $address2,
						'address_city'	=> $city,
						'address_state'	=> $state,
						'address_zip'	=> $pincode,
						'address_country'	=> $country_id,
						'address_phone'	=> $phone,
						'address_create_time'	=> date("Y-m-d H:i:s")
					);
					$this->db->insert('user_addresses', $insert);
					
					if($this->db->affected_rows() > 0){
						$addressid = $this->db->insert_id();
						
						$addresses = $this->get_address_post($user_id);
						$html = $addresses['html'];
						$data = $addressid;
						
						$alerts = array(
							'status'	=> 	true,
							'message'	=>	'<i class="fa fa-check"></i> Save Address Successfully',
							'data'		=>	$data,
							'html' => $html
						);
					$cat = $alerts;
					$this->response($cat, parent::HTTP_OK);	
						
					}else{
						$alerts = array(
							'status'	=> 	false,
							'message'	=>	'<i class="fa fa-times"></i> Something went wrong.Please Try Again!',
							'data'		=>	''
						);
					$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
					}
		}       
    }
	
	public function deliveryaddress_post()
	{
		$currentUrl = str_replace(site_url(),'',current_url());
		$user_id = $this->is_logged_in($currentUrl)['user_id'];	
		$addid=$this->input->post('addid');
		if($addid=='')
		{
			$errors = array(
				'dlvraddressid'	=> '<i class="fa fa-times"></i> Please Select or enter your valid address - Your order will be delivered here'
			);
			
			$cat = array(
				'status'	=> 	false,
				'message'	=>	'<i class="fa fa-times"></i> Validation Errors',
				'data'		=>	$errors
			);
		
			$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
		}
		else
		{
			/* if($cart = $this->cart->contents())
			{
				foreach ($cart as $item){
					$cart_detail = array(
						'product_id'	=> $item['id'],
						'product_quantity' 	=> $item['qty'],
						'product_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'deliver_addres_id' => str_replace('prod_','',$addid)
					);	
				$this->db->insert('carts', $cart_detail);
				
				}
				$alerts = array(
							'status'	=> 	true,
							'message'	=>	'<i class="fa fa-check"></i> Save Successfull',
							'data'		=>	''
						);	
				$this->response($alerts, parent::HTTP_OK);
			} */
				$alerts = array(
						'status'	=> 	true,
						'message'	=>	'<i class="fa fa-check"></i> Save Successfull',
						'data'		=>	''
					);	
				$this->response($alerts, parent::HTTP_OK);
		}
	}
	public function test_get() 
	{
		$data = array();
        $seo = array();
        
		$data['allcartdata']	= $this->get_cartpagedata();
        $seo['url'] = site_url("Information");
        $seo['title'] = lang('logintext') . " - " . WEBSITENAME;
        $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
        $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
        $data['data']['seo'] = $seo;
		$data['layout'] = $this->frontLayout2($data);
        $data['layout'] = $this->frontLayout($data);
		$data['allproducts'] = $this->BillingModel->get_productlist();    	
		$this->load->view('front-2/product_view.tpl', $data);
	}
	
	public function addtocart_post() 
	{
		$data = array();
        $seo = array();
        
        $seo['url'] = site_url("Information");
        $seo['title'] = lang('logintext') . " - " . WEBSITENAME;
        $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
        $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
        $data['data']['seo'] = $seo;
		$data['layout'] = $this->frontLayout2($data);
        $data['layout'] = $this->frontLayout($data);
		$arr='';
		foreach($this->input->post() as $key => $value)
		{
			if($key == 'all_vars' || $key == 'inputquantity' || $key == 'cart_productid' || $key == 'variationtype'){
				
			}else{
				$arr[$key] = $value;				
			}	
		}
		if($arr!=''){$arr2 = $arr;} else { $arr2 = '';}
		//print_r(serialize($arr));
		//print_r(unserialize(serialize($arr)));die;
		$pid = $this->input->post('cart_productid');
		$inputyquantity = $this->input->post('inputquantity');
		//$all_vars = $this->input->post('all_vars');
		
		$product_data = $this->CommonModel->select_data_where('products', array('product_id' => $pid))->result();
		$product_name = substr($product_data[0]->product_name,0,40);
		$product_image = $product_data[0]->product_image;
		$product_price = str_replace('$','',$product_data[0]->product_price)-str_replace('$','',$product_data[0]->product_discount);
		$product_quantity = $product_data[0]->product_quantity;
		$cart = array(
			'id'      => $pid,
			'qty'     => $inputyquantity,
			'price'   => $product_price,
			'name'    => $product_name,
			'image'   => $product_image,
			'options' => $arr2
		);
		
		if($product_quantity==0)
			{
				$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-times"></i> Product is out of stock!',
					'data'		=>	''
				);
				return $this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
			}
		if($inputyquantity>$product_quantity)
			{
				$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-times"></i> Product Quantity is out of stock!',
					'data'		=>	''
				);
				return $this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
			}
				//to insert special characters in cart
				$this->cart->product_name_rules = '[:print:]';
				if($this->cart->insert($cart))
				{
					$cart_check = $this->cart->contents();
					if(empty($cart_check)) {
						$html = '<div id="text">To add products to your shopping cart click on "Add to Cart" Button</div>' ;
						
						
					}else{
						$totalamount = $this->cart->total();
						$rows = count($this->cart->contents());
						
						$html=$this->BillingModel->get_dropdowncart_data(2);
						
						$cartadvdata = array(
							'myproduct_count'	=> 	$rows,
							'mygrand_total'	=>	$totalamount
						);
					} 
					 
					
					$message = array(
							'status'	=> 	true,
							'message'	=>	'<i class="fa fa-check"></i> Add Successfully',
							'data'		=>	$cartadvdata,
							'html'		=>	$html
						);
						
					$this->response($message, parent::HTTP_OK);
				}
				else
				{
					$message = array(
							'status'	=> 	false,
							'message'	=>	'<i class="fa fa-times"></i> Something went wrong.Please Try Again!',
							'data'		=>	''
						);
						
					$this->response($message, parent::HTTP_INTERNAL_SERVER_ERROR);
				}
	      	
	} 
			
    public function deletecartdata_post() 
	{			
		$cart_listid=$this->input->post('cart_listid');
							
			$data = array(
				'rowid'   => $cart_listid,
				'qty'     => 0
			);
			
			$q = $this->cart->update($data);
			
			if($q){
				
				$totalamount = $this->cart->total();
				$rows = count($this->cart->contents());
										
				$cartadvdata = array(
				'myproduct_count'	=> 	$rows,
				'mygrand_total'	=>	$totalamount,
				'mycartrow_id'	=>	$cart_listid
				);
				
				$alerts = array(
					'status'	=> 	true,
					'message'	=>	'<i class="fa fa-check"></i> Remove Success From Cart',
					'data'		=>	$cartadvdata
				);
				
				$this->response($alerts, parent::HTTP_OK);
			}else{
				$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-times"></i> Something went wrong.Please Try Again!',
					'data'		=>	''
				);
				$this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
			}				
    }
	public function updatecartdata_post() 
	{			
		$cart_listid=$this->input->post('cart_listid');
		$pid = $this->input->post('pid');
		$cart_quantity=$this->input->post('quantity');
		
		$product_data = $this->CommonModel->select_data_where('products', array('product_id' => $pid))->result();
		$product_name = substr($product_data[0]->product_name,0,40);
		$product_image = $product_data[0]->product_image;
		$product_price = str_replace('$','',$product_data[0]->product_price)-str_replace('$','',$product_data[0]->product_discount);
		$product_quantity = $product_data[0]->product_quantity;
		
		if($product_quantity==0)
			{
				$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-times"></i> Product is out of stock!',
					'data'		=>	''
				);
				return $this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
			}
		if($cart_quantity>$product_quantity)
			{
				$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-times"></i> Product Quantity is out of stock!',
					'data'		=>	''
				);
				return $this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
			}
		
		$updatedprice='';
		if($cart_quantity!=0)
		{
		$subt = $cart_quantity*$product_price;
		$updatedprice .= '$'. number_format($subt,2).'';
		}
			$data = array(
				'rowid'   => $cart_listid,
				'qty'     => $cart_quantity
			);
						
			if($this->cart->update($data))
			{
				$cart_check = $this->cart->contents();
				if(empty($cart_check)) {
						$html = '<div id="text">To add products to your shopping cart click on "Add to Cart" Button</div>' ;
						$rows = 0;
						$totalamount = 0;
					}else{
						$totalamount = $this->cart->total();
						$rows = count($this->cart->contents());
						
						$html=$this->BillingModel->get_dropdowncart_data(2);
						
						$cartadvdata = array(
						'myproduct_count'	=> 	$rows,
						'mygrand_total'	=>	$totalamount,
						'updatedprice' => $updatedprice
						);
					} 
				 
				$message = array(
						'status'	=> 	true,
						'message'	=>	'<i class="fa fa-check"></i> Update Successfully',
						'data'		=>	$cartadvdata,
						'html'		=>	$html
					);
					
				$this->response($message, parent::HTTP_OK);
			}   
			else{
				$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-times"></i> Something went wrong.Please Try Again!',
					'data'		=>	''
				);
				$this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
			}				
    }
		
	public function removeallcart_post() {
		
        $rowid = $this->input->post('deletecart');
		if ($rowid=="all"){
			$this->cart->destroy();
			
			$html = '<div id="text">To add products to your shopping cart click on "Add to Cart" Button</div>' ;
			$alerts = array(
					'status'	=> 	true,
					'message'	=>	'<i class="fa fa-check"></i> Delete successfully',
					'data'		=>	'',
					'html'		=>	$html
				);
				
			$this->response($alerts, parent::HTTP_OK);
			
		}else{
			
			$message = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-times"></i> Something went wrong.Please Try Again!',
					'data'		=>	''
				);
				
			$this->response($message, parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	/* public function get_dropdowncart_data()
	{
		$cart_check = $this->cart->contents();
		$grand_total = 0;
		$countproducts = 0;
		$html = '';
		foreach ($cart_check as $item){
			$cartproduct_id = $item['id'];
			$productdetail=$this->BillingModel->Product_detail($cartproduct_id);

			//$pname=$pname=substr($productdetail['product_name'],0,10).'...';
			//$pprice=$productdetail['product_price'];
			//$pimage=$productdetail['product_image'];
			//$pid=$productdetail['product_id'];
			$pname=$item['name'];
			$pprice=$item['price'];
			$pimage=$item['image'];
			$pid=$item['id'];
			$pqty=$item['qty'];
			$url_prod_id = site_url()."product/".md5($pid);

			$html .= '<div id="'.$item['rowid'].'" class="forcartrow cart-item product-summary">
				<div class="row">
					<div class="col-xs-4">
						<div class="image">
							<a href="'.$url_prod_id .'"><img src="'.$pimage.'" alt="" style="width:55px;height:70px;"></a>
						</div>
						
					</div>
					<div class="col-xs-7">
						
						<h3 class="name"><a href="'.$url_prod_id .'">'.$pname.'</a></h3>
						<div class="price">$'.number_format($pprice,2).' <span>x '.$item['qty'].'</span></div>
						<div class="price">Total : $'.number_format($item['subtotal'],2).'</div>
						<div calss="hide">'.$grand_total = $grand_total + $item['subtotal'].'</div>
					</div>
					<div class="col-xs-1 action">
						
						<a data-id="'. $item['rowid'].'" data-cartid="'. $item['rowid'].'" title="remove" class="removeFromcart"><i class="fa fa-trash"></i></a>
					</div>
				</div>
			</div>';
			$itemincart=$countproducts++;
			if($itemincart==3)
			{
				break;
			}
		} ;
		return $html;
	} */
	public function get_cartpagedata()
	{
		$cart_check = $this->cart->contents();
		if(empty($cart_check)) {
			$html = '<tr><td colspan="7">
				<div id="text" class="text-center text-danger">
					<h4>Cart Empty! To add products to your shopping cart click on Add to Cart Button
					</h4>
				</div>
				</td></tr>' ;
			$rows = 0;
			$totalamount = 0;
		}else{
			$totalamount = $this->cart->total();
			$rows = count($this->cart->contents());
			
			$html = $this->BillingModel->get_cartpagedata();
		}
		//echo $html;die;
		$mycart_alldata = array(
			'myproduct_count'	=> 	$rows,
			'mygrand_total'	=>	$totalamount,
			'mycart_products'	=>	$html
		);
		return $mycart_alldata;
	}
	public function capatcha_verify_post()
	{
		$currentUrl = str_replace(site_url(),'',current_url());
		$user_id = $this->is_logged_in($currentUrl)['user_id'];	

		$recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
  
		$userIp=$this->input->ip_address();

		$secret='6LcLyhMUAAAAAM74iKscuHmSNTwgq-ZqTnWUQFqy';

		$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success){
			
			$alerts = array(
						'status'	=> 	true,
						'message'	=>	'<i class="fa fa-check"></i> Success',
						'data'		=>	''
					);
	
			$this->response($alerts, parent::HTTP_OK);
			}else{	
			$alerts = array(
						'status'	=> 	false,
						'message'	=>	'<i class="fa fa-check"></i> Sorry Google Recaptcha Unsuccessful!!',
						'data'		=>	''
					);
			$this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
			}
	}
	public function cash_on_delivery_post()
	{
		$currentUrl = str_replace(site_url(),'',current_url());
		$user_id = $this->is_logged_in($currentUrl)['user_id'];	
		
		//$this->form_validation->set_rules('cashtamount', 'Amount', 'trim|required');
        
      
		$addid = trim($this->input->post('cashaddressid'));			
		if($cart = $this->cart->contents())
			{
				$order_number = random_string('alnum',6);
				$totalamount = $this->cart->total();
				$cartgroup_id = MD5(date("Y-m-d H:i:s"));
				
				//Set variables for paypal form
				$returnURL = base_url().'Checkout/success'; //payment success url
				$cancelURL = base_url().'Checkout/cancel'; //payment cancel url
				$notifyURL = base_url().'Checkout/ipn'; //ipn url
				//get particular product data
				
				$userID = 1; //current user id
				$logo = base_url().'assets/frontend/images/logo.png';
				
				$this->paypal_lib->add_field('return', $returnURL);
				$this->paypal_lib->add_field('cancel_return', $cancelURL);
				$this->paypal_lib->add_field('notify_url', $notifyURL);
				$this->paypal_lib->add_field('item_name', 'Test product');
				$this->paypal_lib->add_field('custom', $userID);
				$this->paypal_lib->add_field('item_number',  'test_id');
				$this->paypal_lib->add_field('amount', $totalamount);		
				$this->paypal_lib->image($logo);
				
				$this->paypal_lib->paypal_auto_form();
				
				$alerts = array(
							'status'	=> 	true,
							'message'	=>	'<i class="fa fa-check"></i> Order Successfull',
							'data'		=>	''
						);
				
				$this->response($alerts, parent::HTTP_OK);
				
				/* $order = array(
						'total_price' => $totalamount,
						'user_id'	=> $user_id,
						'payment_type'	=> 0,
						'order_payment_status' => 0,
						'cart_group_id'	=> $cartgroup_id,
						'order_address_id' => str_replace('prod_','',$addid),
						'order_number' => $order_number
					);	
				$bq = $this->db->insert('orders', $order);
				$order_id = $this->db->insert_id();
				if(!empty($order_id))
				{
				foreach ($cart as $item){
					$cart_detail = array(
						'product_id'	=> $item['id'],
						'product_quantity' 	=> $item['qty'],
						'product_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'cart_group_id'	=> $cartgroup_id,
						'deliver_addres_id' => str_replace('prod_','',$addid)
					);
				$aq = $this->db->insert('carts', $cart_detail);
					
				$order_detail = array(
						'order_product_id'	=> $item['id'],
						'order_quantity' 	=> $item['qty'],
						'order_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'shipping_tax'	=> '',
						'shipping_charge' => '',
						'coupon_amount' => '',
						'coupon_id' => '',
						'order_id' => $order_id
					);
				$aq = $this->db->insert('order_detail', $order_detail);	
				
				}				
				$redirect_url = site_url('user/dashboard');
				$alerts = array(
							'status'	=> 	true,
							'message'	=>	'<i class="fa fa-check"></i> Order Successfull',
							'data'		=>	$redirect_url
						);
				$this->cart->destroy();
				
				$this->response($alerts, parent::HTTP_OK);
				}
				else{
					$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-check"></i> Something went wrong!!',
					'data'		=>	''
					);
					$this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
				} */
			}
			else{	
		$alerts = array(
					'status'	=> 	false,
					'message'	=>	'<i class="fa fa-check"></i> Something went wrong!!',
					'data'		=>	''
				);
		$this->response($alerts, parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	} 
	public function verifycoupon_post()
	{
		$coupon_code = htmlspecialchars(trim($this->input->post('checkcoupon_code')));
		
		$check_coupon = $this->CommonModel->select_data_where('coupons', array('coupon_code' => $coupon_code))->result();
		if($check_coupon)
		{
			$totalamount = $this->cart->total();
			$rows = count($this->cart->contents());
			
			$coupon_id = $check_coupon[0]->coupon_id;
			$coupon_code = $check_coupon[0]->coupon_code;
			$order_range = $check_coupon[0]->order_range;
			$coupon_type = $check_coupon[0]->coupon_type;
			$value = $check_coupon[0]->value;
			$coupon_from_datetime = strtotime($check_coupon[0]->coupon_from_datetime);
			$coupon_to_datetime = strtotime($check_coupon[0]->coupon_to_datetime);
			$current_time = strtotime(date("Y-m-d H:i:s"));
			$card_status = $check_coupon[0]->card_status;
			$coupon_status = $check_coupon[0]->coupon_status;
			
			if($card_status==0)
			{
				$response = array('status' => false, 'message' => '<i class="fa fa-times"></i> Your Coupon has been expired','data' => '');
				return $this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR);
			}
			if($coupon_status==0)
			{
				$response = array('status' => false, 'message' => '<i class="fa fa-times"></i> Your Coupon is not valid! please enter a valid code','data' => '');
				return $this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR);
			}
			if($current_time>$coupon_to_datetime)
			{
				$response = array('status' => false, 'message' => '<i class="fa fa-times"></i>Your Coupon has been expired', 'data' => '');
				return $this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR);
			}
			if($rows<2)
			{
				$response = array('status' => false, 'message' => '<i class="fa fa-times"></i> To apply coupon code please purchase atleast 2 items' ,'data' => '');
				return $this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR);
			}
				
				if($coupon_type==0)
				{
					$data = array('mygreatgrand_total' => $totalamount*$value/100,'mycoupon_discount' => $value, 'mycoupon_id' => $coupon_code);
				}elseif($coupon_type==1)
				{
					$data = array('mygreatgrand_total' => $totalamount-$value,'mycoupon_discount' => $value, 'mycoupon_id' => $coupon_code);
				}else
				{
					$response = array('status' => false, 'message' => '<i class="fa fa-times"></i> Something went wrong');
					return $this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR);
				}
				
				$response = array('status' => true, 'message' => '<i class="fa fa-check"></i> Your Coupon code has been accepted', 'data' => $data);
				$this->response($response,parent::HTTP_OK);
				
		}
		else
		{
			$response = array('status' => false, 'message' => '<i class="fa fa-times"></i> Please enter valid coupon code','data' => '');
			$this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	function buy_post(){
		$currentUrl = str_replace(site_url(),'',current_url());
		$user_id = $this->is_logged_in($currentUrl)['user_id'];
		
		$addid = trim($this->input->post('cashaddressid'));
		$paygttype = trim($this->input->post('paygtwtype'));
		$discntvalue = trim($this->input->post('coupondiscnt'));
		$couponid = trim($this->input->post('couponid'));
		
		if(empty($cart = $this->cart->contents()))
			{
				echo'<script>alert("Cart Have No Products. Please Add First");</script>';
				redirect('mycart', 'refresh');
				
				/* $response = array('status' => false, 'message' => '<i class="fa fa-times"></i> Cart Have No Products. Please Add First');
				return $this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR); */
			}

		if(empty($paygttype))
		{
			echo'<script>alert("Something went wrong");</script>';
			redirect('mycart', 'refresh');
				
			/* $response = array('status' => false, 'message' => '<i class="fa fa-times"></i> Something went wrong');
			return $this->response($response,parent::HTTP_INTERNAL_SERVER_ERROR); */
		}
		
		$order_number = "ORDS" . rand(10000,99999999);
		$subtotal = $this->cart->total();
		$totalamount = $subtotal - $discntvalue;
		$cartgroup_id = MD5(date("Y-m-d H:i:s"));
		
		switch (strtolower($paygttype)) {
			case 'cod':
				
				$order = array(
						'total_price' => $totalamount,
						'user_id'	=> $user_id,
						'payment_method'	=> $paygttype,
						'payment_type'	=> 0,
						'order_payment_status' => 0,
						'cart_group_id'	=> $cartgroup_id,
						'discount_coupon_id'	=> $couponid,
						'discount_coupon_amount'	=> $discntvalue,
						'order_address_id' => str_replace('prod_','',$addid),
						'order_number' => $order_number
					);	
				$oq = $this->db->insert('orders', $order);
				$order_id = $this->db->insert_id();
				if(!empty($order_id))
				{
				foreach ($cart as $item){
					
					$order_product_specification='';
					 if(!empty($item['options'])){
						 $order_product_specification = serialize($item['options']);
					 }
					$cart_detail = array(
						'product_id'	=> $item['id'],
						'product_quantity' 	=> $item['qty'],
						'product_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'cart_group_id'	=> $cartgroup_id,
						'cart_product_specification' => $order_product_specification,
						'deliver_addres_id' => str_replace('prod_','',$addid)
					);
				$cartq = $this->db->insert('carts', $cart_detail);
					
				$order_detail = array(
						'order_product_id'	=> $item['id'],
						'order_quantity' 	=> $item['qty'],
						'order_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'shipping_tax'	=> '',
						'shipping_charge' => '',
						'order_product_specification' => $order_product_specification,
						'order_id' => $order_id
					);
				$odq = $this->db->insert('order_detail', $order_detail);	
				
				}
				$transaction = array(
						'order_id' => $order_id,
						'user_id'	=> $user_id
					);	
				$tq = $this->db->insert('transactions', $transaction);
				
				$data['cod']='';
				$data['item_number'] = $order_id;
				$data['payment_amt'] = $totalamount;
				$data['layout'] = $this->frontLayout2($data);
				$data['layout'] = $this->frontLayout($data);
				$this->load->view('front-2/success.tpl', $data);
				}
				
				break;
				
			case 'paypal':
			
				$order = array(
						'total_price' => $totalamount,
						'user_id'	=> $user_id,
						'payment_method'	=> $paygttype,
						'payment_type'	=> 0,
						'order_payment_status' => 0,
						'cart_group_id'	=> $cartgroup_id,
						'discount_coupon_id'	=> $couponid,
						'discount_coupon_amount'	=> $discntvalue,
						'order_address_id' => str_replace('prod_','',$addid),
						'order_number' => $order_number
					);	
				$oq = $this->db->insert('orders', $order);
				$order_id = $this->db->insert_id();
				if(!empty($order_id))
				{
				foreach ($cart as $item){
					
					$order_product_specification='';
					 if(!empty($item['options'])){
						 $order_product_specification = serialize($item['options']);
					 }
					$cart_detail = array(
						'product_id'	=> $item['id'],
						'product_quantity' 	=> $item['qty'],
						'product_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'cart_group_id'	=> $cartgroup_id,
						'cart_product_specification' => $order_product_specification,
						'deliver_addres_id' => str_replace('prod_','',$addid)
					);
				$cartq = $this->db->insert('carts', $cart_detail);
					
				$order_detail = array(
						'order_product_id'	=> $item['id'],
						'order_quantity' 	=> $item['qty'],
						'order_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'shipping_tax'	=> '',
						'shipping_charge' => '',
						'order_product_specification' => $order_product_specification,
						'order_id' => $order_id
					);
				$odq = $this->db->insert('order_detail', $order_detail);	
				
				}
				$transaction = array(
						'order_id' => $order_id,
						'user_id'	=> $user_id
					);	
				$tq = $this->db->insert('transactions', $transaction);
				}
				
				//Set variables for paypal form
				$returnURL = site_url().'Checkout/success'; //payment success url
				$cancelURL = site_url().'Checkout/cancel'; //payment cancel url
				$notifyURL = site_url().'Checkout/ipn'; //ipn url
				//get particular product data
				
				$logo = base_url().'assets/frontend/images/logo.png';
				
				$this->paypal_lib->add_field('return', $returnURL);
				$this->paypal_lib->add_field('cancel_return', $cancelURL);
				$this->paypal_lib->add_field('notify_url', $notifyURL);
				$this->paypal_lib->add_field('item_name', 'MyCart Products');
				$this->paypal_lib->add_field('custom', $user_id);
				$this->paypal_lib->add_field('item_number', $order_id);
				$this->paypal_lib->add_field('amount', 1);		
				$this->paypal_lib->image($logo);
				
				$this->paypal_lib->paypal_auto_form();
				
				break;
			
			case 'paytm':
			
				$order = array(
						'total_price' => $totalamount,
						'user_id'	=> $user_id,
						'payment_method'	=> $paygttype,
						'payment_type'	=> 0,
						'order_payment_status' => 0,
						'cart_group_id'	=> $cartgroup_id,
						'discount_coupon_id'	=> $couponid,
						'discount_coupon_amount'	=> $discntvalue,
						'order_address_id' => str_replace('prod_','',$addid),
						'order_number' => $order_number
					);	
				$oq = $this->db->insert('orders', $order);
				$order_id = $this->db->insert_id();
				if(!empty($order_id))
				{
				foreach ($cart as $item){
					
					$order_product_specification='';
					 if(!empty($item['options'])){
						 $order_product_specification = serialize($item['options']);
					 }
					$cart_detail = array(
						'product_id'	=> $item['id'],
						'product_quantity' 	=> $item['qty'],
						'product_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'cart_group_id'	=> $cartgroup_id,
						'cart_product_specification' => $order_product_specification,
						'deliver_addres_id' => str_replace('prod_','',$addid)
					);
				$cartq = $this->db->insert('carts', $cart_detail);
					
				$order_detail = array(
						'order_product_id'	=> $item['id'],
						'order_quantity' 	=> $item['qty'],
						'order_total' => $item['subtotal'],
						'user_id'	=> $user_id,
						'shipping_tax'	=> '',
						'shipping_charge' => '',
						'order_product_specification' => $order_product_specification,
						'order_id' => $order_id
					);
				$odq = $this->db->insert('order_detail', $order_detail);	
				
				}
				$transaction = array(
						'order_id' => $order_id,
						'user_id'	=> $user_id
					);	
				$tq = $this->db->insert('transactions', $transaction);
				}
				
				$this->load->helper('paytm_helper');
				
				$checkSum = "";
				$paramList = array();

				$ORDER_ID = $order_id;
				$CUST_ID = 'CUST001';
				$INDUSTRY_TYPE_ID = 'Retail';
				$CHANNEL_ID = 'WEB';
				$TXN_AMOUNT = 1;

				// Create an array having all required parameters for creating checksum.
				$paramList["MID"] = PAYTM_MERCHANT_MID;
				$paramList["ORDER_ID"] = $ORDER_ID;
				$paramList["CUST_ID"] = $CUST_ID;
				$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
				$paramList["CHANNEL_ID"] = $CHANNEL_ID;
				$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
				$paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;

				/*
				$paramList["MSISDN"] = $MSISDN; //Mobile number of customer
				$paramList["EMAIL"] = $EMAIL; //Email ID of customer
				$paramList["VERIFIED_BY"] = "EMAIL"; //
				$paramList["IS_USER_VERIFIED"] = "YES"; //

				*/
				$data['paramList'] = $paramList;
				//Here checksum string will return by getChecksumFromArray() function.
				$data['PAYTM_TXN_URL'] = PAYTM_TXN_URL;
				$data['checkSum'] = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
				
				$data['layout'] = $this->frontLayout2($data);
				$data['layout'] = $this->frontLayout($data);
				$this->load->view("front-2/paytm_request.tpl", $data);
				
				break;
		}
	}
	function paytmresponse_post()
	{
		// following files need to be included
		$this->load->helper('paytm_helper');

		$paytmChecksum = "";
		$paramList = array();
		$isValidChecksum = "FALSE";

		$paramList = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

		//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
		$data['isValidChecksum'] = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.
		
		$paypalInfo = $_POST;
		//echo'<pre>';print_R($paypalInfo);die;
		$data['item_number'] = $paypalInfo['ORDERID']; 
		$data['txn_id'] = $paypalInfo["TXNID"];
		$data['payment_amt'] = $paypalInfo["TXNAMOUNT"];
		$data['currency_code'] = $paypalInfo["CURRENCY"];
		$data['status'] = $paypalInfo["RESPMSG"];
		$data['payment_status'] = $paypalInfo["STATUS"];
		
		if($data['isValidChecksum']==true)
		{
			$table='transactions';
			$transdata=array('txn_id' => $data['txn_id'],'total_price' => $data['payment_amt'],'transaction_status' => $data['payment_status'],'payment_status' => 1);
			$where=array('order_id' => $data['item_number']);
			$transaction = $this->CommonModel->update_data($table,$transdata,$where);
			//pass the transaction data to view
			$data['layout'] = $this->frontLayout2($data);
			$data['layout'] = $this->frontLayout($data);
			$this->load->view('front-2/success.tpl', $data);
		}
		else
		{
			//pass the transaction data to view
			$data['layout'] = $this->frontLayout2($data);
			$data['layout'] = $this->frontLayout($data);
			$this->load->view('front-2/cancel.tpl', $data);
		}
		
		/* $data['layout'] = $this->frontLayout2($data);
		$data['layout'] = $this->frontLayout($data);
		$this->load->view("front-2/paytm_response.tpl", $data); */
	}
	//For Payment(paypal)
	function success_post(){
	    //get the transaction data
		$paypalInfo = $this->input->post();
		 //echo'<pre>'; print_r($paypalInfo); die;
		$data['item_number'] = $paypalInfo['item_number']; 
		$data['txn_id'] = $paypalInfo["txn_id"];
		$data['payment_amt'] = $paypalInfo["payment_gross"];
		$data['currency_code'] = $paypalInfo["mc_currency"];
		$data['status'] = $paypalInfo["payment_status"];
		$data['payer_email'] = $paypalInfo["payer_email"];
		//$data['receiver_email'] = $paypalInfo["receiver_email"];
		$data['mc_fee'] = $paypalInfo["mc_fee"];
		$data['payment_status'] = $paypalInfo["payment_status"];
		
		$payment_verify = $this->ipn($paypalInfo);
		if($payment_verify==true)
		{
			$table='transactions';
			$transdata=array('txn_id' => $data['txn_id'],'total_price' => $data['payment_amt'],'payer_email' => $data['payer_email'],'mc_fee' => $data['mc_fee'],'transaction_status' => $data['payment_status'],'payment_status' => 1);
			$where=array('order_id' => $data['item_number']);
			$transaction = $this->CommonModel->update_data($table,$transdata,$where);
			//pass the transaction data to view
			$data['layout'] = $this->frontLayout2($data);
			$data['layout'] = $this->frontLayout($data);
			$this->load->view('front-2/success.tpl', $data);
		}
		else
		{
			//pass the transaction data to view
			$data['layout'] = $this->frontLayout2($data);
			$data['layout'] = $this->frontLayout($data);
			$this->load->view('front-2/cancel.tpl', $data);
		}
		
	 }
	 
	 function cancel_get(){
		$seo['url'] = site_url("Information");
        $seo['title'] = lang('logintext') . " - " . WEBSITENAME;
        $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
        $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
        $data['data']['seo'] = $seo;
		$data['layout'] = $this->frontLayout2($data);
        $data['layout'] = $this->frontLayout($data);
		$data['erroralert'];
        $this->load->view('front-2/cancel.tpl',$data);
	 }
	 
	 public function ipn($paypalInfo){
		//paypal return transaction details array
		$paypalInfo	= $this->input->post();

		$data['user_id'] = $paypalInfo['custom'];
		$data['product_id']	= $paypalInfo["item_number"];
		$data['txn_id']	= $paypalInfo["txn_id"];
		$data['payment_gross'] = $paypalInfo["payment_gross"];
		$data['currency_code'] = $paypalInfo["mc_currency"];
		$data['payer_email'] = $paypalInfo["payer_email"];
		$data['payment_status']	= $paypalInfo["payment_status"];

		$paypalURL = $this->paypal_lib->paypal_url;		
		$result	= $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
		
		//check whether the payment is verified
		if(preg_match("/VERIFIED/i",$result)){
		    return true;
		}
		else{
			return false;
		}
    }
	//For Payment(paypal)

}

?>