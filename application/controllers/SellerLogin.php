<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SellerLogin extends MY_Controller{
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'language', 'url'));
        $this->load->model('InformationModel');
        if (isset($_COOKIE['language'])) {
            $this->lang->load($_COOKIE['language'] . "_landing", $_COOKIE['language']);
        } else {
            $this->lang->load('english_landing', 'english');
        }
    }
	
	public function index_get(){
		/* if($session){
			
		}else{
			
		} */
		
		$this->dashboard_get();
	}
	
	public function login_get(){
		$data = array(
			'active_class'=>'home'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/login.tpl" ,$data);
	}

	public function dashboard_get(){
		$data = array(
			'active_class'=>'home'
		);

		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/dashboard.tpl" ,$data);
	} 

	public function listings_get()
	{
		$data = array(
			'active_class'=>'pages'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/listing.tpl" ,$data);
	} 
	
	public function approval_request_get()
	{
		$data = array(
			'active_class'=>'pages'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/approval_request.tpl" ,$data);
	} 
	
	public function payment_get()
	{
		$data = array(
			'active_class'=>'payments'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/payment.tpl" ,$data);
	} 
	public function payment_statement_get()
	{
		$data = array(
			'active_class'=>'payments'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/payment_statement.tpl" ,$data);
	} 
	public function transaction_get()
	{
		$data = array(
			'active_class'=>'payments'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/transaction.tpl" ,$data);
	} 
	public function invoices_get()
	{
		$data = array(
			'active_class'=>'payments'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/invoices.tpl" ,$data);
	} 

	public function active_orders_get(){
		$data = array(
			'active_class'=>'orders'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/active_orders.tpl" ,$data);
	}
	
	public function cancelled_orders_get(){
		$data = array(
			'active_class'=>'orders'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/cancelled_orders.tpl" ,$data);
	}
	
	public function returns_get(){
		$data = array(
			'active_class'=>'returns'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/returns.tpl" ,$data);
	}
	
	public function advertising_get(){
		$data = array(
			'active_class'=>'advertising'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/advertising.tpl" ,$data);
	}
	
	public function overview_get(){
		$data = array(
			'active_class'=>'performance'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/performance_overview.tpl" ,$data);
	}
	
	public function growth_get(){
		$data = array(
			'active_class'=>'performance'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/growth.tpl" ,$data);
	}
	
	public function org_promotions_get(){
		$data = array(
			'active_class'=>'promotions'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/flip_promotions.tpl" ,$data);
	}
	
	public function promotions_get(){
		$data = array(
			'active_class'=>'promotions'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/promotions.tpl" ,$data);
	}
	
	public function profile_get(){
		$data = array(
			'active_class'=>'profile'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/profile.tpl" ,$data);
	}

	public function modal($template_name){
		$this->load->view("seller/modal_templates/".$template_name);
	}
}