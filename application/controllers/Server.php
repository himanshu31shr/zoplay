<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Server extends MY_Controller{
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'language', 'url'));
        $this->load->model('InformationModel');
        if (isset($_COOKIE['language'])) {
            $this->lang->load($_COOKIE['language'] . "_landing", $_COOKIE['language']);
        } else {
            $this->lang->load('english_landing', 'english');
        }
    }
	
	public function index_get(){
		 $files = array();
	if( !empty(trim($_GET['product_id'])))	{
		$product_id = $_GET['product_id'];	 
	$sql = "SELECT product_images  FROM  products WHERE product_id = $product_id ";
	$r = $this->db->query( $sql );
	
	$r = $r->result_array();
	$m = $r[0];
		 
   $product_images = unserialize($m['product_images']) ;
  
		 foreach( $product_images as $image )
		 {
				$files[] = array
				(
					"name" => "$image" ,
					"url" => base_url()."/files/"."$image" ,
					"size" =>"" ,
					"thumbnailUrl"=>  base_url()."/files/thumbnail/"."$image" ,
					"deleteUrl"=> site_url()."/Server/php?file=$image",
					"deleteType"=> "DELETE" 
				);
			 
		 }
		
	}
	
		 
		 exit(json_encode(array("files" => $files)));

	
	}
	
	
	public function php_post(){
	
		$this->load->library("UploadHandler.php");
		$upload_handler = new UploadHandler();
		
	}
	
	public function index_delete(){
		$this->load->library("UploadHandler.php");
		$upload_handler = new UploadHandler();
	}
	
	
}