<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_api extends My_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
		$this->load->model('CommonModel');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
    
	public function adduser_post() 
	{
		$data = array();       	   	 
		$file_name = $_FILES['image']['name'];
		if(!empty($file_name))
		{
                    $config['upload_path'] = 'assets/uploads/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['overwrite'] = TRUE;					
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $profile_name=$file_name;					
                    if(!$this->upload->do_upload('image',$config)) 
                    {
                            echo $this->upload->display_errors(); 
                    }
		}
                $ip_address= $_SERVER['REMOTE_HOST'];
                $user_data = array('user_name'	=>trim($_POST['user_name']),
                                   'full_name'	=>trim($_POST['full_name']),
                                   'email'	=> trim($_POST['email']),
                                   'password'	=> trim(md5($_POST['password'])),
                                   'status'	=> trim($_POST['status']),
                                   'role'	=>'user',
                                   'ip_address' => $ip_address,
                                   'picture'	=>$profile_name,
                                   );
                $this->CommonModel->insert_data('users',$user_data);
		if($this->db->affected_rows() > 0)
		{
			$userdata = array(
				'userData' => $user_data
			);
			$val = array(
				"status" => TRUE,
				"message" => "UserData inserted successfully!",
				"data" => $userdata,
			);				
			$this->response($val, parent::HTTP_OK);			
		}else{
			$val = array(
				'status' => FALSE,
				'message' => 'Data not insert',
				'data' => '',
			);
			$this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
		}				  
    }
	/*  AKHILESH TENGURIY CODE START */
	public function addEvent_post() 
	{
	
	 if(!empty($_POST['title'])&& !empty($_POST['description'])&& !empty($_POST['event_type'])&& !empty($_POST['event_date'])&& !empty($_POST['event_link']))
	 {
		 $title       =	trim($this->input->post('title'));
		 $description = trim($this->input->post('description'));
		 $event_type  = trim($this->input->post('event_type'));
		 $event_date  = trim($this->input->post('event_date'));
		 $event_time  = trim($this->input->post('event_time'));
		 $event_link  = trim($this->input->post('event_link'));
		 $event_button_name = trim($this->input->post('event_button_name'));
		 $location = trim($this->input->post('location'));
		 $status = trim($this->input->post('status'));
		 $user_data = array(
					'community_event_title'	            =>	$title,
					'community_event_description'	    =>	$description,
					'community_event_type'		  =>	$event_type,
					'community_event_start_datetime'	      =>  $event_date,
					'community_event_link'		  =>	$event_link,
					'community_event_button'      =>$event_button_name,
					'community_event_location'	  =>	$location,
					'community_event_status'	  =>	$status,
					);
		
		$this->CommonModel->insert_data('community_events',$user_data);
		if($this->db->affected_rows() > 0)
		{
			$userdata = array(
				'userData' => $user_data
			);
			$val = array(
				"status" => TRUE,
				"message" => "Event Created successfully!",
				"data" => '',
			);				
			$this->response($val, parent::HTTP_OK);			
		}else{
			$val = array(
				'status' => FALSE,
				'message' => 'Data not insert',
				'data' => '',
			);
			$this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
		}				  
		 
	 }else{
		 $val = array(
				'status' => FALSE,
				'message' => 'Please enter fields',
				'data' => '',
			);
			$this->response($val, parent::HTTP_OK); 	
	 }
        }
	
	
	public function addTeam_post() 
	{
	
	 if(!empty($_POST['teamname'])&& !empty($_POST['teamshortDescription'])&& !empty($_POST['who_can_join'])&& !empty($_POST['tags']))
	 {
		// print_r($_FILES['logo']['tmp_name']);
		 
		 move_uploaded_file($_FILES['logo']['tmp_name'], 'uploads/team/'. $_FILES['logo']['name']);
		 
		 $teamname       =	trim($this->input->post('teamname'));
		 $teamshortDescription = trim($this->input->post('teamshortDescription'));
		 $who_can_join  = trim($this->input->post('who_can_join'));
		 $tags  = trim($this->input->post('tags'));
		
		 $status = trim($this->input->post('status'));
		 $user_data = array(
					'community_team_title'	      =>	$teamname,
					'community_team_description'  =>	$teamshortDescription,					
					'community_team_logo_name'	  =>  	$_FILES['logo']['name'],
					'community_team_join'		  =>	$who_can_join,
					'community_team_tags'		  =>	$tags,				
					'community_team_status'		  =>	$status,
					);
		
		$this->CommonModel->insert_data('community_teams',$user_data);
		if($this->db->affected_rows() > 0)
		{
			$userdata = array(
				'userData' => $user_data
			);
			$val = array(
				"status" => TRUE,
				"message" => "Team added successfully!",
				"data" => '',
			);				
			$this->response($val, parent::HTTP_OK);			
		}else{
			$val = array(
				'status' => FALSE,
				'message' => 'Team not insert',
				'data' => '',
			);
			$this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
		}				  
		 
	 }else{
		 $val = array(
				'status' => FALSE,
				'message' => 'Please enter fields',
				'data' => '',
			);
			$this->response($val, parent::HTTP_OK); 	
		 
	 }
	
		
	}
    public function add_currency_post(){
        $currency_name = $this->input->post('currency_name');
        $currency_code = $this->input->post('currency_code');
        $currency_symbol = $this->input->post('currency_symbol');
        $currency_value = $this->input->post('currency_value');
        $userstatus = $this->input->post('userstatus');

        $data = array();       	   	 				
        $currency_data = array('currency_name'	=>$currency_name,
                                'currency_code'	=> $currency_code,
                                'currency_symbol'=>$currency_symbol,
                                'currency_value'=> $currency_value,
                                'currency_status'=>$userstatus,
                                );
            $this->CommonModel->insert_data('currencies',$currency_data);
             $insert_id = $this->db->insert_id();
            if($insert_id)
            {
                $currencyData = array('currencyData' => $currency_data);
                $val = array(
                        "status" => TRUE,
                        "currency_id" =>$insert_id,
                        "message" => "Currency inserted successfully!",
                        "data" => $currencyData,
                );				
                    $this->response($val, parent::HTTP_OK);			
            }else{
                    $val = array(
                            'status' => FALSE,
                            'message' => 'Data not insert',
                            'data' => '',
                    );
                    $this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
            }				  
    }
	
	
	/* add cayegory */
	/* required field 
		parent_id (0 for root category),category_name
	*/
	public function  add_category_post() {
		$result = $this->CommonModel->addCategory();
		$this->response($result, parent::HTTP_OK);
    }
    
    public function addpackage_post() {
        $package_name =   $this->input->post('package_name');
        $package_amount =    $this->input->post('package_amount');
        $package_day =     $this->input->post('package_day');
        $status =          $this->input->post('status');
        $package_staus =     $this->input->post('package_staus');
        $user_id=1;
        $data = array('user_id'	=>$user_id,
                      'package_name'=>$package_name,
                      'package_amount'	=>$package_amount,
                      'package_days'	=>$package_day,
                      'package_status'	=> $package_staus 
                    );
                $this->CommonModel->insert_data('feature_packages',$data);
                $insert_id = $this->db->insert_id();
		if($insert_id)
		{
			$packagedata = array(
				'userData' => $data
			);
			$val = array(
				"status" => TRUE,
                                "feature_package_id" => $insert_id,
				"message" => "feature packages inserted successfully!",
				"data" => $packagedata
			);				
			$this->response($val, parent::HTTP_OK);			
		}else{
			$val = array(
				'status' => FALSE,
				'message' => 'Data not insert',
				'data' => '',
			);
			$this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
		}
    }
    public function saveaffiliatesettings_post() {
        $one_point = $this->input->post('one_point');
        $one_referral = $this->input->post('one_referral');
        $expiration_duration =$this->input->post('expiration_duration');
        $cookie_period = $this->input->post('cookie_period');
        $status = $this->input->post('status');
        $user_id=1;
        $data = array('user_id'	=>$user_id,
                      'one_point'	=>$one_point,
                      'one_referral'	=>$one_referral,
                      'cookie_expiration_duration'=>$expiration_duration,
                      'cookie_period'=>$cookie_period,
                      'general_setting_status'	=> $status 
                    );
                $this->CommonModel->insert_data('general_settings',$data);
                $insert_id = $this->db->insert_id();
		if($insert_id)
		{
			//$packagedata = array('userData' => $data);
			$val = array(
				"status" => TRUE,
                                "general_setting_id" => $insert_id,
				"message" => "Affiliate settings saved successfully!",
				"data" =>array('userData' => $data)
			);				
			$this->response($val, parent::HTTP_OK);			
		}else{
			$val = array(
				'status' => FALSE,
				'message' => 'Data not insert',
				'data' => '',
			);
			$this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
		}
    }
	/* sandeep naroliya code start*/
	public function addShippingLocation_post() {
		
        $country = $this->input->post('country');
        $status = $this->input->post('status');
		if(empty($country)){
			 $this->set_response(['status'=>false,'message'=>'please select country'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$check = $this->CommonModel->get_record('shippinglocation',array('country_id'=>$country));
			if(empty($check)){
				$this->db->insert('shippinglocation',array('country_id'=>$country,'status'=>$status));
				 $this->set_response(['status'=>true,'message'=>'Location added.'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			}else{
				 $this->set_response(['status'=>false,'message'=>'location Already exist.'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
				
			}
		}
    }
	public function shippinglocationstatus_post() {
		
        $id = $this->input->post('id');
        $status = $this->input->post('status');
		if(empty($id) || empty($status)){
			 $this->set_response(['status'=>false,'message'=>'Invalid Request'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$this->db->update('shippinglocation',array('status'=>$status),array('sl_id'=>$id));
			$this->set_response(['status'=>true,'message'=>'status changed successfully'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			
		}
    }
	public function deleteshippinglocation_post() {
		
        $id = $this->input->post('id');
       
		if(empty($id) ){
			 $this->set_response(['status'=>false,'message'=>'Invalid Request'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$this->db->delete('shippinglocation',array('sl_id'=>$id));
			$this->set_response(['status'=>true,'message'=>'Location deleted successfully'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			
		}
    }
	public function shopstatus_post() {
		
        $id = $this->input->post('id');
        $status = $this->input->post('status');
		if(empty($id) || empty($status)){
			 $this->set_response(['status'=>false,'message'=>'Invalid Request'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$this->db->update('shops',array('status'=>$status),array('shop_id'=>$id));
			$this->set_response(['status'=>true,'message'=>'status changed successfully'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			
		}
    }
	public function sellerstatus_post() {
		
        $id = $this->input->post('id');
        $status = $this->input->post('status');
		if(empty($id) || $status == ''){
			 $this->set_response(['status'=>false,'message'=>'Invalid Request'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$this->db->update('users',array('admin_approved_status'=>$status),array('id'=>$id));
			$this->set_response(['status'=>true,'message'=>'status changed successfully'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			
		}
    }
	public function verified_document_post() {
		
        $id = $this->input->post('id');
        $status = $this->input->post('status');
		if(empty($id) || $status == ''){
			 $this->set_response(['status'=>false,'message'=>'Invalid Request'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$this->db->update('seller_details',array('status'=>$status),array('seller_id'=>$id));
			$this->set_response(['status'=>true,'message'=>'Document verify successfully'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			
		}
    }
	public function bannerstatus_post() {
		
        $id = $this->input->post('id');
        $status = $this->input->post('status');
		if(empty($id) || $status == ''){
			 $this->set_response(['status'=>false,'message'=>'Invalid Request'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$this->db->update('banners',array('status'=>$status),array('banner_id'=>$id));
			$this->set_response(['status'=>true,'message'=>'status changed successfully'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			
		}
    }
	public function deletebanner_post() {
        $id = $this->input->post('id');
		if(empty($id) ){
			 $this->set_response(['status'=>false,'message'=>'Invalid Request'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
		}else{
			$this->db->delete('banners',array('banner_id'=>$id));
			$this->set_response(['status'=>true,'message'=>'banner deleted successfully'], \Restserver\Libraries\REST_Controller::HTTP_OK); 
			
		}
    }
	public function savebanner_post() 
	{
		$data = array();       	   	 
		$file_name = isset($_FILES['image']['name']) ?  time().$_FILES['image']['name'] : '';
		
		$tagline = $this->input->post('tagline');
		$headding = $this->input->post('headding');
		$description = $this->input->post('description');
		$status = $this->input->post('status');
		$banner_id = $this->input->post('banner_id');
		$config['upload_path'] = 'assets/uploads/banner';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $file_name;		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$profile_name=$file_name;
		if(empty($file_name) && empty($banner_id)){
			$val = array(
					"status" => false,
					"message" => "Please upload banner image",
					"data" => '',
				);	
			$this->response($val, parent::HTTP_OK);
			exit();
		}
		if(!empty($file_name) && !$this->upload->do_upload('image',$config)) 
		{
			$res = ['data'=>''];
			$res['message'] = $this->upload->display_errors();
			$res['status'] = false;
			$this->response($res, parent::HTTP_OK);	
			exit();
		}
		$insertdata = array(
			'tagline'	=>trim($tagline),
			'heading'	=>trim($headding),
			'description'	=> trim($description),
			'status'	=> $status,
			
			
		);
		if(!empty($file_name)){
			$insertdata['image'] = $file_name;
		}
		if(!empty($banner_id)){
			$bannerinfo = $this->CommonModel->get_single_record('banners',['banner_id'=>$banner_id]);
			if(!empty($bannerinfo)){
				if(file_exists(FCPATH.'assets/uploads/banner/'.$bannerinfo->image)){
					unlink(FCPATH.'assets/uploads/banner/'.$bannerinfo->image);
				}
				$this->db->update('banners',$insertdata,['banner_id'=>$banner_id]);
			}else{
				$res = ['data'=>''];
				$res['message'] = 'banner not exist';
				$res['status'] = false;
				$this->response($res, parent::HTTP_OK);	
				exit;
			}
		}else{
			$this->db->insert('banners',$insertdata);
		}
		
		$val = array(
			"status" => TRUE,
			"message" => "data saved successfully!",
			"data" => '',
		);				
		$this->response($val, parent::HTTP_OK);			
					  
		
    }
	/*sandeep naroliya code End*/
    
}