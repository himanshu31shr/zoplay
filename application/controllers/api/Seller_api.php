<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seller_api extends My_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model("Seller_model");
    }
    
      public function addseller_post(){
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $data = array();       	   	 				
        $sellersdata = array('email'=>$email,
                              'contact'=>$phone,
                              'role'=>'seller',
                             );
            $this->CommonModel->insert_data('users',$sellersdata);
            $insert_id = $this->db->insert_id();			 
            $this->CommonModel->insert_data('shops',array('seller_id'=>$insert_id));
            if($insert_id)
            {
                $Data = array('sellerData' => $sellersdata);
                $val = array(
                        "status" => TRUE,
                        "seller_id" =>$insert_id,
                        "message" => "seller inserted successfully!",
                        "data" => $Data,
                );				
                    $this->response($val, parent::HTTP_OK);			
            }else{
                    $val = array(
                            'status' => FALSE,
                            'message' => 'Data not insert',
                            'data' => '',
                    );
                    $this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
            }	
      }
      
      
      public function sellerLogin_post(){
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            $sellerDetail = $this->Seller_model->sellerLogin($email,$password);
           
            if(isset($sellerDetail) && !empty($sellerDetail))
            {
                $this->session->set_userdata('sellerData',$sellerDetail);   // set seller data in session
                $Data = array('sellerData' => $sellerDetail);
                $val = array(
                    "status" => TRUE,
                    "message" => "LoggedIn successfully!",
                    "data" => $Data,
                );				
                    $this->response($val, parent::HTTP_OK);			
            }else{
                $val = array(
                        'status' => FALSE,
                        'message' => 'Invalid email id or password.',
                        'data' => '',
                );
                $this->response($val, parent::HTTP_OK); 			
            }
      }
  }