<?php
		defined('BASEPATH') OR exit('No direct script access allowed');

	class Information extends MY_Controller 
		{
           
             
			public function __construct()
				{
					parent::__construct();
						$this->load->helper(array('form','language','url'));
						$this->load->model('InformationModel');
							if(isset($_COOKIE['language']))
								{
									$this->lang->load($_COOKIE['language']."_landing" , $_COOKIE['language']);
								} else {
									$this->lang->load('english_landing' , 'english');
								}
				}
				
			public function index()
				{
					$data = array();
					$seo  = array();
					$checklogin	=	$this->InformationModel->checkloggedin();
						if($checklogin)
							{
								redirect("information/dashboard?token=".md5(time()));	
							}
						$data['checklogin']		=	$checklogin;
						$seo['url']				=	site_url("Information");
						$seo['title']			=	lang('logintext')." - ".WEBSITENAME;
						$seo['metatitle']		=	lang('textmetatitle')." - ".WEBSITENAME;
						$seo['metadescription']	=	lang('textmetadescription')." - ".WEBSITENAME;
								$data['data']['seo']	=	$seo;

									$data['layout'] = $this->frontLayout($data);

									$this->load->view("login.tpl" ,$data);

				} 

			public function login(){
				$data['layout'] = $this->frontLayout2($data);
				$this->load->view("front2/dashboard.tpl" ,$data );
			}
			
			public function dashboard()
				{
					$data = array();
					$seo  = array();
						$checklogin	=	$this->InformationModel->checkloggedin();
							if(!$checklogin)
								{
									redirect("information?token=invalid");	
								}
						$data['checklogin']		=	$checklogin;
						$seo['url']				=	site_url("Admin/dashboard");
						$seo['title']			=	lang('welcometext')." - ".WEBSITENAME;
						$seo['metatitle']		=	lang('welcomemetatitle')." - ".WEBSITENAME;
						$seo['metadescription']	=	lang('welcomemetadescription')." - ".WEBSITENAME;
							$data['data']['seo'] = $seo;
							$data['layout'] = $this->frontLayout($data);
							$this->load->view("front/dashboard.tpl" ,$data );
				} 

			public function products()
				{
					$data = array();
					$seo  = array();
						$checklogin	=	$this->InformationModel->checkloggedin();
							if(!$checklogin)
								{
									redirect("information?token=invalid");	
								}
						$data['checklogin']		=	$checklogin;
						$seo['url']				=	site_url("Admin/products");
						$seo['title']			=	lang('productstext')." - ".WEBSITENAME;
						$seo['metatitle']		=	lang('productsmetatitle')." - ".WEBSITENAME;
						$seo['metadescription']	=	lang('productsmetadescription')." - ".WEBSITENAME;
							$data['data']['seo'] = $seo;
							$data['layout'] = $this->frontLayout($data);
							$this->load->view("front/products.tpl" ,$data );
				}

			public function ebayaccounts()
				{
					$data = array();
					$seo  = array();
						$checklogin	=	$this->InformationModel->checkloggedin();
							if(!$checklogin)
								{
									redirect("information?token=invalid");	
								}
						$data['checklogin']		=	$checklogin;
						$seo['url']				=	site_url("Admin/ebayaccounts");
						$seo['title']			=	lang('ebayaccountstext')." - ".WEBSITENAME;
						$seo['metatitle']		=	lang('ebayaccountsmetatitle')." - ".WEBSITENAME;
						$seo['metadescription']	=	lang('ebayaccountsmetadescription')." - ".WEBSITENAME;
							$data['data']['seo'] = $seo;
							$data['layout'] = $this->frontLayout($data);
							$this->load->view("front/ebayaccounts.tpl" ,$data );
				}

			public function setting()
				{
					$data = array();
					$seo  = array();
						$checklogin	=	$this->InformationModel->checkloggedin();
							if(!$checklogin)
								{
									redirect("information?token=invalid");	
								}
						$data['checklogin']		=	$checklogin;
						$seo['url']				=	site_url("Admin/setting");
						$seo['title']			=	lang('settingtext')." - ".WEBSITENAME;
						$seo['metatitle']		=	lang('settingmetatitle')." - ".WEBSITENAME;
						$seo['metadescription']	=	lang('settingmetadescription')." - ".WEBSITENAME;
							$data['data']['seo'] = $seo;
							$data['layout'] = $this->frontLayout($data);
							$this->load->view("front/setting.tpl" ,$data );
				}

			public function logout()  
				{
					$this->session->sess_destroy();
					header("Location: ".base_url());
				}

		}		
?>