<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
class Products extends My_Controller {

<<<<<<< HEAD
    public function __construct() {
        parent::__construct();
        $this->load->model('ProductModel');
        $this->load->model('AdminModel');
        $this->load->helper('category');
    }

    //@override $user_id here
    // protected $user_id = 1;

    public function index_get() {
        $currentUrl = str_replace(site_url(), '', current_url());
        $this->is_logged_in($currentUrl);
    }

    /**
     * 	Author: Himanshu
     * 	Get all products for a category
     *
     * 	@param string category (category name)
     */
    public function products_list_get($category = "", $option = null) {
        if (empty($data)) {

            // if(empty($data)){
            // }else{
        }
        // }
        $currentUrl = str_replace(site_url(), '', current_url());
        //$user_id = $this->is_logged_in($currentUrl)['user_id'];			
        $category = base64_decode($category);

        $seo['url'] = site_url("Information");
        $seo['title'] = lang('productstext') . " " . $category . " - " . WEBSITENAME;
        $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
        $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
        $data['data']['seo'] = $seo;
        
        //getting category_id from db
        if ($option != null) {
            $category_res = true;
        } else {

            $category_res = $this->CommonModel->select_data_where('categories', array('category_name' => $category))->result();
        }

        if ($category_res) {
            if ($option != null) {
                $cid = $category;
            } else {
              $cid = $category_res[0]->category_id;
            }
         
            $data['allCategories'] = getCategoryParentId($category_res[0]->category_id); 
            
            $products = $this->ProductModel->all_products($cid);
            if ($products) {

                if ($this->session->userdata('user')) {
                    $user_id = $this->is_logged_in()['user_id'];
                    $i = 0;
                    foreach ($products as $p) {
                        $wishlist = $this->CommonModel->select_data_where('wishlist', array('product_id' => $p->product_id, 'user_id' => $user_id))->result();

                        if ($wishlist) {
                            $products[$i]->added_to_wish = 1;
                        } else {
                            $products[$i]->added_to_wish = 0;
                        }
                        $i++;
                    }
                } else {
                    $z = 0;
                    foreach ($products as $p) {
                        $products[$z]->added_to_wish = 0;
                        $z++;
                    }
                }

                $data['products']['status'] = true;
                $data['products']['data'] = $products;
            } else {
                $data['products']['status'] = false;
                $data['products']['data'] = 'No products under this category!';
            }
        } else {
            $data['products']['status'] = false;
            $data['products']['data'] = 'No category with ' . $category . " exists!";
        }

        $data['layout'] = $this->frontLayout2($data);
        $this->load->view("front-2/category.tpl", $data);
    }

    /**
     * 	Author: Himanshu
     * 	Gets more products for a category 
     *
     * 	@param int category_id, int last_id, string criteria (filter function)
     * 	@return std object array
     */
    public function more_products_post() {
        $last_id = $this->input->post('last_id');
        $category_id = $this->input->post('category_id');
        $criteria = $this->input->post('criteria');

        $products = $this->ProductModel->all_products($category_id, $last_id, $criteria);

        if ($products) {
            $grid = '';
            foreach ($products as $p) {
                $price = $p->product_price + $p->product_discount;

                $grid .= '<div class="col-sm-6 col-md-4 wow fadeInUp height">
=======
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('ProductModel');
		$this->load->model('AdminModel');
	}
	
	//@override $user_id here
	// protected $user_id = 1;

	public function index_get()
	{
		$currentUrl = str_replace(site_url(),'',current_url());
		$this->is_logged_in($currentUrl);	
	}

	
	/**
	*	Author: Himanshu
	*	Get all products for a category
	*
	*	@param string category (category name)
 	*/
	public function products_list_get($category="", $option=null)
	{



		if(empty($data)){

		// if(empty($data)){
			         
		// }else{
			
		}
				
		


		// }
		$currentUrl = str_replace(site_url(),'',current_url());
		//$user_id = $this->is_logged_in($currentUrl)['user_id'];			
		$category = base64_decode($category);
		
		$seo['url'] = site_url("Information");
		$seo['title'] = lang('productstext') ." ".$category. " - " . WEBSITENAME;
		$seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
		$seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
		$data['data']['seo'] = $seo;				
		//getting category_id from db
		if($option != null){	
			$category_res = true;
		}else{
			
			$category_res = $this->CommonModel->select_data_where('categories', array('category_name'=>$category))->result();
		}

		if($category_res){ 
			if($option != null){		
				$cid = $category;
			}else{
				$cid = $category_res[0]->category_id;
			}
			$products= $this->ProductModel->all_products($cid);			
			if($products){

				if($this->session->userdata('user')){
					$user_id = $this->is_logged_in()['user_id'];
					$i=0;
					foreach ($products as $p) {
						$wishlist = $this->CommonModel->select_data_where('wishlist', array('product_id' => $p->product_id, 'user_id'=>$user_id))->result();

						if($wishlist){
							$products[$i]->added_to_wish = 1;
						}else{
							$products[$i]->added_to_wish = 0;
						}
						$i++;
					}
				}else{
					$z=0;
					foreach ($products as $p) {
						$products[$z]->added_to_wish = 0;
						$z++;
					}
				}

				$data['products']['status'] = true;
				$data['products']['data'] = $products;

			}else{
				$data['products']['status'] = false;
				$data['products']['data'] = 'No products under this category!';
			}

		}else{
			$data['products']['status'] = false;
			$data['products']['data'] = 'No category with '. $category." exists!";
			
		}

		$data['layout'] = $this->frontLayout2($data);
		$this->load->view("front-2/category.tpl", $data);
	}
	
	
	/**
	*	Author: Himanshu
	*	Gets more products for a category 
	*
	*	@param int category_id, int last_id, string criteria (filter function)
	*	@return std object array
 	*/
	public function more_products_post(){
		$last_id = $this->input->post('last_id');
		$category_id = $this->input->post('category_id');
		$criteria = $this->input->post('criteria');

		$products = $this->ProductModel->all_products($category_id, $last_id, $criteria);
		
		if($products){
			$grid = '';
			foreach($products as $p){
				$price = $p->product_price+$p->product_discount;
				
				$grid .= '<div class="col-sm-6 col-md-4 wow fadeInUp height">
>>>>>>> 309c58dc511fb29bfc29e96298f11d54b16bef5a
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="detail.html"><img  src="' . $p->product_image . '" alt=""></a> </div>
                          <!-- /image -->
                          
                          <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /product-image -->
                        
                        <div class="product-info text-left">
                          <h3 class="name"><a href="detail.html">' . substr($p->product_name, 0, 80) . '</a></h3>
                          <div class="rating rateit-small" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="' . $p->average_rating . '"></div>
                          <div class="description"></div>
                          <div class="product-price"> <span class="price">' . $p->product_price . '</span> <span class="price-before-discount">$' . $price . '</span> </div>
                          <!-- /product-price --> 
                          
                        </div>
                        <!-- /product-info -->
                        <div class="cart clearfix animate-effect">
                          <div class="action">
                            <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                <button onclick="add_to_cart(' . $p->product_id . ',' . substr($p->product_name, 0, 80) . ',' . $p->product_image . ',' . $p->product_price . ')" class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                              </li>
                              <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                              <li class="lnk"> <a class="add-to-cart" href="detail.html" title="Compare"> <i class="fa fa-signal"></i> </a> </li>
                            </ul>
                          </div>
                          <!-- /action --> 
                        </div>
                        <!-- /cart --> 
                      </div>
                      <!-- /product --> 
                      
                    </div>
                    <!-- /products --> 
                  </div>';
<<<<<<< HEAD
            }
            $data = array('status' => true, 'message' => 'Found', 'data' => $grid, 'last_id' => $p->product_id);
            $this->response($data, parent::HTTP_OK);
        } else {
            $data = array('status' => false, 'message' => '<div class="no_products col-md-12"><i class="fa fa-info-circle"></i> No more products in this category</div>', 'data' => '');

            $this->response($data, parent::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 	Author: Himanshu
     * 	Returns data for single product
     *
     * 	@param int category, string product_id (md5)
     * 	@return std object array
     */
    public function single_product_get($product_id) {



        $currentUrl = str_replace(site_url(), '', current_url());
        $user_id = $this->is_logged_in($currentUrl)['user_id'];


        $data = array();
        $detials = $this->ProductModel->single_product(null, $product_id);

        //decrypted product_id
        $product_id = $detials[0]->product_id;

        if ($detials) {

            //checking if deal is activated
            $deal = $this->AdminModel->getDealsOfTheDay($product_id);
            if ($deal['status'] == 'success') {

                $detials[0]->is_deal_active = 1;
                $detials[0]->discounted_price = (float) $detials[0]->product_price - (float) $detials[0]->product_price * (float) $detials[0]->product_discount / 100;
            } else {
                $detials[0]->is_deal_active = 0;
                $detials[0]->discounted_price = $detials[0]->product_price;
            }

            if ($detials[0]->product_quantity > 0) {
                $detials[0]->availability = 'In stock';
            } else {
                $detials[0]->availability = 'Out of stock';
            }

            $detials[0]->product_variations = $detials[0]->product_variations == '' ? '' : unserialize($detials[0]->product_variations);
            $detials[0]->product_images = $detials[0]->product_images == '' ? '' : unserialize($detials[0]->product_images);
            $detials[0]->shipping_charges = $detials[0]->shipping_charges == '' ? : unserialize($detials[0]->shipping_charges);

            $wishlist = '';
            if ($this->session->userdata('user')) {
                $user_id = $this->is_logged_in()['user_id'];
                $wishlist = $this->CommonModel->select_data_where('wishlist', array('product_id' => $product_id, 'user_id' => $user_id))->result();
            }
            if ($wishlist != '') {
                $detials[0]->added_to_wish = 1;
            } else {
                $detials[0]->added_to_wish = 0;
            }


            $j = 0;
            foreach ($detials[0]->shipping_charges as $c) {
                $country_name = getCountries($c['country_id'], 'array');
                $detials[0]->shipping_charges[$j]['country_name'] = $country_name;
                $j++;
            }

            $reviews = $this->ProductModel->reviews($product_id, 3);
            if ($reviews->total_reviews != 0) {
                $num = $reviews->total_reviews;
                $sum = $reviews->total_rating;
                $average = $num != 0 ? (float) $sum / $num : 0;
                $detials[0]->average_rating = $average;
                $detials[0]->total_reviews = $num;
                $detials[0]->reviews = $reviews->reviews;

                $html = '';

                foreach ($reviews->reviews as $r) {
                    $html .= '<div class="review">
=======
				  
			}
			$data = array('status'=>true, 'message'=>'Found', 'data'=>$grid, 'last_id'=>$p->product_id);
			$this->response($data, parent::HTTP_OK);
		}else{
			$data = array('status'=>false, 'message'=>'<div class="no_products col-md-12"><i class="fa fa-info-circle"></i> No more products in this category</div>', 'data'=>'');
			
			$this->response($data, parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	*	Author: Himanshu
	*	Returns data for single product
	*
	*	@param int category, string product_id (md5)
	*	@return std object array
 	*/
	public function single_product_get($product_id){

			

		$currentUrl = str_replace(site_url(),'',current_url());
		//$user_id = $this->is_logged_in($currentUrl)['user_id'];		

		
		$data = array();
		$detials = $this->ProductModel->single_product(null, $product_id);
		
		//decrypted product_id
		$product_id = $detials[0]->product_id;
				
		if($detials){

			//checking if deal is activated
			$deal = $this->AdminModel->getDealsOfTheDay($product_id);
			if($deal['status'] == 'success'){

				$detials[0]->is_deal_active = 1;
				$detials[0]->discounted_price = (float)$detials[0]->product_price  - (float)$detials[0]->product_price* (float)$detials[0]->product_discount/100;
			}else{
				$detials[0]->is_deal_active = 0;
				$detials[0]->discounted_price = $detials[0]->product_price;
			}

			if($detials[0]->product_quantity > 0){
				$detials[0]->availability = 'In stock';
			}else{
				$detials[0]->availability = 'Out of stock';
			}
			
			$detials[0]->product_variations = $detials[0]->product_variations == '' ? '' : unserialize($detials[0]->product_variations );
			$detials[0]->product_images = $detials[0]->product_images == '' ? '' : unserialize($detials[0]->product_images );
			$detials[0]->shipping_charges = $detials[0]->shipping_charges == '0' || $detials[0]->shipping_charges == '' ? '' : unserialize($detials[0]->shipping_charges );

			$wishlist = '';
			if($this->session->userdata('user')){
				$user_id = $this->is_logged_in()['user_id'];
				$wishlist = $this->CommonModel->select_data_where('wishlist', array('product_id' => $product_id, 'user_id'=>$user_id))->result();
			}
			if($wishlist != ''){
				$detials[0]->added_to_wish = 1;
			}else{
				$detials[0]->added_to_wish = 0;
			}


			$j = 0;
			if($detials[0]->shipping_charges != ''){
				foreach($detials[0]->shipping_charges as $c){
					$country_name = getCountries($c['country_id'], 'array');
					$detials[0]->shipping_charges[$j]['country_name'] = $country_name;
					$j++;
				}
			}
			
			$reviews = $this->ProductModel->reviews($product_id, 3);
			if($reviews->total_reviews !=  0){
				$num = $reviews->total_reviews;
				$sum = $reviews->total_rating;
				$average = $num != 0 ? (float) $sum/$num : 0;
				$detials[0]->average_rating = $average;
				$detials[0]->total_reviews = $num;
				$detials[0]->reviews = $reviews->reviews;
				
				$html = '';
				
				foreach($reviews->reviews as $r){
					$html .= '<div class="review">
>>>>>>> 309c58dc511fb29bfc29e96298f11d54b16bef5a
						<div class="review-title">
							<span class="summary">"' . $r->summary . '"</span>
							<span class="summary">- by ' . $r->review_by . '</span>
							<span class="date pull-right"><i class="fa fa-calendar"></i>
								<span>' . time_elapsed_string(strtotime($r->review_added)) . '</span>
							</span>
						</div>
						<div class="rating rateit-small" data-rateit-value="' . $r->rating . '" > </div>
						<div class="text">"' . $r->comment . '"</div>
					</div>';
                }
            } else {
                $detials[0]->average_rating = 0;
                $detials[0]->total_reviews = 0;
                $html = '<div class="review">
						<div class="review-title"><span class="summary">No reviews yet!</div>
					</div>';
            }

            $detials[0]->reviews = $html;
            $data['product']['data'] = $detials[0];
            $data['product']['status'] = true;
        } else {
            $data['product']['data'] = '';
            $data['product']['status'] = false;
            $detials[0]->product_name = 'No such product found';
        }

        $seo['url'] = site_url("Information");
        $seo['title'] = $detials[0]->product_name . " " . lang('productstext') . " - " . WEBSITENAME;
        $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
        $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
        $data['data']['seo'] = $seo;

        $data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/detail.tpl", $data);
    }

    /**
     * 	Author: Himanshu
     * 	Adds reviews for a product 
     *
     * 	@return object 
     */
    public function add_review_post() {
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('summary', 'Summary', 'trim|required');
        $this->form_validation->set_rules('rating', 'Rating', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $val = array(
                'status' => FALSE,
                'message' => '<i class="fa fa-close"></i> Error in validating fields',
                'errorMsg' => array(
                    'name' => form_error('name'),
                    'summary' => form_error('summary'),
                    'rating' => form_error('rating')
                )
            );

            // echo json_encode($val);
            // Set the response and exit

            $this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); // INTERNAL SERVER ERROR (500) being the HTTP response code
        } else {
            //print_r($_POST);
            $name = $this->input->post('name');
            $summary = $this->input->post('summary');
            $rating = $this->input->post('rating');
            $product_id = $this->input->post('product_id');
            $comment = $this->input->post('comment');

            //get from session;
            $currentUrl = str_replace(site_url(), '', current_url());
            $user_id = $this->is_logged_in($currentUrl)['user_id'];

            $data = array(
                'review_by' => $user_id,
                'product_id' => $product_id,
                'shop_id' => 1,
                'comment' => $comment,
                'rating' => $rating,
                'review_status' => 1,
                'summary' => $summary
            );

            $return = $this->CommonModel->insert_data('reviews', $data);

            if ($return == true) {
                $last_id = $this->db->insert_id();
                $data = $this->CommonModel->select_data_where('reviews', array('review_id' => $last_id))->result();

                $return = array('status' => true, 'message' => 'Review added', 'data' => $data);
                $this->response($return, parent::HTTP_OK);
            } else {
                $return = array('status' => false, 'message' => 'Something went wrong!', 'data' => '');
                $this->response($return, parent::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Author: Himanshu
     * 	Renders modals for passed views
     * 	
     * 	@param modal (view name), option (what to render)
     */
    public function load_modal_post() {
        $modal = $this->input->post('modal');

        $option = $this->input->post('option');

        if ($option == 'reviews') {

            $product_id = $this->input->post('product_id');
            $last_id = $this->input->post('lreview_id');

            $reviews = $this->ProductModel->reviews($product_id, 3, $last_id);
            if ($reviews->reviews) {
                $html = '';
                $d = $reviews->reviews;
                foreach ($d as $r) {
                    $html .= '<div class="review">
						<div class="review-title">
							<span class="summary">"' . $r->summary . '"</span>
							<span class="summary">- by ' . $r->review_by . '</span>
							<span class="date pull-right"><i class="fa fa-calendar"></i>
								<span>' . time_elapsed_string(strtotime($r->review_added)) . '</span>
							</span>
						</div>
						<div class="rating rateit-small" data-rateit-value="' . $r->rating . '"  data-rateit-readonly="true"> </div>
						<div class="text">"' . $r->comment . '"</div>
					</div>';
                }
                $last_id = $r->review_id;

                if ($modal == null) {
                    $reviews = array('data' => array('product_id' => $product_id, 'last_id' => $last_id), 'html' => $html);
                    $this->response($reviews, parent::HTTP_OK);
                }
            } else {
                $html = '<div class="review">
						<div class="review-title"><span class="summary">No reviews yet!</div>
					</div>';

                if ($modal == null) {
                    $html2 = '<div class="review">
							<div class="review-title"><span class="summary text text-center">No more reviews!</div>
						</div>';
                    $reviews = array('data' => array('product_id' => $product_id, 'last_id' => $last_id), 'html' => $html2);
                    $this->response($reviews, parent::HTTP_INTERNAL_SERVER_ERROR);
                }
            }

            $load['reviews'] = array('data' => array('product_id' => $product_id, 'last_id' => $last_id), 'html' => $html);
        }

        //loading wishlist modal 
        if ($option == 'wishlist') {
            $currentUrl = str_replace(site_url(), '', current_url());
            $user_id = $this->is_logged_in($currentUrl)['user_id'];
            $data = $this->ProductModel->wishlist($user_id);

            $html = '';
            if ($data) {
                foreach ($data as $a) {
                    $image = image_url($a->product_image);
                    $availability = $a->product_quantity > 0 ? 'In stock' : 'Out of stock!';
                    $html .= '<li class="col-md-12" id="wish_' . $a->product_id . '">
                        <div class="pull-right pull-times">
                            <a onclick="remove_wish(' . $a->product_id . ')"><i class="fa fa-times fa-2x"></i></a>
                        </div>
											<div class="col-md-2">
                        <img class="img-responsive" src="' . $image . '" />
											</div>
											<div class="col-md-10">
                        <a href="' . site_url() . 'product/' . md5($a->product_id) . '" target="_blank"><h4>' . substr($a->product_name, 0, 100) . '</h4></a>
                        <p class="">Description: ' . substr(strip_tags($a->product_description), 0, 100) . '</p>
                        <span class="pull-left">Price: ' . $a->product_price . '</span>
                        <span class="pull-right">Availability: ' . $availability . '</span>
											</div>
<<<<<<< HEAD
										</li>';
                }
            } else {
                $html = '';
            }

            $load['wishlist'] = array('data' => array('product_id' => '', 'last_id' => ''), 'html' => $html);
        }

        if ($modal != null) {
            $this->load->view("front-2/modals/" . $modal . ".tpl", $load);
        }
    }

    /**
     * Author: Himanshu
     * 	Adds to removes products from the wishlist
     *
     * 	@param int flag (0=>remove from wishlist, 1=>add to wishlist) 
     */
    public function wishlist_post($flag) {

        $currentUrl = str_replace(site_url(), '', current_url());
        $user_id = $this->is_logged_in($currentUrl)['user_id'];

        $data = array('product_id' => $product_id, 'user_id' => $user_id);

        if ($flag == 1) {
            $stat = $this->CommonModel->insert_data('wishlist', $data);
            $message = $stat == true ? 'Added to wishlist' : 'Something went wrong!';
        }

        if ($flag == 0) {
            $stat = $this->CommonModel->delete_data('wishlist', $data);
            $message = $stat == true ? 'Removed from wishlist' : 'Something went wrong!';
        }

        if ($stat == true) {
            $response = array('status' => true, 'message' => $message);
            $this->response($response, parent::HTTP_OK);
        } else {
            $response = array('status' => false, 'message' => $message);
            $this->response($response, parent::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function product_comparison_get($id = '') {
        $currentUrl = str_replace(site_url(), '', current_url());
        $userid = $this->is_logged_in($currentUrl());
        if (!$userid) {
            redirect("Products");
        } else {
            $id = base64_decode($id);
            $data = array();
            $seo = array();
            $productid = '';
            if (!empty($id) && is_array($this->session->userdata('product')) && !in_array($id, $this->session->userdata('product'), true) && is_numeric($id)) {
                if (count($this->session->userdata('product')) >= 4) {
                    if (isset($_SESSION['product'])) {
                        $_SESSION['product'][].=$id;
                    } else {
                        $_SESSION['product'][] = $id;
                    }
                    $productid = array('product' => array_slice($this->session->userdata('product'), -4, 4));
                } else {
                    if (isset($_SESSION['product'])) {
                        $_SESSION['product'][].=$id;
                    } else {
                        $_SESSION['product'][] = $id;
                    }
                    $productid = $_SESSION;
                }
                $this->session->set_userdata($productid);
            } else if (!empty($id) && is_numeric($id)) {
                $_SESSION['product'][] = $id;
                $productid = $_SESSION;
            }
            $product = $this->session->userdata('product');
            if (!empty($product)) {
                $pid = '';
                foreach ($product as $key => $proid) {
                    $pid .= $proid . ",";
                    $pro_id = rtrim($pid, ",");
                }
            } else {
                $pro_id = "''";
            }
            $data['productlist'] = $this->db->query("SELECT * FROM `products` WHERE `product_id` IN ($pro_id)")->result();
            $seo['url'] = site_url("Information");
            $seo['title'] = 'Product Comparison';
            $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
            $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
            $data['data']['seo'] = $seo;
            $data['layout'] = $this->frontLayout($data);
            $this->load->view("front-2/product_comparison.tpl", $data);
        }
    }

    public function remove_product_get($pid) {
        $data = array();
        $productid = $this->session->userdata('product');
        if (($key = array_search($pid, $productid)) !== false) {
            unset($_SESSION['product'][$key]);
        }
        redirect('Products/product_comparison');
    }

    public function show_categories_get() {
        $this->load->helper('category');
        $categories = $this->CommonModel->Categories(0);
        $jscategories = $this->CommonModel->jsCategories(0);
        $data = array('categories' => $categories, 'jscategories' => $jscategories);
    }

    public function search_product_post() {
        $pieces = explode(",", $_POST['searchproduct']);
        //echo "<pre>";print_r($pieces);die;
        
    }
    public function searchProduct_post() {
     $subcategory =$this->input->post('category');
     
     echo   $subcatserach = implode(",",$subcategory);
       
     die;
        $last_id = $this->input->post('last_id');
        $category_id = $this->input->post('category_id');
        $criteria = $this->input->post('criteria');

        $products = $this->ProductModel->all_products($category_id, $last_id, $criteria);

        if ($products) {
            $grid = '';
            foreach ($products as $p) {
                $price = $p->product_price + $p->product_discount;

                $grid .= '<div class="col-sm-6 col-md-4 wow fadeInUp height">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="detail.html"><img  src="' . $p->product_image . '" alt=""></a> </div>
                          <!-- /image -->
                          
                          <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /product-image -->
                        
                        <div class="product-info text-left">
                          <h3 class="name"><a href="detail.html">' . substr($p->product_name, 0, 80) . '</a></h3>
                          <div class="rating rateit-small" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="' . $p->average_rating . '"></div>
                          <div class="description"></div>
                          <div class="product-price"> <span class="price">' . $p->product_price . '</span> <span class="price-before-discount">$' . $price . '</span> </div>
                          <!-- /product-price --> 
                          
                        </div>
                        <!-- /product-info -->
                        <div class="cart clearfix animate-effect">
                          <div class="action">
                            <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                <button onclick="add_to_cart(' . $p->product_id . ',' . substr($p->product_name, 0, 80) . ',' . $p->product_image . ',' . $p->product_price . ')" class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                              </li>
                              <li class="lnk wishlist"> <a class="add-to-cart" href="detail.html" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                              <li class="lnk"> <a class="add-to-cart" href="detail.html" title="Compare"> <i class="fa fa-signal"></i> </a> </li>
                            </ul>
                          </div>
                          <!-- /action --> 
                        </div>
                        <!-- /cart --> 
                      </div>
                      <!-- /product --> 
                      
                    </div>
                    <!-- /products --> 
                  </div>';
            }
            $data = array('status' => true, 'message' => 'Found', 'data' => $grid, 'last_id' => $p->product_id);
            $this->response($data, parent::HTTP_OK);
        } else {
            $data = array('status' => false, 'message' => '<div class="no_products col-md-12"><i class="fa fa-info-circle"></i> No more products in this category</div>', 'data' => '');

            $this->response($data, parent::HTTP_INTERNAL_SERVER_ERROR);
        }
     
     
     
        
    }

=======
										</li>'; 
				}
			}else{
				$html = '';
			}

			$load['wishlist'] = array('data'=>array('product_id'=>'','last_id'=>'') ,'html' => $html);
		}	

		if($modal != null){
			$this->load->view("front-2/modals/".$modal.".tpl", $load);		
		}		
	}
	
	/**
	* Author: Himanshu
	*	Adds to removes products from the wishlist
	*
	*	@param int flag (0=>remove from wishlist, 1=>add to wishlist) 
 	*/
	public function wishlist_post($flag){

		$currentUrl = str_replace(site_url(),'',current_url());
		$user_id = $this->is_logged_in($currentUrl)['user_id'];		

		$data = array('product_id'=>$product_id,'user_id'=>$user_id);

		if($flag == 1){
			$stat = $this->CommonModel->insert_data('wishlist',$data);
			$message = $stat == true ? 'Added to wishlist': 'Something went wrong!';
		}

		if($flag == 0){
			$stat = $this->CommonModel->delete_data('wishlist',$data);
			$message = $stat == true ? 'Removed from wishlist': 'Something went wrong!';
		}

		if($stat == true){
			$response = array('status' => true,'message'=> $message);
			$this->response($response, parent::HTTP_OK);
		}else{
			$response = array('status' => false,'message'=> $message);
			$this->response($response, parent::HTTP_INTERNAL_SERVER_ERROR);
		}


	} 
	
	public function product_comparison_get($id='') 
	{
		$currentUrl = str_replace(site_url(),'',current_url());
		$userid = $this->is_logged_in($currentUrl());
		if(!$userid)
		{
			redirect("Products");
		}else{				
			$id =  base64_decode($id);
			$data = array();	  
			$seo = array();
			$productid = '';
			if(!empty($id) && is_array($this->session->userdata('product')) && !in_array($id,$this->session->userdata('product'),true) && is_numeric($id))
			{
				if(count($this->session->userdata('product')) >= 4)
				{		
					if(isset($_SESSION['product'])){
						$_SESSION['product'][].=$id;
					} else {
						$_SESSION['product'][]=$id;
					}						
					$productid = array('product'=>array_slice($this->session->userdata('product'), -4, 4));
				}else{
					if(isset($_SESSION['product'])){
						$_SESSION['product'][].=$id;
					} else {
						$_SESSION['product'][]=$id;
					}
					$productid = $_SESSION;		
				}
				$this->session->set_userdata($productid); 	
			}else if(!empty($id) && is_numeric($id)){
				$_SESSION['product'][]=$id;
				$productid = $_SESSION;	
			}			
			$product = $this->session->userdata('product');
				if(!empty($product)){
					$pid = '';
					foreach($product as $key=>$proid) 
					{
						$pid .= $proid.",";
						$pro_id	=  rtrim($pid,","); 		
					}			
				}else{
					$pro_id ="''";
					}	
			$data['productlist'] = $this->db->query("SELECT * FROM `products` WHERE `product_id` IN ($pro_id)")->result();					
			$seo['url'] = site_url("Information");
			$seo['title'] = 'Product Comparison';
			$seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
			$seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
			$data['data']['seo'] = $seo;
			$data['layout'] = $this->frontLayout($data);
			$this->load->view("front-2/product_comparison.tpl", $data);
		} 
	}
	public function remove_product_get($pid)
	{
		$data = array();
		$productid =  $this->session->userdata('product');
		if(($key = array_search($pid, $productid)) !== false) {
			unset($_SESSION['product'][$key]);
		}
		redirect('Products/product_comparison');
	}
	
	public function show_categories_get()
	{
		$this->load->helper('category');
		$categories = $this->CommonModel->Categories(0);
		$jscategories = $this->CommonModel->jsCategories(0);
		$data = array('categories'=>$categories,'jscategories'=>$jscategories);		
	}	
	
	public function search_product_post()
	{
		$pieces = explode(",", $_POST['searchproduct']);
		//echo "<pre>";print_r($pieces);die;
	}
>>>>>>> 309c58dc511fb29bfc29e96298f11d54b16bef5a
}
