<?php defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

class Test extends My_Controller {

	public function __construct() 
	{
		parent::__construct();
		
	}
	
	public function index_get()
	{
		$r = $this->generate_token( "1" , "abhishek.lal.in@gmail.com" );
		print_r( $r );
	}
	public function d_get($t)
	{
		$r = $this->decode($t);
		print_r( $r );  
	}
	
	
	public function m_get(){
		$this->getUser(1);
	}
	
}