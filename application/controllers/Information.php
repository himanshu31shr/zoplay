<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Information extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('InformationModel');
        $this->load->model('BillingModel');
        $this->load->model('ProductModel');
        $this->load->model('Seller_model');
        $this->load->model('CommonModel');		
				$this->load->library('pdf');
        if (isset($_COOKIE['language'])) {
            $this->lang->load($_COOKIE['language'] . "_landing", $_COOKIE['language']);
        } else {
            $this->lang->load('english_landing', 'english');
        }		
		
    }
	// function is_logged_in()
	// {
	// 	$userinfo = $this->session->userdata("user");
	// 	return (isset($userinfo))?true:false; 
	// }
    public function index_get() {		
        
      redirect('home', 'refresh');
	} 
				
	
	public function home_get()
	{
		$data = array();
    $seo = array();

    $currentUrl = str_replace(site_url(),'',current_url());
		//$this->is_logged_in($currentUrl);	

    $seo['url'] = site_url("Information");
    $seo['title'] = lang('logintext') . " - " . WEBSITENAME;
    $seo['metatitle'] = lang('textmetatitle') . " - " . WEBSITENAME;
    $seo['metadescription'] = lang('textmetadescription') . " - " . WEBSITENAME;
    $data['data']['seo'] = $seo;

    //getting most viewed products
		$data['most_viewed'] = 	$this->front_get('most_viewed');	
		$data['most_recent'] = 	$this->front_get('most_recent');	
		$data['most_ordered'] = 	$this->front_get('most_ordered');	
		$data['popular_shops'] = 	$this->InformationModel->most_popular_shops();	
                $data['banner'] = 	$this->CommonModel->getAll('banners',array('status'=>1));
		$data['featured_products'] = $this->ProductModel->getFeaturedProducts();
                

		$data['layout'] = $this->frontLayout2($data);
    $data['layout'] = $this->frontLayout($data);
		$this->load->view("front-2/home.tpl", $data);
	}
	
	public function contact_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/contact.tpl", $data);
	}

	public function viewallproduct_get($id ="")
	{
		$data = array(); 
		$data['most_viewed'] = 	$this->front_get('most_viewed');	
		$data['most_recent'] = 	$this->front_get('most_recent');	
		$data['most_ordered'] = 	$this->front_get('most_ordered');	
		$data['popular_shops'] = 	$this->InformationModel->most_popular_shops();
		$data['banner'] = 	$this->CommonModel->getAll('banners',array('status'=>1));
		$data['featured_products'] = $this->ProductModel->getFeaturedProducts();
		$data['allpro'] = base64_decode($id);
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/allproductlist.tpl", $data);
	}    
	
	public function category_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/category.tpl", $data);
	}

	// public function blog_get()
	// {
	// 	$data = array();
	// 	$data['layout'] = $this->frontLayout($data);
 //        $this->load->view("front-2/blog.tpl", $data);
	// }

	// public function blog_details_get()
	// {
	// 	$data = array();
	// 	$data['layout'] = $this->frontLayout($data);
 //        $this->load->view("front-2/blog_details.tpl", $data);
	// }

	public function checkout_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/checkout.tpl", $data);
	}
	public function detail_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/detail.tpl", $data);
	}
	public function faq_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/faq.tpl", $data);
	}
	// public function my_wishlist_get()
	// {
	// 	$data = array();
	// 	$data['layout'] = $this->frontLayout($data);
 //        $this->load->view("front-2/my_wishlist.tpl", $data);
	// }
	
	public function shopping_cart_get()
	{
		$data = array();
		
    $seo['url'] = site_url("Admin/dashboard");
    $seo['title'] = lang('welcometext') . " - " . WEBSITENAME;
    $seo['metatitle'] = lang('welcomemetatitle') . " - " . WEBSITENAME;
    $seo['metadescription'] = lang('welcomemetadescription') . " - " . WEBSITENAME;

		$data['products'] = $this->BillingModel->get_allProduct();
		$data['layout'] = $this->frontLayout($data);
    $this->load->view("front-2/shopping_cart.tpl", $data);
	}

	public function term_conditions_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/term_conditions.tpl", $data);
	}
	public function track_orders_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/track_orders.tpl", $data);
	}
	public function profile_get()
	{
		$data = array();
		$data['layout'] = $this->frontLayout($data);
        $this->load->view("front-2/profile.tpl", $data);
	}

	public function sellerinfo_get($seller_id=null)
	{
		$seller_id = base64_decode($seller_id);

		$data = array();
		$data['layout'] = $this->frontLayout($data);
		$seller = $this->CommonModel->select_data_where('shops', array('shop_id' => $seller_id))->result();
		if($seller){
			$products = $this->Seller_model->product_display($seller[0]->seller_id);

			if($products){	
				$i=0;
				foreach ($products as $p) {
					if($this->session->userdata('user')){
						$wishlist = $this->CommonModel->select_data_where('wishlist', array('product_id' => $p->product_id, 'user_id'=>$user_id))->result();

						if($wishlist){
							$products[$i]->added_to_wish = 1;
						}else{
							$products[$i]->added_to_wish = 0;
						}
					}

					$this->db->select('count(review_id) as total_reviews, sum(rating) as total_rating')
													->from('reviews')
													->where('product_id', $p->product_id)->limit(100);
					$reviews_count = $this->db->get()->result();
					//	print_r($reviews_count);die;
					$products[$i]->reviews_count = $reviews_count[0]->total_reviews;
					$products[$i]->total_rating = $reviews_count[0]->total_rating;
					$products[$i]->average_rating = 0;
					if($products[$i]->reviews_count > 0){
						$products[$i]->average_rating = $products[$i]->total_rating/$products[$i]->reviews_count;
					}
					$i++;
				}
				$data['products'] = $products;
			}else{
				$data['products']= 'No products under this category!';
			}
			$data['shop']= $seller[0];
		}else{
			$data['shop'] = '';
			$data['products'] = '';
		}
		//$data['products']->featured_products = $this->CommonModel->select_data_where('shops', array('seller_id' => $seller_id));
    $this->load->view("front-2/SellerView.tpl",$data);
	}

  public function get_more_seller_products_post(){
      	
    $data = array();
		//last id
		$last_id = $this->input->post('last_id');
		$seller_id = $this->input->post('seller_id');

		$data['layout'] = $this->frontLayout($data);
		$products = $this->Seller_model->product_display($seller_id,$last_id);


		if($products){	
			$i=0;
			foreach ($products as $p) {
				if($this->session->userdata('user')){
					$wishlist = $this->CommonModel->select_data_where('wishlist', array('product_id' => $p->product_id, 'user_id'=>$user_id))->result();

					if($wishlist){
						$products[$i]->added_to_wish = 1;
					}else{
						$products[$i]->added_to_wish = 0;
					}
				}

				$this->db->select('count(review_id) as total_reviews, sum(rating) as total_rating')
												->from('reviews')
												->where('product_id', $p->product_id);
				$reviews_count = $this->db->get()->result();
				//	print_r($reviews_count);die;
				$products[$i]->reviews_count = $reviews_count[0]->total_reviews;
				$products[$i]->total_rating = $reviews_count[0]->total_rating;
				$products[$i]->average_rating = 0;
				if($products[$i]->reviews_count > 0){
				$products[$i]->average_rating = $products[$i]->total_rating/$products[$i]->reviews_count;
				}


				$i++;
			}
            	$data['products']['status'] = true;
    	
    	$html = '';
            if(count($products) > 0 ){
            	foreach($products as $p){
            				$count = $p->reviews_count == '' ? 0 : $p->reviews_count;
                $html .= '<div class="col-sm-3 col-lg-3 col-md-3">
                  <div class="thumbnail">
                   <a href="'.site_url().'product/'.md5($p->product_id).'"> <div class="img-container image_size">
                      <img class="img-responsive" src="'.image_url($p->product_image).'"/>
                    </div>
                    </a>
                  
                    <div class="caption" >
                    <div class="prosell">
                      <h4 class="text-capitalize" ><a href="'.site_url().'product/'.md5($p->product_id).'">'.$p->product_name.'</a></h4>
                      <div class="rating rateit-small rateit" data-rateit-readonly="true" data-rateit-value="'.$p->average_rating.'"></div><small style="color: #ccc;font-size: 12px;" class="pull-right">Reviews('.$count.')</small>
                      <div>
                        <small class="text text-primary">Price: $'.$p->product_price.'</small>
                      </div>
                      </div></div>
                    <a href="'.site_url().'product/'.md5($p->product_id).'" class="btn btn-primary btn-block">Buy Now!</a>
                  </div>
                </div>';
    	   		}
    	   		$return = array('status' =>true, 'message'=> 'More products found', 'data'=> $html, 'last_id' => $p->product_id);
    	   		$this->response($return, parent::HTTP_OK);
        	}else{
    		  	$html .= '<div class="col-sm-12 col-lg-12 col-md-12 ">
    		  	<div class="alert alert-dismissible alert-warning text-center">
										No products to display!
										</div></div>';
		    		$return = array('status' =>false, 'message'=> 'No more products found', 'data'=> $html, 'last_id' => 0);
		    		$this->response($return, parent::HTTP_INTERNAL_SERVER_ERROR);
    	    }
    	}else{
    		$html = '<div class="col-sm-12 col-lg-12 col-md-12">
    		<div class="alert alert-dismissible alert-warning text-center">
										No more products!
										</div></div>';
    		$return = array('status' =>false, 'message'=> 'No more products found', 'data'=> $html, 'last_id' => 0);
    		$this->response($return, parent::HTTP_INTERNAL_SERVER_ERROR);
    	}
       
   

}
    public function logout_get() {
        $this->session->sess_destroy();
        header("Location: " . base_url());
    }

	public function categories_get($id)
	{
		$this->CommonModel->jsCategories($id);	
	}
	
	function mypdf_get() 
	{
		$customPaper = array(0,0,800,800);
		$this->pdf->set_paper($customPaper);
		$this->pdf->load_view('demo.php');	
		$this->pdf->render();
		$this->pdf->stream("invoice.pdf");
		$output = $this->pdf->output();
		$file_location = $_SERVER['DOCUMENT_ROOT']."/zoplay/files/"."invoice".".pdf";	
		file_put_contents($file_location, $output);				
	}


	public function front_get($filter){
		
		//if($filter = 'most_viewed'){
			$detials = $this->InformationModel->{$filter}();
		//}else if($filter = 'most_recent'){
			//$detials = $this->InformationModel->most_recent();
		//}
    $i = 0;
    foreach($detials as $mv){
    	$product_id = $mv->product_id;

    	$detials[$i]->product_image = image_url($mv->product_image);
			//checking if deal is activated
			$deal = $this->AdminModel->getDealsOfTheDay($product_id);
			if($deal['status'] == 'success'){

				$detials[$i]->is_deal_active = 1;
				$detials[$i]->discounted_price = (float)$detials[$i]->product_price  - (float)$detials[0]->product_price* (float)$detials[$i]->product_discount/100;
			}else{
				$detials[$i]->is_deal_active = 0;
				$detials[$i]->discounted_price = $detials[$i]->product_price;
			}

			$deal = $this->AdminModel->getDealsOfTheDay($product_id);
			if($deal['status'] == 'success'){

				$detials[$i]->is_deal_active = 1;
				$detials[$i]->discounted_price = (float)$detials[$i]->product_price  - (float)$detials[$i]->product_price* (float)$detials[0]->product_discount/100;
			}else{
				$detials[$i]->is_deal_active = 0;
				$detials[$i]->discounted_price = $detials[$i]->product_price;
			}

			$reviews = $this->ProductModel->reviews($mv->product_id);

			if($reviews){
				$num = $reviews[0]->total_reviews;
				$sum = $reviews[0]->total_rating;
				$average = $num != 0 ? (float) $sum/$num : 0;
				$detials[$i]->average_rating = $average;
				$detials[$i]->total_reviews = $num;
				
			}else{
				$detials[$i]->average_rating = 0;
				$detials[$i]->total_reviews = 0;
			}

			if($this->session->userdata('user')){
				$user_id = $this->is_logged_in()['user_id'];

					$wishlist = $this->CommonModel->select_data_where('wishlist', array('product_id' => $mv->product_id, 'user_id'=>$user_id))->result();

					if($wishlist){
						$detials[$i]->added_to_wish = 1;
					}else{
						$detials[$i]->added_to_wish = 0;
					}

			}else{

					$detials[$i]->added_to_wish = 0;
			}
				$i++;
    }
		
		return $detials;
	}

	public function suggested_products_post(){
		if($this->input->post('category_id')){
			$cat_id = $this->input->post('category_id');
			$ret = $this->InformationModel->suggested_posts($cat_id);

			if($ret){
				$i=0;
				foreach($ret as $r){
					$image = image_url($r->product_image);
					$html[$i]= '    	
								<div class="item item-carousel">
									<div class="products">
										
										<div class="product">		
											<div class="product-image">
												<div class="image">
													<a href="detail.html"><img src="'.$image.'" alt=""></a>
												</div><!-- /.image -->			

											</div><!-- /.product-image -->
								
											<div class="product-info text-left">
												<h3 class="name text-capitalize"><a href="detail.html">'.$r->product_name.'</a></h3>
												<div class="description">'.substr($r->product_description, 0, 80).'</div>

												<div class="product-price">	
													<span class="price">
														$'.$r->product_price.'
													</span>																		
												</div><!-- /.product-price -->
												
											</div><!-- /.product-info -->
										</div><!-- /.product -->
						      
									</div><!-- /.products -->
								</div><!-- /.item -->';
								$i++;
				}

				$this->response(array('status'=>true, 'message' => 'Related products found', 'data' => $html), parent::HTTP_OK);
			}else{
				$this->response(array('status'=>false, 'message' => '<i class="fa fa-meh-o"></i> Oh Snap! Something went wrong.', 'data' => ''), parent::HTTP_INTERNAL_SERVER_ERROR);
			}
		}else{
			$this->response(array('status'=>false, 'message' => '<i class="fa fa-meh-o"></i> No related products found!', 'data' => ''), parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}


?>