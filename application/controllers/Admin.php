<?php defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */



class Admin extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'language', 'url'));
        $this->load->model('InformationModel');

        $this->load->model('AdminModel');

        $this->load->model('CommonModel');
        if (isset($_COOKIE['language'])) {
            $this->lang->load($_COOKIE['language'] . "_landing", $_COOKIE['language']);
        } else{
            $this->lang->load('english_landing', 'english');	
        }
        
        $session = $this->session->userdata('admin');		
        if(!isset($session) && $session==''){
            redirect('AdminLogin');
        }
    }
    public function index_get() {
		
        $data = array();
        $seo = array();
        $data['layout'] = $this->adminLayout($data);
        $this->load->view("admin/index.tpl", $data);
    }
	
	public function login_get() 
	{
            $data = array();
            $this->load->view("admin/login.tpl", $data);
        }
	
	/**
	*	Author: Himanshu
	*
	*	Authenticates admin and initializes session
	*/
	public function doLogin_post() 
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE)
			{
			$val = array(
				'status' => FALSE,
				'message'	=>'<i class="fa fa-close"></i> Error in validating fields',
				'errorMsg' => array(
					'email' => form_error('email') ,
					'password' => form_error('password')
				)
			);

			// echo json_encode($val);
			// Set the response and exit

			$this->response($val); // INTERNAL SERVER ERROR (500) being the HTTP response code
			}
		  else
			{
			$email = $this->input->post('email');
			$password = md5($this->input->post('password')); //Later replace this with SALT
			$role = 'admin'; //setting role for admin
			$user_data = $this->CommonModel->doLogin($email, $password, $role);
			if ($user_data == false)
				{
				$val = array(
					'status' => FALSE,
					'message' => '<i class="fa fa-close"></i> Invalid Email and Password Combination',
					'data' => '',
				);

				// Set the response and exit

				$this->response($val, RestserverLibrariesREST_Controller::HTTP_INTERNAL_SERVER_ERROR); 
				// INTERNAL SERVER ERROR (500) being the HTTP response code

				}
			  else
				{
				/*$val = array(
				"success"=> "<i class='fa fa-check'></i> Logged in successfully!",
				);*/
				
				$userdata = array(
					'userData' => $user_data
				);
				$val = array(
					"status" => TRUE,
					"message" => "<i class='fa fa-check'></i> Logged in successfully!",
					"data" => $userdata,
				);
				/* creating session data */
				$this->session->set_userdata('admin', $userdata);
				$this->response($val, RestserverLibrariesREST_Controller::HTTP_OK); // OK (200) being the HTTP response code
				}
			}

	}
	
    public function userdashboard_get() {
       $data = array();
	   $where=array('status'=>'1','role'=>'user');
	   $wh=array('status'=>'0','role'=>'user');
	   $data['usercount'] = json_encode($this->CommonModel->select_data_where('users',array('role'=>'user'))->num_rows());	   
	   $data['activeUser'] = json_encode($this->CommonModel->select_data_where('users',$where)->num_rows());
	   $data['inactiveUser'] = json_encode($this->CommonModel->select_data_where('users',$wh)->num_rows());
	   $data['userlist'] = json_encode($this->CommonModel->select_data_where('users',array('role'=>'user'))->result());
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/userdashboard.tpl", $data);        
    }
    
    public function userlist_get(){
       $data = array();
	   $where=array('role'=>'user');	   
	   $userList = $this->CommonModel->select_data_where('users',$where)->result();	   
           if(count($userList) > 0)
	   { 
		   $arr = array(
				'status' => TRUE,
				'message' => 'User list render successfully',
				'data' => $userList
		   );
	   }else{
		   $arr = array(
				'status' => FALSE,
				'message' => 'No record found!',
				'data' => ''
		   );
	   }
	   $data['userData'] = json_encode($arr);	   
	   $data['layout'] = $this->adminLayout($data);
           $this->load->view("admin/userlist.tpl", $data);        
    }
	
	public function logout_get() {
              //  echo "akki" ; die;
				   
			$this->session->sess_destroy(); 
            redirect('AdminLogin');			
    }
	
	
	/**
	*	Author: Himanshu
	*
	*	Dashboard for seller
	*/
    public function dashboard_get() 
	{
        $data = array();
		 
		/* get list of pending users */
		$where = array('admin_approved_status' => 0, 'role'=>'seller');
		$pusers = $this->CommonModel->sellerList($where);
		$counts= $this->CommonModel->sellerList(array('role'=>'seller'));
		
		$data['pending_users'] = json_encode($pusers);
		$data['total_seller'] = json_encode(count($counts));
        $data['layout'] = $this->adminLayout($data);
        $this->load->view("admin/seller/dashboard.tpl", $data);
    }
	
	/**
	*	Author: Himanshu
	*
	*	updates seller current flag as active or inactive
	*	@params string flag, int id
	*/
	public function seller_upd_post($flag, $id)
	{
		echo $this->AdminModel->seller_upd($flag, $id);
	}
	
	/**
	*	Author: Himanshu
	*
	*	Generates list of all sellers
	*/
    public function sellerlist_get() 
	{
       $data = array();
       $data['layout'] = $this->adminLayout($data);
	   $data['all_sellers'] = $this->CommonModel->get_record('users',['role'=>'seller'],['id'=>'desc']);
       $this->load->view("admin/seller/sellerlist.tpl", $data);
        
    }
	
	/**
	*	Author: Himanshu
	*
	*	Fulle details of seller
	*/
	public function viewseller_get($id) 
	{
       $data = array();
       $data['layout'] = $this->adminLayout($data);
	   $sdetails = $this->CommonModel->sellerDetails(array('role'=>'seller', 'id' =>$id));
	   $data['seller'] = json_encode($sdetails);
       $this->load->view("admin/seller/viewseller.tpl", $data);
        
    }
	
    public function paiddetails_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/seller/paiddetails.tpl", $data);
        
    }
	
	/**
	*	Author: Himanshu
	*
	*	Generates list of all transactions and orders of all sellers
	*/
	public function siteearning_get() 
	{
        $data = array();
        $data['layout'] = $this->adminLayout($data);
		$seller = $this->AdminModel->sitearnings();				
		
	    if($seller){
			$i=0;
			foreach($seller as $s){
				$seller[$i]->total_transactions = count($s->transactions);
				$seller[$i]->total_orders = count($s->orders);
				
				$txn = $s->transactions;
				$ord = $s->orders;
				
				$tp_txn = 0;
				$tp_ord = 0;
				foreach($txn as $t)
				{
					$tp_txn = $t->total_price + $tp_txn;
				}
				
				foreach($ord as $t)
				{
					$tp_ord = $t->total_price + $tp_txn;
				}
				
				$seller[$i]->tp_txn = $tp_txn;
				$seller[$i]->tp_ord = $tp_ord;
				$i++;
			}
			
		   $val = array(
				'status' => true,
				'message' => 'Data found',
				'data' => $seller,
			);
	    }else{
		   $val = array(
				'status' => false,
				'message' => 'No data found',
				'data' => '',
			);
	    }
		$data['seller'] = json_encode($val);
        $this->load->view("admin/seller/siteearning.tpl", $data);
    }
	
	/**
	*	Author: Himanshu
	*
	*	Generates list of all transactions and orders on COD of all sellers
	*/
	public function codearning_get() 
	{
       $data = array();
       $data['layout'] = $this->adminLayout($data);
	   
	   $seller = $this->AdminModel->sitearnings(0);								// 0: for COD
		
	    if($seller){
			$i=0;
			foreach($seller as $s){
				$seller[$i]->total_transactions = count($s->transactions);
				$seller[$i]->total_orders = count($s->orders);
				
				$txn = $s->transactions;
				$ord = $s->orders;
				
				$tp_txn = 0;
				$tp_ord = 0;
				foreach($txn as $t)
				{
					$tp_txn = $t->total_price + $tp_txn;
				}
				
				foreach($ord as $t)
				{
					$tp_ord = $t->total_price + $tp_txn;
				}
				
				$seller[$i]->tp_txn = $tp_txn;
				$seller[$i]->tp_ord = $tp_ord;
				$i++;
			}
			
		   $val = array(
				'status' => true,
				'message' => 'Data found',
				'data' => $seller,
			);
	    }else{
		   $val = array(
				'status' => false,
				'message' => 'No data found',
				'data' => '',
			);
	    }
		$data['seller'] = json_encode($val);
	   
       $this->load->view("admin/seller/codearning.tpl", $data);
        
    }
	public function multilanguage_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/language_management.tpl", $data);
        
    }
	public function product_feedback_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/reviews/display_product_feedback.tpl", $data);
        
    }
	public function shop_feedback_report_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/reviews/shop_feedback_report.tpl", $data);
        
    }
	public function display_contact_seller_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/contact/display_contact_seller.tpl", $data);
        
    }
	public function display_askquestion_seller_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/contact/display_askquestion_seller.tpl", $data);
        
    }
	public function display_chat_offer_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/contact/display_chat_offer.tpl", $data);
	}
	public function doc_varification_get() {
       $data = array();
	   $query = $this->db->select('*')
					->from('seller_details')
					->get();		
	   $seller_details = $query->result();			
	   foreach($seller_details as $sd){
		   $seller = $this->CommonModel->select_data_where('users',array('id'=>$sd->seller_id))->result();		   
		   foreach($seller as $sell)
		   {
			   $sd->full_name = $sell->full_name;
			   $sd->email = $sell->email;
		   }		   
	   }
	   $data['seller_details'] = $seller_details;
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/seller/doc_verification.tpl", $data);
	}


    public function adduser_post() {
       $data = array();       	   
	  
	    
		   	$file_name = $_FILES['image']['name'];
			if(!empty($file_name))
			{
					$config['upload_path'] = 'assets/uploads/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['overwrite'] = TRUE;					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$profile_name=$file_name;					
				if(!$this->upload->do_upload('image',$config)) 
				{
					echo $this->upload->display_errors(); 
				}
			}
                          $ip_address= $_SERVER['REMOTE_ADDR'];
                          
		    $user_data = array(
                                        'user_name'	=>trim($_POST['user_name']),
                                        'full_name'	=>trim($_POST['full_name']),
                                        'email'		=>trim($_POST['email']),
                                        'password'	=> trim(md5($_POST['password'])),
                                        'status'	=>trim($_POST['userstatus']),
                                        'role'		=>'user',
                                        'ip_address'=>	$ip_address,
                                        'picture'	=>$profile_name
                                        );
			$this->CommonModel->insert_data('users',$user_data);			
	   
	   $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/adduser.tpl", $data);        
    }

	public function adduser_get($id = "") {
		$data = array();
		$userid = base64_decode($id);
		$where = array('id'=>$userid);
		$data['user_data'] = $this->CommonModel->select_data_where('users',$where)->result();
		$data['layout'] = $this->adminLayout($data);
		$this->load->view("admin/adduser.tpl", $data);
	}	
	
	 public function updateuser_post($id="") {
       $data = array();       	   
	  
	    
		   	$file_name = $_FILES['image']['name'];
			if(!empty($file_name))
			{
					$config['upload_path'] = 'assets/uploads/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['overwrite'] = TRUE;					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$profile_name=$file_name;					
				if(!$this->upload->do_upload('image',$config)) 
				{
					echo $this->upload->display_errors(); 
				}
			}
                          $ip_address= $_SERVER['REMOTE_ADDR'];
                          
		    $user_data = array(
                                        'user_name'	=>trim($_POST['user_name']),
                                        'full_name'	=>trim($_POST['full_name']),
                                        'email'		=>trim($_POST['email']),
                                        'password'	=> trim(md5($_POST['password'])),
                                        'status'	=>trim($_POST['userstatus']),
                                        'role'		=>'user',
                                        'ip_address'=>	$ip_address,
                                        'picture'	=>$profile_name
                                        );
		    $userid = base64_decode($id);
		$where = array('id'=>$userid);
			$this->CommonModel->update_data('users',$user_data, $where);			
	   
	        
    }

	public function viewUser_get($id)
	{
	   $data = array();
	   $where = array('id'=>base64_decode($id));
	   $ud = $this->CommonModel->select_data_where('users',$where)->result();
	   $data['user_data'] = json_encode($ud);
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/viewuser.tpl", $data);
	}
     public function shoplist_get() {
        $data = array();
       $data['shoplist'] = $this->CommonModel->get_shops('shops','',['shop_id'=>'DESC']);
	  
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/shoplist.tpl", $data);
        
    }
    public function addfeaturepackage_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/addfeaturepackage.tpl", $data);
    }
    public function featurepackagelist_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/featurepackagelist.tpl", $data);
    }
    public function viwpackage_get($id) {
        $data = array();
        $where = array('feature_package_id'=>base64_decode($id));
        $ud = $this->CommonModel->select_data_where('feature_packages',$where)->result();
        $data['user_data'] = json_encode($ud);
        $data['layout'] = $this->adminLayout($data);
        $this->load->view("admin/viwpackage.tpl", $data);
    }
    public function getPackageList_post(){
          $PackageList = $this->CommonModel->getPackageList();
    }
    public function payment_gateway_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
	   
	   $gateway = $this->CommonModel->select_all('payment_gateways')->result();
	   $data['gateways'] = json_encode($gateway);
	   
       $this->load->view("admin/payment_gateway.tpl", $data);
    }
	
	public function change_gateway_status_post($id, $flag){
		
		if($this->CommonModel->update_data('payment_gateways',array('payment_gateway_status'=>$flag), array('payment_gateway_id'=>$id))){
			
			$arr = array(
				'status' => TRUE,
				'message' => 'Payment mode status changed',
				'data' => $id
			);
		}else{
			$arr = array(
				'status' => false,
				'message' => 'Unable to change the payment mode status',
				'data' => ''
			);
		}
		echo json_encode($arr);
	}
	
	public function del_gateway_post($id){
		$where = array(
			'payment_gateway_id'	=> $id
		);
		$stat = $this->CommonModel->delete_data('payment_gateways', $where);
		
		if($stat == true){
			$arr = array(
				'status' => TRUE,
				'message' => 'Payment mode deleted successfully',
				'data' => $id
			);
		}else{
			$arr = array(
				'status' => false,
				'message' => 'Unable to delete the payment mode',
				'data' => ''
			);
		}
		echo json_encode($arr);
	}
	
	/**
	*	Author: Himanshu
	*  	Defines a new payment gateway in db-
	*	@return json object
	*/
	public function add_gateway_post(){
		
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('alias', 'Alias', 'trim|required');

		if ($this->form_validation->run() == FALSE){
			$arr = array(
				'status' => FALSE,
				'message'	=>'<i class="fa fa-close"></i> Error in validating fields',
				'errorMsg' => array(
					'alias' => form_error('email') ,
				)
			);
		}else{
			$alias = $this->input->post('alias');
			$data = array(
				'payment_gateway_name'	=> $alias,
				'payment_gateway_status'=> 0
			);
			$stat = $this->CommonModel->insert_data('payment_gateways', $data);
			
			
			if($stat == true){
				$data = $this->CommonModel->select_data_where('payment_gateways', array('payment_gateway_id' => $this->db->insert_id()))->result();
				
				$arr = array(
					'status' => TRUE,
					'message' => 'Payment mode added successfully',
					'data' => $data
				);
			}else{
				$arr = array(
					'status' => false,
					'message' => 'Unable to add the payment mode',
					'data' => ''
				);
			}
		}
		echo json_encode($arr);
	}
	
	public function list_pages_get() {
        $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/pages/list_of_pages.tpl", $data);
    }
	public function product_complaints_list_get() {
        $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/complaints/product_complaints_List.tpl", $data);
    }
	public function shop_complaints_list_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/complaints/shop_complaints_list.tpl", $data);
    }
	
	public function currency_list_get() {
       $data = array();	   
	   $currencyList = $this->CommonModel->select_all('currencies')->result();	   
	   if(count($currencyList) > 0)
	   { 
		   $arr = array(
				'status' => TRUE,
				'message' => 'Currency list render successfully',
				'data' => $currencyList
		   );
	   }else{
		   $arr = array(
				'status' => FALSE,
				'message' => 'No record found!',
				'data' => ''
		   );
	   }
	   $data['currencyData'] = json_encode($arr);
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/currency/currency_list.tpl", $data);
    }
	public function view_currency_get($id)
	{
	   $data = array();
	   $where = array('currency_id'=>base64_decode($id));
	   $currency_data = $this->CommonModel->select_data_where('currencies',$where)->result();	   
	   if(count($currency_data) > 0)
	   { 
		   $arr = array(
				'status' => TRUE,
				'message' => 'Currency list render successfully',
				'data' => $currency_data
		   );
	   }else{
		   $arr = array(
				'status' => FALSE,
				'message' => 'No record found!',
				'data' => ''
		   );
	   }
	   $data['currency_data'] = json_encode($arr);
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/currency/viewcurrency.tpl", $data);
	}
	public function add_currency_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/currency/Add_currency.tpl", $data);
    }
	
	
	public function subscribers_list_get() {
        $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/newsletter/subscribers_list.tpl", $data);
    }
	
	public function email_template_list_get() {
        $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/newsletter/email_template_list.tpl", $data);
    }
	public function add_email_template_get() {
        $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/newsletter/add_email_template.tpl", $data);
        }
    public function addproduct_get( $param = null  ) {
       $data = array();
	   if( $param != null){ 
	   $prod_data = $this->db->query("SELECT * FROM products WHERE product_id = $param");
		   if(!empty($prod_data->result_array())){
			   $data['product_data'] =   $prod_data->result_array()[0];
			     $data['layout'] = $this->adminLayout($data);
				$this->load->view("admin/addproduct.tpl", $data); 
		   }else{
			   echo "PRODUCT_NOT_FOUND";
		   }
	   }else{
		    $data['layout'] = $this->adminLayout($data);
				$this->load->view("admin/addproduct.tpl", $data); 
	   }
       
	   
     
    }
    public function productlist_get() {
        $data = array();
		$data['layout'] = $this->adminLayout($data);
	   
		$list = $this->AdminModel->productList();
		
		if($list){
			$data['list'] = json_encode($list);
		}
		
		$this->load->view("admin/productlist.tpl", $data);
    }
    public function recentproductlist_get() {
      
		 $data = array();
		$data['layout'] = $this->adminLayout($data);
	   
		$list = $this->AdminModel->productList("recent");
		
		if($list){
			$data['list'] = json_encode($list);
		}
		
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/recentproductlist.tpl", $data);
    }
     public function deleteproductlist_get() {
        $data = array();
		 $data = array();
		$data['layout'] = $this->adminLayout($data);
	   
		$list = $this->AdminModel->productList("deleted");
		
		if($list){
			$data['list'] = json_encode($list);
		}
		
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/deleteproductlist.tpl", $data);
    }

   /*   public function affiliatesettings_get()
	{
		$data = array();
		$data['layout'] = $this->adminLayout($data);
		$this->load->view("admin/affiliatesettings.tpl", $data);
 */
     public function affiliatesettings_get() {
        $data = array();
        $data['layout'] = $this->adminLayout($data);
        $where = array('general_setting_status !='=>2);
        $data['affiliatesettings'] = $this->CommonModel->select_data_where('general_settings',$where)->row();
        $this->load->view("admin/affiliatesettings.tpl", $data);

    }
     public function deals_get() {
       $data = array();
       
	    $data['list']  = $this->AdminModel->getDealsOfTheDay();
	   
	   $data['layout'] = $this->adminLayout($data);
	   $this->load->view("admin/deals.tpl", $data);
    }
     public function dispute_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/dispute.tpl", $data);
    }
    public function mailchimp_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/mailchimp.tpl", $data);
    }
    public function constantcontact_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/constantcontact.tpl", $data);
    }
    public function zohocrm_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/zohocrm.tpl", $data);
    }
    public function paidpayment_get() {
       $data = array();
	   $orders = $this->CommonModel->select_all('orders')->result();
	   foreach($orders as $order){
		   $userdetails = $this->CommonModel->select_data_where('users',array('id'=>$order->user_id))->result();
		   foreach($userdetails as $users){
			   $order->email = $users->email;
		   }
	   }
	   $data['paidpayment'] = $orders; 
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/paidpayment.tpl", $data);
    }
    public function failedpayment_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/failedpayment.tpl", $data);
    }
    public function transferpayment_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/transferpayment.tpl", $data);
    }
    public function cashdelivery_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/cashdelivery.tpl", $data);
    }
     public function unionpayment_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/unionpayment.tpl", $data);
    }
     public function requestcancel_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/requestcancel.tpl", $data);
    }
     public function currentauctions_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/currentauctions.tpl", $data);
    }
     public function outdatedauctions_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/outdatedauctions.tpl", $data);
    }
    public function event_get() {
       $data = array();
	   $eventList = $this->CommonModel->select_all('community_events')->result();	   
	   if(count($eventList) > 0)
	   { 
		   $arr = array(
				'status' => TRUE,
				'message' => 'Community events list render successfully',
				'data' => $eventList
		   );
	   }else{
		   $arr = array(
				'status' => FALSE,
				'message' => 'No record found!',
				'data' => ''
		   );
	   }
	   $data['eventList'] = json_encode($arr);
	   $data['community_Event'] = $this->CommonModel->select_all('community_events')->result_array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/community/event.tpl", $data);
    } 
	public function add_event_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/community/Add_event.tpl", $data);
    }
	public function add_team_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/community/Add_team.tpl", $data);
    }
     public function teams_get() {
       $data = array();
	    $data['community_teams'] = $this->CommonModel->select_all('community_teams')->result_array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/community/teams.tpl", $data);
    }
     public function communitynews_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/community/communitynews.tpl", $data);
    }
     public function bannerlist_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $data['banners'] = $this->CommonModel->get_record('banners','',['banner_id'=>'desc']);
       $this->load->view("admin/bannerlist.tpl", $data);
    }
     public function addbanner_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
	   if(isset($_GET['banner'])){
		  $data['bannerinfo'] = $this->CommonModel->get_single_record('banners',['banner_id'=>$_GET['banner']]); 
	   }
       $this->load->view("admin/addbanner.tpl", $data);
    }
    public function variationslist_get() {
       $data = array();
	   $data['variation_data'] = $this->CommonModel->select_all('variations')->result();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/variationslist.tpl", $data);
    }
	public function view_variation_get($id)
	{
	   $data = array();
	   $id = base64_decode($id);
	   $where = array('variation_id'=>$id);
	   $data['variation_data'] = $this->CommonModel->select_data_where('variations',$where)->result();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/viewvariation.tpl", $data);
	}
    public function addvariations_get($id="") {
       $data = array();
	   $id = base64_decode($id);
	   $where = array('variation_id'=>$id);
	   $data['variation_data']=$this->CommonModel->select_data_where('variations',$where)->result();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/addvariations.tpl", $data);
    }
	 public function savevariations_post()
	 { 
		 if(is_numeric(base64_decode($_POST['variation_id'])))
		 {
			$status = (!empty($_POST['status'])=='on') ? '1' : '0';		 
			$where = array('variation_id'=>base64_decode($_POST['variation_id']));
			$data = array('attribute_name'=>$_POST['attribute_name'],
					   'scaling_options'=>$_POST['scaling_option'],
					   'attribute_options'=>$_POST['attribute_option'],
					   'variation_status'=>$status
					   ); 
			$res = $this->CommonModel->update_data('variations',$data,$where);
			$response = array("status"=>true , "msg" => "variations has been updated successfully" , "data" => array()); 
			$this->response( $response , parent::HTTP_OK);
		 }else
		 {
			 $status = (!empty($_POST['status'])=='on') ? '1' : '0';		 
			 $data = array('attribute_name'=>$_POST['attribute_name'],
					   'scaling_options'=>$_POST['scaling_option'],
					   'attribute_options'=>$_POST['attribute_option'],
					   'variation_status'=>$status
					   );
			 $res = $this->CommonModel->insert_data('variations',$data);	
			 $response = array("status"=>true , "msg" => "variations has been inserted successfully" , "data" => array()); 
			 $this->response( $response , parent::HTTP_OK);
		 }
	 }	 
     public function couponcodelist_get() {
       $data = array();
	   $data['coupon_data'] = $this->CommonModel->select_data_where('coupons',array('role'=>'admin'))->result();	   
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/couponcodelist.tpl", $data);
    }
     public function giftcardslist_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/giftcardslist.tpl", $data);
    }
    public function giftcardssetting_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/giftcardssetting.tpl", $data);
    }
    public function giftcarddasboard_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/giftcarddasboard.tpl", $data);
    }
	public function theme_list_get() {
       $data = array();
       $data['layout'] = $this->adminLayout($data);
       $this->load->view("admin/layout/theme_list.tpl", $data);
	}
    public function  category_get() {
		$this->load->helper('category');
		$categories = $this->CommonModel->Categories(0);
		$jscategories = $this->CommonModel->jsCategories(0);
		$data = array('categories'=>$categories,'jscategories'=>$jscategories);
		$data['layout'] = $this->adminLayout($data);
		$this->load->view("admin/category.tpl", $data);
    }	

	function mail_exists_post()
	{
		$where = array('email'=>$_POST['email']);
		$query = $this->CommonModel->select_data_where('users',$where)->num_rows();				
		if ($query > 0) {
			echo "1";
		} else {
			echo "0";
		}
	}
    public function adminLayout($data) {
            $this->template['header'] = $this->load->view('admin/includes/header.tpl', $data, true);
            $this->template['footer'] = $this->load->view('admin/includes/footer.tpl', $data, true);
            return $this->template;

    }
	
	
	 public function new_product_post() {
		$product_id = $this->input->post('product_id');
	  
	    $shipping_charges = array();
		
		if(!empty($_POST['shipping_cost_with'])){
			 
			 $shipping_cost_with = $_POST['shipping_cost_with'] ;
		}
		if(!empty($_POST['shipping_cost'])){
			 
			  $shipping_cost = $_POST['shipping_cost'] ;
		}
		if(!empty($_POST['shipping_to']))
		{
		  foreach($_POST['shipping_to'] as $key=> $ship_to)
			
			 $shipping_charges[] = array("country_id" => $ship_to , "shipping_cost" => $shipping_cost[$key] ,  "shipping_cost_with" => $shipping_cost_with[$key] );
		  
		} 
		
	   @$image = $this->input->post("filer")[0] ;

		if(!empty($image)){
				@$image = $this->input->post("filer")[0] ;
		}
		else{
			$image="";
		}
	
		$shipping_charges = serialize($shipping_charges);
	
		$data = array(
			"user_id" => "1" ,
			"product_name" => $this->input->post("product_title") ,
			"product_listing_maker" => $this->input->post("product_listing_maker") ,
			"product_listing_order" => $this->input->post("product_listing_order") ,
			"product_listing_use" => $this->input->post("product_listing_use") ,
			"category_id" => $this->input->post("product_categories") ,
			"product_pricing_format" => $this->input->post("pricing_format") ,
			"product_price" => $this->input->post("price") ,
			"shipping_from" => $this->input->post("shipping_from") ,
			"shipping_process_time" => $this->input->post("shipping_process_time") ,
			"shipping_charges" => $shipping_charges ,
			"shipping_options" =>$this->input->post("shipping_options") ,
			"starting_price" => $this->input->post("auction_starting_price") ,
			"duration" => $this->input->post("auction_duration") ,
			"item_type" => $this->input->post("item_type") ,
			"product_description" =>($this->input->post("product_desc")) ,
			"product_quantity" => $this->input->post("quantity") ,
			"deal_datetime_from" => $this->input->post("deal_from") ,
			"deal_datetime_to" => $this->input->post("deal_to") ,
			"product_discount" => $this->input->post("deal_discount") ,
			"is_featured" => "1" ,
			"product_variations" => serialize($this->input->post("variations")) ,		
			"search_tags" => $this->input->post("product_tags") ,
			"search_materials" => $this->input->post("material_tags") ,
			"product_images" => serialize($this->input->post("filer")) ,
			"product_image" => $image ,
		);
		 if( !empty(trim( $product_id ))){
			$this->db->where("product_id" , $product_id  ); 
		   $s =  $this->db->update("products" , $data);
		  $return = array("status"=>"success" , "msg" => "Changes Saved Successfully....." , "data" => array("id"=> $product_id , "action" => "update" ));
		   
	    }else{
			 $this->db->insert("products" , $data);
		// 	$insert_id = $this->db->query("SELECT * FROM products ORDER BY product_id DESC LIMIT 1") ;
		// 	$insert_id = $insert_id->result_array()[0]['product_id'];
			
		// if( $m == 1)
		// {
		// 	//creating analytics part
		// 	$data = array(
		// 		array(
		// 			'product_analytics_type' 			=>  'product_views',
		// 			'product_analytics_criteria' 	=>  'count',
		// 			'product_analytics_count' 		=>  0,
		// 			'product_analytics_createtime' 	=>  date('Y-m-d H:i:s'),
		// 			'product_analytics_product_id' 	=>  $insert_id,
		// 		),

		// 		array(
		// 			'product_analytics_type' 				=>  'product_sales',
		// 			'product_analytics_criteria' 		=>  'count',
		// 			'product_analytics_count' 			=>  0,
		// 			'product_analytics_createtime' 	=>  date('Y-m-d H:i:s'),
		// 			'product_analytics_product_id' 	=>  $insert_id,
		// 		),

		// 	);

		// 	$this->db->insert_batch('product_analytics',$data);
			
		// 	$return = array("status"=>"success" , "msg" => "Changes Saved Successfully....." , "data" => array("id" => $insert_id  , "action" => "insert"));
		// }else{
		// 	$return = array("status"=>"error" , "msg" => "Error creating product . Please fill maximum data " , "data" => array("id" => ""  , " , action" => "insert"));
		// }	
			
			
			
			
		}
		// echo json_encode( $return );
		
    }
	
	public function get_categories_get(){
		$cat = $_GET['q'];
		$r = $this->CommonModel->getCategories($cat);
		print_r( json_encode( $r  ) );
	}
	
	public function create_analytic_get(){
		$p = $this->db->from('products')->get()->result();
		foreach($p as $pr){
			$data = array(
				array(
					'product_analytics_type' 			=>  'product_views',
					'product_analytics_criteria' 	=>  'count',
					'product_analytics_count' 		=>  0,
					'product_analytics_createtime' 	=>  date('Y-m-d H:i:s'),
					'product_analytics_product_id' 	=>  $pr->product_id,
				),

				array(
					'product_analytics_type' 				=>  'product_sales',
					'product_analytics_criteria' 		=>  'count',
					'product_analytics_count' 			=>  0,
					'product_analytics_createtime' 	=>  date('Y-m-d H:i:s'),
					'product_analytics_product_id' 	=>  $pr->product_id,
				),

			);

			$this->db->insert_batch('product_analytics', $data);
			$analytic_id = $this->db->insert_id();

			// $this->db->where('product_id', $p->product_id);
			// $this->db->update('product', array(''$analytic_id);
			if($this->db->affected_rows() > 0){
				echo 'updated';
			}else{
				echo 'error';
			}
		}
	}
	
	
	/** 
	*    Author : Abhishek 
	*	 @param string $product_id  Product Id as  a parameter for function 
	*/
	
	public function delete_product_get(){
		$id = $_GET['product_id'];
		$this->db->where("product_id" , $id );
		$r = $this->db->update("products" , array("product_status"=>"2"));
		
		if($r == 1)
		{
			$response = array("status"=>"success" , "msg" => "Product Deleted Successfully ...." , "data" => array()); 
			 $this->response( $response , parent::HTTP_OK) ;
			
		}else{
			 $this->response( $response , parent::HTTP_INTERNAL_SERVER_ERROR) ;
		}
	}

	
	public function delete_product_permanently_get(){
		$id = $_GET['product_id'];
		
		$r = $this->db->query("DELETE FROM products WHERE product_id = " . $id);
		
		if($r == 1)
		{
			$response = array("status"=>"success" , "msg" => "Product Deleted Successfully ...." , "data" => array()); 
			 $this->response( $response , parent::HTTP_OK) ;
		}else{
			 $this->response( $response , parent::HTTP_INTERNAL_SERVER_ERROR) ;	
		}	 
	}  
	
	public function add_category_post()
	{
		$data = array('category_name'=>$_POST['category_name'],'parent_id'=>$_POST['parent_id']);
		$res = $this->CommonModel->insert_data('categories',$data);
		echo $res;
	}
	
	public function delete_category_post()
	{
		$where = array('category_id'=>$_POST['category_id']);
		$res = $this->CommonModel->delete_data('categories',$where);
		echo $res;
	}
	
	public function edit_category_post()
	{
		$where = array('category_id'=>$_POST['category_id']);
		$data = array('category_name'=>$_POST['category_name']);
		$res = $this->CommonModel->update_data('categories',$data,$where);
		echo $res;
	}
	
	public function show_category_post()
	{
		$where = $_POST['category_id'];
		$res = $this->db->query("select `category_name` from categories where `category_id`=$where ")->result();				
		echo $res[0]->category_name;		
	}	
	public function change_coupon_status_post()
	{
		$where = array('coupon_id'=>$_POST['coupon_id']);
		$data = array('coupon_status'=> $_POST['status']);
		$res = $this->CommonModel->update_data('coupons',$data,$where);		
	}

	public function add_coupon_get($coupon_id = "")
	{
		$data = array();
		$coupon_id = base64_decode($coupon_id);
		$where = array('coupon_id'=>$coupon_id);
		$data['coupons_data'] = $this->CommonModel->select_data_where('coupons',$where)->result();
		$data['layout'] = $this->adminLayout($data);
		$this->load->view("admin/addcoupons.tpl", $data);
	}
	public function save_coupons_post()
	{
		$coupon_id = base64_decode($_POST['coupon_id']);		
		if(is_numeric($coupon_id))
		{	
			$where = array('coupon_id'=>$coupon_id);
			$data = array('coupon_code'=>$_POST['coupon_code'],
						  'coupon_type'=>$_POST['coupon_type'],
						  'order_range'=>$_POST['min_order'],
						  'value'=>$_POST['coupon_value'],
						  'coupon_from_datetime'=>$_POST['date_from'],
						  'coupon_to_datetime'=>$_POST['date_to'],						
						  );
			$res = $this->CommonModel->update_data('coupons',$data,$where);	
			$response = array("status"=>true , "msg" => "Coupon has been updated successfully" , "data" => array()); 
			$this->response( $response , parent::HTTP_OK);
		}else{
			$data = array('coupon_code'=>$_POST['coupon_code'],
						  'coupon_type'=>$_POST['coupon_type'],
						  'value'=>$_POST['coupon_value'],
						  'coupon_from_datetime'=>$_POST['date_from'],
						  'coupon_to_datetime'=>$_POST['date_to'],	'order_range'=>$_POST['min_order'],					
						  'role'=>'admin',						
						  );
			$res = $this->CommonModel->insert_data('coupons',$data);
			$response = array("status"=>true , "msg" => "Coupon has been added successfully" , "data" => array()); 
			$this->response( $response , parent::HTTP_OK) ;
		}
	}
	
	public function delete_coupon_post()
	{
		$where = array('coupon_id'=>$_POST['coupon_id']);		
		$res = $this->CommonModel->delete_data('coupons',$where);
		if($res == true){
			$this->response(array('status' => true, 'message' => 'Coupon has been  deleted successfully!'), parent::HTTP_OK);
		}else{
			$this->response(array('status' => true, 'message' => 'Something went wrong!'), parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	public function update_userstatus_post()
	{
		$where = array('id'=>$_POST['userid']);
		$data = array('status'=> $_POST['status']);
		$res = $this->CommonModel->update_data('users',$data,$where);
	}
	public function deleteuser_post()
	{
		$where = array('id'=>$_POST['userid']);		
		$res = $this->CommonModel->delete_data('users',$where);
		if($res == true){
			$this->response(array('status' => true, 'message' => 'User has been  deleted successfully!'), parent::HTTP_OK);
		}else{
			$this->response(array('status' => true, 'message' => 'Something went wrong!'), parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function AdminShipping_get() {
		
        $data = array();
        $seo = array();
        $data['locations'] = $this->CommonModel->getShippingLocations();
        $data['layout'] = $this->adminLayout($data);
        $this->load->view("admin/shipping/location.tpl", $data);
    }
	public function addShippingLocation_get() {
        $data = array();
        $data['countries'] = $this->CommonModel->get_record('countries','','name');
        $seo = array();
        $data['layout'] = $this->adminLayout($data);
        $this->load->view("admin/shipping/addShippingLocation.tpl", $data);
    }
}

?>
