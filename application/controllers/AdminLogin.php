<?php defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */



class AdminLogin extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'language', 'url'));
        $this->load->model('InformationModel');

        $this->load->model('AdminModel');

        $this->load->model('CommonModel');
        if (isset($_COOKIE['language'])) {
            $this->lang->load($_COOKIE['language'] . "_landing", $_COOKIE['language']);
        } else {
            $this->lang->load('english_landing', 'english');
        }
		
		if($this->session->userdata('admin')!=''){
			
			redirect('AdminLogin');
		}
	 
		
    }
	/**
	*	Author: Akhilesh & Himanshu
	*
	*	Authenticates admin and initializes session
	*/
	
   
	public function index_get() 
	{
		//print_r($this->session->userdata());
            $data = array();
            $this->load->view("admin/login.tpl", $data);
    }
	
	public function doLogin_post() 
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE)
			{
			$val = array(
				'status' => FALSE,
				'message'	=>'<i class="fa fa-close"></i> Error in validating fields',
				'errorMsg' => array(
					'email' => form_error('email') ,
					'password' => form_error('password')
				)
			);

			// echo json_encode($val);
			// Set the response and exit

			$this->response($val); // INTERNAL SERVER ERROR (500) being the HTTP response code
			}
		  else
			{
			$email = $this->input->post('email');
			$password = md5($this->input->post('password')); 					//Later replace this with SALT
			$role = 'admin'; 								 					//setting role for admin
			$user_data = $this->CommonModel->doLogin($email, $password, $role);
			if ($user_data == false)
				{
				$val = array(
					'status' => FALSE,
					'message' => '<i class="fa fa-close"></i> Invalid Email and Password Combination',
					'data' => '',
					'errorMsg' => ''
				);

				// Set the response and exit

				$this->response($val, parent::HTTP_OK); 
				// INTERNAL SERVER ERROR (500) being the HTTP response code

				}
			  else
				{
				/*$val = array(
				"success"=> "<i class='fa fa-check'></i> Logged in successfully!",
				);*/
				
				$userdata = array(
					'userData' => $user_data, 
					'loggedIn'=>true
				);
				$val = array(
					"status" => TRUE,
					"message" => "<i class='fa fa-check'></i> Logged in successfully!",
					"data" => $userdata,
				);
				/* creating session data */
				$this->session->set_userdata('admin', $userdata);
				$this->response($val, parent::HTTP_OK); // OK (200) being the HTTP response code
				}
			}

	}
	
   
}

?>