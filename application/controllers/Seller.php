<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seller extends MY_Controller{
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'language', 'url'));
        $this->load->model('InformationModel');
        $this->load->model('Seller_model');
		$this->store_salt = true;
		$this->salt_length = 6;
        if (isset($_COOKIE['language'])) {
            $this->lang->load($_COOKIE['language'] . "_landing", $_COOKIE['language']);
        } else {
            $this->lang->load('english_landing', 'english');
        }

        if($this->session->userdata('seller')){
        	self::$_seller_id = $this->session->userdata('seller')->id;
        }else{
        	self::$_seller_id = null;
        }

    }

	private static $_seller_id = null;
	protected $salt = 'k32duem01vZsQ2lB8g0s';
	
	public function index_get(){
		$data = array('active_class'=>'home');
        $data['layout'] = $this->sellerlayout($data);
        $this->load->view("seller/index.tpl" ,$data);

	}
	
	public function sellerLogin_post(){
		$email = $this->input->post('email');
		$password = $this->hash_password($this->input->post('password'));            
		$sellerDetail = $this->Seller_model->sellerLogin($email,$password);
	   
		if(isset($sellerDetail) && !empty($sellerDetail))
		{
			$this->session->set_userdata('seller',$sellerDetail);   // set seller data in session
			$this->session->set_userdata('loggedIn',true);
			$this->session->set_userdata('seller_id',$sellerDetail->id);  
			self::$_seller_id =  $sellerDetail->id;
			$Data = array('sellerData' => $sellerDetail);
			$val = array(
				"status" => TRUE,
				"message" => "LoggedIn successfully!",
				"data" => $Data,
			);				
				$this->response($val, parent::HTTP_OK);			
		}else{
			$val = array(
					'status' => FALSE,
					'message' => 'Invalid email id or password.',
					'data' => '',
			);
			$this->response($val, parent::HTTP_OK); 			
		}
    }
	  
	public function login_get(){
		$data = array(
			'active_class'=>'home'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/login.tpl" ,$data);
	}

	public function dashboard_get(){
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'home'
			);
			$data['layout'] = $this->sellerlayout($data);		
			$this->load->view("seller/dashboard.tpl" ,$data);
		}
	} 

	public function listings_get()
	{
		$data = array(
			'active_class'=>'pages'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/listing.tpl" ,$data);
	} 
	
	public function approval_request_get()
	{
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
		$data = array(
			'active_class'=>'pages'
		);

		$products_data = $this->CommonModel->select_data_where('orders', array('seller_id'=>self::$_seller_id), 'order_id')->result();

		$i = 0;
		$j=0;
		
		$data['products']['pending'] = '';
		$data['products']['count'] = count($products_data);
		
			foreach ($products_data as $p){				
				$name = $this->CommonModel->select_data_where('products', array('product_id'=> $p->order_address_id))->result();
				$products_data[$i]->product_name = $name[0]->product_name;
			
				if($products_data[$i]->order_payment_status == 0){
					$products_data[$i]->order_payment_status_text = 'Pending';
				}else{
					$products_data[$i]->order_payment_status = 'Paid';
				}

				if($products_data[$i]->order_shipping_status == 0){
					$products_data[$i]->order_shipping_status_text = 'Pending';
					$products_data[$i]->next = '<button class="btn btn-success" onclick="flag(1, '.$p->order_id.')">Accept</button> <button class="btn btn-danger" onclick="flag(4, '.$p->order_id.')">Reject</button>';
				}else if($products_data[$i]->order_shipping_status == 1){
					$products_data[$i]->next = '<button class="btn btn-success" onclick="flag(2, '.$p->order_id.')">Ship</button>';
					$products_data[$i]->order_shipping_status_text = 'Processed';
				}else if($products_data[$i]->order_shipping_status == 2){
					$products_data[$i]->next = '<button class="btn btn-success" onclick="flag(3, '.$p->order_id.')">Delivered</button>';
					$products_data[$i]->order_shipping_status_text = 'Shipped';
				}else if($products_data[$i]->order_shipping_status == 3){
					$products_data[$i]->next = 'Waiting for acknowledgement!';
					$products_data[$i]->order_shipping_status_text = 'Delivered';
				}else if($products_data[$i]->order_shipping_status == 4){
					$products_data[$i]->next = '<button class="btn btn-success" onclick="flag(5, '.$p->order_id.')">Refund</button> <button class="btn btn-success" onclick="flag(6, '.$p->order_id.')">Reship</button>';
					$products_data[$i]->order_shipping_status_text = 'Cancelled';
				}else if($products_data[$i]->order_shipping_status == 5){
					$products_data[$i]->next = 'Waiting for acknowledgement!';
					$products_data[$i]->order_shipping_status_text = 'Refund';
				}else if($products_data[$i]->order_shipping_status == 6){
					$products_data[$i]->next = '<button class="btn btn-success" onclick="flag(3, '.$p->order_id.')">Delivered</button>';
					$products_data[$i]->order_shipping_status_text = 'Reshipment';
				}

				if(!empty($p) && $p->order_shipping_status == 0){
					unset($products_data[$i]);
					$temp[$j] = $p;
					$j++;
					$data['products']['pending'] = $temp;
				}
				$i++;

		}

		$data['products']['all'] = $products_data;

		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/approval_request.tpl" ,$data);
		}
	} 
	
	public function payment_get()
	{
		$data = array(
			'active_class'=>'payments'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/payment.tpl" ,$data);
	} 
	public function payment_statement_get()
	{
		$data = array(
			'active_class'=>'payments'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/payment_statement.tpl" ,$data);
	} 
	public function transaction_get()
	{
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'payments'
			);		
			$this->db->select('*')
					 ->from('transactions')
					 ->join('order_detail','transactions.order_id = order_detail.order_id')
					 ->join('orders','transactions.order_id = orders.order_id');
			$query = $this->db->get();		
			$transdetails = $query->result();
			foreach($transdetails as $trans){
				$productdetails  = $this->CommonModel->select_data_where('products',array('product_id'=>$trans->order_product_id))->result();
				foreach($productdetails as $pro){
					$trans->product_name = $pro->product_name;
					$trans->product_image = $pro->product_image;
					$trans->product_price = $pro->product_price;
				}
			}
			$data['transactions'] = $transdetails;
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/transaction.tpl",$data);
		}
	} 
	public function invoices_get()
	{
		$data = array(
			'active_class'=>'payments'
		);
		$data['layout'] = $this->sellerlayout($data);
		$this->load->view("seller/invoices.tpl" ,$data);
	} 

	public function active_orders_get(){
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'orders'
			);
			$this->db->select('*')
					->from('orders')
					->join('order_detail','orders.order_id=order_detail.order_id');
			$this->db->where('orders.seller_id',self::$_seller_id);		
			$this->db->where_not_in('orders.order_shipping_status','4');
			$query = $this->db->get();		
			$result = $query->result();		
			foreach($result as $order){
				$this->db->select("*")
						->from('products')
						->where('product_id',$order->order_product_id);
				$query = $this->db->get();
				$prodetails = $query->result();
				foreach($prodetails as $pdetails)
				{
					$order->product_name = $pdetails->product_name;
					$order->product_image = $pdetails->product_image;
					$order->product_price = $pdetails->product_price;
					$order->product_quantity = $pdetails->product_quantity;
					$order->category_id = $pdetails->category_id;
					$order->product_description = $pdetails->product_description;
					$order->product_discount = $pdetails->product_discount;
				}			
			}
			$data['active_order'] = $result;
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/active_orders.tpl" ,$data);
		}
	}
	
	public function cancelled_orders_get(){
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'orders'
			);		
			$this->db->select('*')
				->from('orders')
				->join('order_detail', 'orders.order_id = order_detail.order_id');
			$this->db->where('seller_id',self::$_seller_id);
			$this->db->where('order_shipping_status','4');
			$query = $this->db->get();		
			$result = $query->result();
			foreach($result as $res)
			{
				$this->db->select('*')
						->from('products')
						->where('product_id',$res->order_product_id);
				$query = $this->db->get();		
				$pro_details = $query->result();			
				foreach($pro_details as $product){
					$res->product_name = $product->product_name;
					$res->product_image = $product->product_image;
					$res->product_price = $product->product_price;
					$res->product_description = $product->product_description;
				}							
			}
			$data['cancelled_orders'] = $result;
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/cancelled_orders.tpl" ,$data);
		}
	}
	
	public function returns_get(){
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'returns'
			);
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/returns.tpl" ,$data);
		}
	}
	
	public function advertising_get(){
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'advertising'
			);
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/advertising.tpl" ,$data);
		}
	}
	
	public function overview_get(){
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'performance'
			);
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/performance_overview.tpl" ,$data);
		}
	}
	
	public function growth_get(){
		$data = array(
			'active_class'=>'performance'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/growth.tpl" ,$data);
	}
	
	public function org_promotions_get(){
		$data = array(
			'active_class'=>'promotions'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/flip_promotions.tpl" ,$data);
	}
	
	public function promotions_get(){
		$data = array(
			'active_class'=>'promotions'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/promotions.tpl" ,$data);
	}
	
	public function profile_get(){
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array(
				'active_class'=>'profile'
			);
			$data['sellerData']= $this->CommonModel->select_data_where('users',array('id'=>self::$_seller_id))->row();				
			$data['pickup_address'] = $this->CommonModel->select_data_where('user_addresses',array('address_user_id'=>self::$_seller_id))->result();$data['business_detail'] = $this->CommonModel->select_data_where('seller_details',array('seller_id'=>self::$_seller_id))->result();
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/profile.tpl" ,$data);
		}
	}

	public function modal($template_name){
		$this->load->view("seller/modal_templates/".$template_name);
	}

	public function checkseller_post(){
		$email =$this->input->post('email');
		$this->db->where('email',$email);
		$query = $this->db->get('users');
		if( $query->num_rows() > 0 ){ 
			echo 'false'; 
		 }else{ 
			echo 'true';
		 }
		 die;
   }
      
      public function seller_product_get($shop_id){
      	 $this->load->model('Seller_model');
      	 $products = $this->Seller_model->Product_display();

  
      	 $this->load->view("front-2/SellerView",$data);
      }
	  
	/*
	*  @author Abhishek
	*
	*
	**/

	public function add_new_listing()
	{
		$data = array(
			'active_class'=>'add_new_listing'
		);
		$data['layout'] = $this->sellerlayout($data);

		$this->load->view("seller/add_new_listing.tpl" ,$data);
		
		
	}

	public function accountCreation_get($seller_id) {			
		$seller_id = base64_decode($seller_id);
		$data = array('active_class'=>'profile');
		$data['layout'] = $this->sellerlayout($data);
		$data['sellerData']= $this->CommonModel->select_data_where('users',array('id'=>$seller_id))->row();	 
		$this->load->view("seller/accountCreation.tpl" ,$data);  
	}

	public function tracking_post($option = null){
		if($option == 'flag'){
			$flag = $this->input->post('flag');

			// if($flag == true ){
			// 	$flag = '1';
			// }else if($flag == false){
			// 	$flag == '0';
			// }

			$order_id = $this->input->post('order_id');
			$where = array('seller_id' => self::$_seller_id, 'order_id' => $order_id);
			$ret = $this->CommonModel->update_data('orders',array('order_shipping_status' => $flag), $where);

			if($ret == true){
				$this->response(array('status' => true, 'message' => 'Successfully changed the order status. Please wait while we process your request!'), parent::HTTP_OK);
			}else{
				$this->response(array('status' => true, 'message' => 'Something went wrong!'), parent::HTTP_INTERNAL_SERVER_ERROR);
			}

		}
	} 
	
	public function addproduct_get( $param = null  ) {	 
		if(!isset(self::$_seller_id))
		{
		redirect('seller');
		}else{
		$data = array();
		$param = base64_decode($param);
		if( $param != null){ 
		$prod_data = $this->db->query("SELECT * FROM products WHERE product_id = $param");
		   if(!empty($prod_data->result_array())){
			    $data['product_data'] =   $prod_data->result_array()[0];			     
				$data['layout'] = $this->sellerlayout($data);		    
				$this->load->view("seller/addproduct.tpl", $data); 
		   }else{
			   echo "PRODUCT_NOT_FOUND";
		   }
	   }else{
		    $data['layout'] = $this->sellerlayout($data);		    
			$this->load->view("seller/addproduct.tpl", $data); 
	      }
		}
    }
	
	public function productlist_get() 
	{
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array();
			$data['layout'] = $this->sellerlayout($data);		
			$list = $this->Seller_model->productList();		
			if($list){
				$data['list'] = json_encode($list);
			}		
			$this->load->view("seller/productlist.tpl", $data);
		}
    }
	
	public function new_product_post() 
	{
		$product_id = $this->input->post('product_id');
	    $shipping_charges = array();		
		if(!empty($_POST['shipping_cost_with'])){			 
			 $shipping_cost_with = $_POST['shipping_cost_with'] ;
		}
		if(!empty($_POST['shipping_cost'])){			 
			  $shipping_cost = $_POST['shipping_cost'] ;
		}
		if(!empty($_POST['shipping_to']))
		{
		  foreach($_POST['shipping_to'] as $key=> $ship_to)			
			 $shipping_charges[] = array("country_id" => $ship_to , "shipping_cost" => $shipping_cost[$key] ,  "shipping_cost_with" => $shipping_cost_with[$key] );		  
		} 		
		@$image = $this->input->post("filer")[0] ;	
		$shipping_charges = serialize($shipping_charges);	
		$data = array(
			// "user_id" => self::$_seller_id,
			"user_id" => '4',
			"product_name" => $this->input->post("product_title") ,
			"product_listing_maker" => $this->input->post("product_listing_maker") ,
			"product_listing_order" => $this->input->post("product_listing_order") ,
			"product_listing_use" => $this->input->post("product_listing_use") ,
			"category_id" => $this->input->post("product_categories") ,
			"product_pricing_format" => $this->input->post("pricing_format") ,
			"product_price" => $this->input->post("price") ,
			"shipping_from" => $this->input->post("shipping_from") ,
			"shipping_process_time" => $this->input->post("shipping_process_time") ,
			"shipping_charges" => $shipping_charges ,
			"shipping_options" =>$this->input->post("shipping_options") ,
			"starting_price" => $this->input->post("auction_starting_price") ,
			"duration" => $this->input->post("auction_duration") ,
			"item_type" => $this->input->post("item_type") ,
			"product_description" =>($this->input->post("product_desc")) ,
			"product_quantity" => $this->input->post("quantity") ,
			"deal_datetime_from" => $this->input->post("deal_from") ,
			"deal_datetime_to" => $this->input->post("deal_to") ,
			"product_discount" => $this->input->post("deal_discount") ,
			"is_featured" => "1" ,
			"product_variations" => serialize($this->input->post("variations")) ,		
			"search_tags" => $this->input->post("product_tags") ,
			"search_materials" => $this->input->post("material_tags") ,
			"product_images" => serialize($this->input->post("filer")) ,
			"product_image" => $image ,
		);
		 if( !empty(trim( $product_id ))){
			$this->db->where("product_id" , $product_id  ); 
			$s =  $this->db->update("products" , $data);
			$return = array("status"=>"success" , "msg" => "Changes Saved Successfully....." , "data" => array("id"=> $product_id , "action" => "update" ));
	    }else{
			$m = $this->db->insert("products" , $data);
			$insert_id = $this->db->query("SELECT * FROM products ORDER BY product_id DESC LIMIT 1") ;
			$insert_id = $insert_id->result_array()[0]['product_id'];			
		if( $m == 1)
		{
			$return = array("status"=>"success" , "msg" => "Changes Saved Successfully....." , "data" => array("id" => $insert_id  , "action" => "insert"));
		}else{
			$return = array("status"=>"error" , "msg" => "Error creating product . Please fill maximum data " , "data" => array("id" => ""  , " , action" => "insert"));
		}			
		}
		echo json_encode( $return );		
    }
	
	public function delete_product_post()
	{
		$id = $_POST['product_id'];
		$this->db->where("product_id" , $id );
		$r = $this->db->update("products" , array("product_status"=>"2"));
		
		if($r == 1)
		{
			$response = array("status"=>"success" , "msg" => "Product Deleted Successfully ...." , "data" => array()); 
			$this->response( $response , parent::HTTP_OK) ;
			
		}else{
			 $this->response( $response , parent::HTTP_INTERNAL_SERVER_ERROR) ;
		}
	}
	
	public function add_coupons_get($coupon_id = "")
	{
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array();
			$coupon_id = base64_decode($coupon_id);
			$where = array('coupon_id'=>$coupon_id);
			$data['coupons_data'] = $this->CommonModel->select_data_where('coupons',$where)->result();
			$data['layout'] = $this->sellerlayout($data);				
			$this->load->view("seller/add_coupons.tpl", $data);
		}
	}
	public function save_coupons_post()
	{
		$this->form_validation->set_rules('coupon_code', 'Coupon code', 'trim|required');
		$this->form_validation->set_rules('coupon_type', 'Coupon type', 'trim|required');
		$this->form_validation->set_rules('coupon_value', 'Coupon value', 'trim|required');
		$this->form_validation->set_rules('date_from','Date from','trim|required');
		$this->form_validation->set_message('min_order', 'Min order' ,'trim|required');
		$this->form_validation->set_message('date_to','Date to','trim|required');		
		if($this->form_validation->run() == false){
			$errors = array(
				'coupon_code'	=> form_error('coupon_code', '',''),
				'coupon_type'	=> form_error('coupon_type', '',''),
				'coupon_value'	=> form_error('coupon_value', '',''),
				'date_from'	=> form_error('date_from', '',''),
				'min_order'	=> form_error('min_order', '',''),
				'date_to'	=> form_error('date_to', '','')
			);			
			$cat = array(
				'status'	=> 	false,
				'msg'		=>	'<i class="fa fa-times"></i> Validation Errors',
				'data'		=>	$errors
			);			
			$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
		}else{
			$coupon_id = base64_decode($_POST['coupon_id']);		
			if(is_numeric($coupon_id))
			{	
				$where = array('coupon_id'=>$coupon_id);
				$data = array('coupon_code'=>$_POST['coupon_code'],
							  'coupon_type'=>$_POST['coupon_type'],
							  'value'=>$_POST['coupon_value'],
							  'coupon_from_datetime'=>$_POST['date_from'],
							  'order_range'=>$_POST['min_order'],
							  'coupon_to_datetime'=>$_POST['date_to'],						
							  );
				$res = $this->CommonModel->update_data('coupons',$data,$where);	
				$response = array("status"=>true , "msg" => "Coupon has been updated successfully" , "data" => array()); 
				$this->response( $response , parent::HTTP_OK);
			}else{
				$data = array('coupon_code'=>$_POST['coupon_code'],
							  'coupon_type'=>$_POST['coupon_type'],
							  'value'=>$_POST['coupon_value'],
							  'coupon_from_datetime'=>$_POST['date_from'],
							  'coupon_to_datetime'=>$_POST['date_to'],'order_range'=>$_POST['min_order'],						
							  'role'=>'seller',						
							  );
				$res = $this->CommonModel->insert_data('coupons',$data);
				$response = array("status"=>true , "msg" => "Coupon has been added successfully" , "data" => array()); 
				$this->response( $response , parent::HTTP_OK) ;
			}
		}
	}
	public function coupons_list_get()
	{
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data = array();
			$data['coupons']= $this->CommonModel->select_data_where('coupons',array('role'=>'seller'))->result();
			$data['layout'] = $this->sellerlayout($data);				
			$this->load->view("seller/coupons_list.tpl", $data);
		}
	}
	public function delete_coupon_post()
	{
		$where = array('coupon_id'=>$_POST['coupon_id']);		
		$res = $this->CommonModel->delete_data('coupons',$where);
		if($res == true){
			$this->response(array('status' => true, 'message' => 'Coupon has been  deleted successfully!'), parent::HTTP_OK);
		}else{
			$this->response(array('status' => true, 'message' => 'Something went wrong!'), parent::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	public function update_coupons_status_post()
	{
		$where = array('coupon_id'=>$_POST['id']);
		$data = array('coupon_status'=> $_POST['status']);
		$res = $this->CommonModel->update_data('coupons',$data,$where);				
	}
	public function myshop_get()
	{
		if(!isset(self::$_seller_id))
		{
			redirect('seller');
		}else{
			$data=array();	
			$data = array(
				'active_class'=>'myshop'
			 );
			
			$shop = $this->CommonModel->select_data_where('shops', array('seller_id'=> self::$_seller_id))->result();
			if(!empty($shop))
			{
				$shop[0]->products = $this->db->from('products')
								->where_in('product_id', explode(',',$shop[0]->featured_products))->get()->result();
				$product = $this->CommonModel->select_data_where('shops', array('seller_id' => self::$_seller_id))->result();
				$data['shop'] = $shop[0];
			}
			$data['layout'] = $this->sellerlayout($data);
			$this->load->view("seller/myshopview.tpl",$data);
		}
  }

  public function shopdetails_post()
  {

	  $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	  $this->form_validation->set_rules('shop_name','Shop name','trim|required');
	  $this->form_validation->set_rules('description','Description','trim|required');
	  $this->form_validation->set_rules('fb','Social links ','trim|required');
	  $this->form_validation->set_rules('gp','Social links ','trim|required');
	  $this->form_validation->set_rules('tw','Social links ','trim|required');
	  
	  if($this->form_validation->run() == false) {
			$errors = array(
				'shop_name'=> form_error('shop_name', '<p class="text text-danger error">','</p>'),
				'description'=> form_error('description', '<p class="text text-danger error">','</p>'),
				'fb'	=> form_error('fb','<p class="text text-danger error">','</p>'),
				'gp'	=> form_error('gp','<p class="text text-danger error">','</p>'),
				'tw'	=> form_error('tw','<p class="text text-danger error">','</p>')
			);
		
			$cat = array(
				'status'	=> 	false,
				'message'	=>	'<i class="fa fa-times"></i> All fields required!',
				'data'		=>	$errors
			);
			
			$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);

	  } else {
	 		if (!empty($_FILES['featured_image']['name'])) {
		 	$config = [
		 		'upload_path'=>FCPATH.'files',
				'allowed_types'=>'jpg|png|gif|jpeg'
            ];

          $config['max_width']=1200;
          $config['max_height']=650;
          $config['encrypt_name'] = TRUE;
          $this->load->library('upload',$config);

        if ( ! $this->upload->do_upload('featured_image')) {
          $error = array('error' => $this->upload->display_errors());

	        $cat = array(
						'status'	=> 	false,
						'message'	=>	'<i class="fa fa-times"></i> Error in uploading the image!',
						'data'		=>	array('featured_image'=> $error['error'])
					);
                    
					$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
        } else { 
          $data = array('upload_data' => $this->upload->data());
          $img_url =  $data['upload_data']['file_name'];

          $data= array(
						'shop_name' => $this->input->post('shop_name'),
						'description'=>$this->input->post('description'),
						'social_links_fb'=>$this->input->post('fb'),
						'social_links_gp'=>$this->input->post('gp'),
						'social_links_tw'=>$this->input->post('tw'),
						'featured_image'=>$img_url
					);
					 
					$stat = $this->CommonModel->update_data('shops', $data, array('seller_id' => self::$_seller_id));

					if($stat == true){
					  $val = array(
							"status" => TRUE,
							"message" => '<i class="fa fa-check"></i> Settings saved!',
							"data" =>array('img_url' => $img_url)
						);
							
					  $this->response($val, parent::HTTP_OK);
					}else{
						$val = array(
							"status" => false,
							"message" => '<i class="fa fa-times"></i> Oh Snap! Something went wrong.',
							"data" =>''
						);
							
					  $this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR);
					}
	      }
      }else{
      	$val = array(
					"status" => false,
					"message" => '<i class="fa fa-times"></i> No image selected!',
					"data" =>''
				);
				 $this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR);
      }    
		}
  }

 
    public function searchproduct_get(){
    		// $product = $this->CommonModel->select_data_where('shops', array('seller_id' => self::$_seller_id))->result();
        
    	     echo $this->Seller_model->search_products(self::$_seller_id);
    	
    }

    public function featured_product_post()
    {
			   
		$data = array(
			"featured_products"	=>	$this->input->post('product1').",".$this->input->post('product2').",".$this->input->post('product3').",".$this->input->post('product4')
		);

		$stat = $this->CommonModel->update_data('shops',$data, array('seller_id' => self::$_seller_id));
		
		if($stat == true){

			
			redirect('seller/myshop');
			// $val = array(
			// 	"status" => TRUE,
			// 	"message" => '<p class="alert alert-success">Data insert successfully!</p>',
			// 	// "data" =>'<p class="alert alert-success">Logged in successfully!</p>'
			// );
			//  $this->response($val, parent::HTTP_OK);
		}else{
                $this->response(array(
						'status'	=> 	false,
						'message'	=>	'<p class="error text text-danger">Try again result not found</p>',
						
					), parent::HTTP_INTERNAL_SERVER_ERROR);
		}

    }

	public function logout_post()
	{
		$this->session->sess_destroy(); 
        redirect('seller');		
	}

	public function save_featured_products_post(){
		$data = '';
		foreach($_POST as $key=>$value){
			$data .= ",";
			$data .= $this->input->post($key);
		}

		$data = array('featured_products' => substr($data, 1, -1));
		$stat = $this->CommonModel->update_data('shops', $data, array('seller_id' => self::$_seller_id));
		redirect('seller/myshop', 'refresh');
	}
	
	public function addseller_post(){
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $data = array();       	   	 				
        $sellersdata = array('email'=>$email,
                              'contact'=>$phone,
                              'role'=>'seller',
                             );
            $this->CommonModel->insert_data('users',$sellersdata);
             $insert_id = $this->db->insert_id();
			 echo "test";die;
            if($insert_id)
            {
                $Data = array('sellerData' => $sellersdata);
                $val = array(
                        "status" => TRUE,
                        "seller_id" =>$insert_id,
                        "message" => "seller inserted successfully!",
                        "data" => $Data,
                );				
                    echo $val;
					//$this->response($val, parent::HTTP_OK);			
            }else{
                    $val = array(
                            'status' => FALSE,
                            'message' => 'Data not insert',
                            'data' => '',
                    );
					 echo $val;
                    // $this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
            }	
      }
	public function hash_password($password)
	{
		if (empty($password))
	    {
				return false;
	    }else{				
				$salt = $this->salt;
				$salt = sha1(md5($password)).$salt; 
				$password = md5($password.$salt);
				return $password;
			}		
	}
	public function saveseller_post($id='') {
            $data = array(); 
			if(isset($_FILES) && !empty($_FILES)){
				$featured_image = $_FILES['image']['name'];
				if(!empty($featured_image))
				{
						$config['upload_path'] = 'assets/uploads/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['overwrite'] = TRUE;					
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						$imagename	=$featured_image;					
					if(!$this->upload->do_upload('image',$config)) 
					{
						echo $this->upload->display_errors(); die;
					}
				}
			}			
            $seller_id = $this->input->post('seller_id');
            $seller_email = $this->input->post('seller_email');
            $seller_phone = $this->input->post('seller_phone');
            $seller_name = $this->input->post('seller_name');
            $seller_password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            
            $sellersdata = array('user_name'=>$seller_name,
                                'full_name'=>$seller_name,
                                 'email'=>$seller_email,
                                 'contact'=>$seller_phone,
                                 'password'=>  $this->hash_password($seller_password)
                             );
			$shopsdata = array('shop_name'=>$_POST['shop_name'],
								'social_links_fb'=>$_POST['facebook_page_link'],
								'social_links_gp'=>$_POST['google_page_link'],
								'social_links_tw'=>$_POST['twitter_link'],
								'description'=>  $_POST['description'],
								'featured_image'=>  $imagename
                             );
           $result =  $this->CommonModel->update_data('users',$sellersdata ,array('id'=>$seller_id));
		   $this->CommonModel->update_data('shops',$shopsdata ,array('seller_id'=>$seller_id));
            if($result)
            {
				$Data = array('sellerData' => $sellersdata);
				$val = array(
						"status" => TRUE,
						"seller_id" =>$seller_id,
						"message" => "seller register successfully!",
						"data" => $Data,
				);				
				$this->response($val, parent::HTTP_OK);			
            }else{
				$val = array(
						'status' => FALSE,
						'message' => 'Data not inserted',
						'data' => '',
				);
				$this->response($val, parent::HTTP_INTERNAL_SERVER_ERROR); 			
            }	
      }
	  
	public function update_profile_post()
	{
		$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('contact_address', 'Contact address', 'trim|required');
		$this->form_validation->set_rules('dob','Date of birth','trim|required');
		$this->form_validation->set_message('fax', 'Fax' ,'trim|required');
		$this->form_validation->set_rules('contact','Phone Number','trim|required|min_length[10]|max_length[10]');
		if($this->form_validation->run() == false){
			$errors = array(
				'full_name'	=> form_error('full_name', '',''),
				'gender'	=> form_error('gender', '',''),
				'contact_address'	=> form_error('contact_address', '',''),
				'dob'	=> form_error('dob', '',''),
				'fax'	=> form_error('fax', '',''),
				'contact'	=> form_error('contact', '','')
			);			
			$cat = array(
				'status'	=> 	false,
				'msg'		=>	'<i class="fa fa-times"></i> Validation Errors',
				'data'		=>	$errors
			);			
			$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
		}else{	
			$id = self::$_seller_id;
			$this->db->where("id" , $id );
			$data = array('full_name'=>$_POST['full_name'],
						   'gender' =>$_POST['gender'],
						   'dob' =>$_POST['dob'],
						   'street_address' =>$_POST['contact_address'],
						   'pincode' =>$_POST['fax'],
						   'contact' =>$_POST['contact'],
							);
			$r = $this->db->update("users" , $data);
			
			if($r == 1)
			{
				$response = array("status"=>true , "msg" => "Product has been update Successfully ...." , "data" => array()); 
				$this->response( $response , parent::HTTP_OK) ;
				
			}else{
				$response = array("status"=>false , "msg" => "Something went wrong" , "data" => array());
				 $this->response( $response , parent::HTTP_INTERNAL_SERVER_ERROR) ;
			}
		}
	}
	
	public function checkoldpassword_post(){
		$oldpassword = $this->hash_password($this->input->post('oldpassword'));
		$this->db->where('id',$_POST['user_id']);
		$this->db->where('password',$oldpassword);
		$query = $this->db->get('users');
		if( $query->num_rows() > 0 ){ 
			echo 'true'; 
		 }else{ 
			echo 'false';
		 }
		 die;
   }
   public function change_sellerpassword_post()
   {
		$this->form_validation->set_rules('oldpassword','Old password','trim|required');
		$this->form_validation->set_rules('newpassword', 'New password' ,'trim|required|min_length[6]');
		$this->form_validation->set_rules('cpassword','Repeat new password','trim|required|matches[newpassword]|min_length[6]');

		if($this->form_validation->run() == false){
		$errors = array(
				'oldpassword'	=> form_error('oldpassword', '<p class="text error text-danger">','</p>'),
				'newpassword'	=> form_error('newpassword', '<p class="text error text-danger">','</p>'),
				'cpassword'	=> form_error('cpassword', '<p class="text error text-danger">','</p>')
			);			
			$cat = array(
				'status'	=> 	false,
				'msg'		=>	'<i class="fa fa-times"></i> Validation Errors',
				'data'		=>	$errors
			);
		$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
		}else{	
			$pwd = $this->hash_password($_POST['newpassword']);
			$data = array('password'=>$pwd);
			$where = array('id'=> $_POST['user_id']);
			$res = $this->CommonModel->update_data('users',$data,$where);	
			if($this->db->affected_rows() > 0 ){	
				$response = array("status"=>true , "msg" => "Password change Successfully...." , "data" => array()); 
				$this->response( $response , parent::HTTP_OK) ;
			 }else{ 
				$response = array("status"=>false , "msg" => "New Password should not be same as old password" , "data" => array());
				$this->response( $response , parent::HTTP_OK) ;
			 }
		}
   }  
   public function deactivate_account_post()
   {
		$data = array('status'=>'0');
		$where = array('id'=> $_POST['id']);
		$res = $this->CommonModel->update_data('users',$data,$where);	
		if($this->db->affected_rows() > 0 ){	
			$response = array("status"=>true , "msg" => "Account has been successfully deactivate" , "data" => array()); 
			$this->response( $response , parent::HTTP_OK) ;
		 }else{ 
			$response = array("status"=>false , "msg" => "Your account already deactivate" , "data" => array());
			$this->response( $response , parent::HTTP_OK) ;
		 }
    }
	public function pickup_address_post()
	{
		$this->form_validation->set_rules('name','Name','trim|required');
		$this->form_validation->set_rules('contact','Contact','trim|required');
		$this->form_validation->set_rules('addressline1','Addressline1','trim|required');
		$this->form_validation->set_rules('addressline2','Addressline2','trim|required');
		$this->form_validation->set_rules('citytown','Citytown','trim|required');
		$this->form_validation->set_rules('spr','State','trim|required');
		$this->form_validation->set_rules('zip','Zip','trim|required');
		$this->form_validation->set_rules('country','Country','trim|required');
		

		if($this->form_validation->run() == false){
		$errors = array(
				'name'	=> form_error('name', '<p class="text error text-danger">','</p>'),
				'contact'	=> form_error('contact', '<p class="text error text-danger">','</p>'),
				'addressline1'	=> form_error('addressline1', '<p class="text error text-danger">','</p>'),
				'addressline2'	=> form_error('addressline2', '<p class="text error text-danger">','</p>'),
				'citytown'	=> form_error('citytown', '<p class="text error text-danger">','</p>'),
				'spr'	=> form_error('spr', '<p class="text error text-danger">','</p>'),
				'zip'	=> form_error('zip', '<p class="text error text-danger">','</p>'),
				'country'	=> form_error('country', '<p class="text error text-danger">','</p>'),
			);			
			$cat = array(
				'status'	=> 	false,
				'msg'		=>	'<i class="fa fa-times"></i> Validation Errors',
				'data'		=>	$errors
			);
		$this->response($cat, parent::HTTP_INTERNAL_SERVER_ERROR);
		}else{	
			$query = $this->CommonModel->select_data_where('user_addresses',array('address_user_id'=>$_POST['user_id']));
			if($query->num_rows() > 0){
				$where = array('address_user_id'=>$_POST['user_id']);
				$data = array(
							  'address_name'=>$_POST['name'],
							  'address_phone'=>$_POST['contact'],
							  'address_line_1'=>$_POST['addressline1'],
							  'address_line_2'=>$_POST['addressline2'],
							  'address_city'=>$_POST['citytown'],
							  'address_state'=>$_POST['spr'],
							  'address_zip'=>$_POST['zip'],
							  'address_country'=>$_POST['country'],					  
							 );
				$this->CommonModel->update_data('user_addresses',$data,$where);
			}else{
				$data = array(
							  'address_user_id'=>$_POST['user_id'],
							  'address_name'=>$_POST['name'],
							  'address_phone'=>$_POST['contact'],
							  'address_line_1'=>$_POST['addressline1'],
							  'address_line_2'=>$_POST['addressline2'],
							  'address_city'=>$_POST['citytown'],
							  'address_state'=>$_POST['spr'],
							  'address_zip'=>$_POST['zip'],
							  'address_country'=>$_POST['country'],					  
							 );
				$this->CommonModel->insert_data('user_addresses',$data);
			}
			if($this->db->affected_rows() > 0){
				$response = array("status"=>true , "msg" => "Address has been successfully updated" , "data" => array()); 
				$this->response( $response , parent::HTTP_OK) ;
			}else{
				$response = array("status"=>true , "msg" => "Address has been successfully updated" , "data" => array()); 
				$this->response( $response , parent::HTTP_OK) ;
			}
		}
	}	
	
	public function business_details_post()
	{
		$query = $this->CommonModel->select_data_where('seller_details',array('seller_id'=>$_POST['user_id']));
		$result = $query->result(); 
		if(isset($_FILES) && !empty($_FILES)){
			$address_proof = $_FILES['address_proof']['name'];
			if(!empty($address_proof))
			{
					$config['upload_path'] = 'assets/uploads/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['overwrite'] = TRUE;					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$aproof=$address_proof;					
				if(!$this->upload->do_upload('address_proof',$config)) 
				{
					echo $this->upload->display_errors(); 
				}
			}
		}		
		if(isset($_FILES) && !empty($_FILES)){
			$cancel_cheque = $_FILES['cancel_cheque']['name'];
			if(!empty($cancel_cheque))
			{
					$config['upload_path'] = 'assets/uploads/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['overwrite'] = TRUE;					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$ccheque=$cancel_cheque;					
				if(!$this->upload->do_upload('cancel_cheque',$config)) 
				{
					echo $this->upload->display_errors(); 
				}
			}
		}
		if(!empty($result)){
			$aproof = $result[0]->address_proof_image;
			$ccheque = $result[0]->cancelled_cheque_image;	
		}			
		if($query->num_rows() > 0){
			$where = array('seller_id'=>$_POST['user_id']);
			if(!empty($_POST['bank_name'])){ 
					$bankname = $_POST['bank_name'] ;
				}else{
					$bankname = "";
				}
			if(!empty($_POST['account_holder_name'])){ 
					$account_holder_name = $_POST['account_holder_name'] ;
				}else{
					$account_holder_name = "";
				}
			if(!empty($_POST['account_number'])){ 
					$account_number = $_POST['account_number'] ;
				}else{
					$account_number = "";
				}
			if(!empty($_POST['ifsc_code'])){ 
					$ifsc_code = $_POST['ifsc_code'] ;
				}else{
					$ifsc_code = "";
				}
			if(!empty($_POST['state'])){ 
					$state = $_POST['state'] ;
				}else{
					$state = "";
				}
			if(!empty($_POST['city'])){ 
					$city = $_POST['city'] ;
				}else{
					$city = "";
				}
			if(!empty($_POST['branch'])){ 
					$branch = $_POST['branch'] ;
				}else{
					$branch = "";
				}
			if(!empty($_POST['tin'])){ 
					$tin = $_POST['tin'] ;
				}else{
					$tin = "";
				}
			if(!empty($_POST['tan'])){ 
					$tan = $_POST['tan'] ;
				}else{
					$tan = "";
				}
			$data = array(					  
						  'bank_name'=>$bankname,					  
						  'account_holder_name'=>$account_holder_name,	'account_number'=>$account_number,
						  'ifsc_code'=>$ifsc_code,					  
						  'state'=>$state,					  
						  'city'=>$city,					  
						  'branch'=>$branch,					  
						  'tin_or_vat'=>$tin,					  
						  'tan'=>$tan,
						  'address_proof_image'=>$aproof,
						  'cancelled_cheque_image'=>$ccheque,
						  );
			$this->CommonModel->update_data('seller_details',$data,$where);	
		}else{
			$data = array(	
						  'business_name'=>$_POST['business_name'],	'business_description'=>$_POST['business_description'],			
						  'seller_id'=>$_POST['user_id'],
						  'business_type'=>$_POST['business_type'],
						  'address_proof_image'=>$aproof,
						  'cancelled_cheque_image'=>$ccheque,
						  );
			$this->CommonModel->insert_data('seller_details',$data);	
		}		
		if($this->db->affected_rows() > 0){
			$response = array("status"=>true , "msg" => "Business details has been successfully updated" , "data" => array()); 
			$this->response( $response , parent::HTTP_OK) ;
		}else{
			$response = array("status"=>true , "msg" => "Business details has been successfully updated" , "data" => array());  
			$this->response( $response , parent::HTTP_OK) ;
		}
	}	
}
?>