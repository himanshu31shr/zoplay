<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class MY_Controller extends \Restserver\Libraries\REST_Controller 
		{ 
			//set the class variable.
			
				public $template  = array();
				public $data      = array();
				protected $user_id = "";
				protected $getUser = array() ;
		
			/*Loading the default libraries, helper, language */
			public $default_email_add = 'donotreply@flipmart.com';

				public function __construct()
				{
					parent::__construct();

					$this->load->helper(array('form','language','url'));
					$this->load->library("JWT");
					$this->load->model('AdminModel');
					$this->load->model('BillingModel');
					// $u = $this->AdminModel->getUser();
					
					// if( $u['status'] == "success")
					// {
					// 	$this->user_id = $u['data']['id'];						
					// 	$this->getUser = $u['data'] ;
					// }elseif( $u['status'] == "error"){
					// 	$this->user_id = $u['data']['redirect'];
					// 	$this->getUser = $u['data']['redirect'] ;
					// }							
				}
			
			public function adminLayout($data) {
				$this->template['header'] = $this->load->view('admin/includes/header.tpl', $data, true);
				$this->template['footer'] = $this->load->view('admin/includes/footer.tpl', $data, true);
				return $this->template;
			}
			
			public function frontLayout( $data ) 
				{
					$data['carthtml']	= $this->get_session_cart();
					$this->template['header']   = $this->load->view('front-2/includes/header.tpl',   $data , true);
					$this->template['footer']   = $this->load->view('front-2/includes/footer.tpl',   $data , true);
				return $this->template ;
				}
			
			public function frontLayout2( $data ) 
				{
					$data['carthtml']	= $this->get_session_cart();
					$this->template['header']   = $this->load->view('front-2/includes/header.tpl',   $data , true);
					$this->template['footer']   = $this->load->view('front-2/includes/footer.tpl',   $data , true);
				return $this->template ;
				}
				
				/*	Seller Page Layout*/
			public function sellerlayout( $data ) 
			{
				$this->template['header']   = $this->load->view('seller/includes/header.tpl',   $data , true);
				$this->template['footer']   = $this->load->view('seller/includes/footer.tpl',   $data , true);
				return $this->template ;
			}
                        
                        /*	Seller inner Page Layout*/
			public function sellerInnerlayout( $data ) 
			{
				$this->template['header']   = $this->load->view('seller/includes/inner_header.tpl',   $data , true);
				$this->template['footer']   = $this->load->view('seller/includes/footer.tpl',   $data , true);
				return $this->template ;
			}
			
			public function get_session_cart()
				{
			
					$cart_check = $this->cart->contents();
					if(empty($cart_check)) {
						$html = '<div id="text">To add products to your shopping cart click on "Add to Cart" Button</div>' ;
						$rows = 0;
						$totalamount = 0;
					}else{
						$totalamount = $this->cart->total();
						$rows = count($this->cart->contents());

						$html=$this->BillingModel->get_dropdowncart_data(2);
					}
	
					$mycart_alldata = array(
						'myproduct_count'	=> 	$rows,
						'mygrand_total'	=>	$totalamount,
						'mycart_products'	=>	$html
					);
					
					return $mycart_alldata;
					return $html;
				}
				
	
	
	
		public function generate_token($user_id , $email){
			
			$CONSUMER_KEY = 'Flipmart';
			$CONSUMER_SECRET = 'AbhishekIsAwesome';
			$CONSUMER_TTL = 86400;
		
			return  $this->jwt->encode(array(
			  'consumerKey'=>$CONSUMER_KEY,
			  'userId'=>$user_id,
			  'userEmail'=>$email ,
			  'issuedAt'=>date(DATE_ISO8601, strtotime("now")),
			  'ttl'=>$CONSUMER_TTL
			), $CONSUMER_SECRET) ;
		}

	
	
	
	
			public function decode($token)
			{
				$response =  $this->jwt->decode($token , "AbhishekIsAwesome" , true );
				if( $response->userId ){
					return array("status"=>"success" , "data" => $response );
				}

				else{
					return array("status"=>"error"  ,  "data" => "" );
				}	
			}
			
			
			public function issueAToken($user_id , $email    ){
				
				
				/* $this->form_validation->set_rules('email', 'Email Address', 'required|email');
				$this->form_validation->set_rules('user_id', 'UserId', 'required');
				
				$this->load->library("form_validation");
				if($this->form_validation->run() == false){
					$return = $this->form_validation->validation_errors();
					return $return ;	
				}else{ */
					$token = $this->generate_token( $user_id , $email );
					return $token;
			//	} 
				
								
			}
			
			
			public function getUser( $user_id )
			{		
			
				$user  =  $this->AdminModel->getUserById( $user_id );
				$user  = $user->result_array();
				if( $user[0])
				{
					return $user[0];
				}
				else{
					   ?>
							<script>
								window.location.replace("<?php echo site_url();?>/seller/login");
							</script>
						<?php 	
					}
				
				
			}
			
			public function get_address_post($user_id=null , $limit = null, $last_id=null,$address_id=null)
				{
					//getting all address

					if($last_id != null){
						$user_id = $this->is_logged_in()['user_id'];	
						$where = array('address_user_id' => $user_id, 'address_id > '=>$last_id-1);
					}else{
						$where = array('address_user_id' => $user_id);
					}
					if($address_id!=null)
					{
						$where = array('address_id' => $address_id);
					}

				$address = $this->CommonModel->select_data_where('user_addresses', $where,'address_id')->result();
				//print_r(sort($address));echo '</pre>';die;
				if($address){
					$html ='';
					$i =0;
					foreach ($address as $a) {
						if($limit!=null)
						{
						if($i > $limit){
							break;
						}
						}
						$country = $this->CommonModel->select_data_where('countries', array('id'=>$a->address_country))->result();
						$a->address_country = ucfirst(strtolower($country[0]->name));
						
						$html .= '<li id="prod_'.$a->address_id.'">
								<address>
								<b class="text text-uppercase">'.$a->address_name.'</b> <br>
								  '.$a->address_line_1.'<br>
								  '.$a->address_line_1.'<br>
								  '.$a->address_city.', '.$a->address_state.'<br>
								  '.$a->address_country.',  ['.$a->address_zip.']<br>
								  <abbr title="Phone">P:</abbr> '.$a->address_phone.'
								</address>
								<a class="remove white label label-primary" onclick="render('.$a->address_id.')"><i class="fa fa-edit"></i> Edit</a> <a class="remove white label label-danger" onclick="delete_address('.$a->address_id.')"><i class="fa fa-trash"></i> Delete</a>
							</li>';
						$i++;
					}
					$data = array('last_id'=>$a->address_id, 'html' => $html);
					if($last_id != null){
						$data['status'] = true;
						$this->response($data, parent::HTTP_OK);
					}
				}else{
					$html = '<li>No address added yet!</li>';
					$data = array('last_id'=>0, 'html' => $html);
					if($last_id != null){
						$data['status'] = false;
						$data['html'] = '<li><i class="fa fa-info"></i> No more addresses!</li>';
						$this->response($data, parent::HTTP_INTERNAL_SERVER_ERROR);
					}
				}
				return $data;
				}
			public function get_single_address_post($address_id)
				{

					$address = $this->CommonModel->select_data_where('user_addresses', array('address_id'=> trim($address_id)))->result();
				
					if($address){
						foreach($address[0] as $key=>$value){
							if($key == 'address_create_time' || $key == 'address_update_time'){
								unset($address[0]->{$key});
							}
						}
						$this->response(array('status' =>true, 'message'=>'rendering address', 'data'=>$address[0]), parent::HTTP_OK);
					}else{
						$this->response(array('status' =>false, 'message'=>'Something went wrong!', 'data'=>''), parent::HTTP_INTERNAL_SERVER_ERROR);
					}
				}

			public function delete_address_post($address_id)
				{
					$ret = $this->CommonModel->delete_data('user_addresses',array('address_id'=> $address_id));

					$ret == true  ? $this->response(array('status' => true) , parent::HTTP_OK) : $this->response(array('status' => false) , parent::HTTP_INTERNAL_SERVER_ERROR);
				}

		

		/**
		*	Author: Himanshu 
		*	
		*	@param string sender_email, string receiver_email, string subject
		*	@param string type (for specifying what is the email is sent for)
		*	@param array data (for rendering any extra data for different mail types)
		*/
		public function common_mailer($sender_email, $receiver_email, $subject, $type = null, $data = null){

			//for user verification
			if($type == 'email_verification'){

				foreach ($data as $key => $value) {
					$$key = $value;
				}				
				$html = '<html>
				<head>
					<meta charset="utf-8"> <!-- utf-8 works for most cases -->
					<meta name="viewport" content="width=device-width"> 
					<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
				    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
					<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

					<!-- Web Font / @font-face : BEGIN -->
					<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
					
					<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
					<!--[if mso]>
						<style>
							* {
								font-family: sans-serif !important;
							}
						</style>
					<![endif]-->
					
					<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
					<!--[if !mso]><!-->
						<!-- insert web font reference, eg: <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"> -->
					<!--<![endif]-->

					<!-- Web Font / @font-face : END -->
					
					<!-- CSS Reset -->
				    <style>

						/* What it does: Remove spaces around the email design added by some email clients. */
						/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
				        html,
				        body {
					        margin: 0 auto !important;
				            padding: 0 !important;
				            height: 100% !important;
				            width: 100% !important;
				        }
				        
				        /* What it does: Stops email clients resizing small text. */
				        * {
				            -ms-text-size-adjust: 100%;
				            -webkit-text-size-adjust: 100%;
				        }
				        
				        /* What is does: Centers email on Android 4.4 */
				        div[style*="margin: 16px 0"] {
				            margin:0 !important;
				        }
				        
				        /* What it does: Stops Outlook from adding extra spacing to tables. */
				        table,
				        td {
				            mso-table-lspace: 0pt !important;
				            mso-table-rspace: 0pt !important;
				        }
				                
				        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
				        table {
				            border-spacing: 0 !important;
				            border-collapse: collapse !important;
				            table-layout: fixed !important;
				            margin: 0 auto !important;
				        }
				        table table table {
				            table-layout: auto; 
				        }
				        
				        /* What it does: Uses a better rendering method when resizing images in IE. */
				        img {
				            -ms-interpolation-mode:bicubic;
				        }
				        
				        /* What it does: A work-around for iOS meddling in triggered links. */
				        *[x-apple-data-detectors] {
				            color: inherit !important;
				            text-decoration: none !important;
				        }

				        /* What it does: A work-around for Gmail meddling in triggered links. */
				        .x-gmail-data-detectors,
				        .x-gmail-data-detectors *,
				        .aBn {
				            border-bottom: 0 !important;
				            cursor: default !important;
				        }

				        /* What it does: Prevents underlining the button text in Windows 10 */
				        .button-link {
				            text-decoration: none !important;
				        }
				      
				        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
				        /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
				        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
				            .email-container {
				                min-width: 375px !important;
				            }
				        }
				    
				    </style>
				    
				    <!-- Progressive Enhancements -->
				    <style>
				        
				        /* What it does: Hover styles for buttons */
				        .button-td,
				        .button-a {
				            transition: all 100ms ease-in;
				        }
				        .button-td:hover,
				        .button-a:hover {
				            background: #555555 !important;
				            border-color: #555555 !important;
				        }

				        /* Media Queries */
				        @media screen and (max-width: 600px) {

				            .email-container {
				                width: 100% !important;
				                margin: auto !important;
				            }

				            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
				            .fluid {
				                max-width: 100% !important;
				                height: auto !important;
				                margin-left: auto !important;
				                margin-right: auto !important;
				            }

				            /* What it does: Forces table cells into full-width rows. */
				            .stack-column,
				            .stack-column-center {
				                display: block !important;
				                width: 100% !important;
				                max-width: 100% !important;
				                direction: ltr !important;
				            }
				            /* And center justify these ones. */
				            .stack-column-center {
				                text-align: center !important;
				            }
				        
				            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
				            .center-on-narrow {
				                text-align: center !important;
				                display: block !important;
				                margin-left: auto !important;
				                margin-right: auto !important;
				                float: none !important;
				            }
				            table.center-on-narrow {
				                display: inline-block !important;
				            }
				                
				        }

				    </style>

					</head>
					<body>
					<center style="width: 100%; background: #222222;">
						<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">(Optional) Email verification pending!.</div>

						<table class="email-container" style="margin: auto;" border="0" width="600" cellspacing="0" cellpadding="0" align="center">
						<tbody>
						<tr>
						<td style="padding: 20px 0; text-align: center;"><img style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" src="http://placehold.it/200x50" alt="alt_text" width="200" height="50" border="0" /></td>
						</tr>
						</tbody>
						</table> 

						<table class="email-container" style="margin: auto;" border="0" width="600" cellspacing="0" cellpadding="0" align="center">
						<tbody><!-- Hero Image, Flush : END --> 
						<tr>
						<td style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" bgcolor="#ffffff">
						<h2><strong>Welcome to Flipmart!</strong></h2>
						<p style="text-align: left;">Dear '.$user_name.',&nbsp;</p>
						<p style="text-align: left;">Thanks for joining FlipMart! We now require you to verify your email address, to give the access. Please verify your email by clicking on the button below</p>
					
						<p style="text-align: left;">Just enter following code (without quotes):</p>
						<p><strong>"'.$token.'"</strong></p>
						</td>
						</tr>
						</tbody>
						</table>

						<table class="email-container" style="margin: auto;" border="0" width="600" cellspacing="0" cellpadding="0" align="center">
						<tbody>
						<tr>
						<td class="x-gmail-data-detectors" style="padding: 40px 10px; width: 100%; font-size: 12px; font-family: sans-serif; line-height: 18px; text-align: center; color: #888888;">FlipMart INC<br />123 Fake Street, SpringField, OR, 97477 US<br />(123) 456-7890</td>
						</tr>
						</tbody>
						</table>
						</center>
						</body>
						</html>';

						$headers[] = 'To: '.$user_name.' <'.$receiver_email.'>';
			}
			
			//$attachment = chunk_split(base64_encode(file_get_contents('logo.png'))); 
			//include attachment
			// $message .= "--PHP-mixed-$random_hash\r\n"."Content-Type: application/zip; 
			// name=\"logo.png\"\r\n"."Content-Transfer-Encoding: 
			// base64\r\n"."Content-Disposition: attachment\r\n\r\n";
			// $message .= $attachment;
			// $message .= "/r/n--PHP-mixed-$random_hash--";
			
			//common mail function
			// To send HTML mail, the Content-type header must be set
			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=iso-8859-1';

			// Additional headers
			$headers[] = 'From: FlipMart <'.$sender_email.'>';
			if($_SERVER['HTTP_HOST'] == 'localhost'){
				return false;																																									
			}else{
				return mail($receiver_email, $subject, $html, implode("\r\n", $headers));
			}
		}		
		
		function is_logged_in($currentUrl=null)
		{
			$userinfo = $this->session->userdata("user");
			if(isset($userinfo)) 
			{ 
				return $userinfo;
			}else{				
				redirect(base_url().'index.php?returnurl='.base64_encode($currentUrl)); 
			}
		}
}	