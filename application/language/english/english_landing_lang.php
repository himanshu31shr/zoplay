<?php
/*
 * English language
 */
$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_ip_address_time_limit'] = 'This IP Address has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';

		$lang['logintext']					=	"Login to Portal";
		$lang['textmetatitle']				=	"Login to Portal";
		$lang['textmetadescription']		=	"Login to Portal";
		
		$lang['welcometext']				=	"Dashboard";
		$lang['welcomemetatitle']			=	"Dashboard";
		$lang['welcomemetadescription']		=	"Dashboard";
		
		$lang['productstext']				=	"Product Listing";
		$lang['productsmetatitle']			=	"Product Listing";
		$lang['productsmetadescription']	=	"Product Listing";
		
		$lang['ebayaccountstext']			=	"All ebay Accounts";
		$lang['ebayaccountsmetatitle']		=	"All ebay Accounts";
		$lang['ebayaccountsmetadescription']=	"All ebay Accounts";
		
		$lang['settingtext']				=	"Setting";
		$lang['settingmetatitle']			=	"Setting";
		$lang['settingmetadescription']		=	"Setting";
?>
